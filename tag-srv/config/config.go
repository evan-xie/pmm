package config

import (
		"pmm/public/config"
)

const (
	TagTypeFolder = "folder"
	TagTypeTag = "tag"

	TagGroupPublic = "public"
	TagGroupPrivate = "private"
)

var (
	ServiceName = config.Namespace + "srv.tag"

	//默认每页条数
	PageSize int64 = 20
	//默认页数
	Page int64 = 1
	//默认最大每页条数
	DefaultMaxPageSize int64 = 100

	//标签 类型
	TagType = []string{TagTypeFolder,TagTypeTag}

	//默认顶层pid值
	DefaultTagTopPidValue = "-"
	//标签组 类型
	TagGroupState = []string{TagGroupPublic,TagGroupPrivate}
	//标签组 模型
	TagGroupModule = []string{"company","user","project"}
)
