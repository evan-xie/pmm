package handler

import (
	"context"
	"log"
	"errors"
	"pmm/public/proto"
	"pmm/tag-srv/database"
	"pmm/public/utils/validate"
	gv "github.com/asaskevich/govalidator"
		"pmm/public/config"
	"github.com/jinzhu/gorm"
	sconf "pmm/tag-srv/config"
	"math"
	mes "github.com/micro/go-micro/errors"
	pb "pmm/public/proto/tag"
	"strings"
	)



type TagHandler struct {
	//logger *zap.Logger
	Model database.Tag
	db *gorm.DB
	Group TagGroupHandler
}


// new一个TagHandler
func NewTagHandler() *TagHandler{
	return &TagHandler{
		//logger:tl.Instance().Named("TagHandler"),
		Model:database.Tag{},
		db:database.Conn,
	}
}


func (h *TagHandler) CreateTag(ctx context.Context,req *pb.Tag,rsp *pb.Response)error{
	log.Println("新增Tag...")
	if !gv.IsNull(req.Pid) {
		//验证上一级是否为folder
		_,err := h.checkFolder(req.Pid)
		if err != nil {
			return err
		}
	}
	info,err := h.create(req,"tag")
	if err != nil {
		return err
	}
	rsp.Tag = info
	return nil
}

func (h *TagHandler) CreateFolder(ctx context.Context,req *pb.Tag,rsp *pb.Response)error{
	log.Println("新增Folder...")
	info,err := h.create(req,"folder")
	if err != nil {
		return err
	}
	rsp.Tag = info
	return nil
}

func (h *TagHandler) Update(ctx context.Context,req *pb.Tag,rsp *pb.Response)error{
	log.Println("修改...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("tag-srv","标签编号不合法").Error())
	}

	if gv.IsNull(req.Name) && gv.IsNull(req.TagGroup) {
		return errors.New(mes.BadRequest("tag-srv","请求参数不能为空").Error())
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.BadRequest("tag-srv","标签不存在").Error())
	}

	//判断是否唯一
	check := h.Model.IsExist(&pb.Tag{Name:req.Name,TagGroup:req.TagGroup},false)
	if check != nil && info.Id != check.Id {
		return errors.New(mes.BadRequest("tag-srv","要修改成的标签已存在").Error())
	}

	if !gv.IsNull(req.TagGroup) {
		//验证标签组
		group,err := h.Group.checkStatus(req.TagGroup)
		if err != nil {
			return err
		}
		if group == nil {
			return errors.New(mes.BadRequest("tag-srv","所属标签组不存在").Error())
		}
	}

	err := h.Model.Update(req,"status","type")
	if err != nil {
		return errors.New(mes.InternalServerError("tag-srv",err.Error()).Error())
	}

	info = h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.BadRequest("tag-srv","标签不存在").Error())
	}

	rsp.Tag = &info.Tag
	return nil
}

func (h *TagHandler) Delete(ctx context.Context,req *proto.IdRequest,rsp *pb.DelResponse)error{
	log.Println("删除...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("tag-srv","标签编号不合法").Error())
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.NotFound("tag-srv","标签不存在").Error())
	}
	id := []string{req.Id}
	if info.Type == sconf.TagTypeFolder {
		//循环删除标签夹下的标签
		ids := h.getChild(req.Id)
		id = append(ids,req.Id)
	}

	err := h.Model.Delete(id...)
	if err != nil {
		return errors.New(mes.InternalServerError("tag-srv",err.Error()).Error())
	}
	rsp.Success = true
	return nil
}

func (h *TagHandler) Open(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("启用...")

	info,err := h.changeStatus(req.Id,config.OPEN_STATUS)
	if err != nil {
		return err
	}

	rsp.Tag = &info.Tag
	return nil
}

func (h *TagHandler) Close(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("禁用...")

	info,err := h.changeStatus(req.Id,config.CLOSE_STATUS)
	if err != nil {
		return err
	}

	rsp.Tag = &info.Tag
	return nil
}

func (h *TagHandler) Get(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("查询...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New("编号不合法")
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.NotFound("tag-srv","编号不存在").Error())
	}

	rsp.Tag = &info.Tag
	return nil
}

func (h *TagHandler) GetPage(ctx context.Context,req *pb.PageRequest,rsp *pb.PageResponse)error{
	log.Println("根据页面查询...")
	//设置默认每页条数,页数
	count,err := h.Model.GetCount(req)
	if err != nil {
		return errors.New(mes.InternalServerError("tag-srv",err.Error()).Error())
	}
	if *count == 0 {
		rsp.Page = &proto.Page{}
		return nil
	}
	lr := &pb.ListRequest{
		Default:&proto.ListRequest{},
		Tag:req.Tag,
	}

	if req.Default != nil {
		if req.Default.Size <= 0 || req.Default.Size > sconf.DefaultMaxPageSize{
			req.Default.Size = sconf.PageSize
		}
		if req.Default.Page <= 0 {
			req.Default.Page = sconf.Page
		}

		lr.Default = &proto.ListRequest{
			Start:(req.Default.Page-1)*req.Default.Size,
			Limit:req.Default.Size,
			Search:req.Default.Search,
			Ids:req.Default.Ids,
		}
	}else {
		req.Default = &proto.PageRequest{
			Size:sconf.PageSize,
			Page:sconf.Page,
		}
	}
	list,err := h.Model.GetList(lr)
	if err != nil {
		return errors.New(mes.InternalServerError("tag-srv",err.Error()).Error())
	}

	page := &proto.Page{
		Total:*count,
		Index:req.Default.Page,
		TotalPage:int64(math.Ceil(float64(*count)/float64(req.Default.Size))),
		PageSize:req.Default.Size,
	}
	rsp.Page = page
	rsp.List = list
	return nil
}

func (h *TagHandler) GetList(ctx context.Context,req *pb.ListRequest,rsp *pb.ListResponse)error{
	log.Println("查询所有...")
	list,err := h.Model.GetList(req)
	if err != nil {
		return errors.New(mes.InternalServerError("tag-srv",err.Error()).Error())
	}
	rsp.List = list
	return nil
}

func (h *TagHandler) GetTree(ctx context.Context,req *pb.TreeRequest,rsp *pb.TreeResponse)error{
	log.Println("查询所有并取得树形结果...")
	//取得单个文件夹下的tree结构
	if !gv.IsNull(req.Id) {
		perms,err := h.treeById(req.Id)
		//perms,err := h.treeById(req.Id,req.TagGroup)
		if err != nil {
			return err
		}
		rsp.Trees = perms
		return nil
	}
	if gv.IsNull(req.TagGroup){
		return errors.New(mes.BadRequest("tag-srv","标签组不能为空").Error())
	}
	//取得多个标签组下的tree结构
	t := &pb.ListRequest{
		Tag:&pb.Tag{
			TagGroup:req.TagGroup,
			Type:sconf.TagTypeFolder,
			Status:config.OPEN_STATUS,
		},
	}

	t.Tag.Pid = sconf.DefaultTagTopPidValue
	list,err := h.Model.GetList(t)
	if err != nil {
		return errors.New(mes.InternalServerError("tag-srv",err.Error()).Error())
	}
	//制定Tree
	perms := h.tree(list)
	rsp.Trees = perms

	return nil
}

func (h *TagHandler) changeStatus(id string,status int)(*database.Tag,error){
	if validate.HasAndCheckString(gv.IsUUIDv4, id) == false {
		return nil,errors.New(mes.BadRequest("tag-srv","标签编号不合法").Error())
	}

	info := h.Model.GetInfoById(id,false)
	if info == nil {
		return nil,errors.New(mes.NotFound("tag-srv","标签不存在").Error())
	}

	u := &pb.Tag{
		Id:id,
		Status:int64(status),
	}

	err := h.Model.Update(u)
	if err != nil {
		return nil,errors.New(mes.InternalServerError("tag-srv",err.Error()).Error())
	}
	info = h.Model.GetInfoById(id,false)

	return info,nil
}

func (h *TagHandler) create(req *pb.Tag, t string) (*pb.Tag, error) {
	if gv.IsNull(req.Name) {
		return nil,errors.New(mes.BadRequest("tag-srv","标签不能为空").Error())
	}
	if gv.IsNull(req.TagGroup) {
		return nil,errors.New(mes.BadRequest("tag-srv","所属标签组不能为空").Error())
	}
	//验证标签组
	group,err := h.Group.checkStatus(req.TagGroup)
	if err != nil {
		return nil,err
	}
	if group == nil {
		return nil,errors.New(mes.BadRequest("tag-srv","所属标签组不存在").Error())
	}
	if !gv.IsIn(t, sconf.TagType...) {
		return nil,errors.New(mes.BadRequest("tag-srv","仅能创建标签和标签夹").Error())
	}

	req.Type = strings.ToLower(t)
	//检验传入参数是否合理...
	model := h.Model
	//验证用户是否存在
	u := model.IsExist(req,false)
	if u != nil {
		return nil,errors.New(mes.BadRequest("tag-srv","标签/标签夹已存在").Error())
	}

	if gv.IsNull(req.Pid) {
		req.Pid = sconf.DefaultTagTopPidValue
	}else {
		_,err := h.checkFolder(req.Pid)
		if err != nil {
			return nil,err
		}
	}

	info,err := model.Create(req)
	if err != nil {
		return nil,errors.New(mes.InternalServerError("tag-srv",err.Error()).Error())
	}
	return &info.Tag,nil
}

func (h *TagHandler) checkFolder(id string) (*pb.Tag,error) {
	if validate.MustAndCheckString(gv.IsUUIDv4, id) == false {
		return nil,errors.New("编号不合法")
	}
	f := pb.Tag{
		Id:id,
		Type:sconf.TagTypeFolder,
	}
	info := h.Model.IsExist(&f,false)
	if info == nil {
		return nil,errors.New(mes.NotFound("tag-srv","标签夹不存在").Error())
	}
	if info.Status == config.CLOSE_STATUS {
		return nil,errors.New(mes.NotFound("tag-srv","标签夹被禁用").Error())
	}
	return &info.Tag,nil
}

func (h *TagHandler) getChild(id string) []string{
	if gv.IsNull(id) {
		return nil
	}
	//循环删除标签夹下的标签
	list,_ := h.Model.GetList(&pb.ListRequest{
		Tag:&pb.Tag{
			Pid:id,
		},
	})

	if list == nil {
		return nil
	}

	var tags []string
	for _,v := range list{
		tags = append(tags,v.Id)
		if v.Type == sconf.TagTypeFolder {
			l := h.getChild(v.Id)
			tags = append(tags, l...)
		}
	}
	return tags
}

func (h *TagHandler) tree(list []*pb.Tag) []*pb.TagTree  {
	var perms []*pb.TagTree
	if list == nil {
		 return perms
	}
	for _, v := range list {
		if v.Type != sconf.TagTypeFolder {
			continue
		}
		child := pb.TagTree{
			Tag:v,
			Child:[]*pb.TagTree{},
		}
		h.mkTree(v.Id,&child)
		perms = append(perms, &child)
	}
	return perms
}

func (h *TagHandler) mkTree(pid string,treeNode *pb.TagTree) error {
	t := &pb.ListRequest{
		Tag:&pb.Tag{
			Pid:pid,
			Status:config.OPEN_STATUS,
		},
	}
	childs,err := h.Model.GetList(t)
	if err != nil {
		return err
	}
	for _, value := range childs {
		child := pb.TagTree{
			Tag:value,
			Child:[]*pb.TagTree{},
		}
		treeNode.Child = append(treeNode.Child,&child)
		if value.Type == sconf.TagTypeFolder {
			h.mkTree(value.Id,&child)
		}
	}
	return nil
}

func (h *TagHandler) treeById(id string) ([]*pb.TagTree,error) {
	var perms []*pb.TagTree
	folder,err := h.checkFolder(id)
	if err != nil {
		return nil,errors.New(mes.InternalServerError("tag-srv",err.Error()).Error())
	}
	if folder == nil {
		return perms,nil
	}
/*	if folder.TagGroup != group {
		return nil,errors.New(mes.BadRequest("tag-srv","编号并不在此标签组内").Error())
	}*/
	child := pb.TagTree{
		Tag:folder,
		Child:[]*pb.TagTree{},
	}
	h.mkTree(folder.Id,&child)
	perms = append(perms, &child)
	return perms,nil
}