package handler

import (
	"pmm/tag-srv/database"
	"github.com/jinzhu/gorm"
	"log"
	pb "pmm/public/proto/tag/group"
	"context"
	mes "github.com/micro/go-micro/errors"
	gv "github.com/asaskevich/govalidator"
	"github.com/pkg/errors"
	sconf "pmm/tag-srv/config"
	"pmm/public/utils/validate"
	"pmm/public/config"
	"math"
	"pmm/public/proto"
	"strings"
)

type TagGroupHandler struct {
	//logger *zap.Logger
	Model database.TagGroup
	db *gorm.DB
}


// new一个TagHandler
func NewTagGroupHandler() *TagGroupHandler{
	return &TagGroupHandler{
		//logger:tl.Instance().Named("TagHandler"),
		Model:database.TagGroup{},
		db:database.Conn,
	}
}


func (h *TagGroupHandler) Create(ctx context.Context,req *pb.TagGroup,rsp *pb.Response)error{
	log.Println("新增...")
	if gv.IsNull(req.Module) {
		return errors.New(mes.BadRequest("tag-srv","所属模块不能为空").Error())
	}
	req.Module = strings.ToLower(req.Module)
	if !gv.IsIn(req.Module, sconf.TagGroupModule...) {
		return errors.New(mes.BadRequest("tag-srv","所属模块类型错误").Error())
	}
	if validate.MustAndCheckString(gv.IsUUIDv4, req.RelationId) == false {
		return errors.New(mes.BadRequest("tag-srv","关联编号不合法").Error())
	}
	if gv.IsNull(req.Name) {
		return errors.New(mes.BadRequest("tag-srv","模块名称不能为空").Error())
	}
	if gv.IsNull(req.State) || !gv.IsIn(req.State,sconf.TagGroupState...) {
		req.State = strings.ToLower(sconf.TagGroupPrivate)
	}
	/***此处应该验证关联的编号是否启用***/

	ctg := &pb.TagGroup{
		Module:req.Module,
		RelationId:req.RelationId,
		State:req.State,
		Name:req.Name,
	}
	tagg,err := h.Model.Create(ctg)
	if err != nil {
		return errors.New(mes.InternalServerError("tag-srv",err.Error()).Error())
	}

	rsp.TagGroup = &tagg.TagGroup
	return nil
}

func (h *TagGroupHandler) Update(ctx context.Context,req *pb.TagGroup,rsp *pb.Response)error{
	log.Println("更新...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("tag-srv","标签组编号不合法").Error())
	}
	if gv.IsNull(req.State) && !gv.IsIn(req.State,sconf.TagGroupState...) {
		return errors.New(mes.BadRequest("tag-srv","标签组共享状态不合法").Error())
	}
	if gv.IsNull(req.Module) {
		req.Module = strings.ToLower(req.Module)
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.BadRequest("tag-srv","标签组不存在").Error())
	}

	ctg := &pb.TagGroup{
		Module:req.Module,
		RelationId:req.RelationId,
		State:req.State,
		Name:req.Name,
	}
	err := h.Model.Update(ctg)
	if err != nil {
		return errors.New(mes.InternalServerError("tag-srv",err.Error()).Error())
	}
	info = h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.BadRequest("tag-srv","标签组不存在").Error())
	}
	rsp.TagGroup = &info.TagGroup
	return nil
}

func (h *TagGroupHandler) Delete(ctx context.Context,req *proto.IdRequest,rsp *pb.DelResponse)error{
	log.Println("删除...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("tag-srv","标签编号不合法").Error())
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.NotFound("tag-srv","标签不存在").Error())
	}

	err := h.Model.Delete(req.Id)

	if err != nil {
		return errors.New(mes.InternalServerError("tag-srv",err.Error()).Error())
	}
	rsp.Success = true
	return nil
}

func (h *TagGroupHandler) Open(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("启用...")
	info,err := h.changeStatus(req.Id,config.OPEN_STATUS)
	if err != nil {
		return err
	}

	rsp.TagGroup = &info.TagGroup
	return nil
}

func (h *TagGroupHandler) Close(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("禁用...")
	info,err := h.changeStatus(req.Id,config.CLOSE_STATUS)
	if err != nil {
		return err
	}

	rsp.TagGroup = &info.TagGroup
	return nil
}

func (h *TagGroupHandler) Get(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("查询...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("tag-srv","标签组编号不合法").Error())
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.NotFound("tag-srv","编号不存在").Error())
	}

	rsp.TagGroup = &info.TagGroup
	return nil
}

func (h *TagGroupHandler) GetPage(ctx context.Context,req *pb.PageRequest,rsp *pb.PageResponse)error{
	log.Println("根据页面查询...")
	//设置默认每页条数,页数
	count,err := h.Model.GetCount(req)
	if err != nil {
		return errors.New(mes.InternalServerError("tag-srv",err.Error()).Error())
	}
	if *count == 0 {
		rsp.Page = &proto.Page{}
		return nil
	}
	lr := &pb.ListRequest{
		Default:&proto.ListRequest{},
		TagGroup:req.TagGroup,
	}

	if req.Default != nil {
		if req.Default.Size <= 0 || req.Default.Size > sconf.DefaultMaxPageSize{
			req.Default.Size = sconf.PageSize
		}
		if req.Default.Page <= 0 {
			req.Default.Page = sconf.Page
		}

		lr.Default = &proto.ListRequest{
			Start:(req.Default.Page-1)*req.Default.Size,
			Limit:req.Default.Size,
			Search:req.Default.Search,
			Ids:req.Default.Ids,
		}
	}else {
		req.Default = &proto.PageRequest{
			Size:sconf.PageSize,
			Page:sconf.Page,
		}
	}
	list,err := h.Model.GetList(lr)
	if err != nil {
		return errors.New(mes.InternalServerError("tag-srv",err.Error()).Error())
	}

	page := &proto.Page{
		Total:*count,
		Index:req.Default.Page,
		TotalPage:int64(math.Ceil(float64(*count)/float64(req.Default.Size))),
		PageSize:req.Default.Size,
	}
	rsp.Page = page
	rsp.List = list
	return nil
}

func (h *TagGroupHandler) GetList(ctx context.Context,req *pb.ListRequest,rsp *pb.ListResponse)error{
	log.Println("查询所有...")
	list,err := h.Model.GetList(req)
	if err != nil {
		return errors.New(mes.InternalServerError("tag-srv",err.Error()).Error())
	}
	rsp.List = list
	return nil
}

func (h *TagGroupHandler) changeStatus(id string,status int)(*database.TagGroup,error){
	if validate.HasAndCheckString(gv.IsUUIDv4, id) == false {
		return nil,errors.New(mes.BadRequest("tag-srv","标签组编号不合法").Error())
	}

	info := h.Model.GetInfoById(id,false)
	if info == nil {
		return nil,errors.New(mes.NotFound("tag-srv","标签组不存在").Error())
	}

	u := &pb.TagGroup{
		Id:id,
		Status:int64(status),
	}

	err := h.Model.Update(u)
	if err != nil {
		return nil,errors.New(mes.InternalServerError("tag-srv",err.Error()).Error())
	}
	info = h.Model.GetInfoById(id,false)

	return info,nil
}

func (h *TagGroupHandler) checkStatus(id string)(*database.TagGroup,error){
	if validate.HasAndCheckString(gv.IsUUIDv4, id) == false {
		return nil,errors.New(mes.BadRequest("tag-srv","标签组编号不合法").Error())
	}

	info := h.Model.GetInfoById(id,true)
	if info == nil {
		return nil,errors.New(mes.NotFound("tag-srv","标签组不存在").Error())
	}
	return info,nil
}