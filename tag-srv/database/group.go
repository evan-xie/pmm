package database

import (
	"pmm/public/model"
	 pb "pmm/public/proto/tag/group"
	"pmm/public/config"
	"pmm/public/utils"
	"github.com/jinzhu/gorm"
	"time"
	"github.com/satori/go.uuid"
	"log"
)

// 设置的表名
func (TagGroup) TableName() string {
	return "tag_group"
}

type TagGroup struct {
	pb.TagGroup
	model.Comm
}

/**
创建
 */
func (repo *TagGroup) Create(data *pb.TagGroup) (*TagGroup,error) {
	tg := &TagGroup{
		TagGroup:*data,
	}
	if err := Conn.Create(&tg).Error; err != nil {
		return nil,err
	}
	return tg,nil
}

/**
更新
condition 条件 参数1为是否更新密码 2位是否更新状态
 */
func (repo *TagGroup) Update(u *pb.TagGroup,condition... string) (error)  {
	tg := &TagGroup{}
	tg.TagGroup = *u

	query := Conn.Model(&tg)
	//过滤参数
	if len(condition) >= 1 {
		query = query.Omit(condition...)
	}

	if err := query.Updates(tg).Error;err != nil {
		return err
	}
	return nil
}

/**
 判断是否存在 存在返回查询到的结果
 */
func (repo *TagGroup) IsExist(pb *pb.TagGroup,isOpen bool) (*TagGroup) {
	if pb == nil {
		return nil
	}

	tg := &TagGroup{}

	query := Conn
	if isOpen {
		query = query.Where("status = ?",config.OPEN_STATUS)
	}
	err := query.Where(pb).First(&tg).Error

	if err == nil {
		return tg
	}

	return nil
}

/**
根据id取得信息
 */
func (repo *TagGroup) GetInfoById(id string,isOpen bool) (*TagGroup) {
	if id == "" {
		return nil
	}

	tg := &TagGroup{}

	query := Conn.Where("id = ?",id)
	if isOpen {
		query = query.Where("status = ?",config.OPEN_STATUS)
	}
	err := query.First(&tg).Error

	if err == nil {
		return tg
	}

	return nil
}

/**
根据编号删除
 */
func (repo *TagGroup) Delete(id ...string) (error) {
	str := ""
	if len(id) > 1 {
		str = "id in ("
		for _, value := range id {
			str = str+"'"+value+"',"
		}
		str = string([]rune(str)[:len(str)-1])+")"
	}else {
		str = "id = '" + id[0] +"'"
	}

	err := Conn.Where(str).Delete(&TagGroup{}).Error

	if err != nil {
		return err
	}

	return nil
}

/**
取得列表
 */
func (repo *TagGroup) GetList(req *pb.ListRequest,args ...interface{}) ([]*pb.TagGroup,error) {
	var tgs []*pb.TagGroup
	tg := req.TagGroup
	fs := utils.GetDbNameByStruct(Conn,pb.TagGroup{})
	query := Conn.Model(&TagGroup{}).Where(tg)
	//过滤参数
	if len(fs) > 1 {
		query = query.Select(fs)
	}
	if req.Default != nil {
		if req.Default.Start > 0 {
			query = query.Offset(req.Default.Start)
		}
		if req.Default.Limit > 0 {
			query = query.Limit(req.Default.Limit)
		}
		if req.Default.Search != nil {
			query = utils.AndOrLikes(query,req.Default.Search,true)
		}
		if len(req.Default.Ids) > 0{
			query = query.Where(req.Default.Ids)
		}
	}

	if args != nil && len(args) > 0 {
		for _, value := range args {
			query = query.Where(value)
		}
	}

	err := query.Scan(&tgs).Error
	if err != nil {
		return nil,err
	}
	return tgs,nil
}

func (repo *TagGroup) GetCount(req *pb.PageRequest,args ...interface{}) (*int64,error) {
	data := req.TagGroup
	query := Conn.Model(&TagGroup{}).Where(data)
	if req.Default != nil {
		if req.Default.Search != nil {
			query = utils.AndOrLikes(query,req.Default.Search,true)
		}
		if len(req.Default.Ids) > 0 {
			query = query.Where(req.Default.Ids)
		}
	}
	if args != nil && len(args) > 0 {
		for _, value := range args {
			query = query.Where(value)
		}
	}
	var count *int64
	err := query.Count(&count).Error
	if err != nil {
		return count,err
	}
	return count,nil
}

func (repo *TagGroup) BeforeCreate(scope *gorm.Scope) error {
	uid, err := uuid.NewV4()
	if err != nil {
		log.Fatalf("created uuid error: %v\n", err)
	}
	scope.SetColumn("Id", uid.String())
	scope.SetColumn("CreatedAt", time.Now().Format("2006-01-02 15:04:05"))
	scope.SetColumn("UpdatedAt", time.Now().Format("2006-01-02 15:04:05"))
	scope.SetColumn("Status", config.OPEN_STATUS)
	return nil
}