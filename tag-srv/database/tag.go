package database

import (
	"pmm/public/config"
	"pmm/public/model"
	pb "pmm/public/proto/tag"
	"pmm/public/utils"
)

// 设置Tag的表名为`Tag`
func (Tag) TableName() string {
	return "tag"
}

type Tag struct {
	pb.Tag
	model.Comm
}

/**
创建
 */
func (repo *Tag) Create(u *pb.Tag) (*Tag,error) {
	Tag := &Tag{
		Tag:*u,
	}
	if err := Conn.Create(&Tag).Error; err != nil {
		return nil,err
	}
	return Tag,nil
}

/**
更新
condition 条件 参数1为是否更新密码 2位是否更新状态
 */
func (repo *Tag) Update(u *pb.Tag,condition... string) (error)  {
	d := &Tag{}
	d.Tag = *u

	query := Conn.Model(&d)
	//过滤参数
	if len(condition) >= 1 {
		query = query.Omit(condition...)
	}

	if err := query.Updates(d).Error;err != nil {
		return err
	}
	return nil
}


/**
 判断是否存在 存在返回查询到的结果
 */
func (repo *Tag) IsExist(pb *pb.Tag,isOpen bool) (*Tag) {
	if pb == nil {
		return nil
	}

	Tag := &Tag{}

	query := Conn
	if isOpen {
		query = query.Where("status = ?",config.OPEN_STATUS)
	}
	err := query.Where(pb).First(&Tag).Error

	if err == nil {
		return Tag
	}

	return nil
}

/**
根据id取得信息
 */
func (repo *Tag) GetInfoById(id string,isOpen bool) (*Tag) {
	if id == "" {
		return nil
	}

	Tag := &Tag{}

	query := Conn.Where("id = ?",id)
	if isOpen {
		query = query.Where("status = ?",config.OPEN_STATUS)
	}
	err := query.First(&Tag).Error

	if err == nil {
		return Tag
	}

	return nil
}

/**
根据编号删除
 */
func (repo *Tag) Delete(id ...string) (error) {
	str := ""
	if len(id) > 1 {
		str = "id in ("
		for _, value := range id {
			str = str+"'"+value+"',"
		}
		str = string([]rune(str)[:len(str)-1])+")"
	}else {
		str = "id = '" + id[0] +"'"
	}

	err := Conn.Where(str).Delete(&Tag{}).Error

	if err != nil {
		return err
	}

	return nil
}


/**
取得列表
 */
func (repo *Tag) GetList(req *pb.ListRequest,args ...interface{}) ([]*pb.Tag,error) {
	var tags []*pb.Tag
	tag := req.Tag
	fs := utils.GetDbNameByStruct(Conn,pb.Tag{})
	query := Conn.Model(&Tag{}).Where(tag)
	//过滤参数
	if len(fs) > 1 {
		query = query.Select(fs)
	}
	if req.Default != nil {
		if req.Default.Start > 0 {
			query = query.Offset(req.Default.Start)
		}
		if req.Default.Limit > 0 {
			query = query.Limit(req.Default.Limit)
		}
		if req.Default.Search != nil {
			query = utils.AndOrLikes(query,req.Default.Search,true)
		}
		if len(req.Default.Ids) > 0{
			query = query.Where(req.Default.Ids)
		}
	}

	if args != nil && len(args) > 0 {
		for _, value := range args {
			query = query.Where(value)
		}
	}

	err := query.Scan(&tags).Error
	if err != nil {
		return nil,err
	}
	return tags,nil
}

func (repo *Tag) GetCount(req *pb.PageRequest,args ...interface{}) (*int64,error) {
	data := req.Tag
	query := Conn.Model(&Tag{}).Where(data)
	if req.Default != nil {
		if req.Default.Search != nil {
			query = utils.AndOrLikes(query,req.Default.Search,true)
		}
		if len(req.Default.Ids) > 0 {
			query = query.Where(req.Default.Ids)
		}
	}
	if args != nil && len(args) > 0 {
		for _, value := range args {
			query = query.Where(value)
		}
	}
	var count *int64
	err := query.Count(&count).Error
	if err != nil {
		return count,err
	}
	return count,nil
}
