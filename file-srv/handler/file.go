package handler

import (
	"github.com/jinzhu/gorm"
	"log"
	pb "pmm/public/proto/file"
	"context"
	mes "github.com/micro/go-micro/errors"
	gv "github.com/asaskevich/govalidator"
	"github.com/pkg/errors"
	sconf "pmm/file-srv/config"
	"pmm/public/utils/validate"
	"pmm/public/config"
	"pmm/public/proto"
	"pmm/file-srv/database"
	"math"
)

type FileHandler struct {
	//logger *zap.Logger
	Model database.File
	db *gorm.DB
}


// new一个TagHandler
func NewFileHandler() *FileHandler{
	return &FileHandler{
		//logger:tl.Instance().Named("TagHandler"),
		Model:database.File{},
		db:database.Conn,
	}
}


func (h *FileHandler) Create(ctx context.Context,req *pb.File,rsp *pb.Response)error{
	log.Println("新增...")
	if gv.IsNull(req.Path) {
		return errors.New(mes.BadRequest("File-srv","保存路径不能为空").Error())
	}
	if gv.IsNull(req.Md5) {
		return errors.New(mes.BadRequest("File-srv","文件md5不能为空").Error())
	}
	if gv.IsNull(req.Sha1) {
		return errors.New(mes.BadRequest("File-srv","文件sha1不能为空").Error())
	}
	if gv.IsNull(req.Type) {
		return errors.New(mes.BadRequest("File-srv","文件类型不能为空").Error())
	}
	if gv.IsNull(req.Mimetype) {
		return errors.New(mes.BadRequest("File-srv","文件mimetype不能为空").Error())
	}

	info := h.Model.IsExist(req,false)
	if info != nil {
		rsp.File = &info.File
		return nil
		//return errors.New(mes.BadRequest("File-srv","此文件已存在").Error())
	}

	
	info,err := h.Model.Create(req)
	if err != nil {
		return errors.New(mes.InternalServerError("File-srv",err.Error()).Error())
	}
	rsp.File = &info.File
	return nil
}

func (h *FileHandler) Update(ctx context.Context,req *pb.File,rsp *pb.Response)error{
	log.Println("更新...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("File-srv","文件编号不合法").Error())
	}

	old := h.Model.GetInfoById(req.Id,false)
	if old == nil {
		return errors.New(mes.BadRequest("File-srv","文件不存在").Error())
	}

	err := h.Model.Update(req)
	if err != nil {
		return errors.New(mes.InternalServerError("File-srv",err.Error()).Error())
	}
	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.BadRequest("File-srv","文件不存在").Error())
	}
	rsp.File = &info.File
	return nil
}

func (h *FileHandler) Delete(ctx context.Context,req *proto.IdRequest,rsp *proto.BoolResponse)error{
	log.Println("删除...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("File-srv","文件编号不合法").Error())
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.NotFound("File-srv","文件不存在").Error())
	}

	err := h.Model.Delete(req.Id)

	if err != nil {
		return errors.New(mes.InternalServerError("File-srv",err.Error()).Error())
	}
	rsp.Success = true
	return nil
}

func (h *FileHandler) Open(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("启用...")
	info,err := h.changeStatus(req.Id,config.OPEN_STATUS)
	if err != nil {
		return err
	}

	rsp.File = &info.File
	return nil
}

func (h *FileHandler) Close(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("禁用...")
	info,err := h.changeStatus(req.Id,config.CLOSE_STATUS)
	if err != nil {
		return err
	}

	rsp.File = &info.File
	return nil
}

func (h *FileHandler) Get(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("查询...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New("编号不合法")
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.NotFound("File-srv","文件不存在").Error())
	}

	rsp.File = &info.File
	return nil
}

func (h *FileHandler) GetPage(ctx context.Context,req *pb.PageRequest,rsp *pb.PageResponse)error{
	log.Println("根据页面查询...")
	//设置默认每页条数,页数
	count,err := h.Model.GetCount(req)
	if err != nil {
		return errors.New(mes.InternalServerError("File-srv",err.Error()).Error())
	}

	if *count == 0 {
		rsp.Page = &proto.Page{}
		return nil
	}
	lr := &pb.ListRequest{
		Default:&proto.ListRequest{},
		File:req.File,
	}

	if req.Default != nil {
		if req.Default.Size <= 0 || req.Default.Size > sconf.DefaultMaxPageSize{
			req.Default.Size = sconf.PageSize
		}
		if req.Default.Page <= 0 {
			req.Default.Page = sconf.Page
		}

		lr.Default = &proto.ListRequest{
			Start:(req.Default.Page-1)*req.Default.Size,
			Limit:req.Default.Size,
			Search:req.Default.Search,
			Ids:req.Default.Ids,
		}
	}else {
		req.Default = &proto.PageRequest{
			Size:sconf.PageSize,
			Page:sconf.Page,
		}
	}

	list,err := h.Model.GetList(lr)
	if err != nil {
		return errors.New(mes.InternalServerError("File-srv",err.Error()).Error())
	}

	page := &proto.Page{
		Total:*count,
		Index:req.Default.Page,
		TotalPage:int64(math.Ceil(float64(*count)/float64(req.Default.Size))),
		PageSize:req.Default.Size,
	}
	rsp.Page = page
	rsp.List = list
	return nil
}

func (h *FileHandler) GetList(ctx context.Context,req *pb.ListRequest,rsp *pb.ListResponse)error{
	log.Println("查询所有...")
	list,err := h.Model.GetList(req)
	if err != nil {
		return errors.New(mes.InternalServerError("File-srv",err.Error()).Error())
	}
	rsp.List = list
	return nil
}

func (h *FileHandler) Upload(ctx context.Context,req *pb.Upload,rsp *pb.Response)error{

	return nil
}

func (h *FileHandler) changeStatus(id string,status int)(*database.File,error){
	if validate.HasAndCheckString(gv.IsUUIDv4, id) == false {
		return nil,errors.New(mes.BadRequest("File-srv","项目节点编号不合法").Error())
	}

	info := h.Model.GetInfoById(id,false)
	if info == nil {
		return nil,errors.New(mes.NotFound("File-srv","项目节点不存在").Error())
	}

	u := &pb.File{
		Id:id,
		Status:int64(status),
	}

	err := h.Model.Update(u)
	if err != nil {
		return nil,errors.New(mes.InternalServerError("File-srv",err.Error()).Error())
	}
	info = h.Model.GetInfoById(id,false)

	return info,nil
}

func (h *FileHandler) checkStatus(id string)(*database.File,error){
	if validate.HasAndCheckString(gv.IsUUIDv4, id) == false {
		return nil,errors.New(mes.BadRequest("File-srv","项目编号不合法").Error())
	}

	info := h.Model.GetInfoById(id,true)
	if info == nil {
		return nil,errors.New(mes.NotFound("File-srv","项目不存在").Error())
	}
	return info,nil
}

