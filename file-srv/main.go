package main

import (
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/server"
	"log"
	"pmm/file-srv/config"
	"pmm/file-srv/database"
	"pmm/file-srv/handler"
	"pmm/public/proto/file"
)

func main() {
	//连接数据库
	db,err := database.CreateConnection()
	defer db.Close()
	db.LogMode(true)

	if err != nil {
		log.Fatalf("数据库连接失败: %v\n",err)
	}

	// 全局禁用表名复数
	db.SingularTable(true) // 如果设置为true,`User`的默认表名为`user`,使用`TableName`设置的表名不受影响

	// 自动检查 表 结构是否变化
	db.AutoMigrate(database.File{})//项目

	srv := micro.NewService(
		micro.Name(config.ServiceName),
		micro.Version("latest"),
		//micro.WrapHandler(fanc),//注册拦截器 //注册验证
	)

	srv.Init()

	//注册推送监听
	//publisher := micro.NewPublisher("node",srv.Client())
	//注册日志

	//注册服务

	file.RegisterFileServiceHandler(srv.Server(),handler.NewFileHandler(),server.InternalHandler(true))

	log.Println("启动file-srv服务...")
	if err := srv.Run(); err != nil {
		log.Fatalf("file-srv服务启动失败: %v\n", err)
	}
}
