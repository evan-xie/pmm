package main

import (
	microclient "github.com/micro/go-micro/client"
	"context"
	"log"
	"os"
	"github.com/micro/go-micro/cmd"
	pb "pmm/public/proto/rule"
	"pmm/public/config"
	"math/rand"
	"strconv"
	"fmt"
	"pmm/public/proto"
	"encoding/json"
)

type Test struct {
	cli pb.RuleServiceClient
	data pb.Rule
}

func main() {
	cmd.Init()
	// 创建 微服务的客户端
	client := pb.NewRuleServiceClient(config.Namespace+"srv.user",microclient.DefaultClient)

	//测试数据
	data := &Test{
		cli:client,
	}

	//CreateFolder(*data)
	//GetList(*data)
	//GetPage(*data)
	//Delete(*data)
	//Create(*data)
	//GetTree(*data)
	testUpdateRoleRule(*data)
	// 直接退出即可
	os.Exit(0)
}

func testUpdateRoleRule(test Test)  {
	test.data.Id = "15412a67-b280-4dc7-811a-689d3f4322c3"
	//Update(test)
	Open(test)
}

func Create(test Test)  {
	data :=pb.Rule{
		Name:"测试"+strconv.Itoa(rand.Intn(100)),
		Title:"测试"+strconv.Itoa(rand.Intn(100)),
		Domain:"app1",
		Ismenu:-1,
		Pid:"fc1ab278-b494-428e-a7db-66f8dbfc3862",
	}
	rsp,err := test.cli.Create(context.TODO(),&data)
	if err != nil {
		log.Fatalf("创建失败: %v", err)
	}
	log.Println("创建成功: ", rsp)
	test.data.Id = rsp.Rule.Id
	Get(test)
	Update(test)
	Close(test)
	Open(test)
	Delete(test)
}

func Delete(test Test)  {
	data := proto.IdRequest{Id:test.data.Id}
	rsp,err := test.cli.Delete(context.TODO(),&data)
	if err != nil {
		log.Fatalf("删除失败: %v", err)
	}
	log.Println("删除成功:", rsp)
}
func Update(test Test)  {
	data :=pb.Rule{
		Id:test.data.Id,
		Name:"测试"+strconv.Itoa(rand.Intn(100)),
		Title:"测试"+strconv.Itoa(rand.Intn(100)),
		Domain:"app1",
		Ismenu:-1,
	}
	log.Println(data.Title)
	rsp,err := test.cli.Update(context.TODO(),&data)
	if err != nil {
		log.Fatalf("更新失败: %v", err)
	}
	log.Println("更新成功:", rsp)
	log.Println(rsp.Rule)
	log.Println(rsp.Rule.Title)
}
func Open(test Test)  {
	data := proto.IdRequest{Id:test.data.Id}
	rsp,err := test.cli.Open(context.TODO(),&data)
	if err != nil {
		log.Fatalf("启用失败: %v", err)
	}
	log.Println("更新成功:", rsp)
}
func Close(test Test)  {
	data := proto.IdRequest{Id:test.data.Id}
	rsp,err := test.cli.Close(context.TODO(),&data)
	if err != nil {
		log.Fatalf("禁用失败: %v", err)
	}
	log.Println("禁用成功:", rsp)
}
func Get(test Test)  {
	data := proto.IdRequest{Id:test.data.Id}
	rsp,err := test.cli.Get(context.TODO(),&data)
	if err != nil {
		log.Fatalf("查询失败: %v", err)
	}
	log.Println("查询成功:", rsp)
}
func GetList(test Test)  {
	rep := pb.ListRequest{}
	rsp,err := test.cli.GetList(context.TODO(),&rep)
	if err != nil {
		log.Fatalf("获取列表失败: %v", err)
	}
	log.Println("获取列表成果:")
	for _,v := range rsp.Rules{
		fmt.Println(v)
	}
}
func GetPage(test Test)  {
	rep := pb.PageRequest{}
	rsp,err := test.cli.GetPage(context.TODO(),&rep)
	if err != nil {
		log.Fatalf("获取分页失败: %v", err)
	}
	log.Println("获取分页成功:")
	log.Println(rsp.Page.String())
	for _,v := range rsp.Rules{
		fmt.Println(v)
	}
}
func GetTree(test Test) {
	rep := pb.TreeRequest{
		Id:"15412a67-b280-4dc7-811a-689d3f4322c3",
	}
	rsp,err := test.cli.GetTree(context.TODO(),&rep)
	if err != nil {
		log.Fatalf("获取Tree失败: %v", err)
	}
	log.Println("获取Tree成功:")
	log.Println(rsp)
	if b, err := json.Marshal(rsp.Trees); err == nil {
		fmt.Println(string(b))
	}
}

