package main

import (
	microclient "github.com/micro/go-micro/client"
	"context"
	"log"
	"os"
	"github.com/micro/go-micro/cmd"
	pb "pmm/public/proto/node/config"
	"pmm/public/config"
				"pmm/public/proto"
	"github.com/satori/go.uuid"
)

type Test struct {
	cli pb.NodeConfigServiceClient
	data pb.NodeConfig
}

func main() {
	cmd.Init()
	// 创建 微服务的客户端
	client := pb.NewNodeConfigServiceClient(config.Namespace+"srv.node",microclient.DefaultClient)

	//测试数据
	data := &Test{
		cli:client,
	}

	//CreateFolder(*data)
	//Delete(*data)
	Create(*data)
	//GetTree(*data)
	// 直接退出即可
	os.Exit(0)
}

func Create(test Test)  {
	cid,_ := uuid.NewV4()
	data := pb.NodeConfig{
		Id:cid.String(),
		NodeUnit:"day",
	}
	rsp,err := test.cli.Create(context.TODO(),&data)
	if err != nil {
		log.Fatalf("创建失败: %v", err)
	}
	log.Println("创建成功: ", rsp)
	test.data.Id = rsp.NodeConfig.Id
	Get(test)
	Update(test)
	Close(test)
	Open(test)
	Delete(test)
}

func Delete(test Test)  {
	data := proto.IdRequest{Id:test.data.Id}
	rsp,err := test.cli.Delete(context.TODO(),&data)
	if err != nil {
		log.Fatalf("删除失败: %v", err)
	}
	log.Println("删除成功:", rsp)
}
func Update(test Test)  {
	data :=pb.NodeConfig{
		NodeUnit:"hour",
	}
	data.Id = test.data.Id
	rsp,err := test.cli.Update(context.TODO(),&data)
	if err != nil {
		log.Fatalf("更新失败: %v", err)
	}
	log.Println("更新成功:", rsp)
}
func Open(test Test)  {
	data := proto.IdRequest{Id:test.data.Id}
	rsp,err := test.cli.Open(context.TODO(),&data)
	if err != nil {
		log.Fatalf("启用失败: %v", err)
	}
	log.Println("更新成功:", rsp)
}
func Close(test Test)  {
	data := proto.IdRequest{Id:test.data.Id}
	rsp,err := test.cli.Close(context.TODO(),&data)
	if err != nil {
		log.Fatalf("禁用失败: %v", err)
	}
	log.Println("禁用成功:", rsp)
}
func Get(test Test)  {
	data := proto.IdRequest{Id:test.data.Id}
	rsp,err := test.cli.Get(context.TODO(),&data)
	if err != nil {
		log.Fatalf("查询失败: %v", err)
	}
	log.Println("查询成功:", rsp)
}



