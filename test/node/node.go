package main

import (
	"context"
	microclient "github.com/micro/go-micro/client"
	"github.com/micro/go-micro/cmd"
	"log"
	"math/rand"
	"os"
	"pmm/public/config"
	"pmm/public/proto"
	pb "pmm/public/proto/node"
	"strconv"
)

type Test struct {
	cli pb.NodeServiceClient
	data pb.Node
}

func main() {
	cmd.Init()
	// 创建 微服务的客户端
	client := pb.NewNodeServiceClient(config.Namespace+"srv.node",microclient.DefaultClient)

	//测试数据
	data := &Test{
		cli:client,
	}
	//Update(*data)
	//CreateFolder(*data)
	//GetList(*data)
	//GetPage(*data)
	////Delete(*data)
	//Create(*data)
	Update(*data)
	//GetTree(*data)
	// 直接退出即可
	os.Exit(0)
}

func Create(test Test)  {
	//cid,_ := uuid.NewV4()
	data := pb.Node{
		Title:"测试"+strconv.Itoa(rand.Intn(100)),
		Pid:"60bfcb19-1454-4db0-b3e2-a16ea3c193ed",
		//Pid:"f36b962b-3278-4a57-ac35-20d4e2f8a43b",
		StartDate:"2006-01-02 15:04:05",
		Duration:4,
		Progress:0,
		ProjectId:"2b210b8c-a90f-4d09-8c80-da8db8dfcad6",
	}
	rsp,err := test.cli.Create(context.TODO(),&data)
	if err != nil {
		log.Fatalf("创建失败: %v", err)
	}
	log.Println("创建成功: ", rsp)
	test.data.Id = rsp.Node.Id
	//Get(test)
	Update(test)
	//Close(test)
	//Open(test)
	//Delete(test)
}

func Delete(test Test)  {
	data := proto.IdRequest{Id:test.data.Id}
	rsp,err := test.cli.Delete(context.TODO(),&data)
	if err != nil {
		log.Fatalf("删除失败: %v", err)
	}
	log.Println("删除成功:", rsp)
}
func Update(test Test)  {
	data :=pb.Node{
		Id:"0122ff85-d44d-40fc-82db-9de04d52b5a6",
		Duration:20,
		Progress:0,
		Title:"软件开发",
	}
	log.Println(data.Title)
	rsp,err := test.cli.Update(context.TODO(),&data)
	if err != nil {
		log.Fatalf("更新失败: %v", err)
	}
	log.Println("更新成功:", rsp)
}
func Open(test Test)  {
	data := proto.IdRequest{Id:test.data.Id}
	rsp,err := test.cli.Open(context.TODO(),&data)
	if err != nil {
		log.Fatalf("启用失败: %v", err)
	}
	log.Println("更新成功:", rsp)
}
func Close(test Test)  {
	data := proto.IdRequest{Id:test.data.Id}
	rsp,err := test.cli.Close(context.TODO(),&data)
	if err != nil {
		log.Fatalf("禁用失败: %v", err)
	}
	log.Println("禁用成功:", rsp)
}
func Get(test Test)  {
	data := proto.IdRequest{Id:test.data.Id}
	rsp,err := test.cli.Get(context.TODO(),&data)
	if err != nil {
		log.Fatalf("查询失败: %v", err)
	}
	log.Println("查询成功:", rsp)
}
/*func GetList(test Test)  {
	rep := pb.ListRequest{}
	rsp,err := test.cli.GetList(context.TODO(),&rep)
	if err != nil {
		log.Fatalf("获取列表失败: %v", err)
	}
	log.Println("获取列表成果:")
	for _,v := range rsp.Nodes{
		fmt.Println(v)
	}
}
func GetPage(test Test)  {
	rep := pb.PageRequest{}
	rsp,err := test.cli.GetPage(context.TODO(),&rep)
	if err != nil {
		log.Fatalf("获取分页失败: %v", err)
	}
	log.Println("获取分页成功:")
	log.Println(rsp.Page.String())
	for _,v := range rsp.Nodes{
		fmt.Println(v)
	}
}*/


