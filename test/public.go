package main

import (
	"errors"
	"fmt"
	"gitee.com/johng/gf/g/util/gvalid"
	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
	"log"
	"math/rand"
	"pmm/public/config"
	"pmm/public/database"
	"pmm/public/model"
	"pmm/public/proto"
	"pmm/public/proto/template"
	"pmm/public/utils"
	"reflect"
	"strconv"
	"time"
)

type Template struct {
	template.Template
	Comm
}
type Comm struct {
	DeletedAt *time.Time
}
/**
创建
 */
func (m *Template) BeforeCreate(scope *gorm.Scope) error {
	uid, err := uuid.NewV4()
	if err != nil {
		log.Fatalf("created uuid error: %v\n", err)
	}
	scope.SetColumn("Id", uid.String())
	scope.SetColumn("CreatedAt", time.Now().Format("2006-01-02 15:04:05"))
	scope.SetColumn("UpdatedAt", time.Now().Format("2006-01-02 15:04:05"))
	scope.SetColumn("Status", config.OPEN_STATUS)
	return nil
}
var Conn1 *gorm.DB

func SetConn1(db *gorm.DB) {
	Conn1 = db
}
// 设置的表名
func (Template) TableName() string {
	return "template"
}

func main() {
	//连接数据库
	db,err := database.CreateConnection()
	defer db.Close()
	db.LogMode(true)

	if err != nil {
		log.Fatalf("数据库连接失败: %v\n",err)
	}
	model.SetConn(db)
	SetConn1(db)
	db.SingularTable(true)
	//db.AutoMigrate(Template{})
	db.AutoMigrate(model.Template{})


	//d := Template{}
	//
	//Do(dd,d)
	//fmt.Println(dd)
	//fmt.Println(d)
/*	th.BH.Request = data
	if err := th.BH.Update();err != nil {
		fmt.Println(err)
	}*/

	/*	BM := new(Base)
		BM.Data = dd
		BM.Model = dd
		BM.Query = db


		if err := BM.Create(nil);err != nil {
			fmt.Println(err)
		}
		fmt.Println(dd)

		dd.Title =  "title"+strconv.Itoa(rand.Intn(100))
		BM.Data = dd
		if err := BM.Update();err != nil {
			fmt.Println(err)
		}
		fmt.Println(dd)

		f := &Template{}
		f.Id = dd.Id
		BM.Data = dd
		if err := BM.GetInfo(false);err != nil {
			fmt.Println(err)
		}
		fmt.Println(dd)*/

	dd := &Template{}
	dd.Title = "222"
	dd.Value = "333"
	dd.Module = "123"
	//dd.Id = "559d4483-c897-4961-a49d-d598c4759259"

	t := new(BaseHandler)
	t.Request = dd
	t.Object = dd

	rule := map[string]string{
		"title": "required",
	}
	message := map[string]interface{}{
		"title" : "名称不能为空",
	}
	t.V = Validate{
		Rule:rule,
		Messages:message,
	}
	if err := t.Create(true); err != nil {
		fmt.Println(err)
	}
	fmt.Println(dd)

	dd.Id = "f1e3a58e-3529-48bf-9250-b35d310968ac"
	dd.Title =  "title"+strconv.Itoa(rand.Intn(100))
	t.Request = dd
	t.Object = dd
	if err := t.Update(); err != nil {
		fmt.Println(err)
	}
	fmt.Println(dd)

	dd.Id = "f1e3a58e-3529-48bf-9250-b35d310968ac"
	t.Request = dd
	if err := t.GetInfoById(dd.Id,false); err != nil {
		fmt.Println(err)
	}
	fmt.Println(dd)

}

func Do(data *Template,r Template) {
	th := NewTemplateHandler()
	th.BH.Request = data
	if err := th.BH.Update();err != nil {
		fmt.Println(err)
	}
	r = th.BH.Data.(Template)

}
type TemplateHandler struct {
	//logger *zap.Logger
	Model *Template
	BH BaseHandler
}
func NewTemplateHandler() *TemplateHandler{
	return &TemplateHandler{
		Model:&Template{},
		BH:BaseHandler{
			Object:&Template{},
			ObjName:"模板",
		},
	}
}
func (h *TemplateHandler) Create(data *model.Template) error {
	rule := map[string]string{
		"title": "required",
	}
	message := map[string]interface{}{
		"title" : "名称不能为空",
	}
	m := NewTemplateHandler()
	m.BH.V = Validate{
		Rule:rule,
		Messages:message,
	}
	if err := m.BH.Validate(*data);err != nil{
		return err
	}

	m.BH.Request = data

	err := m.BH.Create(false)
	if err != nil {
		return err
	}
	return nil
}
func (h *TemplateHandler) Update(data *model.Template) error {
	m := NewTemplateHandler()
	m.BH.Request = data
	if err := m.BH.Update();err != nil {
		return err
	}

	fmt.Println("====")
	fmt.Println(m.BH.Data)
	data = m.BH.Data.(*model.Template)

	return nil
}

type BaseHandler struct {
	Request interface{}
	Object interface{}
	ObjName string
	Data interface{}
	V Validate
}
type Validate struct {
	Rule map[string]string
	Messages map[string]interface{}
}
func (b *BaseHandler) Validate(data interface{}) error {
	if data == nil {
		data = b.Request
	}
	params := utils.StructToMap(data,true)
	if err := gvalid.CheckMap(params, b.V.Rule, b.V.Messages);err != nil {
		return errors.New(err.FirstString())
	}
	return nil
}
func (b *BaseHandler) Create(exist bool) error {
	err := b.Validate(nil)
	if err != nil {
		return err
	}

	req := b.Request
	bm := &model.Base{}
	bm.Data = req
	bm.Model = b.Object

/*	if exist {
		info,err := bm.IsExist(false)
		if err != nil {
			return err
		}
		if info != nil {
			return errors.New(b.ObjName+"已存在")
		}
	}*/

	if err := bm.Create(); err != nil {
		return err
	}
	b.Data = bm.GetData()
	//发送成功消息
	//if err := h.Publisher.Publish(ctx, info); err != nil {
	//	return err
	//}

	return nil
}
func (b *BaseHandler) Update() error {
	bm := &model.Base{}
	bm.Data = b.Request
	bm.Model = b.Object

	err := bm.Update()
	if err != nil {
		return err
	}

	id := b.GetId()
	if err := b.GetInfoById(id,true); err != nil {
		return err
	}
	b.Data = bm.GetData()

	return nil
}
func (b *BaseHandler) GetInfoById(id string,isOpen bool) error {
	data := make(map[string]interface{})
	data["id"] = id

	/*	s := b.Object
		err := utils.MapToStruct(data,s)
		if err != nil {
			return errors.New("取得详情请求错误：类型转化错误")
		}*/
	bm := &model.Base{}
	bm.Data = data
	bm.Model = b.Object

	err := bm.GetInfo(isOpen)
	if err != nil {
		return err
	}
	b.Data = bm.GetData()

	return nil
}
func (b *BaseHandler) GetList(list interface{}) (interface{},error)  {
	bm := new(model.Base)
	bm.Model = b.Object
	bm.Data = b.Request

	if err := bm.GetList(list); err != nil {
		return nil,err
	}

	l := bm.GetData()
	return l,nil
}
func (b *BaseHandler) GetId() string {
	t := b.Request
	v := reflect.ValueOf(t)
	if v.Kind() == reflect.Ptr {
		v = v.Elem()
	}
	id := v.FieldByName("Id").String()

	return id
}

/*func (b *BaseHandler) IsExist(id string,isOpen bool) error {
	//验证是否存在
	err := b.GetInfoById(id,true)
	if err != nil {
		return err
	}
	info := b.GetData()
	if info == nil {
		return errors.New("请求"+b.ObjName+"不存在")
	}
	return nil
}
func (b *BaseHandler) GetData() interface{} {
	return b.Data
}*/


type Base struct {
	Model interface{}
	Data interface{}
	ListRequest *proto.ListRequest
	ListResponse interface{}
	Fields []string
	Args  []interface{}
	Query *gorm.DB
}
func (b *Base) Create(where ...interface{}) error {
	query := b.Query
	if query == nil {
		query = Conn1
	}

	if where != nil {
		for _, value := range where {
			query = query.Where(value)
		}
	}

	db := query.Create(b.Data)
	if err := db.Error; err != nil {
		return err
	}
	return nil
}
func (b *Base) Update(condition... string) error {
	query := b.Query
	if query == nil {
		query = Conn1
	}

	query = query.Model(b.Data)
	//过滤参数
	if len(condition) >= 1 {
		query = query.Omit(condition...)
	}

	if err := query.Updates(b.Data).Error;err != nil {
		return err
	}
	return nil
}

/**
删除
参数可以为[]string{"id","id"},string
 */
func (b *Base) Delete() error {
	if b.Data == nil {
		return errors.New("删除条件不能为空")
	}

	v := reflect.ValueOf(b.Data)
	t := v.Kind()

	query := Conn1
	switch t {
	case reflect.Slice:
		id := b.Data.([]string)
		query = query.Where(id)
		break
	case reflect.String:
		id := b.Data.(string)
		query = query.Where("id = '" + id +"'")
		break
	}

	res := query.Delete(b.Data)
	if err := res.Error; err != nil {
		return err
	}
	return nil
}
func (b *Base) GetInfo(isOpen bool) error {
	if b.Data == nil {
		return nil
	}

	query := Conn1.Where(b.Data)
	if isOpen {
		query = query.Where("status = ?",config.OPEN_STATUS)
	}

	res := query.First(b.Model)
	if err := res.Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			b.Data = nil
		}
		return nil
	}
	b.Data = res.Value
	return nil
}

func (b *Base) IsExist(isOpen bool) (interface{},error) {
	data := b.Data
	if data == nil {
		return nil,errors.New("查询条件不能为空")
	}

	query := Conn1.Where(data)
	if isOpen {
		query = query.Where("status = ?",config.OPEN_STATUS)
	}

	res := query.First(b.Model)
	if err := res.Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil,nil
		}
		return nil,err
	}
	b.Data = res.Value
	b.Data = res.Value
	return b.Data,nil
}

func (b *Base) GetList() error {
	model := b.Model

	query := Conn1.Model(model)

	if b.Data != nil {
		query = query.Where(b.Data)
	}

	//取得字段 过滤参数
	fs := utils.GetDbNameByStruct(Conn1,model)
	if len(fs) > 1 {
		query = query.Select(fs)
	}

	lr := b.ListRequest
	if lr != nil {
		if lr.Start > 0 {
			query = query.Offset(lr.Start)
		}
		if lr.Limit > 0 {
			query = query.Limit(lr.Limit)
		}
		if lr.Search != nil {
			query = utils.AndOrLikes(query,lr.Search,true)
		}
		if len(lr.Ids) > 0{
			query = query.Where(lr.Ids)
		}
	}

	args := b.Args
	if args != nil && len(args) > 0 {
		for _, value := range args {
			query = query.Where(value)
		}
	}

	re := query.Scan(b.Data)
	if err:= re.Error;err != nil {
		return err
	}
	return nil
}

/**
返回查询个数
 */
func (b *Base) GetCount() error {
	data := b.Data

	query := Conn1.Model(b.Model)

	if b.Data != nil {
		query = query.Where(data)
	}

	lr := b.ListRequest
	if lr != nil {
		if lr.Search != nil {
			query = utils.AndOrLikes(query,lr.Search,true)
		}
		if len(lr.Ids) > 0{
			query = query.Where(lr.Ids)
		}
	}

	args := b.Args
	if args != nil && len(args) > 0 {
		for _, value := range args {
			query = query.Where(value)
		}
	}
	var count *int64
	err := query.Count(&count).Error
	if err != nil {
		return err
	}

	b.Data = count
	return nil
}

func (b *Base) GetData() interface{} {
	return b.Data
}

