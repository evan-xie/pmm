package handler

import (
	"log"
	"context"
	pb "pmm/public/pb/tag"
)

type Test struct {
	cli pb.TagServiceClient
	data pb.Tag
}

func CreateFolder(data Test)  {
	rsp,err := data.cli.CreateFolder(context.TODO(),&data.data)
	if err != nil {
		log.Fatalf("创建Folder失败: %v", err)
	}
	log.Println("创建Folder成功: ", rsp)
}
func CreateTag(data Test)  {
	rsp,err := data.cli.CreateTag(context.TODO(),&data.data)
	if err != nil {
		log.Fatalf("创建Folder失败: %v", err)
	}
	log.Println("创建Folder成功: ", rsp)
}

