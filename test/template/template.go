package main

import (
	microclient "github.com/micro/go-micro/client"
	"log"
	"math/rand"
	"os"
	"github.com/micro/go-micro/cmd"
	pb "pmm/public/proto/template"
	"pmm/public/config"
	"strconv"
	"context"
)

type Test struct {
	cli pb.TemplateServiceClient
	data pb.Template
}

func main() {
	cmd.Init()
	// 创建 微服务的客户端
	client := pb.NewTemplateServiceClient(config.Namespace+"srv.public",microclient.DefaultClient)

	//测试数据
	data := &Test{
		cli:client,
		data:pb.Template{
			Title:"123213123",
			Value:"folder",
			Module:"Node",
		},
	}
	Create(data)
	// 直接退出即可
	os.Exit(0)
}

func Create(test *Test)  {
	test.data.Title = "title"+strconv.Itoa(rand.Intn(100))
	rsp,err := test.cli.Create(context.TODO(),&test.data)
	if err != nil {
		log.Fatalf("创建失败: %v", err)
	}
	log.Println("创建成功: ", rsp)
	test.data.Id = rsp.Template.Id
	Update(test)

}

func Update(test *Test)  {
	test.data.Title = "title"+strconv.Itoa(rand.Intn(100))
	rsp,err := test.cli.Update(context.TODO(),&test.data)
	if err != nil {
		log.Fatalf("修改失败: %v", err)
	}
	log.Println("修改成功: ", rsp)

}