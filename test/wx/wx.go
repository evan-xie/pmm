package main

import (
	"context"
	microclient "github.com/micro/go-micro/client"
	"github.com/micro/go-micro/cmd"
	"log"
	"os"
	"pmm/public/config"
	"pmm/public/proto/wxApp"
)

type Test struct {
	cli wxApp.WxAppServiceClient
	data wxApp.WxRegisterReq
}

func main() {
	cmd.Init()
	// 创建 微服务的客户端
	client := wxApp.NewWxAppServiceClient(config.Namespace+"srv.user",microclient.DefaultClient)

	//测试数据
	data := &Test{
		cli:client,
	}
	Reg(*data)
	//CreateFolder(*data)
	//GetList(*data)
	//GetPage(*data)
	////Delete(*data)
	//GetTree(*data)
	// 直接退出即可
	os.Exit(0)
}
func Reg(test Test)  {
	//cid,_ := uuid.NewV4()
	data := wxApp.WxRegisterReq{}
	rsp,err := test.cli.WxRegister(context.TODO(),&data)
	if err != nil {
		log.Fatalf("创建失败: %v", err)
	}
	log.Println("创建成功: ", rsp)

}
