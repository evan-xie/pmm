package main

import (
		"pmm/public/utils"
	"fmt"
			"pmm/user-srv/database"
	"log"
	"reflect"

	"github.com/asaskevich/govalidator"
	"github.com/jinzhu/gorm"
	"pmm/public/pb/tag"
	)

func main() {
	//连接数据库
	db,err := database.CreateConnection()
	defer db.Close()
	db.LogMode(true)

	if err != nil {
		log.Fatalf("数据库连接失败: %v\n",err)
	}

	getChild("f9d09c76-99d3-44bb-9522-666a2a4d5048",db)

}
func TestSmartPrint() {
	type User struct {
		Name string
		Id int
		Addr string
	}
	x := User{
		Name:"aaa",
		Id:1,
	}
	SmartPrint(x)
}
func SmartPrint(i interface{}){
	var kv = make(map[string]interface{})
	vValue := reflect.ValueOf(i)
	vType :=reflect.TypeOf(i)
	for i:=0;i<vValue.NumField();i++{
		kv[vType.Field(i).Name] = vValue.Field(i)
	}
	fmt.Println("获取到数据:")
	fmt.Println(kv)
	for k,v :=range kv{
		fmt.Print(k)
		fmt.Print(":")
		fmt.Print(v)
		fmt.Println()
	}
}

func testDb() {
	u := database.User{}
	u.Status = 0
	var us []*database.User
	fs := utils.GetDbNameByStruct(u.OperationDB(&u),u,"password")
	u.OperationDB(&u).Select(fs).Where(u).Offset(0).Limit(0).Omit("password").Find(&us)

	//user := query.Value
	//fmt.Println(query.Value)
	//fmt.Println(u)
	for k,v := range us {
		fmt.Println(k)
		fmt.Println(v)
	}
}

func getChild(id string,db *gorm.DB) []string {
	c := make(chan tag.Tag)
	//e := make(chan string)


	go Product(c,id,db)


	var ids []string
/*	for v := range c {
		ids = append(ids,v.Id)
		if v.Type == "folder" {
			go Product(c,v.Id,db)
		}
	}*/
	for i := 0; i < 100; i++ {
		fmt.Println(<-c)
		v := <-c
		if v.Type == "folder" {
			go Product(c,v.Id,db)
		}
		if i == 50 {
			close(c)
		}
	}
	fmt.Println(ids)
	fmt.Println(len(ids))
/*	for v := range e {
		fmt.Println(v)
	}*/
	return ids
}
func Product(p chan tag.Tag,id string,db *gorm.DB){
	if govalidator.IsNull(id) {
		return
	}
	//循环删除标签夹下的标签
	t := tag.Tag{
		Pid:id,
	}
	var list []tag.Tag
	db.Table("tag").Where(t).Find(&list)

	if list == nil {
		return
	}

	for _,v := range list{
		if govalidator.IsNull(v.Id){
			continue
		}
		p <- v
		fmt.Printf("插入%v ：%v \n",v.Type,v.Id)

	}
	//close(p)
}

