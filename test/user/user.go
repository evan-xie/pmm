package main

import (
	"context"
	microclient "github.com/micro/go-micro/client"
	"github.com/micro/go-micro/cmd"
	"log"
	"os"
	"pmm/public/config"
	pb "pmm/public/proto/user"
)

type Test struct {
	cli pb.UserServiceClient
	data pb.User
}

func main() {
	cmd.Init()
	// 创建 微服务的客户端
	client := pb.NewUserServiceClient(config.Namespace+"srv.user",microclient.DefaultClient)

	//测试数据
	data := &Test{
		cli:client,
	}

	//CreateFolder(*data)
	//GetList(*data)
	//GetPage(*data)
	//Delete(*data)
	//Create(*data)
	//GetTree(*data)
	Update(*data)
	// 直接退出即可
	os.Exit(0)
}





func Update(test Test)  {
	data :=pb.User{
		Id:"7c44b784-3234-4976-bcb3-66ea71b5fba2",
		Password:"15800336903",
	}

	rsp,err := test.cli.Update(context.TODO(),&data)
	if err != nil {
		log.Fatalf("更新失败: %v", err)
	}
	log.Println("更新成功:", rsp)

}


