package main

import (
	microclient "github.com/micro/go-micro/client"
	"context"
	"log"
	"os"
	"github.com/micro/go-micro/cmd"
	pb "pmm/public/proto/user/role"
	"pmm/public/config"
	"github.com/satori/go.uuid"
	"fmt"
)

type Test struct {
	cli pb.UserRoleServiceClient
	data pb.UserRole
}

func main() {
	cmd.Init()
	// 创建 微服务的客户端
	client := pb.NewUserRoleServiceClient(config.Namespace+"srv.user",microclient.DefaultClient)
	uid,_ := uuid.NewV4()
	rid,_ := uuid.NewV4()
	//测试数据
	data := &Test{
		cli:client,
		data:pb.UserRole{
			UserId:uid.String(),
			RoleId:rid.String(),
		},
	}

	//CreateFolder(*data)
	//GetList(*data)
	//GetPage(*data)
	//Delete(*data)
	Create(*data)
	Delete(*data)
	Create(*data)
	DeleteUser(*data)
	Create(*data)
	DeleteRole(*data)
	Create(*data)
	GetUserList(*data)
	GetUserPage(*data)
	GetRoleList(*data)
	GetRolePage(*data)
	// 直接退出即可
	os.Exit(0)
}


func Create(test Test)  {
	data := test.data
	rsp,err := test.cli.Create(context.TODO(),&data)
	if err != nil {
		log.Fatalf("创建失败: %v", err)
	}
	log.Println("创建成功: ", rsp)
	//Get(test)
	//Update(test)
	//Close(test)
	//Open(test)
	//Delete(test)
}

func Delete(test Test)  {
	data := test.data
	rsp,err := test.cli.Delete(context.TODO(),&data)
	if err != nil {
		log.Fatalf("删除失败: %v", err)
	}
	log.Println("删除成功:", rsp)
}
func DeleteUser(test Test)  {
	data := test.data
	rsp,err := test.cli.DeleteUsers(context.TODO(),&data)
	if err != nil {
		log.Fatalf("删除user失败: %v", err)
	}
	log.Println("删除user成功:", rsp)
}
func DeleteRole(test Test)  {
	data := test.data
	rsp,err := test.cli.DeleteRoles(context.TODO(),&data)
	if err != nil {
		log.Fatalf("删除role失败: %v", err)
	}
	log.Println("删除role成功:", rsp)
}
func GetUserList(test Test)  {
	rep := pb.ListUserRequest{}
	rep.RoleId = "e114a230-3eb4-4ec3-892d-39dd08823a9e"
	//rep.RoleId = test.data.RoleId
	rsp,err := test.cli.GetUsersByRole(context.TODO(),&rep)
	if err != nil {
		log.Fatalf("获取Users列表失败: %v", err)
	}
	log.Println("获取Users列表成果:")
	for _,v := range rsp.Users{
		fmt.Println(v)
	}
}
func GetUserPage(test Test)  {
	rep := pb.PageUserRequest{}
	rep.RoleId = "e114a230-3eb4-4ec3-892d-39dd08823a9e"
	//rep.RoleId = test.data.RoleId
	rsp,err := test.cli.GetUserPageByRole(context.TODO(),&rep)
	if err != nil {
		log.Fatalf("获取Users分页失败: %v", err)
	}
	log.Println("获取Users分页成功:")
	log.Println(rsp.Page.String())
	for _,v := range rsp.Users{
		fmt.Println(v)
	}
}
func GetRoleList(test Test)  {
	rep := pb.ListRoleRequest{}
	rep.UserId = "ecc3954f-02ed-4201-9614-4664152b67bb"
	//rep.UserId = test.data.UserId
	rsp,err := test.cli.GetRolesByUser(context.TODO(),&rep)
	if err != nil {
		log.Fatalf("获取Roles列表失败: %v", err)
	}
	log.Println("获取Roles列表成果:")
	for _,v := range rsp.Roles{
		fmt.Println(v)
	}
}
func GetRolePage(test Test)  {
	rep := pb.PageRoleRequest{}
	rep.UserId = "ecc3954f-02ed-4201-9614-4664152b67bb"
	//rep.UserId = test.data.UserId
	rsp,err := test.cli.GetRolePageByUser(context.TODO(),&rep)
	if err != nil {
		log.Fatalf("获取Roles分页失败: %v", err)
	}
	log.Println("获取Roles分页成功:")
	log.Println(rsp.Page.String())
	for _,v := range rsp.Roles{
		fmt.Println(v)
	}
}

