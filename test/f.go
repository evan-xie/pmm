package main

import (
	"log"
	"net/http"
)

func main() {
	httpserver()
}
func httpserver() {

	//文件浏览
	http.Handle("/", http.FileServer(http.Dir("file")))
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}