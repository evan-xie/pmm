package main

import (
	"encoding/json"
	"fmt"
	gv "github.com/asaskevich/govalidator"
	"github.com/jinzhu/gorm"
	mes "github.com/micro/go-micro/errors"
	"math"
	"php"
	"pmm/public/utils"
	"pmm/public/utils/validate"
	sconf "pmm/user-srv/config"
	"pmm/user-srv/database"
	"reflect"
	"strconv"
	"strings"
	"time"
)

func main() {
	/*	client := redis.NewClient(&redis.Options{
		Addr:     "www.greenment.cn:6379",
		Password: "5E39bJJUgcxYrWC594dT", // no password set
		DB:       1,  // use default DB
	})
*/

	//v := new(Verify)
	//r,err := v.PhoneCode("18710700730")
	//fmt.Println(r)
	//fmt.Println(err)

	//fmt.Println(v.CheckCode("18710700730",int64(111111)))
/*	err := utils.SendMail("378521513@qq.com","验证码","您的验证码为：556456，有效时间为30分钟","html")
	if err != nil {
		fmt.Println(err)
	}*/


	//tm2, _ := time.Parse("2006-01-02T15:04:05+08:00", "2018-09-05T15:41:23+08:00")
	//
	//fmt.Println(tm2.Format("2006-01-02 15:04:05"))

/*
	n := user2.User{
		//CreatedAt:"2018-09-05 15:41:23",
		CreatedAt:"2018-09-05T15:41:23+08:00",
	}
	list := []user2.User{n,n,n}
	// get
/*	immutable := reflect.ValueOf(*n)
	val := immutable.FieldByName("CreatedAt").String()
	fmt.Printf("N=%d\n", val) // prints 1

	// set
	mutable := reflect.ValueOf(n).Elem()
	mutable.FieldByName("CreatedAt").SetString("444556464")
	fmt.Printf("N=%d\n", n.CreatedAt) // prints 7*/
	//fmt.Println(*n)

	//FmtDate(n,&n)
/*	hashedPwd,err := bcrypt.GenerateFromPassword([]byte("111111"),bcrypt.DefaultCost)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(hashedPwd))*/
	//fmt.Println(list)

	d := &Rule{}
	mutable := reflect.ValueOf(d).Elem()
	fmt.Println(mutable.Field(0).Kind())
	fmt.Println(GetStructFields(d))
	//fmt.Println(StructToMapByGorm(db.Model(Rule{}),d,&d))

}
func GetStructFields(s interface{}) []string {
	t := reflect.TypeOf(s)

	fs := t.Elem()
	var a []string
	for i := 0; i < fs.NumField(); i++ {
		f := fs.Field(i)
		tag := f.Tag.Get("json")
		if tag == "" || tag == "-" {
			continue
		}
		tag = strings.Replace(tag,",omitempty","",-1)
		a = append(a, tag)
	}
	return a
}
type Rule struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Type                 string   `protobuf:"bytes,2,opt,name=type,proto3" json:"type,omitempty"`
	Pid                  string   `protobuf:"bytes,3,opt,name=pid,proto3" json:"pid,omitempty"`
	Name                 string   `protobuf:"bytes,4,opt,name=name,proto3" json:"name,omitempty"`
	Title                string   `protobuf:"bytes,5,opt,name=title,proto3" json:"title,omitempty"`
	Icon                 string   `protobuf:"bytes,6,opt,name=icon,proto3" json:"icon,omitempty"`
	Conditions           string   `protobuf:"bytes,7,opt,name=conditions,proto3" json:"conditions,omitempty"`
	Domain               string   `protobuf:"bytes,8,opt,name=domain,proto3" json:"domain,omitempty"`
	Ismenu               int64    `protobuf:"zigzag64,9,opt,name=ismenu,proto3" json:"ismenu,omitempty"`
	Remark               string   `protobuf:"bytes,10,opt,name=remark,proto3" json:"remark,omitempty"`
	Method               string   `protobuf:"bytes,14,opt,name=method,proto3" json:"method,omitempty"`
	Status               int64    `protobuf:"zigzag64,11,opt,name=status,proto3" json:"status,omitempty"`
	CreatedAt            string   `protobuf:"bytes,12,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt            string   `protobuf:"bytes,13,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}
func StructToMapByGorm(db *gorm.DB,st interface{},in interface{}) map[string]interface{} {
	if in == nil {
		return nil
	}

	fs := db.NewScope(st).GetStructFields()
	m := make(map[string]string)

	for _,f :=range fs{
		m[f.Name] = f.DBName
	}

	var kv = make(map[string]interface{})
	vValue := reflect.ValueOf(in)
	vType :=reflect.TypeOf(in)
	for i:=0;i<vValue.NumField();i++{
		dbName := m[vType.Field(i).Name]
		val := vValue.Field(i)
		kv[dbName] = val
	}

	return kv
}
func SmartPrint1(in interface{}){
	var kv = make(map[string]interface{})
	vValue := reflect.ValueOf(in)
	vType :=reflect.TypeOf(in)
	for i:=0;i<vValue.NumField();i++{
		kv[vType.Field(i).Name] = vValue.Field(i)
	}
	fmt.Println("获取到数据:")
	for k,v :=range kv{
		fmt.Print(k)
		fmt.Print(":")
		fmt.Print(v)
		fmt.Println()
	}
}

func FmtDate(o,obj interface{}) {
	immutable := reflect.ValueOf(o)
	mutable := reflect.ValueOf(obj).Elem()

	fs := []string{"CreatedAt","UpdatedAt"}
	for _, f := range fs {
		v := immutable.FieldByName(f).String()
		if gv.IsNull(v) {
			continue
		}
		t, err := time.Parse("2006-01-02T15:04:05+08:00", v)
		if err != nil {
			t,_ = time.Parse("2006-01-02 15:04:05",v)
		}
		mutable.FieldByName(f).SetString(t.Format("2006-01-02 15:04:05"))
	}
}

type Verify struct {
	Set int64
	Code int64
	Number int64
}

var redis1 = database.ConnRedis()

func (v *Verify) PhoneCode(mobile string) (remain int64,err error) {
	if !validate.IsMobile(mobile){
		return 0,mes.BadRequest("user-srv","手机号码格式不正确")
	}

	cn := sconf.VerifyCodePrefix+mobile
	i := sconf.VerifyCodeInterval

	val,err := redis1.Get(cn).Result()
/*	if err != nil {
		fmt.Println(err)
		return 0,mes.InternalServerError("user-srv","Redis服务器异常,请稍后再试")
	}*/
	if !gv.IsNull(val) {
		var m Verify
		json.Unmarshal([]byte(val),&m)
		t := time.Now().Unix()- m.Set
		return int64(i) - t,nil
	}

	code := php.Rand(100000,999999)
	l := sconf.VerifyCodeLifeCycle
	x := math.Ceil(float64(l)/60)
	msg := "短信验证码为:  "+strconv.FormatInt(code,10)+"  ,有效期为"+strconv.FormatFloat(x,'E', -1,64)+"分钟【绿然环境】"
	if err := utils.SendSMS(mobile,msg);err != nil {
		return 0,mes.InternalServerError("user-srv",err.Error())
	}
	data := map[string]interface{}{
		"set":time.Now().Unix(),
		"code":code,
		"number":sconf.VerifyCodeMaxNumber,
	}
	j,_ := json.Marshal(data)
	ex := time.Duration(l)*time.Second
	err = redis1.Set(cn,string(j),ex).Err()
	if err != nil {
		return 0,mes.InternalServerError("user-srv","Redis服务器异常,请稍后再试")
	}
	return int64(i),nil
}

func (v *Verify) CheckCode(key string,code int64) bool {
	if gv.IsNull(key) || code == 0 {
		return false
	}

	cn := sconf.VerifyCodePrefix+key
	val,_ := redis1.Get(cn).Result()
	if gv.IsNull(val) {
		return false
	}

	var m Verify
	fmt.Println(json.Unmarshal([]byte(val),&m))
	if m.Code != code {
		if m.Number <= 0 {
			return false
		}
		m.Number --
		j,_ := json.Marshal(m)
		ex := time.Duration(int64(sconf.VerifyCodeLifeCycle) - time.Now().Unix() - m.Set)*time.Second
		redis1.Set(cn,j,ex)
		return false
	}
	redis1.Del(cn)
	return true
}