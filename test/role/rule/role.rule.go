package main

import (
	microclient "github.com/micro/go-micro/client"
	"context"
	"log"
	"os"
	"github.com/micro/go-micro/cmd"
	pb "pmm/public/proto/role/rule"
	"pmm/public/config"
	"fmt"
)

type Test struct {
	cli pb.RoleRuleServiceClient
	data pb.RoleRule
}

func main() {
	cmd.Init()
	// 创建 微服务的客户端
	client := pb.NewRoleRuleServiceClient(config.Namespace+"srv.user",microclient.DefaultClient)

	//测试数据
	data := &Test{
		cli:client,
	}

	//CreateFolder(*data)
	//GetList(*data)

	//Delete(*data)
	Create(*data)
	// 直接退出即可
	os.Exit(0)
}


func Create(test Test)  {
	data :=pb.RoleRule{
		RoleId:"e114a230-3eb4-4ec3-892d-39dd08823a9e",
		RuleId:"15412a67-b280-4dc7-811a-689d3f4322c3",
	}
	rsp,err := test.cli.Create(context.TODO(),&data)
	if err != nil {
		log.Fatalf("创建失败: %v", err)
	}
	log.Println("创建成功: ", rsp)
	test.data =data
	//Get(test)
	//Update(test)
	//Close(test)
	//Open(test)
	Delete(test)
	//DeleteRoles(test)
	//DeleteRules(test)
	GetList(test)
}

func Delete(test Test)  {
	data := test.data
	rsp,err := test.cli.Delete(context.TODO(),&data)
	if err != nil {
		log.Fatalf("删除失败: %v", err)
	}
	log.Println("删除成功:", rsp)
}
func DeleteRoles(test Test)  {
	data := test.data
	rsp,err := test.cli.DeleteRoles(context.TODO(),&data)
	if err != nil {
		log.Fatalf("批量删除角色失败: %v", err)
	}
	log.Println("批量删除角色成功:", rsp)
}
func DeleteRules(test Test)  {
	data := test.data
	rsp,err := test.cli.DeleteRules(context.TODO(),&data)
	if err != nil {
		log.Fatalf("批量删除规则失败: %v", err)
	}
	log.Println("批量删除规则成功:", rsp)
}
func GetList(test Test)  {
	rep := test.data
	rsp,err := test.cli.GetRuleIdsByRole(context.TODO(),&rep)
	if err != nil {
		log.Fatalf("获取列表失败: %v", err)
	}
	log.Println("获取列表成果:")
	for _,v := range rsp.RuleIds{
		fmt.Println(v)
	}
}

