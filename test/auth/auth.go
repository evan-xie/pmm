package main

import (
	microclient "github.com/micro/go-micro/client"
	"context"
	"log"
	"os"
	"github.com/micro/go-micro/cmd"
	pb "pmm/public/proto/auth"
	"pmm/public/config"
		)

type Test struct {
	cli pb.AuthServiceClient
	data pb.AuthRule
}

func main() {
	cmd.Init()
	// 创建 微服务的客户端
	client := pb.NewAuthServiceClient(config.Namespace+"srv.user",microclient.DefaultClient)

	//测试数据
	data := &Test{
		cli:client,
	}

	//AuthRoleRule(*data)
	GetUserInfo(*data)
	// 直接退出即可
	os.Exit(0)
}


func AuthRoleRule(test Test)  {
	//b := e.Enforce("efea76d6-193a-4b6a-b362-fcdde70847f8","测试28","*","app1")
	data := test.data
	data.RoleId = "efea76d6-193a-4b6a-b362-fcdde70847f8"
	data.Rule = "测试28"
	data.Method = "GET"
	data.Domain = "app1"
	rsp,err := test.cli.AuthRoleRule(context.TODO(),&data)
	if err != nil {
		log.Fatalf("验证失败: %v", err)
	}
	log.Println("验证成功: ", rsp)
}

func GetUserInfo(test Test) {
	req := &pb.Token{
		TokenStr:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiIqIiwiZXhwIjoxNTM2MjI1NTYwLCJqdGkiOiIzNDI4OWNjMjQxYjBlNzhkNjMyODdjODc4MDY1ZjU0NCIsImlhdCI6MTUzNjIyNDY2MCwiaXNzIjoiY29tLmdyZWVubWVudC5zcnYudXNlciIsIm5iZiI6MTUzNjIyNDY2MCwic3ViIjoiMDBjMWZhOGEtYmRiYS00ZDhlLThiOGUtYTNjOGQ3Njk1ZTVkIn0.CVLNl_gWKE6-p4642ewThOc_1a4Zu8DagvfJySheIFI",
		TokenType:"access",
	}
	rsp,err := test.cli.GetUserInfoByToken(context.TODO(),req)
	if err != nil {
		log.Fatalf("验证失败: %v", err)
	}
	log.Println("验证成功: ", rsp.Id)
}

