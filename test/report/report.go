package main

import (
	"context"
	microclient "github.com/micro/go-micro/client"
	"github.com/micro/go-micro/cmd"
	"log"
	"math/rand"
	"os"
	"pmm/public/config"
	"pmm/public/proto"
	"pmm/public/proto/report"
	"strconv"
)

type Test struct {
	cli report.ReportServiceClient
	data report.Report
}

func main() {
	cmd.Init()
	// 创建 微服务的客户端
	client := report.NewReportServiceClient(config.Namespace+"srv.public",microclient.DefaultClient)

	//测试数据
	data := &Test{
		cli:client,
		data:report.Report{
			Title:"123213123",
			Value:"json",
			//Id:"85840013-da48-4d50-b5f5-57d43361d309",
		},
	}
	//GetList(data)
	GetPage(data)
	//Create(data)
	// 直接退出即可
	os.Exit(0)
}

func Create(test *Test)  {
	test.data.Title = "title"+strconv.Itoa(rand.Intn(100))
	rsp,err := test.cli.Create(context.TODO(),&test.data)
	if err != nil {
		log.Fatalf("创建失败: %v", err)
	}
	log.Println("创建成功: ", rsp)
	test.data.Id = rsp.Report.Id
	Update(test)
	Close(test)
	Open(test)
	Get(test)
	Delete(test)

}

func Update(test *Test)  {
	test.data.Title = "title"+strconv.Itoa(rand.Intn(100))
	rsp,err := test.cli.Update(context.TODO(),&test.data)
	if err != nil {
		log.Fatalf("修改失败: %v", err)
	}
	log.Println("修改成功: ", rsp)

}

func Delete(test *Test)  {
	data := proto.IdRequest{Id:test.data.Id}
	rsp,err := test.cli.Delete(context.TODO(),&data)
	if err != nil {
		log.Fatalf("修改失败: %v", err)
	}
	log.Println("修改成功: ", rsp)

}

func Close(test *Test)  {
	data := proto.IdRequest{Id:test.data.Id}
	rsp,err := test.cli.Close(context.TODO(),&data)
	if err != nil {
		log.Fatalf("禁用失败: %v", err)
	}
	log.Println("禁用成功: ", rsp)

}
func Open(test *Test)  {
	data := proto.IdRequest{Id:test.data.Id}
	rsp,err := test.cli.Open(context.TODO(),&data)
	if err != nil {
		log.Fatalf("启用失败: %v", err)
	}
	log.Println("启用成功: ", rsp)

}

func Get(test *Test)  {
	data := proto.IdRequest{Id:test.data.Id}
	rsp,err := test.cli.Get(context.TODO(),&data)
	if err != nil {
		log.Fatalf("获取失败: %v", err)
	}
	log.Println("获取成功: ", rsp)

}

func GetList(test *Test) {
	rep := report.ListRequest{}
	rsp,err := test.cli.GetList(context.TODO(),&rep)
	if err != nil {
		log.Fatalf("获取失败: %v", err)
	}
	log.Println("获取成功: ", rsp.List)
}

func GetPage(test *Test) {
	rep := report.PageRequest{}
	rsp,err := test.cli.GetPage(context.TODO(),&rep)
	if err != nil {
		log.Fatalf("获取分页失败: %v", err)
	}
	log.Println("获取分页成功:")
	log.Println(rsp.Page.String())
	log.Println(rsp.List)

}