package main

import (
	"github.com/casbin/casbin"
	"github.com/casbin/gorm-adapter"
	_ "github.com/go-sql-driver/mysql"
	"github.com/casbin/protobuf-adapter"
	"fmt"
)

func main() {
	//Protobuf()
	mysql()
}

func Protobuf() {
	// Initialize a Protobuf adapter and use it in a Casbin enforcer:
	b := []byte{} // b stores Casbin policy in Protocol Buffers.
	a := protobufadapter.NewAdapter(&b) // Use b as the data source.

	e := casbin.NewEnforcer("./auth-srv/conf/rbac_model.conf", a)

	// Load the policy from Protocol Buffers bytes b.
	e.LoadPolicy()

	// Modify the policy.
	e.AddPolicy("alice", "data1", "read")

	// Check the permission.
	e.Enforce("alice", "data1", "read")

	// Modify the policy.
	//e.AddPolicy("alice", "data1", "read")
	// e.RemovePolicy(...)

	// Save the policy back to Protocol Buffers bytes b.
	e.SavePolicy()
}

func mysql() {
	// Initialize a Gorm adapter and use it in a Casbin enforcer:
	// The adapter will use the MySQL database named "casbin".
	// If it doesn't exist, the adapter will create it automatically.
	a := gormadapter.NewAdapter("mysql", "root:greenment@tcp(www.greenment.cn:3306)/project",true) // Your driver and data source.
	//e := casbin.NewEnforcer("./auth-srv/conf/rbac_model_with_resource_roles.conf", a)
	er := casbin.NewEnforcer("./auth-srv/conf/restful.conf", a,false)
	//casbin.
	//er.EnableLog(false)//屏蔽输出
	//er := casbin.NewEnforcer("./auth-srv/conf/restful.conf", a)

	// Or you can use an existing DB "abc" like this:
	// The adapter will use the table named "casbin_rule".
	// If it doesn't exist, the adapter will create it automatically.
	// a := gormadapter.NewAdapter("mysql", "mysql_username:mysql_password@tcp(127.0.0.1:3306)/abc", true)

	// Load the policy from DB.
	//e.LoadPolicy()
	er.LoadPolicy()
	// Check the permission.
	//e.Enforce("45646546545645646546546465456465", "data1", "read")
	fmt.Println("-------------------------------")
	//fmt.Println(e.AddRoleForUser("45646546545645646546546465456465","user"))
	//e.AddPermissionForUser("user1","rule1","get","*","11111")
	//er.AddPolicy("alice","/alice_data/:resource","GET")
	er.AddPolicy("alice","/alice_data2/","GET")
	er.AddPolicy("alice1","/alice_data2/","GET")
	er.AddPolicy("alice2","/alice_data2/","GET")
	er.AddPolicy("alice3","/alice_data2/","GET","app2")
	er.AddPolicy("alice3","/alice_data2/","POST","app1")
	//er.AddPolicy("alice","/alice_data2/:id/using/:resId","GET")

	//er.DeletePermissionsForUser("alice3")
	//er.DeletePermission("/alice_data2/")

	//er.DeletePermissionsForUser("alice")
	fmt.Println(er.GetPolicy())
	fmt.Println(er.GetNamedPolicy("p"))
	//fmt.Println(er.GetPermissionsForUser("alice3"))
	//fmt.Println(er.Enforce("alice","/alice_data2/1","GET"))
	//fmt.Println(er.Enforce("alice","/alice_data2/asdsadas/using/sad","GET"))
	//e.DeletePermissionForUser("user1","rule1","get")
	//e.DeletePermission("rule1")
//	e.AddNamedPolicy("g2","yonaghu","data1")
//	fmt.Println(	e.EnforceSafe("u1","r1"))
	//e.AddRoleForUser("u1","r1")
	//e.AddRoleForUserInDomain("u1","r1","d1")
	//fmt.Println(e.GetPermissionsForUser("user1"))

/*	fmt.Println(e.GetRolesForUser("45646546545645646546546465456465"))
	e.DeleteRoleForUser("45646546545645646546546465456465","user")
	e.AddRoleForUserInDomain("45646546545645646546546465456465","user1","app1")
	fmt.Println(e.GetRolesForUserInDomain("45646546545645646546546465456465","app1"))*/

	fmt.Println("-------------------------------")
	//e.DeleteRoleForUser("45646546545645646546546465456465","user")
	fmt.Println("-------------------------------")

	//e.DeleteRole("user")
	// Modify the policy.
	//e.AddPolicy("45646546545645646546546465456465", "data1", "read")
	// e.RemovePolicy(...)

	// Save the policy back to DB.
	//e.SavePolicy()
	er.SavePolicy()
}