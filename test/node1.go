package main

import (
	"fmt"
	"log"
	database2 "pmm/project-srv/database"
	"pmm/public/database"
	"pmm/public/model"
	"pmm/public/proto"
	"pmm/public/proto/node"
	"pmm/public/proto/project"
	"pmm/public/utils"
	database3 "pmm/user-srv/database"
	"time"
)

type Node struct {
	node.Node
	Comm1
}

type Project struct {
	project.Project
	Comm1
}


type Comm1 struct {
	DeletedAt *time.Time
}
func main() {
	//连接数据库
	db,err := database.CreateConnection()
	defer db.Close()
	db.LogMode(true)

	if err != nil {
		log.Fatalf("数据库连接失败: %v\n",err)
	}
	db.SingularTable(true)
	model.SetConn(db)

	pid := "b73ad310-aafd-4628-8d90-c5716134a665"

	p := &Project{}
	p.Id = pid

	pm := new(model.Base)
	pm.Model = p
	pm.Data = p

	if err := pm.GetInfo(true); err != nil {
		fmt.Println(err)
	}
	fmt.Println(p)

	n := &Node{}
	n.ProjectId = pid
	var list []*Node

	m := new(model.Base)
	m.Model = n
	m.Data = n

	if err := m.GetList(&list);err != nil {
		fmt.Println(err)
	}
	fmt.Println(list)

	var html = "<style> .box h1,.box h2{width:100%;text-align: center;} .main{width: 80%;margin:0 auto;border:1px solid black;}td{padding:0 15px;}</style><div class='box'><h1>Weekly Progress Report</h1><h2>项目周报</h2><div class='content'><div class='main'><table width='100%' height='123' border='1' cellpadding='0' cellspacing='0'><tr><td ><h3> 一、目前项目完成情况</br>Accomplished Tasks in Last Week</h3></td> </tr><tr><td width='100%'><ul>"
	t := getTree(list,"-")
	html += getNodeHtml(t)
	html += "</ul></td></tr>"

	html += "<tr><td><h3>二、沟通记录 Communication Records</h3></td></tr></tr><tr><td><strong >"+p.Remark+"</br></strong></td></tr>"

	html += "<tr><td ><h3>三、下周计划：Planned Tasks for Next Week</h3></td></tr><tr><td><strong ></br></strong></td></tr>"

	html += "</table></div></div></div>"
	fmt.Println(html)

	pu := &database2.ProjectUser{}
	pu.ProjectId = pid
	pum := new(model.Base)
	pum.Data = pu
	pum.Model = pu

	var pul []*database2.ProjectUser
	if err := pum.GetList(&pul);err != nil {
		fmt.Println(err)
	}
	fmt.Println(pul)

	var uids []string
	for _, pu := range pul {
		if pu.UserId != "" {
			uids = append(uids, pu.UserId)
		}
	}
	if len(uids) > 0 {
		u := &database3.User{}
		listRequest := &proto.ListRequest{}
		listRequest.Ids = uids

		um := new(model.Base)
		um.Model = u
		um.Data = u
		um.ListRequest = listRequest

		var users []*database3.User
		if err := um.GetList(&users);err != nil {
			fmt.Println(err)
		}

		for _, user := range users {
			if user.Email == "" {
				continue
			}
			re := utils.SendMail(user.Email,p.Title+"项目周报",html,"html")
			if re != nil{
				fmt.Println("发送失败")
			}
			fmt.Println("发送成功")
		}
	}

}
type NodeTree struct {
	Node                 *Node
	Child                []*NodeTree
}

func getTree(list []*Node,pid string) []*NodeTree {
	if len(list) == 0 {
		return nil
	}
	var nodeTreeArr []*NodeTree
	for _, n := range list {
		if n.Pid != pid {
			continue
		}
		var nodeTree NodeTree
		nodeTree.Node = n
		nodeTree.Child = getTree(list,n.Id)
		nodeTreeArr = append(nodeTreeArr, &nodeTree)
	}
	return nodeTreeArr
}

func getNodeHtml(treeNode []*NodeTree) string {
	var html string
	for _, tree := range treeNode {
		n := tree.Node

		//判断是否完成
		if n.Progress >= 1 {
			ua, err := time.Parse("2006-01-02T15:04:05+08:00", n.UpdatedAt)
			if err != nil {
				ua,_ = time.Parse("2006-01-02 15:04:05",n.UpdatedAt)
			}
			ts := ua.Format("2006年01月02日")
			html += "<li>"+ts+"	<strong>已完成 "+n.Title+"</strong>"
		}else{
			ts := time.Now().Format("2006年01月02日")
			p := fmt.Sprintf("%.2f",n.Progress*100)+"%"
			html += "<li>"+ts+" <strong>"+n.Title+"</strong> 的进度为"+p
		}
		if len(tree.Child) > 0 {
			html += "<ul>"
			html += getNodeHtml(tree.Child)
			html += "</ul>"
		}
		html += "</li>"
	}
	return html
}