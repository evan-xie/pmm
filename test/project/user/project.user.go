package main

import (
	microclient "github.com/micro/go-micro/client"
	"context"
	"log"
	"os"
	"github.com/micro/go-micro/cmd"
	pb "pmm/public/proto/project/user"
	"pmm/public/config"
			"fmt"
	"pmm/public/proto"
	"github.com/satori/go.uuid"
)

type Test struct {
	cli pb.ProjectUserServiceClient
	data pb.ProjectUser
}

func main() {
	cmd.Init()
	// 创建 微服务的客户端
	client := pb.NewProjectUserServiceClient(config.Namespace+"srv.project",microclient.DefaultClient)

	//测试数据
	data := &Test{
		cli:client,
	}

	//CreateFolder(*data)
	GetList(*data)
	GetPage(*data)
	//Delete(*data)
	Create(*data)
	//GetTree(*data)
	// 直接退出即可
	os.Exit(0)
}

func Create(test Test)  {
	uid,_ := uuid.NewV4()
	pid,_ := uuid.NewV4()

	data := pb.ProjectUser{
		UserId:uid.String(),
		ProjectId:pid.String(),
	}
	rsp,err := test.cli.Create(context.TODO(),&data)
	if err != nil {
		log.Fatalf("创建失败: %v", err)
	}
	log.Println("创建成功: ", rsp)
	test.data.Id = rsp.ProjectUser.Id
	Get(test)
	Update(test)
	Close(test)
	Open(test)
	GetProjectId(test)
	GetUserId(test)
	Delete(test)

}
func Delete(test Test)  {
	data := proto.IdRequest{Id:test.data.Id}
	rsp,err := test.cli.Delete(context.TODO(),&data)
	if err != nil {
		log.Fatalf("删除失败: %v", err)
	}
	log.Println("删除成功:", rsp)
}
func Update(test Test)  {
	data :=pb.ProjectUser{
		IsManager:config.OPEN_STATUS,
		Id:test.data.Id,
	}

	rsp,err := test.cli.Update(context.TODO(),&data)
	if err != nil {
		log.Fatalf("更新失败: %v", err)
	}
	log.Println("更新成功:", rsp)
}
func Open(test Test)  {
	data := proto.IdRequest{Id:test.data.Id}
	rsp,err := test.cli.Open(context.TODO(),&data)
	if err != nil {
		log.Fatalf("启用失败: %v", err)
	}
	log.Println("更新成功:", rsp)
}
func Close(test Test)  {
	data := proto.IdRequest{Id:test.data.Id}
	rsp,err := test.cli.Close(context.TODO(),&data)
	if err != nil {
		log.Fatalf("禁用失败: %v", err)
	}
	log.Println("禁用成功:", rsp)
}
func Get(test Test)  {
	data := proto.IdRequest{Id:test.data.Id}
	rsp,err := test.cli.Get(context.TODO(),&data)
	if err != nil {
		log.Fatalf("查询失败: %v", err)
	}
	log.Println("查询成功:", rsp)
}
func GetList(test Test)  {
	rep := pb.ListRequest{}
	rsp,err := test.cli.GetList(context.TODO(),&rep)
	if err != nil {
		log.Fatalf("获取列表失败: %v", err)
	}
	log.Println("获取列表成果:")
	for _,v := range rsp.ProjectUsers{
		fmt.Println(v)
	}
}
func GetPage(test Test)  {
	rep := pb.PageRequest{}
	rsp,err := test.cli.GetPage(context.TODO(),&rep)
	if err != nil {
		log.Fatalf("获取分页失败: %v", err)
	}
	log.Println("获取分页成功:")
	log.Println(rsp.Page.String())
	for _,v := range rsp.ProjectUsers{
		fmt.Println(v)
	}
}
func GetProjectId(test Test)  {
	rep := test.data
	//rep.UserId = ""
	rsp,err := test.cli.GetProjectIdByUser(context.TODO(),&rep)
	if err != nil {
		log.Fatalf("获取项目编号失败: %v", err)
	}
	log.Println("获取项目编号成功:")
	log.Println(rsp)
}
func GetUserId(test Test)  {
	rep := test.data
	//rep.ProjectId = ""
	rsp,err := test.cli.GetUserIdByProject(context.TODO(),&rep)
	if err != nil {
		log.Fatalf("获取用户编号失败: %v", err)
	}
	log.Println("获取用户编号成功:")
	log.Println(rsp)

}



