package main

import (
	microclient "github.com/micro/go-micro/client"
	"context"
	"log"
	"os"
	"github.com/micro/go-micro/cmd"
	pb "pmm/public/proto/tag/group"
	"pmm/public/config"
	"math/rand"
	"strconv"
	"fmt"
	)

type Test struct {
	cli pb.TagGroupServiceClient
	data pb.TagGroup
}

func main() {
	cmd.Init()
	// 创建 微服务的客户端
	client := pb.NewTagGroupServiceClient(config.Namespace+"srv.tag",microclient.DefaultClient)

	//测试数据
	data := &Test{
		cli:client,
	}

	//CreateFolder(*data)
	GetList(*data)
	GetPage(*data)
	//Delete(*data)
	Create(*data)
	// 直接退出即可
	os.Exit(0)
}


func Create(test Test)  {
	data :=pb.CreateRequest{
		Name:"测试组"+strconv.Itoa(rand.Intn(100)),
		Module:"company",
		RelationId:"f9d09c76-99d3-44bb-9522-666a2a4d5048",
		State:"private",
	}
	rsp,err := test.cli.Create(context.TODO(),&data)
	if err != nil {
		log.Fatalf("创建失败: %v", err)
	}
	log.Println("创建成功: ", rsp)
	test.data.Id = rsp.TagGroup.Id
	Get(test)
	Update(test)
	Close(test)
	Open(test)
	Delete(test)
}

func Delete(test Test)  {
	data := pb.IdRequest{Id:test.data.Id}
	rsp,err := test.cli.Delete(context.TODO(),&data)
	if err != nil {
		log.Fatalf("删除失败: %v", err)
	}
	log.Println("删除成功:", rsp)
}
func Update(test Test)  {
	data :=pb.UpdateRequest{
		Name:"测试组"+strconv.Itoa(rand.Intn(100)),
		Module:"company",
		RelationId:"f9d09c76-99d3-44bb-9522-666a2a4d5048",
		State:"private",
		Id:test.data.Id,
	}
	rsp,err := test.cli.Update(context.TODO(),&data)
	if err != nil {
		log.Fatalf("更新失败: %v", err)
	}
	log.Println("更新成功:", rsp)
}
func Open(test Test)  {
	data := pb.IdRequest{Id:test.data.Id}
	rsp,err := test.cli.Open(context.TODO(),&data)
	if err != nil {
		log.Fatalf("启用失败: %v", err)
	}
	log.Println("更新成功:", rsp)
}
func Close(test Test)  {
	data := pb.IdRequest{Id:test.data.Id}
	rsp,err := test.cli.Close(context.TODO(),&data)
	if err != nil {
		log.Fatalf("启用失败: %v", err)
	}
	log.Println("启用成功:", rsp)
}
func Get(test Test)  {
	data := pb.IdRequest{Id:test.data.Id}
	rsp,err := test.cli.Get(context.TODO(),&data)
	if err != nil {
		log.Fatalf("查询失败: %v", err)
	}
	log.Println("查询成功:", rsp)
}
func GetList(test Test)  {
	rep := pb.ListRequest{}
	rsp,err := test.cli.GetList(context.TODO(),&rep)
	if err != nil {
		log.Fatalf("获取列表失败: %v", err)
	}
	log.Println("获取列表成果:")
	for _,v := range rsp.TagGroups{
		fmt.Println(v)
	}
}
func GetPage(test Test)  {
	rep := pb.PageRequest{}
	rsp,err := test.cli.GetPage(context.TODO(),&rep)
	if err != nil {
		log.Fatalf("获取分页失败: %v", err)
	}
	log.Println("获取分页成功:")
	for _,v := range rsp.TagGroups{
		fmt.Println(v)
	}
}

