package main

import (
	microclient "github.com/micro/go-micro/client"
	"context"
	"log"
	"os"
	"github.com/micro/go-micro/cmd"
	 pb "pmm/public/proto/tag"
	"pmm/public/config"
		"math/rand"
	"strconv"
	"fmt"
	"encoding/json"
)

type Test struct {
	cli pb.TagServiceClient
	data pb.Tag
}

func main() {
	cmd.Init()
	// 创建 微服务的客户端
	client := pb.NewTagServiceClient(config.Namespace+"srv.tag",microclient.DefaultClient)

	//测试数据
	data := &Test{
		cli:client,
		data:pb.Tag{
			Name:"",
			TagGroup:"9ebaad58-2cbd-46b9-b256-22aaff96c190",
			Type:"folder",
			Pid:"b53fa391-44ae-4ac1-ada8-949be820f996",
		},
	}

	CreateFolder(*data)
	GetList(*data)
	GetPage(*data)
	//Delete(*data)
	GetTree(*data)
	// 直接退出即可
	os.Exit(0)
}


func CreateFolder(test Test)  {
	test.data.Name = "folder"+strconv.Itoa(rand.Intn(100))
	rsp,err := test.cli.CreateFolder(context.TODO(),&test.data)
	if err != nil {
		log.Fatalf("创建失败: %v", err)
	}
	log.Println("创建Folder成功: ", rsp)
	test.data.Pid = rsp.Tag.Id
	CreateTag(test)

}
func CreateTag(test Test)  {
	test.data.Name = "tag"+strconv.Itoa(rand.Intn(100))
	rsp,err := test.cli.CreateTag(context.TODO(),&test.data)
	if err != nil {
		log.Fatalf("创建Tag失败: %v", err)
	}
	log.Println("创建Tag成功: ", rsp)
	test.data.Id = rsp.Tag.Id
	Update(test)
	Get(test)
	Close(test)
	Open(test)
	Delete(test)
}
func Delete(test Test)  {
	//test.data.Id = "c23e3bda-8f1f-47c8-9dfa-94856ced0317"
	rsp,err := test.cli.Delete(context.TODO(),&test.data)
	if err != nil {
		log.Fatalf("删除失败: %v", err)
	}
	log.Println("删除成功:", rsp)
}
func Update(test Test)  {
	test.data.Name = "tag"+strconv.Itoa(rand.Intn(10000))
	rsp,err := test.cli.Update(context.TODO(),&test.data)
	if err != nil {
		log.Fatalf("更新失败: %v", err)
	}
	log.Println("更新成功:", rsp)
}
func Open(test Test)  {
	rsp,err := test.cli.Open(context.TODO(),&test.data)
	if err != nil {
		log.Fatalf("启用失败: %v", err)
	}
	log.Println("更新成功:", rsp)
}
func Close(test Test)  {
	rsp,err := test.cli.Close(context.TODO(),&test.data)
	if err != nil {
		log.Fatalf("启用失败: %v", err)
	}
	log.Println("启用成功:", rsp)
}
func Get(test Test)  {
	rsp,err := test.cli.Get(context.TODO(),&test.data)
	if err != nil {
		log.Fatalf("查询失败: %v", err)
	}
	log.Println("查询成功:", rsp)
}
func GetList(test Test)  {
	rep := pb.ListRequest{}
	rsp,err := test.cli.GetList(context.TODO(),&rep)
	if err != nil {
		log.Fatalf("获取列表失败: %v", err)
	}
	log.Println("获取列表成果:")
	for _,v := range rsp.Tags{
		fmt.Println(v)
	}
}
func GetPage(test Test)  {
	rep := pb.PageRequest{}
	rsp,err := test.cli.GetPage(context.TODO(),&rep)
	if err != nil {
		log.Fatalf("获取分页失败: %v", err)
	}
	log.Println("获取分页成功:")
	for _,v := range rsp.Tags{
		fmt.Println(v)
	}
	fmt.Println(rsp.Page)
}
func GetTree(test Test) {
	rep := pb.TreeRequest{
		TagGroup:"greenment",
		Id:"b53fa391-44ae-4ac1-ada8-949be820f996",
	}
	rsp,err := test.cli.GetTree(context.TODO(),&rep)
	if err != nil {
		log.Fatalf("获取Tree失败: %v", err)
	}
	log.Println("获取Tree成功:")
	log.Println(rsp)
	if b, err := json.Marshal(rsp.Trees); err == nil {
		fmt.Println(string(b))
	}

}
