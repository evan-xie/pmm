package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"pmm/public/utils"
	"pmm/public/utils/oss"
	"pmm/user-srv/config"
	"strings"
	"time"
)

func main() {
	token,err := utils.GetWxAccessToken()
	url := config.CreateWXAQRCode
	url  = strings.Replace(url, "ACCESS_TOKEN",  token,  -1)
	resp, err := http.Post(url,"application/json",
		strings.NewReader("{\"path\": \"pages/index?query=1\"}"))
	if err != nil {
		fmt.Println(err.Error())
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {

	}

	data ,err2 := ioutil.ReadAll(resp.Body)
	if err2 != nil {
		fmt.Printf("读取数据失败")
	}
	o,err := oss.Init()
	fileName := "file/"+time.Now().Format("2006-01-02")+"/1111111"+".jpg"
	err = o.PutObject(fileName, bytes.NewReader([]byte(data)))
	if err != nil {
		fmt.Println("Error:", err)
	}



	fmt.Println(data)
	ioutil.WriteFile(fmt.Sprintf("./1.jpg"), data, 0644)

}
