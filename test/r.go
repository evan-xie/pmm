package main

import (
	"fmt"
	"github.com/axgle/mahonia"
	"github.com/labstack/echo"
)

func main(){
	e := echo.New()

	e.GET("/a",t1)
	e.GET("/a/search/:aa",t1)
	e.GET("/a/search/:aa/:aaa",t1)

	e.Start(":8005")
}
func t1(c echo.Context) error {
	str := c.Param("aa")
	decoder := mahonia.NewDecoder("gbk")
	if decoder == nil {
		fmt.Println("编码不存在!")
	}
	fmt.Println(str)
	s := decoder.ConvertString(str)
	fmt.Println(s)
	return nil
}