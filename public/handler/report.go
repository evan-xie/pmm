package handler

import (
	"errors"
	"fmt"
	"pmm/public/model"
	"pmm/public/proto"
	"pmm/public/proto/report"
)

type ReportHandler struct {
	//logger *zap.Logger
	Model *model.Report
	BH BaseHandler
}

func NewReportHandler() *ReportHandler{
	return &ReportHandler{
		Model:&model.Report{},
		BH:BaseHandler{
			Object:&model.Report{},
			ObjName:"报告",
		},
	}
}

func (h *ReportHandler) InitReportHandler() *BaseHandler {
	return &BaseHandler{
		Object:&model.Report{},
		ObjName:"报告",
		V:Validate{
			Rule:map[string]string{
				"id": "required|regex:^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$",
				"title": "required",
				"value": "required",
			},
			Messages: map[string]interface{}{
				"id" : "编号不能为空|编号格式不合法",
				"title" : "标题不能为空",
				"value" : "报告内容不能为空",
			},
		},
	}
}

func (h *ReportHandler) GetValidateRule() Validate {
	return Validate{
		Rule:map[string]string{
			"id": "required|regex:^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$",
			"title": "required",
			"value": "required",
		},
		Messages: map[string]interface{}{
			"id" : "编号不能为空|编号格式不合法",
			"title" : "标题不能为空",
			"value" : "报告内容不能为空",
		},
	}
}

func (h *ReportHandler) Create(data *model.Report) error {
	rule := map[string]string{
		"title": "required",
		"value": "required",
	}
	message := map[string]interface{}{
		"title" : "标题不能为空",
		"value" : "报告内容不能为空",
	}
	bh := new(BaseHandler)
	bh.V = Validate{
		Rule:rule,
		Messages:message,
	}

	if err := bh.Validate(*data);err != nil{
		return err
	}

	bh.Object = data
	bh.Request = data
	err := bh.Create(false)
	if err != nil {
		return err
	}

	return nil
}

func (h *ReportHandler) Update(data *model.Report) error {
	fmt.Printf("内存地址：%p\n", &data)
	fmt.Printf("指针的内存地址：%p\n", data)

	bh := h.InitReportHandler()
	c := map[string]string{
		"id" :"regex:^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$",
		"title": "required",
		"value": "required",
	}
	v := bh.GetValidateRule(c,h.GetValidateRule())
	bh.V = v
	if err := bh.Validate(*data);err != nil{
		return err
	}

	if bh.CheckById(data.Id, false) == false {
		return errors.New("报告不存在")
	}

	bh.Request = data
	if err := bh.Update(); err != nil {
		return err
	}

	i,err := bh.GetInfoById(data.Id,false)
	if err != nil {
		return err
	}

	info := i.(*model.Report)
	data.Report = info.Report
	fmt.Printf("内存地址：%p\n", &data)
	fmt.Printf("指针的内存地址：%p\n", data)

	return nil
}

func (h *ReportHandler) Delete(data *model.Report) error {
	bh := h.InitReportHandler()
	c := map[string]string{
		"id": "required|regex:^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$",
	}
	bh.V = bh.GetValidateRule(c,h.GetValidateRule())
	if err := bh.Validate(*data);err != nil{
		return err
	}

	if bh.CheckById(data.Id, false) == false {
		return errors.New("报告不存在")
	}

	if err := bh.DeleteById(data.Id); err != nil {
		return err
	}
	return nil
}

func (h *ReportHandler) ChangeStatus(data *model.Report,status int64) error {
	bh := h.InitReportHandler()
	c := map[string]string{
		"id": "required|regex:^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$",
	}
	bh.V = bh.GetValidateRule(c,h.GetValidateRule())
	if err := bh.Validate(*data);err != nil{
		return err
	}

	data.Status = status
	bh.Request = data
	if err := bh.Update(); err != nil {
		return err
	}

	info := bh.Data
	i := info.(*model.Report)
	data.Report = i.Report
	return nil
}

func (h *ReportHandler) Get(data *model.Report) error {
	bh := h.InitReportHandler()

	//验证
	c := map[string]string{
		"id": "required|regex:^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$",
	}
	if err := bh.ValidateByConfig(*data,c);err != nil{
		return err
	}

	i,err := bh.GetInfoById(data.Id,false)
	if err != nil {
		return err
	}

	info := i.(*model.Report)
	data.Report = info.Report
	return nil
}

func (h *ReportHandler) GetList(req *report.ListRequest,list *[]*report.Report) error{
	bh := h.InitReportHandler()
	bh.Request  = req.Report
	bh.ListRequest = req.Default
	
	err := bh.GetList(list)
	if err != nil {
		return err
	}

	return nil
}

func (h *ReportHandler) GetPage(req *report.PageRequest,page *report.PageResponse) error{
	bh := h.InitReportHandler()
	bh.Request  = req.Report
	bh.PageRequest = req.Default

	var p proto.Page
	var list []*report.Report

	err := bh.GetPage(&p,&list)
	if err != nil {
		return err
	}

	page.Page = &p
	page.List = list

	return nil
}
