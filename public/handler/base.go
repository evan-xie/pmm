package handler

import (
	"errors"
	"math"
	"php"
	"pmm/public/config"
	"pmm/public/proto"
	"reflect"

	"gitee.com/johng/gf/g/util/gvalid"
	"pmm/public/model"
	"pmm/public/utils"
)

type BaseHandler struct {
	Request interface{}
	Object interface{}
	ObjName string
	Data interface{}
	V Validate
	ListRequest *proto.ListRequest
	PageRequest *proto.PageRequest
}

type Validate struct {
	Rule map[string]string
	Messages map[string]interface{}
}

func NewBaseHandler() *BaseHandler {
	return &BaseHandler{
	}
}

/**

	例子
	params := map[string]string {
		"passport"  : "john",
		"password"  : "123456",
		"password2" : "1234567",
	}
	rules := map[string]string {
		"passport"  : "required|length:6,16",
		"password"  : "required|length:6,16|same:password2",
		"password2" : "required|length:6,16",
	}
	msgs  := map[string]interface{} {
		"passport" : "账号不能为空|账号长度应当在:min到:max之间",
		"password" : map[string]string {
			"required" : "密码不能为空",
			"same"     : "两次密码输入不相等",
		},
	}
	e := gvalid.CheckMap(params, rules, msgs)
	a := e.String()
	b,h := e.FirstItem()
	c,i := e.FirstRule()
	d := e.FirstString()
	g := e.Map()
	f := e.Maps()
	fmt.Println(e,a,b,c,d,g,f,h,i)
 */

func (b *BaseHandler) Validate(data interface{}) error {
	if data == nil {
		data = b.Request
	}
	params := utils.StructToMap(data,true)
	if err := gvalid.CheckMap(params, b.V.Rule, b.V.Messages);err != nil {
		return errors.New(err.FirstString())
	}
	return nil
}
/**
map[string][]string{
	"id"=["request","regex:^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$"]
}
 */
func (b *BaseHandler) GetValidateRule(c map[string]string,validate Validate) Validate {
	if c == nil {
		return Validate{}
	}

	rule := map[string]string{}
	mess := map[string]interface{}{}

	for f, rs := range c {
		var rr []string
		var mm []string
		rrr := php.Explode("|",rs)
		for _, r := range rrr {
			vr := validate.Rule[f]
			vrs := php.Explode("|",vr)
			var vrsi []interface{}
			for _, v := range vrs {
				vrsi = append(vrsi, v)
			}
			if php.InArray(r,vrsi...) == false {
				continue
			}
			rr = append(rr, r)
			n := php.IndexOf(r,vrsi...)
			if n <= 0 {
				n = 0
			}
			vm := validate.Messages[f]
			if reflect.TypeOf(vm).Kind() == reflect.String {
				vmm := vm.(string)
				vms := php.Explode("|",vmm)
				mm = append(mm, vms[n])
			}
			if reflect.TypeOf(vm).Kind() == reflect.Map {
				vmm := vm.(map[string]string)
				mm = append(rr,vmm[f])
			}
		}
		if len(rr) > 0 {
			rule[f] = php.Impload("|",rr)
		}
		if len(mm) > 0 {
			mess[f] = php.Impload("|",mm)
		}
	}

	return Validate{
		Rule:rule,
		Messages:mess,
	}
}
func (b *BaseHandler) ValidateByConfig(data interface{},c map[string]string) error {
	v := b.GetValidateRule(c,b.V)
	b.V = v
	return b.Validate(data)
}

func (b *BaseHandler) Create(exist bool) error {
	err := b.Validate(nil)
	if err != nil {
		return err
	}

	req := b.Request
	bm := &model.Base{}
	bm.Request = req
	bm.Model = b.Object

	/*	if exist {
			info,err := bm.IsExist(false)
			if err != nil {
				return err
			}
			if info != nil {
				return errors.New(b.ObjName+"已存在")
			}
		}*/

	if err := bm.Create(); err != nil {
		return err
	}
	b.Data = bm.GetData()
	//发送成功消息
	//if err := h.Publisher.Publish(ctx, info); err != nil {
	//	return err
	//}

	return nil
}
func (b *BaseHandler) Update() error {
	bm := new(model.Base)
	bm.Request = b.Request
	bm.Model = b.Object

	id := b.GetId()
	if b.CheckById(id, false) == false {
		return errors.New(b.ObjName+"不存在")
	}

	err := bm.Update()
	if err != nil {
		return err
	}

	info,err := b.GetInfoById(id,false)
	if  err != nil {
		return err
	}
	b.Data = info

	return nil
}
func (b *BaseHandler) GetInfoById(id string,isOpen bool) (interface{},error) {
	data := make(map[string]interface{})
	data["id"] = id

	/*	s := b.Object
		err := utils.MapToStruct(data,s)
		if err != nil {
			return errors.New("取得详情请求错误：类型转化错误")
		}*/
	bm := new(model.Base)
	bm.Request = data
	bm.Model = b.Object

	err := bm.GetInfo(isOpen)
	if err != nil {
		return nil,err
	}
	i := bm.GetData()

	return i,nil
}
func (b *BaseHandler) CheckById(id string,isOpen bool) bool {
	i,err := b.GetInfoById(id,isOpen)
	if err != nil {
		return false
	}
	if i == nil {
		return false
	}
	return true
}
func (b *BaseHandler) DeleteById(id string) error {
	bm := new(model.Base)
	bm.Request = id
	bm.Model = b.Object

	if err := bm.Delete();err != nil {
		return err
	}

	return nil
}
func (b *BaseHandler) GetId() string {
	t := b.Request
	v := reflect.ValueOf(t)
	if v.Kind() == reflect.Ptr {
		v = v.Elem()
	}
	id := v.FieldByName("Id").String()

	return id
}

func (b *BaseHandler) GetList(list interface{}) error {
	bm := &model.Base{}
	bm.Model = b.Object
	bm.Request = b.Request
	bm.ListRequest = b.ListRequest


	if err := bm.GetList(list); err != nil {
		return err
	}

	return nil
}

func (b *BaseHandler) GetCount() (int64,error) {
	bm := &model.Base{}
	bm.Model = b.Object
	bm.Request = b.Request

	if err := bm.GetCount(); err != nil {
		return 0,err
	}
	var count *int64
	count = bm.GetData().(*int64)
	return *count,nil
}

func (b *BaseHandler) GetPage(page *proto.Page,list interface{}) error {
	count, err := b.GetCount()
	if  err != nil {
		return err
	}

	if count == 0{
		page = &proto.Page{}
		return nil
	}

	pd := b.GetPageRequestByRequest(b.PageRequest)
	l := b.GetListRequestByPageRequest(b.PageRequest)

	b.ListRequest = l
	err = b.GetList(list)
	if err != nil {
		return err
	}

	page.Total = count
	page.Index = pd.Page
	page.TotalPage = int64(math.Ceil(float64(count)/float64(pd.Size)))
	page.PageSize = pd.Size

	return nil
}

func (b *BaseHandler) GetListRequestByPageRequest(d *proto.PageRequest) *proto.ListRequest {
	l := &proto.ListRequest{}
	if d != nil {
		if d.Size <= 0 || d.Size > config.DefaultMaxPageSize{
			d.Size = config.PageSize
		}
		if d.Page <= 0 {
			d.Page = config.Page
		}

		l = &proto.ListRequest{
			Start:(d.Page-1)*d.Size,
			Limit:d.Size,
			Search:d.Search,
			Ids:d.Ids,
		}
	}else{
		l = &proto.ListRequest{
			Start:(config.Page-1)*config.PageSize,
			Limit:config.PageSize,
		}
	}
	return l
}

func (b *BaseHandler) GetPageRequestByRequest(d *proto.PageRequest) *proto.PageRequest {
	pr := &proto.PageRequest{}
	if d != nil {
		if d.Size <= 0 || d.Size > config.DefaultMaxPageSize{
			pr.Size = config.PageSize
		}
		if d.Page <= 0 {
			pr.Page = config.Page
		}
	}else{
		pr.Page = config.Page
		pr.Size = config.PageSize

	}
	return pr
}