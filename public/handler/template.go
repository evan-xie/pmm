package handler

import (
	"pmm/public/model"
)

type TemplateHandler struct {
	//logger *zap.Logger
	Model *model.Template
	BH BaseHandler
}

func NewTemplateHandler() *TemplateHandler{
	return &TemplateHandler{
		Model:&model.Template{},
		BH:BaseHandler{
			Object:&model.Template{},
			ObjName:"模板",
		},
	}
}

func (h *TemplateHandler) Create(data *model.Template) error {
	rule := map[string]string{
		"title": "required",
		"module": "required",
	}
	message := map[string]interface{}{
		"title" : "名称不能为空",
		"module" : "所属模型不能为空",
	}
	m := NewTemplateHandler()
	m.BH.V = Validate{
		Rule:rule,
		Messages:message,
	}
	if err := m.BH.Validate(*data);err != nil{
		return err
	}

	m.BH.Request = data
	err := m.BH.Create(false)
	if err != nil {
		return err
	}

	return nil
}

func (h *TemplateHandler) Update(data *model.Template) error {
	rule := map[string]string{
		"id": "required|regex:^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$",
		"title": "required",
		"module": "required",
	}
	message := map[string]interface{}{
		"id" : "编号不能为空|编号格式不合法",
		"title" : "名称不能为空",
		"module" : "所属模型不能为空",
	}
	m := NewTemplateHandler()
	m.BH.V = Validate{
		Rule:rule,
		Messages:message,
	}
	if err := m.BH.Validate(*data);err != nil{
		return err
	}

	//if err := m.BH.GetInfoById(data.Id, true);err != nil {
	//	return err
	//}

	m.BH.Request = data
	if err := m.BH.Update();err != nil {
		return err
	}

	data = m.BH.Data.(*model.Template)

	return nil
}

