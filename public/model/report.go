package model

import (
	"github.com/jinzhu/gorm"
	"pmm/public/config"
	"pmm/public/proto/report"
	"time"
	"github.com/satori/go.uuid"
	"log"
)

// 设置的表名
func (Report) TableName() string {
	return "report"
}

type Report struct {
	report.Report
	Comm
}

type Reports []*report.Report

func InitReportModel() *Base {
	return &Base{
		Model:&Report{},
	}
}


func (m *Report) BeforeCreate(scope *gorm.Scope) error {
	uid, err := uuid.NewV4()
	if err != nil {
		log.Fatalf("created uuid error: %v\n", err)
	}
	scope.SetColumn("Id", uid.String())
	scope.SetColumn("CreatedAt", time.Now().Format("2006-01-02 15:04:05"))
	scope.SetColumn("UpdatedAt", time.Now().Format("2006-01-02 15:04:05"))
	scope.SetColumn("Status", config.OPEN_STATUS)
	return nil
}


func (m *Report) BeforeUpdate(scope *gorm.Scope) error {
	scope.SetColumn("UpdatedAt", time.Now().Format("2006-01-02 15:04:05"))
	return nil
}

