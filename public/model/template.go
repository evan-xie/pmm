package model

import (
	"github.com/jinzhu/gorm"
	"pmm/public/config"
	"pmm/public/proto/template"
	"time"
	"github.com/satori/go.uuid"
	"log"
)

// 设置的表名
func (Template) TableName() string {
	return "template"
}

type Template struct {
	template.Template
	Comm
}

type Templates []*Template

func (m *Template) BeforeCreate(scope *gorm.Scope) error {
	uid, err := uuid.NewV4()
	if err != nil {
		log.Fatalf("created uuid error: %v\n", err)
	}
	scope.SetColumn("Id", uid.String())
	scope.SetColumn("CreatedAt", time.Now().Format("2006-01-02 15:04:05"))
	scope.SetColumn("UpdatedAt", time.Now().Format("2006-01-02 15:04:05"))
	scope.SetColumn("Status", config.OPEN_STATUS)
	return nil
}


