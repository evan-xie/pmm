package model

import (
	"errors"
	"github.com/jinzhu/gorm"
	"pmm/public/config"
	"pmm/public/proto"
	"pmm/public/utils"
	"reflect"
	"time"
)

type Base struct {
	Model interface{}
	Request interface{}
	ListRequest *proto.ListRequest
	ListResponse interface{}
	Data interface{}
	Fields []string
	Args  []interface{}
}

var Conn *gorm.DB

func SetConn(db *gorm.DB) {
	Conn = db
}

type Comm struct {
	DeletedAt *time.Time
}

func RegisterModel(model interface{}) {
	// 自动检查 表结构是否变化
	Conn.AutoMigrate(model)
}

func (b *Base) Create(where ...interface{}) error {
	query := Conn
	if where != nil {
		for _, value := range where {
			query = query.Where(value)
		}
	}

	db := query.Create(b.Request)
	if err := db.Error; err != nil {
		return err
	}
	return nil
}

func (b *Base) Update(condition... string) error {
	query := Conn.Model(b.Model)
	//过滤参数
	if len(condition) >= 1 {
		query = query.Omit(condition...)
	}

	if err := query.Updates(b.Request).Error;err != nil {
		return err
	}
	return nil
}

/**
删除
参数可以为[]string{"id","id"},string
 */
func (b *Base) Delete() error {
	if b.Request == nil {
		return errors.New("删除条件不能为空")
	}

	v := reflect.ValueOf(b.Request)
	t := v.Kind()

	query := Conn.Model(b.Model)
	switch t {
	case reflect.Slice:
		id := b.Request.([]string)
		query = query.Where(id)
		break
	case reflect.String:
		id := b.Request.(string)
		query = query.Where("id = '" + id +"'")
		break
	}

	res := query.Delete(b.Model)
	if err := res.Error; err != nil {
		return err
	}
	return nil
}

func (b *Base) GetInfo(isOpen bool) error {
	if b.Request == nil {
		return nil
	}

	query := Conn.Where(b.Request)
	if isOpen {
		query = query.Where("status = ?",config.OPEN_STATUS)
	}

	res := query.First(b.Model)
	if err := res.Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			b.Data = nil
		}
		return nil
	}
	b.Data = res.Value
	return nil
}

func (b *Base) IsExist(isOpen bool) (interface{},error) {
	data := b.Request
	if data == nil {
		return nil,errors.New("查询条件不能为空")
	}

	query := Conn.Where(data)
	if isOpen {
		query = query.Where("status = ?",config.OPEN_STATUS)
	}

	res := query.First(b.Model)
	if err := res.Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil,nil
		}
		return nil,err
	}
	b.Data = res.Value
	b.Data = res.Value
	return b.Data,nil
}

/**
var list []*node.Node
GetList(&list)
 */
func (b *Base) GetList(list interface{}) error {
	model := b.Model
	query := Conn.Model(model)

	if b.Request != nil {
		query = query.Where(b.Request)
	}

	//取得字段 过滤参数
	fs := utils.GetDbNameByStruct(Conn,model)
	if len(fs) > 1 {
		query = query.Select(fs)
	}

	//params := utils.StructToMap(b.Request,true)
	//lr := params["default"].(*proto.ListRequest)
	//err := utils.MapToStruct(d,lr)
	//if err != nil {
	//	return errors.New("取得详情请求错误：类型转化错误")
	//}
	lr := b.ListRequest

	if lr != nil {
		if lr.Start > 0 {
			query = query.Offset(lr.Start)
		}
		if lr.Limit > 0 {
			if lr.Limit > config.DefaultMaxPageSize {
				lr.Limit = config.DefaultMaxPageSize
				query = query.Limit(lr.Limit)
			}else {
				query = query.Limit(lr.Limit)
			}
		}
		if lr.Search != nil {
			query = utils.AndOrLikes(query,lr.Search,true)
		}
		if len(lr.Ids) > 0{
			query = query.Where(lr.Ids)
		}
	}

	args := b.Args
	if args != nil && len(args) > 0 {
		for _, value := range args {
			query = query.Where(value)
		}
	}

	re := query.Scan(list)
	if err:= re.Error;err != nil {
		return err
	}

	return nil
}

/**
返回查询个数
 */
func (b *Base) GetCount() error {
	data := b.Request

	query := Conn.Model(b.Model)

	if b.Data != nil {
		query = query.Where(data)
	}

	lr := b.ListRequest
	if lr != nil {
		if lr.Search != nil {
			query = utils.AndOrLikes(query,lr.Search,true)
		}
		if len(lr.Ids) > 0{
			query = query.Where(lr.Ids)
		}
	}

	args := b.Args
	if args != nil && len(args) > 0 {
		for _, value := range args {
			query = query.Where(value)
		}
	}
	var count *int64
	err := query.Count(&count).Error
	if err != nil {
		return err
	}

	b.Data = count
	return nil
}

func (b *Base) GetData() interface{} {
	return b.Data
}