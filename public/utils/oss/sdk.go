package oss

import (
	"errors"
	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"pmm/public/config"
)

func Init() (*oss.Bucket, error) {
	client, err := oss.New(config.Endpoint, config.AccessKeyId, config.AccessKeySecret)
	if err != nil {
		return nil,errors.New("OSS服务异常:"+err.Error())
	}
	// 获取存储空间。
	bucket, err := client.Bucket(config.BucketName)
	if err != nil {
		return nil,errors.New("OSS服务异常:"+err.Error())
	}
	return bucket,nil
}