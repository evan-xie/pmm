package tree

import "pmm/public/pb/tag"

type NormalTree interface {
	GetChildren() []interface{}
	AddChild(map[string]interface{}, func(map[string]interface{}) interface{}) interface{}
	GetName() string
}

func CreateByPath(tree NormalTree, path []string, createFunc func(map[string]interface{}) interface{}) NormalTree {
	for _, v := range path {
		index := -1
		children := tree.GetChildren()
		for k1, v1 := range children {
			vData := v1.(NormalTree)
			if vData.GetName() == v {
				index = k1
				break
			}
		}

		if -1 == index {
			index = len(children)
			tree.AddChild(map[string]interface{}{"value": v}, createFunc)
			children = tree.GetChildren()
		}

		tree = children[index].(NormalTree)
	}

	return tree
}


type Node struct{
	Name string
	Children []Node
	CalcRule string // 自定义属性
}

// 满足接口的函数：获取到子节点数组，返回的数组元素必须要实现NormalTree的接口
// 因为children数组的元素是Node，而*Node才实现了NormalTree接口，所以需要将[]Node转换为[]*Node
func (self *Node) GetChildren() []interface{} {
	if nil == self.Children || 0 == len(self.Children) {
		return nil
	}

	arr := make([]interface{}, len(self.Children))
	for k := range self.Children {
		arr[k] = &self.Children[k]
	}

	return arr
}

// 满足接口的函数：增加子节点，其中使用了自定义的create函数。返回结果是满足NormalTree接口的子节点
func (self *Node) AddChild(node map[string]interface{}, create func(map[string]interface{}) interface{}) interface{} {
	child := create(node).(Node)
	self.Children = append(self.Children, child)

	return &self.Children[len(self.Children)-1]
}

// 满足接口的函数：获取节点名字
func (self *Node) GetName() string {
	return self.Name
}

// 在AddChild中使用的create函数，参数data为固定的map:{"value": 节点名称}。返回结果是新的节点
func (self *Node) CreateNode(data map[string]interface{}) interface{} {
	node := Node{
		Name:    data["value"].(string),
	}

	return node
}

func tree(s struct{}, list interface{},mm map[string]interface{}) interface{}{
	if list != nil {
		return nil
	}

	var m []map[string]interface{}
	for _,v :=range m {
		if v["id"] == "" {

		}
	}
	mm := make(map[string]interface{})

}

func ft(list *[]tag.Tag,pid string){

}