package log

import (
	"go.uber.org/zap"
)

var Logger *zap.Logger

// Instance 唯一实例
func Instance() *zap.Logger {
	return Logger
}


// Init 初始化,srvName 生成的日志文件夹名字
//func Init(srvName string) *zap.Logger {
//	instance = NewLogger(srvName)
//	return instance
//}

// NewLogger 新建日志
//func NewLogger(srvName string) *zap.Logger {
//
//	directory := "/home/ricoder/gopath/src/mewe_job/GoMicroDemo/"
//	if len(directory) == 0 {
//		directory = path.Join("..", "log", srvName)
//	} else {
//		directory = path.Join(directory, "log", srvName)
//	}
//	writers := []zapcore.WriteSyncer{newRollingFile(directory)}
//	writers = append(writers, os.Stdout)
//	logger, _ := newZapLogger(true, zapcore.NewMultiWriteSyncer(writers...))
//	zap.RedirectStdLog(logger)
//
//	/*updateLogLevel( serviceName, dyn, isProduction)
//	go func() {
//		ticker := time.NewTicker(30 * time.Second)
//		for range ticker.C {
//			updateLogLevel( serviceName, dyn, isProduction)
//		}
//	}()*/
//
//	return logger
//}