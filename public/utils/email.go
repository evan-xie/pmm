package utils

import (
	"errors"
	"net/smtp"
	"strings"
)

const (
	host = "smtp.mxhichina.com:25"
	charset = "UTF-8"
	user = "it@greenment.cn"
	pwd = "Greenment150605"
)

func SendMail(to,subject, body, mailtype string) error{
	err := SendToMail(user, pwd, host, to, subject, body, mailtype)
	if err != nil {
		return errors.New("邮件发送失败,请稍受重试")
	}
	return nil
}

func SendToMail(user, password, host, to, subject, body, mailtype string) error {
	hp := strings.Split(host, ":")
	auth := smtp.PlainAuth("", user, password, hp[0])
	var content_type string
	if mailtype == "html" {
		content_type = "Content-Type: text/" + mailtype + "; charset="+charset
	} else {
		content_type = "Content-Type: text/plain" + "; charset="+charset
	}

	msg := []byte("To: " + to + "\r\nFrom: " + user + ">\r\nSubject: " + subject +"\r\n" + content_type + "\r\n\r\n" + body)
	send_to := strings.Split(to, ";")
	err := smtp.SendMail(host, auth, user, send_to, msg)
	return err
}
