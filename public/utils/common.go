package utils

import (
	"encoding/json"
	"errors"
	"github.com/jinzhu/gorm"
	"crypto/md5"
	"encoding/hex"
	"net/http"
	usconf "pmm/user-srv/config"
	"reflect"
	"fmt"
	"php"
	"github.com/asaskevich/govalidator"
	"strconv"
	"strings"
	"time"
)

func AndOrs(query *gorm.DB, m map[string]interface{},searchEmpty bool) (*gorm.DB) {
	if m == nil {
		return query
	}

	str := ""
	args := make([]interface{},0,len(m))

	for k,v := range m {
		if searchEmpty && (v == nil || v == "") {
			continue
		}
		str = str + k + " = ? or "
		args = append(args,v)
	}

	str = string([]rune(str)[:len(str)-4])

	return query.Where(str,args...)
}

func AndOrLikes(query *gorm.DB, m map[string]string, searchEmpty bool) (*gorm.DB) {
	if m == nil {
		return query
	}

	str := ""
	args := make([]interface{},0,len(m))

	for k,v := range m {
		if searchEmpty &&  v == "" {
			continue
		}
		str = str + k + " like ? or "
		args = append(args,"%"+v+"%")
	}

	str = string([]rune(str)[:len(str)-4])

	return query.Where(str,args...)
}

func MapStrToMapInterface(m map[string]string) map[string]interface{}  {
	mi := make(map[string]interface{})
	for key, value := range m {
		mi[key] = value
	}
	return  mi
}

/**
md5加密
 */
func Md5(str string) string  {
	h := md5.New()
	h.Write([]byte(str))
	return hex.EncodeToString(h.Sum(nil))
}

/**
struct 根据tag的某个属性作为键 转换成 map
 */
func StructToMapByTag(addr interface{},in interface{},tag string) map[string]interface{} {
	t := reflect.TypeOf(addr)
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	if t.Kind() != reflect.Struct {
		return nil
	}

	fn := t.NumField()
	m := make(map[string]interface{})
	for i := 0; i < fn; i++ {
		f := t.Field(i)
		tag := f.Tag.Get(tag)
		name := string(f.Name)
		if tag == "" {
			continue
		}
		val := GetStructValByName(in,name)
		m[tag] = val
	}
	return m
}

func MapMerge(){

}

func GetStructName(obj interface{}) string {
	if obj == nil {
		return ""
	}
	obj1 := reflect.TypeOf(obj)
	if obj1.Kind() == reflect.Ptr {
		obj1 = obj1.Elem()
	}
	x := php.Explode(".",obj1.String())
	return string(x[1])
}

/**
	obj传入struct示例
	lower 是否转为小写
	ex 例外过滤数据
 */
func StructToMap(obj interface{},lower bool,ex ...string) map[string]interface{}{
	if obj == nil {
		return nil
	}
	args := []string{"xxx_no_unkeyed_literal","xxx_unrecognized","xxx_sizecache","xxx_nounkeyedliteral","comm","base"}
	args = append(args,ex...)
	objName := GetStructName(obj)

	obj1 := reflect.TypeOf(obj)
	obj2 := reflect.ValueOf(obj)


	if obj2.Kind() == reflect.Ptr {
		obj1 = obj1.Elem()
		obj2 = obj2.Elem()
	}


	var data = make(map[string]interface{})
	for i := 0; i < obj1.NumField(); i++ {
		oo := obj1.Field(i)
		name := oo.Name
		if lower {
			name = strings.ToLower(name)
		}
		if govalidator.IsIn(name, args...) || name == objName {
			continue
		}

		if oo.Type.Kind() == reflect.Struct {
			o := obj2.Field(i).Interface()
			d := StructToMap(o,true)
			for key, value := range d {
				data[key] = value
			}
		}
		data[name] = obj2.Field(i).Interface()
	}
	return data
}

func SmartPrint(in interface{}){
	var kv = make(map[string]interface{})
	vValue := reflect.ValueOf(in)
	vType :=reflect.TypeOf(in)
	for i:=0;i<vValue.NumField();i++{
		kv[vType.Field(i).Name] = vValue.Field(i)
	}
	fmt.Println("获取到数据:")
	for k,v :=range kv{
		fmt.Print(k)
		fmt.Print(":")
		fmt.Print(v)
		fmt.Println()
	}
}
/**
struct 根据tag的某个属性作为键 转换成 map 以来gorm
 */
func StructToMapByGorm(db *gorm.DB,st interface{},in interface{}) map[string]interface{} {
	if in == nil {
		return nil
	}

	fs := db.NewScope(st).GetStructFields()
	m := make(map[string]string)

	for _,f :=range fs{
		m[f.Name] = f.DBName
	}

	var kv = make(map[string]interface{})
	vValue := reflect.ValueOf(in)
	vType :=reflect.TypeOf(in)
	for i:=0;i<vValue.NumField();i++{
		dbName := m[vType.Field(i).Name]
		val := vValue.Field(i)
		kv[dbName] = val
	}

	return kv
}

func GetStructJsonFields(s interface{}) []string {
	t := reflect.TypeOf(s)
	fs := t.Elem()
	var a []string
	for i := 0; i < fs.NumField(); i++ {
		f := fs.Field(i)
		tag := f.Tag.Get("json")
		if tag == "" || tag == "-" {
			continue
		}
		tag = strings.Replace(tag,",omitempty","",-1)
		a = append(a, tag)
	}
	return a
}

func GetStructFields(s interface{}) []string {
	t := reflect.TypeOf(s)
	fs := t.Elem()
	var a []string
	for i := 0; i < fs.NumField(); i++ {
		f := fs.Field(i).Name
		a = append(a, f)
	}
	return a
}

func GetStructFieldsMap (s interface{}) map[string]string {
	t := reflect.TypeOf(s)
	fs := t.Elem()
	m := map[string]string{}
	for i := 0; i < fs.NumField(); i++ {
		name := fs.Field(i).Name
		if name == "" {
			continue
		}
		tag := fs.Field(i).Tag.Get("json")
		if tag == "" || tag == "-" {
			continue
		}
		tag = strings.Replace(tag,",omitempty","",-1)
		m[name] = tag
	}
	return m
}

/**
四舍五入取2位小说
 */
func Decimal(value float64) float64 {
	value, _ = strconv.ParseFloat(fmt.Sprintf("%.2f", value), 64)
	return value
}

/**
取得struct的数据库字段表
 */
func GetDbNameByStruct(db *gorm.DB, st interface{},ex... interface{}) []string {
	if st == nil {
		return nil
	}
	args := []interface{}{"xxx_no_unkeyed_literal","xxx_unrecognized","xxx_sizecache","model","list_default"}
	args = append(args,ex...)

	fs := db.NewScope(st).GetStructFields()
	ms := make([]string,0,len(fs))
	for _,f :=range fs{
		if php.InArray(f.DBName, args...) {
			continue
		}
		ms = append(ms, f.DBName)
	}
	return ms
}

/**
根据struct 的Name取得相应的值
 */
func GetStructValByName(in interface{}, name string) interface{} {
	v := reflect.ValueOf(in)
	val := v.FieldByName(name)
	return val
}


func Struct2Map(obj interface{}) map[string]interface{} {
	t := reflect.TypeOf(obj)
	v := reflect.ValueOf(obj)

	var data = make(map[string]interface{})
	for i := 0; i < t.NumField(); i++ {
		data[strings.ToLower(t.Field(i).Name)] = v.Field(i).Interface()
	}
	return data
}

func MapToWhereStr(m map[string]string) string {
	str := ""
	for k,v := range m{
		str = str + k + " like '%"+v+"%' and "
	}
	str = string([]rune(str)[:len(str)-4])

	return str
}

/**
取得树形结构图
 */
func tree(list []map[string]string, pid string,key string,pidName string) []interface{} {
	var arr []interface{}
	for _,v := range list{
		if v[pidName] != pid {
			continue
		}
		c := tree(list,v[key],key,pidName)
		vv := map[string]interface{}{
			key:v[key],
			pidName:v[pidName],
			"child":c,
		}
		arr = append(arr,vv)
	}
	return arr
}


func child(list []map[string]string,pid string, key string,pidName string) ([]map[string]interface{},[]string) {
	var arr []map[string]interface{}
	var in []string
	for _,v := range list{
		if v[pidName] != pid {
			continue
		}
		in = append(in,v[key])
		c,i := child(list,v[key],key,pidName)
		vv := map[string]interface{}{
			key:v[key],
			pidName:v[pidName],
			"child":c,
		}
		arr = append(arr,vv)
		in = append(in,i...)
	}
	return arr,in
}

/**
取得树形结构 无第一级目录情况下
 */
func treeNoPid(list []map[string]string, key string,pidName string) []interface{} {
	var arr []interface{}
	var in []string
	for _,v := range list{
		if in != nil && govalidator.IsIn(v[key], in...) {
			continue
		}
		vv,i := child(list,v[key],key,pidName)
		in = append(in,v[key])
		vs := map[string]interface{}{
			key:v[key],
			pidName:v[pidName],
			"child":vv,
		}
		in = append(in,i...)
		arr = append(arr,vs)
	}
	return arr
}

/*获取小程序 access_token*/

func GetWxAccessToken() (string, error) {
	appID         := usconf.AppID
	secret        := usconf.AppSecret
	AccessTokenURL := usconf.AccessTokenURL
	AccessTokenURL  = strings.Replace(AccessTokenURL, "APPID",  appID,  -1)
	AccessTokenURL  = strings.Replace(AccessTokenURL, "SECRET", secret, -1)

	resp, err := http.Get(AccessTokenURL)
	if err != nil {
		fmt.Println(err.Error())
		return "",errors.New("取得access-token失败")
	}

	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return "",errors.New("取得access-token失败")
	}

	var data map[string]interface{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return "",errors.New("取得access-token失败")
	}

	if data["errcode"] != nil {
		return "",errors.New("取得access-token失败")
	}

	at := data["access_token"].(string)
	if govalidator.IsNull(at) {
		return "",errors.New("取得access-token失败")
	}

	return at,nil
}

//用map填充结构
func MapToStruct(data map[string]interface{}, obj interface{}) error {
	for k, v := range data {
		err := SetField(obj, k, v)
		if err != nil {
			return err
		}
	}
	return nil
}

//用map的值替换结构的值
func SetField(obj interface{}, name string, value interface{}) error {
	structValue := reflect.ValueOf(obj).Elem()        //结构体属性值
	structFieldValue := structValue.FieldByName(name) //结构体单个属性值

	if !structFieldValue.IsValid() {
		return fmt.Errorf("No such field: %s in obj", name)
	}

	if !structFieldValue.CanSet() {
		return fmt.Errorf("Cannot set %s field value", name)
	}

	structFieldType := structFieldValue.Type() //结构体的类型
	val := reflect.ValueOf(value)              //map值的反射值

	var err error
	if structFieldType != val.Type() {
		val, err = TypeConversion(fmt.Sprintf("%v", value), structFieldValue.Type().Name()) //类型转换
		if err != nil {
			return err
		}
	}

	structFieldValue.Set(val)
	return nil
}

//类型转换
func TypeConversion(value string, ntype string) (reflect.Value, error) {
	if ntype == "string" {
		return reflect.ValueOf(value), nil
	} else if ntype == "time.Time" {
		t, err := time.ParseInLocation("2006-01-02 15:04:05", value, time.Local)
		return reflect.ValueOf(t), err
	} else if ntype == "Time" {
		t, err := time.ParseInLocation("2006-01-02 15:04:05", value, time.Local)
		return reflect.ValueOf(t), err
	} else if ntype == "int" {
		i, err := strconv.Atoi(value)
		return reflect.ValueOf(i), err
	} else if ntype == "int8" {
		i, err := strconv.ParseInt(value, 10, 64)
		return reflect.ValueOf(int8(i)), err
	} else if ntype == "int32" {
		i, err := strconv.ParseInt(value, 10, 64)
		return reflect.ValueOf(int64(i)), err
	} else if ntype == "int64" {
		i, err := strconv.ParseInt(value, 10, 64)
		return reflect.ValueOf(i), err
	} else if ntype == "float32" {
		i, err := strconv.ParseFloat(value, 64)
		return reflect.ValueOf(float32(i)), err
	} else if ntype == "float64" {
		i, err := strconv.ParseFloat(value, 64)
		return reflect.ValueOf(i), err
	}

	//else if .......增加其他一些类型的转换

	return reflect.ValueOf(value), errors.New("未知的类型：" + ntype)
}