/*
必须配合 	github.com/asaskevich/govalidator 使用
 */

package validate

import (
	"regexp"
	)

type checkStringFunc func(str string) bool
type checkStringsFunc func(str string,in... string) bool


//值不为空 然后验证
func HasAndCheckString(fun checkStringFunc,str string) bool  {
	if str == "" {
		return true
	}

	return 	fun(str)
}

func HasAndCheckStrings(fun checkStringsFunc,str string,args... string) bool {
	if str == "" {
		return true
	}

	return fun(str,args...)
}

//值不能空 然后验证
func MustAndCheckString(fun checkStringFunc,str string) bool{
	if str == "" {
		return false
	}

	return 	fun(str)
}
func MustAndCheckStrings(fun checkStringsFunc,str string,args... string) bool {
	if str == "" {
		return false
	}

	return fun(str,args...)
}
/*
判断是否为手机号码
 */
func IsMobile(str string) bool {
	reg := `^((13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])+\d{8})$`
	rex := regexp.MustCompile(reg)

	return rex.MatchString(str)
}