package utils

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/asaskevich/govalidator"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

const SEND_ONE_URL  = "http://sms-api.luosimao.com/v1/send.json"

const key = "key-48e08721590a231b7d2c60de268404dc"

func SendSMS(mobile string,msg string) error{

	if govalidator.IsNull(mobile) || govalidator.IsNull(msg) {
		return errors.New("手机号码或内容不能为空")
	}
	client := &http.Client{}

	req, err := http.NewRequest("POST", SEND_ONE_URL, strings.NewReader("mobile="+mobile+"&message="+msg))
	if err != nil {
		return errors.New("短信发送失败,请稍受重试")
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("HTTPAUTH", "name=anny")
	req.SetBasicAuth("api",key)
	resp, err := client.Do(req)

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		// handle error
		return errors.New("短信发送失败,请稍受重试")
	}
	var m map[string]string
	json.Unmarshal(body,&m)
	fmt.Println(m["error"])
	code,_ := strconv.Atoi(m["error"])
	if code < 0 {
		return errors.New("短信发送失败,请稍受重试")
	}
	return nil
}