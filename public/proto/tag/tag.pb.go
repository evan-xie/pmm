// Code generated by protoc-gen-go. DO NOT EDIT.
// source: proto/tag/tag.proto

package tag // import "pmm/public/proto/tag"

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"
import proto1 "pmm/public/proto"

import (
	client "github.com/micro/go-micro/client"
	server "github.com/micro/go-micro/server"
	context "golang.org/x/net/context"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type Tag struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	TagGroup             string   `protobuf:"bytes,3,opt,name=tag_group,json=tagGroup,proto3" json:"tag_group,omitempty"`
	Pid                  string   `protobuf:"bytes,4,opt,name=pid,proto3" json:"pid,omitempty"`
	Type                 string   `protobuf:"bytes,6,opt,name=type,proto3" json:"type,omitempty"`
	Status               int64    `protobuf:"zigzag64,7,opt,name=status,proto3" json:"status,omitempty"`
	CreatedAt            string   `protobuf:"bytes,8,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt            string   `protobuf:"bytes,9,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Tag) Reset()         { *m = Tag{} }
func (m *Tag) String() string { return proto.CompactTextString(m) }
func (*Tag) ProtoMessage()    {}
func (*Tag) Descriptor() ([]byte, []int) {
	return fileDescriptor_tag_7b90513173513c6c, []int{0}
}
func (m *Tag) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Tag.Unmarshal(m, b)
}
func (m *Tag) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Tag.Marshal(b, m, deterministic)
}
func (dst *Tag) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Tag.Merge(dst, src)
}
func (m *Tag) XXX_Size() int {
	return xxx_messageInfo_Tag.Size(m)
}
func (m *Tag) XXX_DiscardUnknown() {
	xxx_messageInfo_Tag.DiscardUnknown(m)
}

var xxx_messageInfo_Tag proto.InternalMessageInfo

func (m *Tag) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *Tag) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *Tag) GetTagGroup() string {
	if m != nil {
		return m.TagGroup
	}
	return ""
}

func (m *Tag) GetPid() string {
	if m != nil {
		return m.Pid
	}
	return ""
}

func (m *Tag) GetType() string {
	if m != nil {
		return m.Type
	}
	return ""
}

func (m *Tag) GetStatus() int64 {
	if m != nil {
		return m.Status
	}
	return 0
}

func (m *Tag) GetCreatedAt() string {
	if m != nil {
		return m.CreatedAt
	}
	return ""
}

func (m *Tag) GetUpdatedAt() string {
	if m != nil {
		return m.UpdatedAt
	}
	return ""
}

type Request struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Request) Reset()         { *m = Request{} }
func (m *Request) String() string { return proto.CompactTextString(m) }
func (*Request) ProtoMessage()    {}
func (*Request) Descriptor() ([]byte, []int) {
	return fileDescriptor_tag_7b90513173513c6c, []int{1}
}
func (m *Request) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Request.Unmarshal(m, b)
}
func (m *Request) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Request.Marshal(b, m, deterministic)
}
func (dst *Request) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Request.Merge(dst, src)
}
func (m *Request) XXX_Size() int {
	return xxx_messageInfo_Request.Size(m)
}
func (m *Request) XXX_DiscardUnknown() {
	xxx_messageInfo_Request.DiscardUnknown(m)
}

var xxx_messageInfo_Request proto.InternalMessageInfo

type Response struct {
	Tag                  *Tag     `protobuf:"bytes,1,opt,name=tag,proto3" json:"tag,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Response) Reset()         { *m = Response{} }
func (m *Response) String() string { return proto.CompactTextString(m) }
func (*Response) ProtoMessage()    {}
func (*Response) Descriptor() ([]byte, []int) {
	return fileDescriptor_tag_7b90513173513c6c, []int{2}
}
func (m *Response) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Response.Unmarshal(m, b)
}
func (m *Response) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Response.Marshal(b, m, deterministic)
}
func (dst *Response) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Response.Merge(dst, src)
}
func (m *Response) XXX_Size() int {
	return xxx_messageInfo_Response.Size(m)
}
func (m *Response) XXX_DiscardUnknown() {
	xxx_messageInfo_Response.DiscardUnknown(m)
}

var xxx_messageInfo_Response proto.InternalMessageInfo

func (m *Response) GetTag() *Tag {
	if m != nil {
		return m.Tag
	}
	return nil
}

type DelResponse struct {
	Success              bool     `protobuf:"varint,1,opt,name=success,proto3" json:"success,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *DelResponse) Reset()         { *m = DelResponse{} }
func (m *DelResponse) String() string { return proto.CompactTextString(m) }
func (*DelResponse) ProtoMessage()    {}
func (*DelResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_tag_7b90513173513c6c, []int{3}
}
func (m *DelResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_DelResponse.Unmarshal(m, b)
}
func (m *DelResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_DelResponse.Marshal(b, m, deterministic)
}
func (dst *DelResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_DelResponse.Merge(dst, src)
}
func (m *DelResponse) XXX_Size() int {
	return xxx_messageInfo_DelResponse.Size(m)
}
func (m *DelResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_DelResponse.DiscardUnknown(m)
}

var xxx_messageInfo_DelResponse proto.InternalMessageInfo

func (m *DelResponse) GetSuccess() bool {
	if m != nil {
		return m.Success
	}
	return false
}

type TreeRequest struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	TagGroup             string   `protobuf:"bytes,2,opt,name=tag_group,json=tagGroup,proto3" json:"tag_group,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *TreeRequest) Reset()         { *m = TreeRequest{} }
func (m *TreeRequest) String() string { return proto.CompactTextString(m) }
func (*TreeRequest) ProtoMessage()    {}
func (*TreeRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_tag_7b90513173513c6c, []int{4}
}
func (m *TreeRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_TreeRequest.Unmarshal(m, b)
}
func (m *TreeRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_TreeRequest.Marshal(b, m, deterministic)
}
func (dst *TreeRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_TreeRequest.Merge(dst, src)
}
func (m *TreeRequest) XXX_Size() int {
	return xxx_messageInfo_TreeRequest.Size(m)
}
func (m *TreeRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_TreeRequest.DiscardUnknown(m)
}

var xxx_messageInfo_TreeRequest proto.InternalMessageInfo

func (m *TreeRequest) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *TreeRequest) GetTagGroup() string {
	if m != nil {
		return m.TagGroup
	}
	return ""
}

type TagTree struct {
	Tag                  *Tag       `protobuf:"bytes,1,opt,name=tag,proto3" json:"tag,omitempty"`
	Child                []*TagTree `protobuf:"bytes,2,rep,name=child,proto3" json:"child,omitempty"`
	XXX_NoUnkeyedLiteral struct{}   `json:"-"`
	XXX_unrecognized     []byte     `json:"-"`
	XXX_sizecache        int32      `json:"-"`
}

func (m *TagTree) Reset()         { *m = TagTree{} }
func (m *TagTree) String() string { return proto.CompactTextString(m) }
func (*TagTree) ProtoMessage()    {}
func (*TagTree) Descriptor() ([]byte, []int) {
	return fileDescriptor_tag_7b90513173513c6c, []int{5}
}
func (m *TagTree) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_TagTree.Unmarshal(m, b)
}
func (m *TagTree) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_TagTree.Marshal(b, m, deterministic)
}
func (dst *TagTree) XXX_Merge(src proto.Message) {
	xxx_messageInfo_TagTree.Merge(dst, src)
}
func (m *TagTree) XXX_Size() int {
	return xxx_messageInfo_TagTree.Size(m)
}
func (m *TagTree) XXX_DiscardUnknown() {
	xxx_messageInfo_TagTree.DiscardUnknown(m)
}

var xxx_messageInfo_TagTree proto.InternalMessageInfo

func (m *TagTree) GetTag() *Tag {
	if m != nil {
		return m.Tag
	}
	return nil
}

func (m *TagTree) GetChild() []*TagTree {
	if m != nil {
		return m.Child
	}
	return nil
}

type TreeResponse struct {
	Trees                []*TagTree `protobuf:"bytes,1,rep,name=trees,proto3" json:"trees,omitempty"`
	XXX_NoUnkeyedLiteral struct{}   `json:"-"`
	XXX_unrecognized     []byte     `json:"-"`
	XXX_sizecache        int32      `json:"-"`
}

func (m *TreeResponse) Reset()         { *m = TreeResponse{} }
func (m *TreeResponse) String() string { return proto.CompactTextString(m) }
func (*TreeResponse) ProtoMessage()    {}
func (*TreeResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_tag_7b90513173513c6c, []int{6}
}
func (m *TreeResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_TreeResponse.Unmarshal(m, b)
}
func (m *TreeResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_TreeResponse.Marshal(b, m, deterministic)
}
func (dst *TreeResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_TreeResponse.Merge(dst, src)
}
func (m *TreeResponse) XXX_Size() int {
	return xxx_messageInfo_TreeResponse.Size(m)
}
func (m *TreeResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_TreeResponse.DiscardUnknown(m)
}

var xxx_messageInfo_TreeResponse proto.InternalMessageInfo

func (m *TreeResponse) GetTrees() []*TagTree {
	if m != nil {
		return m.Trees
	}
	return nil
}

// 取得列表的请求参数
type ListRequest struct {
	Default              *proto1.ListRequest `protobuf:"bytes,1,opt,name=default,proto3" json:"default,omitempty"`
	Tag                  *Tag                `protobuf:"bytes,2,opt,name=tag,proto3" json:"tag,omitempty"`
	XXX_NoUnkeyedLiteral struct{}            `json:"-"`
	XXX_unrecognized     []byte              `json:"-"`
	XXX_sizecache        int32               `json:"-"`
}

func (m *ListRequest) Reset()         { *m = ListRequest{} }
func (m *ListRequest) String() string { return proto.CompactTextString(m) }
func (*ListRequest) ProtoMessage()    {}
func (*ListRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_tag_7b90513173513c6c, []int{7}
}
func (m *ListRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ListRequest.Unmarshal(m, b)
}
func (m *ListRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ListRequest.Marshal(b, m, deterministic)
}
func (dst *ListRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ListRequest.Merge(dst, src)
}
func (m *ListRequest) XXX_Size() int {
	return xxx_messageInfo_ListRequest.Size(m)
}
func (m *ListRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_ListRequest.DiscardUnknown(m)
}

var xxx_messageInfo_ListRequest proto.InternalMessageInfo

func (m *ListRequest) GetDefault() *proto1.ListRequest {
	if m != nil {
		return m.Default
	}
	return nil
}

func (m *ListRequest) GetTag() *Tag {
	if m != nil {
		return m.Tag
	}
	return nil
}

type ListResponse struct {
	List                 []*Tag   `protobuf:"bytes,1,rep,name=list,proto3" json:"list,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ListResponse) Reset()         { *m = ListResponse{} }
func (m *ListResponse) String() string { return proto.CompactTextString(m) }
func (*ListResponse) ProtoMessage()    {}
func (*ListResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_tag_7b90513173513c6c, []int{8}
}
func (m *ListResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ListResponse.Unmarshal(m, b)
}
func (m *ListResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ListResponse.Marshal(b, m, deterministic)
}
func (dst *ListResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ListResponse.Merge(dst, src)
}
func (m *ListResponse) XXX_Size() int {
	return xxx_messageInfo_ListResponse.Size(m)
}
func (m *ListResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_ListResponse.DiscardUnknown(m)
}

var xxx_messageInfo_ListResponse proto.InternalMessageInfo

func (m *ListResponse) GetList() []*Tag {
	if m != nil {
		return m.List
	}
	return nil
}

// 取得单页的请求参数
type PageRequest struct {
	Default              *proto1.PageRequest `protobuf:"bytes,1,opt,name=default,proto3" json:"default,omitempty"`
	Tag                  *Tag                `protobuf:"bytes,2,opt,name=tag,proto3" json:"tag,omitempty"`
	XXX_NoUnkeyedLiteral struct{}            `json:"-"`
	XXX_unrecognized     []byte              `json:"-"`
	XXX_sizecache        int32               `json:"-"`
}

func (m *PageRequest) Reset()         { *m = PageRequest{} }
func (m *PageRequest) String() string { return proto.CompactTextString(m) }
func (*PageRequest) ProtoMessage()    {}
func (*PageRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_tag_7b90513173513c6c, []int{9}
}
func (m *PageRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_PageRequest.Unmarshal(m, b)
}
func (m *PageRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_PageRequest.Marshal(b, m, deterministic)
}
func (dst *PageRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_PageRequest.Merge(dst, src)
}
func (m *PageRequest) XXX_Size() int {
	return xxx_messageInfo_PageRequest.Size(m)
}
func (m *PageRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_PageRequest.DiscardUnknown(m)
}

var xxx_messageInfo_PageRequest proto.InternalMessageInfo

func (m *PageRequest) GetDefault() *proto1.PageRequest {
	if m != nil {
		return m.Default
	}
	return nil
}

func (m *PageRequest) GetTag() *Tag {
	if m != nil {
		return m.Tag
	}
	return nil
}

type PageResponse struct {
	List                 []*Tag       `protobuf:"bytes,1,rep,name=list,proto3" json:"list,omitempty"`
	Page                 *proto1.Page `protobuf:"bytes,2,opt,name=page,proto3" json:"page,omitempty"`
	XXX_NoUnkeyedLiteral struct{}     `json:"-"`
	XXX_unrecognized     []byte       `json:"-"`
	XXX_sizecache        int32        `json:"-"`
}

func (m *PageResponse) Reset()         { *m = PageResponse{} }
func (m *PageResponse) String() string { return proto.CompactTextString(m) }
func (*PageResponse) ProtoMessage()    {}
func (*PageResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_tag_7b90513173513c6c, []int{10}
}
func (m *PageResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_PageResponse.Unmarshal(m, b)
}
func (m *PageResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_PageResponse.Marshal(b, m, deterministic)
}
func (dst *PageResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_PageResponse.Merge(dst, src)
}
func (m *PageResponse) XXX_Size() int {
	return xxx_messageInfo_PageResponse.Size(m)
}
func (m *PageResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_PageResponse.DiscardUnknown(m)
}

var xxx_messageInfo_PageResponse proto.InternalMessageInfo

func (m *PageResponse) GetList() []*Tag {
	if m != nil {
		return m.List
	}
	return nil
}

func (m *PageResponse) GetPage() *proto1.Page {
	if m != nil {
		return m.Page
	}
	return nil
}

func init() {
	proto.RegisterType((*Tag)(nil), "tag.Tag")
	proto.RegisterType((*Request)(nil), "tag.Request")
	proto.RegisterType((*Response)(nil), "tag.Response")
	proto.RegisterType((*DelResponse)(nil), "tag.DelResponse")
	proto.RegisterType((*TreeRequest)(nil), "tag.TreeRequest")
	proto.RegisterType((*TagTree)(nil), "tag.TagTree")
	proto.RegisterType((*TreeResponse)(nil), "tag.TreeResponse")
	proto.RegisterType((*ListRequest)(nil), "tag.ListRequest")
	proto.RegisterType((*ListResponse)(nil), "tag.ListResponse")
	proto.RegisterType((*PageRequest)(nil), "tag.PageRequest")
	proto.RegisterType((*PageResponse)(nil), "tag.PageResponse")
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ client.Option
var _ server.Option

// Client API for TagService service

type TagServiceClient interface {
	CreateTag(ctx context.Context, in *Tag, opts ...client.CallOption) (*Response, error)
	CreateFolder(ctx context.Context, in *Tag, opts ...client.CallOption) (*Response, error)
	Update(ctx context.Context, in *Tag, opts ...client.CallOption) (*Response, error)
	Delete(ctx context.Context, in *proto1.IdRequest, opts ...client.CallOption) (*DelResponse, error)
	Open(ctx context.Context, in *proto1.IdRequest, opts ...client.CallOption) (*Response, error)
	Close(ctx context.Context, in *proto1.IdRequest, opts ...client.CallOption) (*Response, error)
	Get(ctx context.Context, in *proto1.IdRequest, opts ...client.CallOption) (*Response, error)
	GetList(ctx context.Context, in *ListRequest, opts ...client.CallOption) (*ListResponse, error)
	GetPage(ctx context.Context, in *PageRequest, opts ...client.CallOption) (*PageResponse, error)
	GetTree(ctx context.Context, in *TreeRequest, opts ...client.CallOption) (*TreeResponse, error)
}

type tagServiceClient struct {
	c           client.Client
	serviceName string
}

func NewTagServiceClient(serviceName string, c client.Client) TagServiceClient {
	if c == nil {
		c = client.NewClient()
	}
	if len(serviceName) == 0 {
		serviceName = "tag"
	}
	return &tagServiceClient{
		c:           c,
		serviceName: serviceName,
	}
}

func (c *tagServiceClient) CreateTag(ctx context.Context, in *Tag, opts ...client.CallOption) (*Response, error) {
	req := c.c.NewRequest(c.serviceName, "TagService.CreateTag", in)
	out := new(Response)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *tagServiceClient) CreateFolder(ctx context.Context, in *Tag, opts ...client.CallOption) (*Response, error) {
	req := c.c.NewRequest(c.serviceName, "TagService.CreateFolder", in)
	out := new(Response)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *tagServiceClient) Update(ctx context.Context, in *Tag, opts ...client.CallOption) (*Response, error) {
	req := c.c.NewRequest(c.serviceName, "TagService.Update", in)
	out := new(Response)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *tagServiceClient) Delete(ctx context.Context, in *proto1.IdRequest, opts ...client.CallOption) (*DelResponse, error) {
	req := c.c.NewRequest(c.serviceName, "TagService.Delete", in)
	out := new(DelResponse)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *tagServiceClient) Open(ctx context.Context, in *proto1.IdRequest, opts ...client.CallOption) (*Response, error) {
	req := c.c.NewRequest(c.serviceName, "TagService.Open", in)
	out := new(Response)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *tagServiceClient) Close(ctx context.Context, in *proto1.IdRequest, opts ...client.CallOption) (*Response, error) {
	req := c.c.NewRequest(c.serviceName, "TagService.Close", in)
	out := new(Response)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *tagServiceClient) Get(ctx context.Context, in *proto1.IdRequest, opts ...client.CallOption) (*Response, error) {
	req := c.c.NewRequest(c.serviceName, "TagService.Get", in)
	out := new(Response)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *tagServiceClient) GetList(ctx context.Context, in *ListRequest, opts ...client.CallOption) (*ListResponse, error) {
	req := c.c.NewRequest(c.serviceName, "TagService.GetList", in)
	out := new(ListResponse)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *tagServiceClient) GetPage(ctx context.Context, in *PageRequest, opts ...client.CallOption) (*PageResponse, error) {
	req := c.c.NewRequest(c.serviceName, "TagService.GetPage", in)
	out := new(PageResponse)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *tagServiceClient) GetTree(ctx context.Context, in *TreeRequest, opts ...client.CallOption) (*TreeResponse, error) {
	req := c.c.NewRequest(c.serviceName, "TagService.GetTree", in)
	out := new(TreeResponse)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for TagService service

type TagServiceHandler interface {
	CreateTag(context.Context, *Tag, *Response) error
	CreateFolder(context.Context, *Tag, *Response) error
	Update(context.Context, *Tag, *Response) error
	Delete(context.Context, *proto1.IdRequest, *DelResponse) error
	Open(context.Context, *proto1.IdRequest, *Response) error
	Close(context.Context, *proto1.IdRequest, *Response) error
	Get(context.Context, *proto1.IdRequest, *Response) error
	GetList(context.Context, *ListRequest, *ListResponse) error
	GetPage(context.Context, *PageRequest, *PageResponse) error
	GetTree(context.Context, *TreeRequest, *TreeResponse) error
}

func RegisterTagServiceHandler(s server.Server, hdlr TagServiceHandler, opts ...server.HandlerOption) {
	s.Handle(s.NewHandler(&TagService{hdlr}, opts...))
}

type TagService struct {
	TagServiceHandler
}

func (h *TagService) CreateTag(ctx context.Context, in *Tag, out *Response) error {
	return h.TagServiceHandler.CreateTag(ctx, in, out)
}

func (h *TagService) CreateFolder(ctx context.Context, in *Tag, out *Response) error {
	return h.TagServiceHandler.CreateFolder(ctx, in, out)
}

func (h *TagService) Update(ctx context.Context, in *Tag, out *Response) error {
	return h.TagServiceHandler.Update(ctx, in, out)
}

func (h *TagService) Delete(ctx context.Context, in *proto1.IdRequest, out *DelResponse) error {
	return h.TagServiceHandler.Delete(ctx, in, out)
}

func (h *TagService) Open(ctx context.Context, in *proto1.IdRequest, out *Response) error {
	return h.TagServiceHandler.Open(ctx, in, out)
}

func (h *TagService) Close(ctx context.Context, in *proto1.IdRequest, out *Response) error {
	return h.TagServiceHandler.Close(ctx, in, out)
}

func (h *TagService) Get(ctx context.Context, in *proto1.IdRequest, out *Response) error {
	return h.TagServiceHandler.Get(ctx, in, out)
}

func (h *TagService) GetList(ctx context.Context, in *ListRequest, out *ListResponse) error {
	return h.TagServiceHandler.GetList(ctx, in, out)
}

func (h *TagService) GetPage(ctx context.Context, in *PageRequest, out *PageResponse) error {
	return h.TagServiceHandler.GetPage(ctx, in, out)
}

func (h *TagService) GetTree(ctx context.Context, in *TreeRequest, out *TreeResponse) error {
	return h.TagServiceHandler.GetTree(ctx, in, out)
}

func init() { proto.RegisterFile("proto/tag/tag.proto", fileDescriptor_tag_7b90513173513c6c) }

var fileDescriptor_tag_7b90513173513c6c = []byte{
	// 572 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x9c, 0x94, 0x6d, 0x8b, 0xd3, 0x40,
	0x10, 0xc7, 0xdb, 0x24, 0x6d, 0x9a, 0x69, 0x95, 0x73, 0x3d, 0x8e, 0xd0, 0x53, 0x28, 0x2b, 0x9c,
	0x15, 0xa4, 0x95, 0xfa, 0x46, 0x0f, 0x11, 0xce, 0x3b, 0x2c, 0x07, 0x82, 0x12, 0xeb, 0x1b, 0x41,
	0x8e, 0x6d, 0x32, 0xae, 0x81, 0xb4, 0x89, 0xd9, 0x8d, 0xe0, 0x67, 0xf0, 0x5b, 0xf9, 0xc9, 0x64,
	0x1f, 0x52, 0x73, 0xde, 0x59, 0x7a, 0xf7, 0xa2, 0x30, 0x3b, 0x33, 0xbf, 0xc9, 0x7f, 0xa6, 0xb3,
	0x0b, 0xf7, 0x8b, 0x32, 0x97, 0xf9, 0x54, 0x32, 0xae, 0x7e, 0x13, 0x7d, 0x22, 0xae, 0x64, 0x7c,
	0xb8, 0x6f, 0x22, 0xc5, 0x6a, 0x35, 0x5d, 0x32, 0x81, 0x26, 0x44, 0x7f, 0xb7, 0xc1, 0x5d, 0x30,
	0x4e, 0xee, 0x82, 0x93, 0x26, 0x61, 0x7b, 0xd4, 0x1e, 0x07, 0x91, 0x93, 0x26, 0x84, 0x80, 0xb7,
	0x66, 0x2b, 0x0c, 0x1d, 0xed, 0xd1, 0x36, 0x39, 0x84, 0x40, 0x32, 0x7e, 0xc1, 0xcb, 0xbc, 0x2a,
	0x42, 0x57, 0x07, 0x7a, 0x92, 0xf1, 0xb9, 0x3a, 0x93, 0x3d, 0x70, 0x8b, 0x34, 0x09, 0x3d, 0xed,
	0x56, 0xa6, 0x2a, 0x21, 0x7f, 0x16, 0x18, 0x76, 0x4d, 0x09, 0x65, 0x93, 0x03, 0xe8, 0x0a, 0xc9,
	0x64, 0x25, 0x42, 0x7f, 0xd4, 0x1e, 0x93, 0xc8, 0x9e, 0xc8, 0x43, 0x80, 0xb8, 0x44, 0x26, 0x31,
	0xb9, 0x60, 0x32, 0xec, 0x69, 0x22, 0xb0, 0x9e, 0x13, 0xa9, 0xc2, 0x55, 0x91, 0xd4, 0xe1, 0xc0,
	0x84, 0xad, 0xe7, 0x44, 0xd2, 0x00, 0xfc, 0x08, 0xbf, 0x57, 0x28, 0x24, 0x3d, 0x82, 0x5e, 0x84,
	0xa2, 0xc8, 0xd7, 0x02, 0xc9, 0x10, 0x54, 0xe3, 0xba, 0xa9, 0xfe, 0xac, 0x37, 0x51, 0xf3, 0x58,
	0x30, 0x1e, 0x29, 0x27, 0x7d, 0x0c, 0xfd, 0x33, 0xcc, 0x36, 0xa9, 0x21, 0xf8, 0xa2, 0x8a, 0x63,
	0x14, 0x42, 0xa7, 0xf7, 0xa2, 0xfa, 0x48, 0x8f, 0xa1, 0xbf, 0x28, 0x11, 0x6d, 0xfd, 0x2b, 0x73,
	0xba, 0x34, 0x13, 0xe7, 0xf2, 0x4c, 0xe8, 0x39, 0xf8, 0x0b, 0xc6, 0x15, 0xbe, 0x4d, 0x0b, 0xa1,
	0xd0, 0x89, 0xbf, 0xa5, 0x59, 0x12, 0x3a, 0x23, 0x77, 0xdc, 0x9f, 0x0d, 0xea, 0xa8, 0xfe, 0xae,
	0x09, 0xd1, 0x19, 0x0c, 0x8c, 0x0c, 0x2b, 0x98, 0x42, 0x47, 0x96, 0x88, 0x4a, 0xee, 0x35, 0x8c,
	0x0e, 0x51, 0x0e, 0xfd, 0x77, 0xa9, 0x90, 0xb5, 0xf4, 0x57, 0xe0, 0x27, 0xf8, 0x95, 0x55, 0x99,
	0xb4, 0x32, 0xe8, 0xa4, 0x58, 0xad, 0x26, 0x45, 0xb5, 0xcc, 0xd2, 0xd8, 0xac, 0xc3, 0x44, 0x6f,
	0x46, 0x03, 0x8a, 0x6a, 0xa4, 0x6e, 0xc0, 0xb9, 0x6e, 0x98, 0x4f, 0x61, 0x60, 0x18, 0x2b, 0xee,
	0x01, 0x78, 0x59, 0x2a, 0xa4, 0xd5, 0xf6, 0x37, 0x59, 0x7b, 0x95, 0xac, 0x0f, 0x8c, 0xe3, 0x8d,
	0x65, 0x35, 0xa0, 0xdd, 0x64, 0x7d, 0x81, 0x81, 0x61, 0x76, 0x91, 0x45, 0xa6, 0xe0, 0x15, 0x8c,
	0xa3, 0x2d, 0x75, 0xb8, 0x4d, 0x84, 0x4e, 0x9c, 0xfd, 0xf2, 0x00, 0x16, 0x8c, 0x7f, 0xc4, 0xf2,
	0x47, 0x1a, 0x23, 0x39, 0x82, 0xe0, 0x54, 0x2f, 0xac, 0xba, 0x4e, 0x9b, 0xe2, 0xc3, 0x3b, 0xda,
	0xaa, 0x35, 0xd0, 0x16, 0x79, 0x02, 0x03, 0x93, 0xf7, 0x36, 0xcf, 0x12, 0x2c, 0xb7, 0xa5, 0x3e,
	0x82, 0xee, 0x27, 0xbd, 0xe4, 0xdb, 0x92, 0x5e, 0x43, 0xf7, 0x0c, 0x33, 0x94, 0x48, 0x46, 0xff,
	0xd1, 0x7c, 0x9e, 0xd8, 0xb1, 0x0d, 0xf7, 0x34, 0xdc, 0x58, 0x7c, 0xda, 0x22, 0x2f, 0xc1, 0x7b,
	0x5f, 0xe0, 0x7a, 0x07, 0xfa, 0xca, 0xa7, 0x8f, 0xa1, 0x73, 0x9a, 0xe5, 0x02, 0x6f, 0xc3, 0xbe,
	0x00, 0x77, 0x8e, 0xf2, 0x36, 0xe4, 0x33, 0xf0, 0xe7, 0x28, 0xd5, 0xc2, 0x11, 0xd3, 0x4f, 0x63,
	0x5f, 0x87, 0xf7, 0x1a, 0x9e, 0x7f, 0x08, 0xf5, 0xd7, 0x59, 0xa2, 0xb1, 0x4a, 0x96, 0x68, 0x2e,
	0xca, 0x86, 0xd0, 0x37, 0xd7, 0x10, 0x8d, 0x37, 0xc0, 0x12, 0xcd, 0xeb, 0x48, 0x5b, 0x6f, 0x0e,
	0x3e, 0xef, 0xab, 0xa7, 0xd5, 0x74, 0x32, 0xdd, 0xbc, 0xc2, 0xcb, 0xae, 0x36, 0x9f, 0xff, 0x09,
	0x00, 0x00, 0xff, 0xff, 0xab, 0x87, 0xaf, 0x1e, 0x99, 0x05, 0x00, 0x00,
}
