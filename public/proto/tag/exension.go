package tag

import (
	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
	"github.com/labstack/gommon/log"
	"time"

	)

const (
	OPEN_STATUS  = 1
	CLOSE_STATUS  = 0
)

func (tag *Tag) BeforeCreate(scope *gorm.Scope) error {
	uid, err := uuid.NewV4()
	if err != nil {
		log.Fatalf("created uuid error: %v\n", err)
	}
	scope.SetColumn("Id", uid.String())
	scope.SetColumn("CreatedAt", time.Now().Format("2006-01-02 15:04:05"))
	scope.SetColumn("UpdatedAt", time.Now().Format("2006-01-02 15:04:05"))
	scope.SetColumn("Status", OPEN_STATUS)
	return nil
}



func (tag *Tag) BeforeSave(scope *gorm.Scope) (err error) {
	return nil
}

