package main

import (
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/server"
	"log"
	"pmm/public-srv/handler"
	"pmm/public/config"
	"pmm/public/database"
	"pmm/public/model"
	"pmm/public/proto/report"
	"pmm/public/proto/template"
)

func main() {
	//连接数据库
	db,err := database.CreateConnection()
	defer db.Close()
	db.LogMode(true)

	if err != nil {
		log.Fatalf("数据库连接失败: %v\n",err)
	}
	model.SetConn(db)

	// 全局禁用表名复数
	db.SingularTable(true) // 如果设置为true,`User`的默认表名为`user`,使用`TableName`设置的表名不受影响

	// 自动检查 表结构是否变化
	db.AutoMigrate(model.Template{})
	db.AutoMigrate(model.Report{})


	srv := micro.NewService(
		micro.Name(config.Namespace+"srv.public"),
		micro.Version("latest"),
	)
	srv.Init()

	//注册推送监听
	publisher := micro.NewPublisher("public",srv.Client())

	//注册服务
	template.RegisterTemplateServiceHandler(srv.Server(),handler.NewTemplateHandler(publisher),server.InternalHandler(true))
	report.RegisterReportServiceHandler(srv.Server(),handler.NewReportHandler(publisher),server.InternalHandler(true))

	log.Println("启动public-srv服务...")
	if err := srv.Run(); err != nil {
		log.Fatalf("public-srv服务启动失败: %v\n", err)
	}
}



