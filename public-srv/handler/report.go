package handler

import (
	"context"
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/micro/go-micro"
	mes "github.com/micro/go-micro/errors"
	"log"
	"pmm/public/config"
	"pmm/public/handler"
	"pmm/public/model"
	"pmm/public/proto"
	"pmm/public/proto/report"
)

type ReportHandler struct {
	//logger *zap.Logger
	Model model.Base
	db *gorm.DB
	Publisher    micro.Publisher
}

func NewReportHandler(pub micro.Publisher) *ReportHandler{
	return &ReportHandler{
		//logger:tl.Instance().Named("TagHandler"),
		Model:model.Base{},
		Publisher:pub,
	}
}


func (h *ReportHandler) Create(ctx context.Context,req *report.Report,rsp *report.Response)error{
	log.Println("新增...")

	ph := handler.ReportHandler{}
	data := &model.Report{}
	data.Report = *req
	err := ph.Create(data)
	if err != nil {
		return errors.New(mes.InternalServerError("report-srv",err.Error()).Error())
	}

	rsp.Report = &data.Report
	return nil
}

func (h *ReportHandler) Update(ctx context.Context,req *report.Report,rsp *report.Response)error{
	log.Println("更新...")

	ph := handler.ReportHandler{}
	data := &model.Report{}
	data.Report = *req
	fmt.Printf("内存地址：%p\n", &data)
	fmt.Printf("指针的内存地址：%p\n", data)
	err := ph.Update(data)
	if err != nil {
		return errors.New(mes.InternalServerError("report-srv",err.Error()).Error())
	}
	rsp.Report = &data.Report

	return nil
}

func (h *ReportHandler) Delete(ctx context.Context,req *proto.IdRequest,rsp *proto.BoolResponse)error{
	log.Println("删除...")
	data := &model.Report{}
	data.Id = req.Id

	ph := handler.ReportHandler{}
	if err := ph.Delete(data); err != nil {
		return errors.New(mes.InternalServerError("report-srv",err.Error()).Error())
	}
	rsp.Success = true

	return nil
}

func (h *ReportHandler) Open(ctx context.Context,req *proto.IdRequest,rsp *report.Response)error{
	log.Println("启用...")
	data := &model.Report{}
	data.Id = req.Id

	ph := handler.ReportHandler{}
	if err := ph.ChangeStatus(data,config.OPEN_STATUS); err != nil {
		return errors.New(mes.InternalServerError("report-srv",err.Error()).Error())
	}
	rsp.Report = &data.Report
	return nil
}

func (h *ReportHandler) Close(ctx context.Context,req *proto.IdRequest,rsp *report.Response)error{
	log.Println("禁用...")
	data := &model.Report{}
	data.Id = req.Id

	ph := handler.ReportHandler{}
	if err := ph.ChangeStatus(data,config.CLOSE_STATUS); err != nil {
		return errors.New(mes.InternalServerError("report-srv",err.Error()).Error())
	}
	rsp.Report = &data.Report
	return nil
}

func (h *ReportHandler) Get(ctx context.Context,req *proto.IdRequest,rsp *report.Response)error{
	log.Println("查询...")

	data := &model.Report{}
	data.Id = req.Id

	ph := handler.ReportHandler{}
	if err := ph.Get(data); err != nil {
		return errors.New(mes.InternalServerError("report-srv",err.Error()).Error())
	}
	rsp.Report = &data.Report
	return nil
}

func (h *ReportHandler) GetPage(ctx context.Context,req *report.PageRequest,rsp *report.PageResponse)error{
	log.Println("根据页面查询...")
	ph := handler.ReportHandler{}
	var page report.PageResponse
	err := ph.GetPage(req,&page)
	if err != nil {
		return errors.New(mes.InternalServerError("report-srv",err.Error()).Error())
	}

	rsp.Page = page.Page
	rsp.List = page.List
	return nil
}

func (h *ReportHandler) GetList(ctx context.Context,req *report.ListRequest,rsp *report.ListResponse)error{
	log.Println("查询所有...")
	ph := handler.ReportHandler{}
	var list []*report.Report
	err := ph.GetList(req,&list)
	if err != nil {
		return errors.New(mes.InternalServerError("report-srv",err.Error()).Error())
	}
	rsp.List = list
	return nil
}