package handler

import (
	"context"
	"errors"
	"github.com/jinzhu/gorm"
	"github.com/micro/go-micro"
	mes "github.com/micro/go-micro/errors"
	"log"
	"pmm/public/handler"
	"pmm/public/model"
	"pmm/public/proto"
	pb "pmm/public/proto/template"
)

type TemplateHandler struct {
	//logger *zap.Logger
	Model model.Base
	db *gorm.DB
	Publisher    micro.Publisher
}

func NewTemplateHandler(pub micro.Publisher) *TemplateHandler{
	return &TemplateHandler{
		//logger:tl.Instance().Named("TagHandler"),
		Model:model.Base{},
		Publisher:pub,
	}
}


func (h *TemplateHandler) Create(ctx context.Context,req *pb.Template,rsp *pb.Response)error{
	log.Println("新增...")
	th := handler.NewTemplateHandler()
	data := th.Model
	data.Template = *req

	err := th.Create(data)
	if err != nil {
		return errors.New(mes.BadRequest("public-srv",err.Error()).Error())
	}

	rsp.Template = &data.Template
	return nil
}

func (h *TemplateHandler) Update(ctx context.Context,req *pb.Template,rsp *pb.Response)error{
	log.Println("更新...")
	th := handler.NewTemplateHandler()
	data := th.Model
	data.Template = *req

	err := th.Update(data)
	if err != nil {
		return errors.New(mes.BadRequest("public-srv",err.Error()).Error())
	}

	rsp.Template = &data.Template

	return nil
}

func (h *TemplateHandler) Delete(ctx context.Context,req *proto.IdRequest,rsp *proto.BoolResponse)error{
	log.Println("删除...")
	/*if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("template-srv","项目编号不合法").Error())
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.NotFound("template-srv","项目不存在").Error())
	}

	err := h.Model.Delete(req.Id)

	if err != nil {
		return errors.New(mes.InternalServerError("template-srv",err.Error()).Error())
	}
	rsp.Success = true*/
	return nil
}

func (h *TemplateHandler) Open(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("启用...")
	/*info,err := h.changeStatus(req.Id,config.OPEN_STATUS)
	if err != nil {
		return err
	}

	rsp.Template = &info.Template*/
	return nil
}

func (h *TemplateHandler) Close(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("禁用...")
	/*info,err := h.changeStatus(req.Id,config.CLOSE_STATUS)
	if err != nil {
		return err
	}

	rsp.Template = &info.Template*/
	return nil
}

func (h *TemplateHandler) Get(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("查询...")
	/*if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New("编号不合法")
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.NotFound("template-srv","项目不存在").Error())
	}

	rsp.Template = &info.Template*/
	return nil
}

func (h *TemplateHandler) GetPage(ctx context.Context,req *pb.PageRequest,rsp *pb.PageResponse)error{
	log.Println("根据页面查询...")
	//设置默认每页条数,页数
	/*count,err := h.Model.GetCount(req)
	if err != nil {
		return errors.New(mes.InternalServerError("template-srv",err.Error()).Error())
	}
	if *count == 0 {
		rsp.Page = &proto.Page{}
		return nil
	}
	lr := &pb.ListRequest{
		Default:&proto.ListRequest{},
		Template:req.Template,
	}

	if req.Default != nil {
		if req.Default.Size <= 0 || req.Default.Size > sconf.DefaultMaxPageSize{
			req.Default.Size = sconf.PageSize
		}
		if req.Default.Page <= 0 {
			req.Default.Page = sconf.Page
		}

		lr.Default = &proto.ListRequest{
			Start:(req.Default.Page-1)*req.Default.Size,
			Limit:req.Default.Size,
			Search:req.Default.Search,
			Ids:req.Default.Ids,
		}
	}else {
		req.Default = &proto.PageRequest{
			Size:sconf.PageSize,
			Page:sconf.Page,
		}
	}

	list,err := h.Model.GetList(lr)
	if err != nil {
		return errors.New(mes.InternalServerError("template-srv",err.Error()).Error())
	}

	page := &proto.Page{
		Total:*count,
		Index:req.Default.Page,
		TotalPage:int64(math.Ceil(float64(*count)/float64(req.Default.Size))),
		PageSize:req.Default.Size,
	}
	rsp.Page = page
	rsp.List = list*/
	return nil
}

func (h *TemplateHandler) GetList(ctx context.Context,req *pb.ListRequest,rsp *pb.ListResponse)error{
	log.Println("查询所有...")
	/*list,err := h.Model.GetList(req)
	if err != nil {
		return errors.New(mes.InternalServerError("template-srv",err.Error()).Error())
	}
	rsp.List = list*/
	return nil
}
