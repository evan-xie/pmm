package main

import (
	"pmm/company-srv/database"
	"log"
	 pb "pmm/public/proto/company"
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/server"
	"pmm/company-srv/config"
	"pmm/company-srv/handler"
)

func main() {
	//连接数据库
	db,err := database.CreateConnection()
	defer db.Close()
	db.LogMode(true)

	if err != nil {
		log.Fatalf("数据库连接失败: %v\n",err)
	}

	// 全局禁用表名复数
	db.SingularTable(true) // 如果设置为true,`User`的默认表名为`user`,使用`TableName`设置的表名不受影响

	// 自动检查 表 结构是否变化
	db.AutoMigrate(database.Company{})

	srv := micro.NewService(
		micro.Name(config.ServiceName),
		micro.Version("latest"),
	)

	srv.Init()

	//注册推送监听

	//注册服务
	pb.RegisterCompanyServiceHandler(srv.Server(),handler.NewCompanyHandler(),server.InternalHandler(true))

	log.Println("启动company-srv服务...")
	if err := srv.Run(); err != nil {
		log.Fatalf("company-srv服务启动失败: %v\n", err)
	}
}