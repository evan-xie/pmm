package config

import (
		"pmm/public/config"
)

var (
	ServiceName = config.Namespace + "srv.company"

	//默认每页条数
	PageSize int64 = 20
	//默认页数
	Page int64 = 1
	//默认最大每页条数
	DefaultMaxPageSize int64 = 100

)
