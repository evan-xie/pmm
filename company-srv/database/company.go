package database

import (
	"pmm/public/model"
	 pb "pmm/public/proto/company"
	"pmm/public/config"
	"pmm/public/utils"
	"github.com/jinzhu/gorm"
	"time"
	"github.com/satori/go.uuid"
	"log"
)

// 设置的表名
func (Company) TableName() string {
	return "company"
}

type Company struct {
	pb.Company
	model.Comm
}

/**
创建
 */
func (repo *Company) Create(data *pb.Company) (*Company,error) {
	d := &Company{
		Company:*data,
	}
	if err := Conn.Create(&d).Error; err != nil {
		return nil,err
	}
	return d,nil
}

/**
更新
condition 条件 参数1为是否更新密码 2位是否更新状态
 */
func (repo *Company) Update(data *pb.Company,condition... string) (error)  {
	d := &Company{}
	d.Company = *data

	query := Conn.Model(&d)
	//过滤参数
	if len(condition) >= 1 {
		query = query.Omit(condition...)
	}

	if err := query.Updates(d).Error;err != nil {
		return err
	}
	return nil
}

/**
 判断是否存在 存在返回查询到的结果
 */
func (repo *Company) IsExist(pb *pb.Company,isOpen bool) (*Company) {
	if pb == nil {
		return nil
	}

	d := &Company{}
	d.Name = pb.Name

	query := Conn
	if isOpen {
		query = query.Where("status = ?",config.OPEN_STATUS)
	}
	err := query.Where(pb).First(&d).Error

	if err == nil {
		return d
	}

	return nil
}

/**
根据id取得信息
 */
func (repo *Company) GetInfoById(id string,isOpen bool) (*Company) {
	if id == "" {
		return nil
	}

	d := &Company{}

	query := Conn.Where("id = ?",id)
	if isOpen {
		query = query.Where("status = ?",config.OPEN_STATUS)
	}
	err := query.First(&d).Error

	if err == nil {
		return d
	}

	return nil
}

/**
根据编号删除
 */
func (repo *Company) Delete(id ...string) (error) {
	str := ""
	if len(id) > 1 {
		str = "id in ("
		for _, value := range id {
			str = str+"'"+value+"',"
		}
		str = string([]rune(str)[:len(str)-1])+")"
	}else {
		str = "id = '" + id[0] +"'"
	}

	err := Conn.Where(str).Delete(&Company{}).Error

	if err != nil {
		return err
	}

	return nil
}

/**
取得列表
 */
func (repo *Company) GetList(req *pb.ListRequest,args ...interface{}) ([]*pb.Company,error) {
	var users []*pb.Company
	user := req.Company
	fs := utils.GetDbNameByStruct(Conn,pb.Company{},"password")
	query := Conn.Model(&Company{}).Where(user)

	//过滤参数
	if len(fs) > 1 {
		query = query.Select(fs)
	}
	if req.Default != nil {
		if req.Default.Start > 0 {
			query = query.Offset(req.Default.Start)
		}
		if req.Default.Limit > 0 {
			query = query.Limit(req.Default.Limit)
		}
		if req.Default.Search != nil {
			//m := utils.MapToWhereStr(req.Default.Search)
			//query = query.Where(m)
			query = utils.AndOrLikes(query,req.Default.Search,true)
		}
		if len(req.Default.Ids) > 0{
			query = query.Where(req.Default.Ids)
		}
	}
	if args != nil && len(args) > 0 {
		for _, value := range args {
			query = query.Where(value)
		}
	}

	err := query.Scan(&users).Error
	if err != nil {
		return nil,err
	}
	return users,nil
}

func (repo *Company) GetCount(req *pb.PageRequest,args ...interface{}) (*int64,error) {
	user := req.Company
	query := Conn.Model(&Company{}).Where(user)
	if req.Default != nil && req.Default.Search != nil {
		query = utils.AndOrLikes(query,req.Default.Search,true)
	}
	if args != nil && len(args) > 0 {
		for _, value := range args {
			query = query.Where(value)
		}
	}

	var count *int64
	err := query.Count(&count).Error
	if err != nil {
		return count,err
	}
	return count,nil
}
func (repo *Company) BeforeCreate(scope *gorm.Scope) error {
	uid, err := uuid.NewV4()
	if err != nil {
		log.Fatalf("created uuid error: %v\n", err)
	}
	scope.SetColumn("Id", uid.String())
	scope.SetColumn("CreatedAt", time.Now().Format("2006-01-02 15:04:05"))
	scope.SetColumn("UpdatedAt", time.Now().Format("2006-01-02 15:04:05"))
	scope.SetColumn("Status", config.OPEN_STATUS)
	return nil
}