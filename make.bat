CHCP 65001
@echo

cd D:\Go\src\pmm
XCOPY D:\Go\src\pmm\user-srv\config D:\Go\src\pmm\run\user-srv\config/S

go build -o run/api-srv.exe api-srv/main.go
            
go build -o run/company-srv.exe company-srv/main.go
            
go build -o run/file-srv.exe file-srv/main.go
            
go build -o run/node-srv.exe node-srv/main.go
            
go build -o run/project-srv.exe project-srv/main.go
            
go build -o run/tag-srv.exe tag-srv/main.go
            
go build -o run/user-srv.exe user-srv/main.go