build:
	#创建基础
	protoc --proto_path=. --go_out=plugins=micro:../ ./proto/pmm/base.proto
	#创建用户
	protoc --proto_path=. --go_out=plugins=micro:../ ./proto/user/user.proto
	#创建标签
	protoc --proto_path=. --go_out=plugins=micro:../ ./proto/tag/tag.proto
	#创建标签组
	protoc --proto_path=. --go_out=plugins=micro:../ ./proto/tag/group/tag.group.proto
	#创建公司
	protoc --proto_path=. --go_out=plugins=micro:../ ./proto/company/company.proto
	#创建 角色
	protoc --proto_path=. --go_out=plugins=micro:../ ./proto/role/role.proto
	#创建 用户-角色 关联
	protoc --proto_path=. --go_out=plugins=micro:./public ./proto/aur/auth.user.role.proto
	#创建 用户-角色 关联
	protoc --proto_path=. --go_out=plugins=micro:./public ./proto/user/role/user.role.proto
	#创建 规则
	protoc --proto_path=. --go_out=plugins=micro:../ ./proto/rule/rule.proto
	#创建 规则-角色 关联
	protoc --proto_path=. --go_out=plugins=micro:../ ./proto/role/rule/role.rule.proto
	#创建 规则
	protoc --proto_path=. --go_out=plugins=micro:../ ./proto/auth/auth.proto
	#创建 项目
	protoc --proto_path=. --go_out=plugins=micro:../ ./proto/project.proto
	#创建 项目-用户关系
	protoc --proto_path=. --go_out=plugins=micro:../ ./proto/project.user.proto
	#创建 项目-用户关系
	protoc --proto_path=. --go_out=plugins=micro:../ ./proto/project.WXACode.proto
	#创建 项目-节点配置
	protoc --proto_path=. --go_out=plugins=micro:../ ./proto/project.node.config.proto
	#创建 节点
	protoc --proto_path=. --go_out=plugins=micro:../ ./proto/node.proto
	#手机验证码
	protoc --proto_path=. --go_out=plugins=micro:../ ./proto/verify.code.proto
	#微信小程序
	protoc --proto_path=. --go_out=plugins=micro:../ ./proto/wxApp.proto

	#文件
	protoc --proto_path=. --go_out=plugins=micro:../ ./proto/file.proto
	#模板
	protoc --proto_path=. --go_out=plugins=micro:../ ./proto/template.proto
	#报告
	protoc --proto_path=. --go_out=plugins=micro:../ ./proto/report.proto