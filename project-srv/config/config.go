package config

import (
	"time"
	"pmm/public/config"
)

const RuleTypeFile  = "file" //权限节点
const RuleTypeMennu  = "menu" //权限节点
const IsMenuTrue  = 1
const IsMenuFalse = -1

var (
	ServiceName = config.Namespace + "srv.project"
	//access token 生命周期 15min
	AccessTokenLifeCycle = time.Minute * 15
	//access key 秘钥
	AccessTokenPrivateKey = "com.greenment.user.auth.access.token.private.key"

	//refresh Token 生命周期 30day
	RefreshTokenLifeCycle = time.Hour * 24 * 30
	//refresh key 秘钥
	RefreshTokenPrivateKey = "com.greenment.user.auth.refresh.token.private.key"

	//默认每页条数
	PageSize int64 = 20
	//默认页数
	Page int64 = 1
	//默认最大每页条数
	DefaultMaxPageSize int64 = 100

	//默认规则类型
	DefaultRuleType = RuleTypeFile
	RuleTypes = []string{RuleTypeFile,RuleTypeMennu}

	//默认Rule顶层pid值
	DefaultRuleTopPidValue = "-"
	//默认domain值
	DefaultRuleDomainValue = "*"

	//节点单位
	NodeUnit = []string{"hour","day","week","month","year"}
	//默认节点单位
	DefaultNodeUnit = "day"

)
