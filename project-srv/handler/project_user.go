package handler

import (
	"github.com/jinzhu/gorm"
	"log"
	pb "pmm/public/proto/project/user"
	"context"
	mes "github.com/micro/go-micro/errors"
	gv "github.com/asaskevich/govalidator"
	"github.com/pkg/errors"
	sconf "pmm/project-srv/config"
	"pmm/public/utils/validate"
	"pmm/public/config"
	"math"
	"pmm/public/proto"
	"pmm/project-srv/database"
)

type ProjectUserHandler struct {
	//logger *zap.Logger
	Model database.ProjectUser
	db *gorm.DB
}


// new一个TagHandler
func NewProjectUserHandler() *ProjectUserHandler{
	return &ProjectUserHandler{
		//logger:tl.Instance().Named("TagHandler"),
		Model:database.ProjectUser{},
		db:database.Conn,
	}
}


func (h *ProjectUserHandler) Create(ctx context.Context,req *pb.ProjectUser,rsp *pb.Response)error{
	log.Println("新增...")
	if gv.IsNull(req.UserId) || !gv.IsUUIDv4(req.UserId) {
		return errors.New(mes.BadRequest("project-srv","用户编号不合法").Error())
	}
	if gv.IsNull(req.ProjectId) || !gv.IsUUIDv4(req.ProjectId){
		return errors.New(mes.BadRequest("project-srv","项目编号不合法").Error())
	}
	if req.IsManager != 0 {
		if req.IsManager != config.OPEN_STATUS && req.IsManager != config.CLOSE_STATUS {
			return errors.New(mes.BadRequest("project-srv","是否项目经理参数格式错误").Error())
		}
	}else {
		req.IsManager = config.CLOSE_STATUS
	}

	if req.IsDirectors != 0 {
		if req.IsDirectors != config.OPEN_STATUS && req.IsDirectors != config.CLOSE_STATUS {
			return errors.New(mes.BadRequest("project-srv","是项目总监参数格式错误").Error())
		}
	}else {
		req.IsDirectors = config.CLOSE_STATUS
	}
	
	info := h.Model.IsExist(req,false)
	if info != nil {
		return errors.New(mes.BadRequest("project-srv","用户已和项目关联，请勿重复操作").Error())
	}
	
	info,err := h.Model.Create(req)
	if err != nil {
		return errors.New(mes.InternalServerError("project-srv",err.Error()).Error())
	}

	rsp.ProjectUser = &info.ProjectUser
	return nil
}

func (h *ProjectUserHandler) Update(ctx context.Context,req *pb.ProjectUser,rsp *pb.Response)error{
	log.Println("更新...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("project-srv","项目用户关联编号不合法").Error())
	}
	if !gv.IsNull(req.UserId) && !gv.IsUUIDv4(req.UserId) {
		return errors.New(mes.BadRequest("project-srv","用户编号不合法").Error())
	}
	if !gv.IsNull(req.ProjectId) && !gv.IsUUIDv4(req.ProjectId){
		return errors.New(mes.BadRequest("project-srv","项目编号不合法").Error())
	}
	if req.IsManager != 0 && (req.IsManager != config.OPEN_STATUS && req.IsManager != config.CLOSE_STATUS){
		return errors.New(mes.BadRequest("project-srv","是否项目经理参数格式错误").Error())
	}
	if req.IsDirectors != 0 && (req.IsDirectors != config.OPEN_STATUS && req.IsDirectors != config.CLOSE_STATUS){
		return errors.New(mes.BadRequest("project-srv","是项目总监参数格式错误").Error())
	}


	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.BadRequest("project-srv","项目用户关联不存在").Error())
	}
	if !gv.IsNull(req.UserId) && req.UserId != info.UserId{
		return errors.New(mes.BadRequest("project-srv","不可修改关联人员").Error())
	}
	if !gv.IsNull(req.ProjectId) && req.ProjectId != info.ProjectId{
		return errors.New(mes.BadRequest("project-srv","不可修改关联项目").Error())
	}


	err := h.Model.Update(req,"status")
	if err != nil {
		return errors.New(mes.InternalServerError("project-srv",err.Error()).Error())
	}
	info = h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.BadRequest("project-srv","项目关联不存在").Error())
	}
	
	rsp.ProjectUser = &info.ProjectUser
	return nil
}

func (h *ProjectUserHandler) Delete(ctx context.Context,req *proto.IdRequest,rsp *proto.BoolResponse)error{
	log.Println("删除...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("project-srv","项目关联人员编号不合法").Error())
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.NotFound("project-srv","项目关联人员关系不存在").Error())
	}

	err := h.Model.Delete(req.Id)

	if err != nil {
		return errors.New(mes.InternalServerError("project-srv",err.Error()).Error())
	}
	rsp.Success = true
	return nil
}

func (h *ProjectUserHandler) Open(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("启用...")
	info,err := h.changeStatus(req.Id,config.OPEN_STATUS)
	if err != nil {
		return err
	}

	rsp.ProjectUser = &info.ProjectUser
	return nil
}

func (h *ProjectUserHandler) Close(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("禁用...")
	info,err := h.changeStatus(req.Id,config.CLOSE_STATUS)
	if err != nil {
		return err
	}

	rsp.ProjectUser = &info.ProjectUser
	return nil
}

func (h *ProjectUserHandler) Get(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("查询...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New("编号不合法")
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.NotFound("project-srv","项目不存在").Error())
	}

	rsp.ProjectUser = &info.ProjectUser
	return nil
}

func (h *ProjectUserHandler) GetPage(ctx context.Context,req *pb.PageRequest,rsp *pb.PageResponse)error{
	log.Println("根据页面查询...")
	//设置默认每页条数,页数
	count,err := h.Model.GetCount(req)
	if err != nil {
		return errors.New(mes.InternalServerError("project-srv",err.Error()).Error())
	}

	lr := &pb.ListRequest{
		Default:&proto.ListRequest{},
		ProjectUser:req.ProjectUser,
	}

	if req.Default != nil {
		if req.Default.Size <= 0 || req.Default.Size > sconf.DefaultMaxPageSize{
			req.Default.Size = sconf.PageSize
		}
		if req.Default.Page <= 0 {
			req.Default.Page = sconf.Page
		}

		lr.Default = &proto.ListRequest{
			Start:(req.Default.Page-1)*req.Default.Size,
			Limit:req.Default.Size,
			Search:req.Default.Search,
		}
	}else {
		req.Default = &proto.PageRequest{
			Size:sconf.PageSize,
			Page:sconf.Page,
		}
	}

	list,err := h.Model.GetList(lr)
	if err != nil {
		return errors.New(mes.InternalServerError("project-srv",err.Error()).Error())
	}

	page := &proto.Page{
		Total:*count,
		Index:req.Default.Page,
		TotalPage:int64(math.Ceil(float64(*count)/float64(req.Default.Size))),
		PageSize:req.Default.Size,
	}
	rsp.Page = page
	rsp.List = list
	return nil
}

func (h *ProjectUserHandler) GetList(ctx context.Context,req *pb.ListRequest,rsp *pb.ListResponse)error{
	log.Println("查询所有...")
	list,err := h.Model.GetList(req)
	if err != nil {
		return errors.New(mes.InternalServerError("project-srv",err.Error()).Error())
	}
	rsp.List = list
	return nil
}

func (h *ProjectUserHandler) GetProjectIdByUser(ctx context.Context,req *pb.ProjectUser,rsp *pb.ProjectId)error{
	log.Println("查询关于用户的项目...")
	d := &pb.ListRequest{}
	req.ProjectId = ""
	d.ProjectUser = req
	list,err := h.Model.GetList(d)
	if err != nil {
		return errors.New(mes.InternalServerError("project-srv",err.Error()).Error())
	}
	if list != nil && len(list) > 0 {
		for _, value := range list {
			rsp.ProjectIds = append(rsp.ProjectIds, value.ProjectId)
		}
	}
	return nil
}

func (h *ProjectUserHandler) GetUserIdByProject(ctx context.Context,req *pb.ProjectUser,rsp *pb.UserId)error{
	log.Println("查询项目的用户...")
	d := &pb.ListRequest{}
	req.UserId = ""
	d.ProjectUser = req
	list,err := h.Model.GetList(d)
	if err != nil {
		return errors.New(mes.InternalServerError("project-srv",err.Error()).Error())
	}
	if list != nil && len(list) > 0 {
		for _, value := range list {
			rsp.UserIds = append(rsp.UserIds, value.UserId)
		}
	}
	return nil
}

func (h *ProjectUserHandler) changeStatus(id string,status int)(*database.ProjectUser,error){
	if validate.HasAndCheckString(gv.IsUUIDv4, id) == false {
		return nil,errors.New(mes.BadRequest("project-srv","项目关联人员编号不合法").Error())
	}

	info := h.Model.GetInfoById(id,false)
	if info == nil {
		return nil,errors.New(mes.NotFound("project-srv","项目关联人员不存在").Error())
	}

	u := &pb.ProjectUser{
		Id:id,
		Status:int64(status),
	}

	err := h.Model.Update(u)
	if err != nil {
		return nil,errors.New(mes.InternalServerError("project-srv",err.Error()).Error())
	}
	info = h.Model.GetInfoById(id,false)

	return info,nil
}

func (h *ProjectUserHandler) checkStatus(id string)(*database.ProjectUser,error){
	if validate.HasAndCheckString(gv.IsUUIDv4, id) == false {
		return nil,errors.New(mes.BadRequest("project-srv","项目编号不合法").Error())
	}

	info := h.Model.GetInfoById(id,true)
	if info == nil {
		return nil,errors.New(mes.NotFound("project-srv","项目不存在").Error())
	}
	return info,nil
}