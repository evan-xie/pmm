package handler

import (
	"bytes"
	"context"
	gv "github.com/asaskevich/govalidator"
	"github.com/jinzhu/gorm"
	mes "github.com/micro/go-micro/errors"
	"github.com/pkg/errors"
	"io/ioutil"
	"log"
	"net/http"
	"php"
	pb "pmm/public/proto/project/wxaCode"
	"pmm/public/utils/oss"

	"pmm/public/config"
	"pmm/public/utils"
	"pmm/public/utils/validate"
	usconf "pmm/user-srv/config"

	"pmm/project-srv/database"
	"pmm/public/proto"
	"strconv"
	"strings"
	"time"
)

type ProjectWXACodeHandler struct {
	//logger *zap.Logger
	Model database.ProjectWXACode
	db *gorm.DB
	Project database.Project
}


// new一个TagHandler
func NewProjectWXACodeHandler() *ProjectWXACodeHandler{
	return &ProjectWXACodeHandler{
		//logger:tl.Instance().Named("TagHandler"),
		Model:database.ProjectWXACode{},
		db:database.Conn,
		Project:database.Project{},
	}
}


func (h *ProjectWXACodeHandler) Create(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("新增...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("project-srv","项目编号不合法").Error())
	}
	info,err := h.getWXACode(req.Id)
	if err != nil {
		return err
	}
	rsp.ProjectWXACode = &info.ProjectWXACode
	return nil
}

func (h *ProjectWXACodeHandler) Update(ctx context.Context,req *pb.ProjectWXACode,rsp *pb.Response)error{
	log.Println("更新...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("project-srv","项目编号不合法").Error())
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.BadRequest("project-srv","项目二维码信息不存在").Error())
	}
	err := h.Model.Update(req,"status")
	if err != nil {
		return errors.New(mes.InternalServerError("project-srv",err.Error()).Error())
	}
	info = h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.BadRequest("project-srv","项目二维码信息不存在").Error())
	}
	rsp.ProjectWXACode = &info.ProjectWXACode
	return nil
}

func (h *ProjectWXACodeHandler) Delete(ctx context.Context,req *proto.IdRequest,rsp *proto.BoolResponse)error{
	log.Println("删除...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("project-srv","项目关联人员编号不合法").Error())
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.NotFound("project-srv","项目二维码信息不存在").Error())
	}

	err := h.Model.Delete(req.Id)

	if err != nil {
		return errors.New(mes.InternalServerError("project-srv",err.Error()).Error())
	}
	rsp.Success = true
	return nil
}

func (h *ProjectWXACodeHandler) Open(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("启用...")
	info,err := h.changeStatus(req.Id,config.OPEN_STATUS)
	if err != nil {
		return err
	}
	rsp.ProjectWXACode = &info.ProjectWXACode
	return nil
}

func (h *ProjectWXACodeHandler) Close(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("禁用...")
	info,err := h.changeStatus(req.Id,config.CLOSE_STATUS)
	if err != nil {
		return err
	}
	rsp.ProjectWXACode = &info.ProjectWXACode
	return nil
}

func (h *ProjectWXACodeHandler) Get(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("查询...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New("编号不合法")
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.NotFound("project-srv","项目不存在").Error())
	}
	rsp.ProjectWXACode = &info.ProjectWXACode
	return nil
}

func (h *ProjectWXACodeHandler) CheckCode(ctx context.Context,req *pb.CheckCodeReq,rsp *proto.BoolResponse)error{
	log.Println("验证...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("project-srv","项目编号不合法").Error())
	}
	if gv.IsNull(req.Code){
		return errors.New(mes.BadRequest("project-srv","验证码不能为空").Error())
	}
	info := h.Model.GetInfoById(req.Id,true)
	if info == nil {
		return errors.New(mes.BadRequest("project-srv","项目二维码不存在").Error())
	}
	if info.Code != req.Code {
		return errors.New(mes.BadRequest("project-srv","验证码错误").Error())
	}
	rsp.Success = true
	return nil
}

func (h *ProjectWXACodeHandler) changeStatus(id string,status int)(*database.ProjectWXACode,error){
	if validate.HasAndCheckString(gv.IsUUIDv4, id) == false {
		return nil,errors.New(mes.BadRequest("project-srv","项目关联人员编号不合法").Error())
	}

	info := h.Model.GetInfoById(id,false)
	if info == nil {
		return nil,errors.New(mes.NotFound("project-srv","项目二维码信息不存在").Error())
	}

	u := &pb.ProjectWXACode{
		Id:id,
		Status:int64(status),
	}

	err := h.Model.Update(u)
	if err != nil {
		return nil,errors.New(mes.InternalServerError("project-srv",err.Error()).Error())
	}
	info = h.Model.GetInfoById(id,false)

	return info,nil
}

func (h *ProjectWXACodeHandler) checkStatus(id string)(*database.ProjectWXACode,error){
	if validate.HasAndCheckString(gv.IsUUIDv4, id) == false {
		return nil,errors.New(mes.BadRequest("project-srv","项目编号不合法").Error())
	}

	info := h.Model.GetInfoById(id,true)
	if info == nil {
		return nil,errors.New(mes.NotFound("project-srv","项目二维码信息不存在").Error())
	}
	return info,nil
}

func (h *ProjectWXACodeHandler) getWXACode(id string)(*database.ProjectWXACode,error){
	if validate.HasAndCheckString(gv.IsUUIDv4, id) == false {
		return nil,errors.New(mes.BadRequest("project-srv","项目编号不合法").Error())
	}

	p := h.Project.GetInfoById(id,false)
	if p == nil {
		return nil,errors.New(mes.NotFound("project-srv","项目不存在或被禁用").Error())
	}

/*	info := h.Model.GetInfoById(id,false)
	if info != nil {
		return  info,nil
	}*/

	at,err := utils.GetWxAccessToken()
	if err != nil {
		return nil,errors.New(mes.NotFound("project-srv",err.Error()).Error())
	}

	url := usconf.CreateWXAQRCode
	url  = strings.Replace(url, "ACCESS_TOKEN",  at,  -1)
	resp, err := http.Post(url,"application/json",
		strings.NewReader("{\"path\": \"pages/login/login?project_id="+id+"\"}"))
	if err != nil {
		return nil,errors.New(mes.NotFound("project-srv","生成二维码失败").Error())
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return nil,errors.New(mes.NotFound("project-srv","生成二维码失败").Error())
	}
	data ,err2 := ioutil.ReadAll(resp.Body)
	if err2 != nil {
		return nil,errors.New(mes.NotFound("project-srv","二维码图片保存失败").Error())
	}
	o,err := oss.Init()
	if err != nil {
		return nil,errors.New(mes.NotFound("project-srv",err.Error()).Error())
	}
	fileName := "file/"+time.Now().Format("2006-01-02")+"/"+id+".jpg"
	err = o.PutObject(fileName, bytes.NewReader([]byte(data)))
	if err != nil {
		return nil,errors.New(mes.NotFound("project-srv",err.Error()).Error())
	}
	//path := "./uploads/"+time.Now().Format("2006-01-02")+"/"+id+".jpg"
	//ioutil.WriteFile(fmt.Sprintf(path), data, 0644)

	d := &pb.ProjectWXACode{
		Id:id,
		Code:strconv.FormatInt(php.Rand(100000,999999),10),
		Url:config.Host+"/"+fileName,
	}
	i := h.Model.GetInfoById(id,false)
	var info *database.ProjectWXACode
	if i != nil {
		err = h.Model.Update(d)
		if err != nil {
			return nil,errors.New(mes.NotFound("project-srv",err.Error()).Error())
		}
		info = h.Model.GetInfoById(id,false)
	}else{
		info,err = h.Model.Create(d)
		if err != nil {
			return nil,errors.New(mes.NotFound("project-srv",err.Error()).Error())
		}
	}



	return info,nil
}