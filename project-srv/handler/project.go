package handler

import (
	"context"
	gv "github.com/asaskevich/govalidator"
	"github.com/jinzhu/gorm"
	"github.com/micro/go-micro"
	mes "github.com/micro/go-micro/errors"
	"github.com/pkg/errors"
	"log"
	"math"
	sconf "pmm/project-srv/config"
	"pmm/project-srv/database"
	"pmm/public/config"
	"pmm/public/proto"
	pb "pmm/public/proto/project"
	"pmm/public/utils/validate"
)

type ProjectHandler struct {
	//logger *zap.Logger
	Model database.Project
	db *gorm.DB
	Publisher    micro.Publisher
}


// new一个TagHandler
func NewProjectHandler(pub micro.Publisher) *ProjectHandler{
	return &ProjectHandler{
		//logger:tl.Instance().Named("TagHandler"),
		Model:database.Project{},
		db:database.Conn,
		Publisher:pub,
	}
}


func (h *ProjectHandler) Create(ctx context.Context,req *pb.Project,rsp *pb.Response)error{
	log.Println("新增...")
	if gv.IsNull(req.Title) {
		return errors.New(mes.BadRequest("project-srv","项目名称不能为空").Error())
	}
	if gv.IsNull(req.ClientCompany) || !gv.IsUUIDv4(req.ClientCompany) {
		return errors.New(mes.BadRequest("project-srv","客户公司编号格式错误").Error())
	}
	if gv.IsNull(req.ServiceCompany) || !gv.IsUUIDv4(req.ServiceCompany){
		return errors.New(mes.BadRequest("project-srv","服务方公司编号格式错误").Error())
	}
	if gv.IsNull(req.Type) {
		return errors.New(mes.BadRequest("project-srv","项目类型不能为空").Error())
	}
	if !gv.IsNull(req.DeadLine) && !gv.IsTime(req.DeadLine,"2006-01-02 15:04:05"){
		return errors.New(mes.BadRequest("project-srv","最后期限格式错误").Error())
	}
	if !gv.IsNull(req.StartTime) && !gv.IsTime(req.StartTime,"2006-01-02 15:04:05"){
		return errors.New(mes.BadRequest("project-srv","开始时间格式错误").Error())
	}
	if !gv.IsNull(req.EndTime) && !gv.IsTime(req.EndTime,"2006-01-02 15:04:05"){
		return errors.New(mes.BadRequest("project-srv","结束时间格式错误").Error())
	}

/*	if !gv.IsNull(req.NodeUnit){
		if !gv.IsIn(req.NodeUnit, sconf.NodeUnit...) {
			return errors.New(mes.BadRequest("project-srv","节点单位类型错误").Error())
		}
	}else {
		req.NodeUnit = sconf.DefaultNodeUnit
	}*/

	
	info := h.Model.IsExist(&pb.Project{
		Title:req.Title,
		ClientCompany:req.ClientCompany,
		ServiceCompany:req.ServiceCompany,
	},false)
	if info != nil {
		return errors.New(mes.BadRequest("project-srv","此项目已存在").Error())
	}
	if !gv.IsNull(req.Number) {
		info = h.Model.IsExist(&pb.Project{Number:req.Number},false)
		if info != nil {
			return errors.New(mes.BadRequest("project-srv","此项目号已被占用").Error())
		}
	}
	
	info,err := h.Model.Create(req)
	if err != nil {
		return errors.New(mes.InternalServerError("project-srv",err.Error()).Error())
	}

	//发送注册成功消息
/*	if err := h.Publisher.Publish(ctx, info); err != nil {
		return err
	}*/
	wx:= ProjectWXACodeHandler{}
	wx.getWXACode(info.Id)
	rsp.Project = &info.Project
	return nil
}

func (h *ProjectHandler) Update(ctx context.Context,req *pb.Project,rsp *pb.Response)error{
	log.Println("更新...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("project-srv","项目编号不合法").Error())
	}
	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.BadRequest("project-srv","项目不存在").Error())
	}
	//if  !gv.IsNull(req.ServiceCompany) && req.ClientCompany != info.ClientCompany {
	//	return errors.New(mes.BadRequest("project-srv","服务方公司不可修改").Error())
	//}
	//if !gv.IsNull(req.ClientCompany) && req.ServiceCompany != info.ServiceCompany {
	//	return errors.New(mes.BadRequest("project-srv","客户方公司不可修改").Error())
	//}
	if !gv.IsNull(req.DeadLine) && !gv.IsTime(req.DeadLine,"2006-01-02 15:04:05"){
		return errors.New(mes.BadRequest("project-srv","最后期限格式错误").Error())
	}
	if !gv.IsNull(req.StartTime) && !gv.IsTime(req.StartTime,"2006-01-02 15:04:05"){
		return errors.New(mes.BadRequest("project-srv","开始时间格式错误").Error())
	}
	if !gv.IsNull(req.EndTime) && !gv.IsTime(req.EndTime,"2006-01-02 15:04:05"){
		return errors.New(mes.BadRequest("project-srv","结束时间格式错误").Error())
	}
	data := &pb.Project{
		Title:req.Title,
	}
	if !gv.IsNull(req.Title) {
		data.ServiceCompany = info.ServiceCompany
		data.ClientCompany = info.ClientCompany
		info := h.Model.IsExist(data,false)
		if info != nil && info.Id != req.Id{
			return errors.New(mes.BadRequest("project-srv","项目名称已被占用").Error())
		}
	}
	if !gv.IsNull(req.Number) {
		info = h.Model.IsExist(&pb.Project{Number:req.Number},false)
		if info != nil && info.Id != req.Id{
			return errors.New(mes.BadRequest("project-srv","项目号已被占用").Error())
		}
	}

	err := h.Model.Update(req)
	if err != nil {
		return errors.New(mes.InternalServerError("project-srv",err.Error()).Error())
	}
	info = h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.BadRequest("project-srv","项目不存在").Error())
	}
	
	rsp.Project = &info.Project
	return nil
}

func (h *ProjectHandler) Delete(ctx context.Context,req *proto.IdRequest,rsp *proto.BoolResponse)error{
	log.Println("删除...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("project-srv","项目编号不合法").Error())
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.NotFound("project-srv","项目不存在").Error())
	}

	err := h.Model.Delete(req.Id)

	if err != nil {
		return errors.New(mes.InternalServerError("project-srv",err.Error()).Error())
	}
	rsp.Success = true
	return nil
}

func (h *ProjectHandler) Open(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("启用...")
	info,err := h.changeStatus(req.Id,config.OPEN_STATUS)
	if err != nil {
		return err
	}

	rsp.Project = &info.Project
	return nil
}

func (h *ProjectHandler) Close(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("禁用...")
	info,err := h.changeStatus(req.Id,config.CLOSE_STATUS)
	if err != nil {
		return err
	}

	rsp.Project = &info.Project
	return nil
}

func (h *ProjectHandler) Get(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("查询...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New("编号不合法")
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.NotFound("project-srv","项目不存在").Error())
	}

	rsp.Project = &info.Project
	return nil
}

func (h *ProjectHandler) GetPage(ctx context.Context,req *pb.PageRequest,rsp *pb.PageResponse)error{
	log.Println("根据页面查询...")
	//设置默认每页条数,页数
	count,err := h.Model.GetCount(req)
	if err != nil {
		return errors.New(mes.InternalServerError("project-srv",err.Error()).Error())
	}
	if *count == 0 {
		rsp.Page = &proto.Page{}
		return nil
	}
	lr := &pb.ListRequest{
		Default:&proto.ListRequest{},
		Project:req.Project,
	}

	if req.Default != nil {
		if req.Default.Size <= 0 || req.Default.Size > sconf.DefaultMaxPageSize{
			req.Default.Size = sconf.PageSize
		}
		if req.Default.Page <= 0 {
			req.Default.Page = sconf.Page
		}

		lr.Default = &proto.ListRequest{
			Start:(req.Default.Page-1)*req.Default.Size,
			Limit:req.Default.Size,
			Search:req.Default.Search,
			Ids:req.Default.Ids,
		}
	}else {
		req.Default = &proto.PageRequest{
			Size:sconf.PageSize,
			Page:sconf.Page,
		}
	}

	list,err := h.Model.GetList(lr)
	if err != nil {
		return errors.New(mes.InternalServerError("project-srv",err.Error()).Error())
	}

	page := &proto.Page{
		Total:*count,
		Index:req.Default.Page,
		TotalPage:int64(math.Ceil(float64(*count)/float64(req.Default.Size))),
		PageSize:req.Default.Size,
	}
	rsp.Page = page
	rsp.List = list
	return nil
}

func (h *ProjectHandler) GetList(ctx context.Context,req *pb.ListRequest,rsp *pb.ListResponse)error{
	log.Println("查询所有...")
	list,err := h.Model.GetList(req)
	if err != nil {
		return errors.New(mes.InternalServerError("project-srv",err.Error()).Error())
	}
	rsp.List = list
	return nil
}

func (h *ProjectHandler) changeStatus(id string,status int)(*database.Project,error){
	if validate.HasAndCheckString(gv.IsUUIDv4, id) == false {
		return nil,errors.New(mes.BadRequest("project-srv","项目编号不合法").Error())
	}

	info := h.Model.GetInfoById(id,false)
	if info == nil {
		return nil,errors.New(mes.NotFound("project-srv","项目不存在").Error())
	}

	u := &pb.Project{
		Id:id,
		Status:int64(status),
	}

	err := h.Model.Update(u)
	if err != nil {
		return nil,errors.New(mes.InternalServerError("project-srv",err.Error()).Error())
	}
	info = h.Model.GetInfoById(id,false)

	return info,nil
}

func (h *ProjectHandler) checkStatus(id string)(*database.Project,error){
	if validate.HasAndCheckString(gv.IsUUIDv4, id) == false {
		return nil,errors.New(mes.BadRequest("project-srv","项目编号不合法").Error())
	}

	info := h.Model.GetInfoById(id,true)
	if info == nil {
		return nil,errors.New(mes.NotFound("project-srv","项目不存在").Error())
	}
	return info,nil
}