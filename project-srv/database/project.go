package database

import (
	"github.com/asaskevich/govalidator"
	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
	"log"
	"pmm/public/config"
	"pmm/public/model"
	pb "pmm/public/proto/project"
	"pmm/public/utils"
	"time"
)

// 设置的表名
func (Project) TableName() string {
	return "project"
}

type Project struct {
	pb.Project
	model.Comm
}

/**
创建
 */
func (repo *Project) Create(data *pb.Project) (*Project,error) {
	d := &Project{
		Project:*data,
	}
	if err := Conn.Create(&d).Error; err != nil {
		return nil,err
	}
	return d,nil
}

/**
更新
condition 条件 参数1为是否更新密码 2位是否更新状态
 */
func (repo *Project) Update(data *pb.Project,condition... string) (error)  {
	d := &Project{}
	d.Project = *data

	query := Conn.Model(&d)
	//过滤参数
	if len(condition) >= 1 {
		query = query.Omit(condition...)
	}

	if err := query.Updates(d).Error;err != nil {
		return err
	}
	return nil
}

/**
 判断是否存在 存在返回查询到的结果
 */
func (repo *Project) IsExist(pb *pb.Project,isOpen bool) (*Project) {
	if pb == nil {
		return nil
	}

	d := &Project{}
	d.Title = pb.Title
	d.ServiceCompany = pb.ServiceCompany
	d.ClientCompany = pb.ClientCompany
	d.Number = pb.Number

	query := Conn
	if isOpen {
		query = query.Where("status = ?",config.OPEN_STATUS)
	}
	err := query.Where(pb).First(&d).Error

	if err == nil {
		return d
	}

	return nil
}

/**
根据id取得信息
 */
func (repo *Project) GetInfoById(id string,isOpen bool) (*Project) {
	if id == "" {
		return nil
	}

	d := &Project{}

	query := Conn.Where("id = ?",id)
	if isOpen {
		query = query.Where("status = ?",config.OPEN_STATUS)
	}
	err := query.First(&d).Error

	if err == nil {
		return d
	}

	return nil
}

/**
根据编号删除
 */
func (repo *Project) Delete(id ...string) (error) {
	str := ""
	if len(id) > 1 {
		str = "id in ("
		for _, value := range id {
			str = str+"'"+value+"',"
		}
		str = string([]rune(str)[:len(str)-1])+")"
	}else {
		str = "id = '" + id[0] +"'"
	}

	err := Conn.Where(str).Delete(&Project{}).Error

	if err != nil {
		return err
	}

	return nil
}

/**
取得列表
 */
func (repo *Project) GetList(req *pb.ListRequest,args ...interface{}) ([]*pb.Project,error) {
	var list []*pb.Project
	d := req.Project
	x := req.Project
	fs := utils.GetDbNameByStruct(Conn,pb.Project{})
	if !govalidator.IsNull(x.StartTime) {
		d.StartTime = ""
	}
	if !govalidator.IsNull(x.EndTime) {
		d.EndTime = ""
	}
	if !govalidator.IsNull(x.DeadLine) {
		d.DeadLine = ""
	}
	query := Conn.Model(&Project{}).Where(d)
	if !govalidator.IsNull(x.StartTime) {
		query = query.Where("create_at >= ?",x.StartTime)
	}
	if !govalidator.IsNull(x.EndTime) {
		query = query.Where("create_at <= ?",x.EndTime)
	}

	//过滤参数
	if len(fs) > 1 {
		query = query.Select(fs)
	}
	if req.Default != nil {
		if req.Default.Start > 0 {
			query = query.Offset(req.Default.Start)
		}
		if req.Default.Limit > 0 {
			query = query.Limit(req.Default.Limit)
		}
		if req.Default.Search != nil {
			query = utils.AndOrLikes(query,req.Default.Search,true)
		}
		if len(req.Default.Ids) > 0{
			query = query.Where(req.Default.Ids)
		}
	}

	if args != nil && len(args) > 0 {
		for _, value := range args {
			query = query.Where(value)
		}
	}
	err := query.Scan(&list).Error
	if err != nil {
		return nil,err
	}
	return list,nil
}

func (repo *Project) GetCount(req *pb.PageRequest,args ...interface{}) (*int64,error) {
	data := req.Project
	query := Conn.Model(&Project{}).Where(data)
	if req.Default != nil {
		if req.Default.Search != nil {
			query = utils.AndOrLikes(query,req.Default.Search,true)
		}
		if len(req.Default.Ids) > 0 {
			query = query.Where(req.Default.Ids)
		}
	}
	if args != nil && len(args) > 0 {
		for _, value := range args {
			query = query.Where(value)
		}
	}
	var count *int64
	err := query.Count(&count).Error
	if err != nil {
		return count,err
	}
	return count,nil
}

func (repo *Project) BeforeCreate(scope *gorm.Scope) error {
	uid, err := uuid.NewV4()
	if err != nil {
		log.Fatalf("created uuid error: %v\n", err)
	}
	scope.SetColumn("Id", uid.String())
	scope.SetColumn("CreatedAt", time.Now().Format("2006-01-02 15:04:05"))
	scope.SetColumn("UpdatedAt", time.Now().Format("2006-01-02 15:04:05"))
	scope.SetColumn("Status", config.OPEN_STATUS)
	return nil
}