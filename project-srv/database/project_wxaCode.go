package database

import (
	"github.com/jinzhu/gorm"
	"pmm/public/config"
	"pmm/public/model"
	pb "pmm/public/proto/project/wxaCode"
	"time"
)

// 设置的表名
func (ProjectWXACode) TableName() string {
	return "project_wxacode"
}

type ProjectWXACode struct {
	pb.ProjectWXACode
	model.Comm
}

/**
创建
 */
func (repo *ProjectWXACode) Create(data *pb.ProjectWXACode) (*ProjectWXACode,error) {
	d := &ProjectWXACode{
		ProjectWXACode:*data,
	}
	if err := Conn.Create(&d).Error; err != nil {
		return nil,err
	}
	return d,nil
}

/**
更新
condition 条件 参数1为是否更新密码 2位是否更新状态
 */
func (repo *ProjectWXACode) Update(data *pb.ProjectWXACode,condition... string) (error)  {
	d := &ProjectWXACode{}
	d.ProjectWXACode = *data

	query := Conn.Model(&d)
	//过滤参数
	if len(condition) >= 1 {
		query = query.Omit(condition...)
	}

	if err := query.Updates(d).Error;err != nil {
		return err
	}
	return nil
}


/**
根据id取得信息
 */
func (repo *ProjectWXACode) GetInfoById(id string,isOpen bool) (*ProjectWXACode) {
	if id == "" {
		return nil
	}

	d := &ProjectWXACode{}

	query := Conn.Where("id = ?",id)
	if isOpen {
		query = query.Where("status = ?",config.OPEN_STATUS)
	}
	err := query.First(&d).Error

	if err == nil {
		return d
	}

	return nil
}

/**
根据编号删除
 */
func (repo *ProjectWXACode) Delete(id ...string) (error) {
	str := ""
	if len(id) > 1 {
		str = "id in ("
		for _, value := range id {
			str = str+"'"+value+"',"
		}
		str = string([]rune(str)[:len(str)-1])+")"
	}else {
		str = "id = '" + id[0] +"'"
	}

	err := Conn.Where(str).Delete(&ProjectWXACode{}).Error

	if err != nil {
		return err
	}

	return nil
}



func (repo *ProjectWXACode) BeforeCreate(scope *gorm.Scope) error {
	scope.SetColumn("CreatedAt", time.Now().Format("2006-01-02 15:04:05"))
	scope.SetColumn("UpdatedAt", time.Now().Format("2006-01-02 15:04:05"))
	scope.SetColumn("Status", config.OPEN_STATUS)
	return nil
}

func (repo *ProjectWXACode) BeforeUpdate(scope *gorm.Scope) error {
	scope.SetColumn("UpdatedAt", time.Now().Format("2006-01-02 15:04:05"))
	return nil
}