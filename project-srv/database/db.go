package database

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"os"
	"fmt"
		"github.com/casbin/gorm-adapter"
)

//数据库实例
var Conn *gorm.DB

const (
	DEFAULT_DB_HOST = "www.greenment.cn:3306"
	DEFAULT_DB_USER = "root"
	DEFAULT_DB_PASSWORD = "greenment"
	DEFAULT_DB_NAME = "pmm"
)

func CreateConnection() (*gorm.DB, error) {
	host := os.Getenv("DB_HOST")
	user := os.Getenv("DB_USER")
	password := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")

	if host == ""{
		host = DEFAULT_DB_HOST
	}
	if user == ""{
		user = DEFAULT_DB_USER
	}
	if password == ""{
		password = DEFAULT_DB_PASSWORD
	}
	if dbName == ""{
		dbName = DEFAULT_DB_NAME
	}

	//gorm.Open("mysql","root:123456@tcp(127.0.0.1:3306)/testdb?charset=utf8")
	db,err := gorm.Open("mysql",
		fmt.Sprintf(
			"%s:%s@tcp(%s)/%s?charset=utf8&parseTime=True&loc=Local",
			user, password , host, dbName,
		),
	)
	Conn = db
	return db,err
}

func ConnAuthCasbin() (*gormadapter.Adapter) {
	host := os.Getenv("DB_HOST")
	user := os.Getenv("DB_USER")
	password := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")

	if host == ""{
		host = DEFAULT_DB_HOST
	}
	if user == ""{
		user = DEFAULT_DB_USER
	}
	if password == ""{
		password = DEFAULT_DB_PASSWORD
	}
	if dbName == ""{
		dbName = DEFAULT_DB_NAME
	}
	a := gormadapter.NewAdapter("mysql",
		fmt.Sprintf(
			"%s:%s@tcp(%s)/%s",
			user, password , host, dbName,
		),true)
	return a
}
/*func CreateConnection() {
	host := os.Getenv("DB_HOST")
	user := os.Getenv("DB_USER")
	password := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")

	if host == ""{
		host = DEFAULT_DB_HOST
	}
	if user == ""{
		user = DEFAULT_DB_USER
	}
	if password == ""{
		password = DEFAULT_DB_PASSWORD
	}
	if dbName == ""{
		dbName = DEFAULT_DB_NAME
	}

	//gorm.Open("mysql","root:123456@tcp(127.0.0.1:3306)/testdb?charset=utf8")

	mysqlDSN := user+":"+password+"@("+host+")/"+dbName+"?charset=utf8"
	db,err := gorm.Open("mysql",mysqlDSN)
	if err != nil {
		log.Fatalf("数据库连接失败: %v\n",err)
	}

	db.LogMode(true)
	DbConn = db
}*/
