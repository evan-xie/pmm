package plugin

import (
	"fmt"
	"github.com/asaskevich/govalidator"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	mes "github.com/micro/go-micro/errors"
	"php"
	"pmm/public/config"
	"pmm/public/proto"
	"pmm/public/utils"
	"pmm/user-srv/utils/token"
	"reflect"
	"strconv"
	"time"
)

func FmtErr(err error, c echo.Context) error {
	e := mes.Parse(err.Error())
	er := map[string]string{
		"error":e.Detail,
	}
	return c.JSON(int(e.Code),er)
}

func FmtDate(o,obj interface{}) {
	immutable := reflect.ValueOf(o)
	mutable := reflect.ValueOf(obj).Elem()

	fs := []string{"CreatedAt","UpdatedAt"}
	for _, f := range fs {
		v := immutable.FieldByName(f).String()
		if govalidator.IsNull(v) {
			continue
		}
		t, err := time.Parse("2006-01-02T15:04:05+08:00", v)
		if err != nil {
			t,_ = time.Parse("2006-01-02 15:04:05",v)
		}
		mutable.FieldByName(f).SetString(t.Format("2006-01-02 15:04:05"))
	}
}

func FmtList(list interface{}) interface{} {
	ls := reflect.ValueOf(list)
	if ls.Kind() == reflect.Array {
		if ls.Len() > 0{
			return list
		}
	}
	ll := map[string]interface{}{}
	return ll
}

func GetParams(c echo.Context, s interface{}) {
	fs := utils.GetStructFieldsMap(s)
	mutable := reflect.ValueOf(s).Elem()

	for n, f := range fs {
		t := mutable.FieldByName(n).Kind()
		p := GetParam(c,f)
		switch t {
			case reflect.String:
				mutable.FieldByName(n).SetString(p)
			case reflect.Int64:
				i,_ := strconv.ParseInt(p,10,64)
				mutable.FieldByName(n).SetInt(i)
			case reflect.Bool:
				var b bool
				if p == "1" {
					b = true
				}else {
					b = false
				}
				mutable.FieldByName(n).SetBool(b)
			case reflect.Float64:
				i,_ := strconv.ParseFloat(p,64)
				mutable.FieldByName(n).SetFloat(i)
		}
	}
}

func GetParam(c echo.Context,field string) string {
	p := c.Param(field)
	if !govalidator.IsNull(p) {
		return p
	}
	p = c.QueryParam(field)
	if !govalidator.IsNull(p) {
		return p
	}
	p = c.FormValue(field)
	if !govalidator.IsNull(p) {
		return p
	}
	return ""
}

func GetIdByRequest(c echo.Context) *proto.IdRequest {
	u := &proto.IdRequest{}
	u.Id = GetParam(c,"id")
	return u
}

func GetListRequestByRequest(c echo.Context,fields []string) *proto.ListRequest {
	req := &proto.ListRequest{}
	ids := GetParam(c,"ids")
	if !govalidator.IsNull(ids) {
		ida := php.Explode(",",ids)
		req.Ids = ida
	}

	start := GetParam(c,"start")
	if !govalidator.IsNull(start) {
		si,_ := strconv.ParseInt(start,10,64)
		req.Start = si
	}

	limit := GetParam(c,"limit")
	if !govalidator.IsNull(limit) {
		l,_ := strconv.ParseInt(limit,10,64)
		req.Limit = l
	}

	search := GetParam(c,"search")
	if !govalidator.IsNull(search) {
		m := make(map[string]string)
		for _, f := range fields {
			m[f] = search
		}
		req.Search = m
	}

	return req
}

func GetPageRequestByRequest(c echo.Context,fields []string) *proto.PageRequest {
	req := &proto.PageRequest{}
	ids := GetParam(c,"ids")
	if !govalidator.IsNull(ids) {
		ida := php.Explode(",",ids)
		req.Ids = ida
	}

	page := GetParam(c,"page")
	if !govalidator.IsNull(page) {
		p,_ := strconv.ParseInt(page,10,64)
		req.Page = p
	}else {
		req.Page = config.Page
	}

	size := GetParam(c,"size")
	if !govalidator.IsNull(size) {
		l,_ := strconv.ParseInt(size,10,64)
		req.Size = l
	}

	search := GetParam(c,"search")
	if !govalidator.IsNull(search) {
		m := make(map[string]string)
		for _, f := range fields {
			m[f] = search
		}
		req.Search = m
	}

	return req
}

func GetUid(c echo.Context) string {
	t := c.Get("user").(*jwt.Token)
	fmt.Println("token:"+t.Raw)
	u:=token.TokenUtils{}
	cu,_ := u.Decode(t.Raw,"access")
	return cu.Subject
}

func GetToken(c echo.Context) string{
	t := c.Get("user").(*jwt.Token)
	return t.Raw
}