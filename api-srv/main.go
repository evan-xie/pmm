package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"net/http"
	"pmm/api-srv/handler"
	"pmm/node-srv/config"
	"time"
)


func main() {
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORS())

	e.GET("/", func(c echo.Context) error {
		m := map[string]string{
			"title":"项目管理1.0",
			"author":"evan.xie",
		}
		return c.JSON(http.StatusOK,m)
	})

	userSrv := new(handler.UserService)
	e.POST("/register",userSrv.Register)
	e.GET("/code/:key",userSrv.Code)
	e.POST("/login",userSrv.Login)
	e.POST("/wxResister",userSrv.WxRegister)
	e.POST("/wxResisterByUserInfo",userSrv.WxRegisterByUserInfo)
	e.POST("/wxLogin",userSrv.WxLogin)


	userGroup := e.Group("/user",JwtCheck())
	UserGroup(*userGroup) //用户
	e.PUT("/changePwd",userSrv.ChangeLoginPwd,JwtCheck())

	companyGroup := e.Group("/company",JwtCheck())
	CompanyGroup(*companyGroup)//公司

	roleGroup := e.Group("/role",JwtCheck())
	RoleGroup(*roleGroup)

	ruleGroup := e.Group("/rule",JwtCheck())
	RuleGroup(*ruleGroup)//规则

	projectGroup := e.Group("/project",JwtCheck())
	ProjectGroup(*projectGroup)

	tagGroup := e.Group("/tag",JwtCheck())
	TagGroup(*tagGroup)

	clientGroup := e.Group("/client",JwtCheck())
	ClientGroup(*clientGroup)

	fileSrv := new(handler.FileService)
	f := e.Group("/oss")
	f.GET("/callback",fileSrv.GetOssCallback)
	f.POST("/callback",fileSrv.OssCallback)

	s := &http.Server{
		Addr:         ":8000",
		ReadTimeout:  20 * time.Minute,
		WriteTimeout: 20 * time.Minute,
	}
	e.Logger.Fatal(e.StartServer(s))
}

func JwtCheck() echo.MiddlewareFunc {
	JWT := middleware.JWTWithConfig(middleware.JWTConfig{
		SigningKey: []byte(config.AccessTokenPrivateKey),
		TokenLookup: "header:access-token",
		AuthScheme:"",
	})
	return JWT
}

func UserGroup(g echo.Group)  {
	userSrv := new(handler.UserService)
	g.GET("/:id",userSrv.Get)
	g.GET("",userSrv.Page)
	g.GET("/page/:page",userSrv.Page)
	g.GET("/search/:search",userSrv.Page)
	g.GET("/search/:search/:page",userSrv.Page)
	g.GET("/info",userSrv.GetInfo)

	g.POST("",userSrv.Create)
	g.DELETE("/:id",userSrv.Delete)
	g.PUT("/:id",userSrv.Update)
	g.PUT("/:id/open",userSrv.Open)
	g.PUT("/:id/close",userSrv.Close)
	g.PUT("/:id/changePwd",userSrv.ChangePwd)

	g.PUT("/info",userSrv.UpdateInfo)


	userRoleSrv := new(handler.UserRoleService)
	g.GET("/:user_id/role",userRoleSrv.RolePage)
	g.GET("/:user_id/role/:page",userRoleSrv.RolePage)

	g.POST("/role",userRoleSrv.Create)
	g.DELETE("/role/:user_id/:role_id",userRoleSrv.Delete)
	g.DELETE("/:user_id/role",userRoleSrv.DeleteAllRoleForUser)

	projectUserSrv := new(handler.ProjectUserService)
	g.GET("/:user_id/project",projectUserSrv.GetProjectByUser)
	g.GET("/:user_id/project/page/:page",projectUserSrv.GetProjectByUser)
	g.GET("/:user_id/project/search/:search",projectUserSrv.GetProjectByUser)
	g.GET("/:user_id/project/search/:search/:page",projectUserSrv.GetProjectByUser)
}

func ClientGroup(g echo.Group)  {
	projectClientSrv := new(handler.ProjectClientService)
	g.GET("/:user_id/project",projectClientSrv.GetProjectByUser)
	g.GET("/:user_id/project/page/:page",projectClientSrv.GetProjectByUser)
	g.GET("/:user_id/project/search/:search",projectClientSrv.GetProjectByUser)
	g.GET("/:user_id/project/search/:search/:page",projectClientSrv.GetProjectByUser)
}

func CompanyGroup(g echo.Group)  {
	companySrv := new(handler.CompanyService)
	//companySrv.Init()
	userSrv := new(handler.UserService)
	g.GET("",companySrv.Page)
	g.GET("/list",companySrv.List)
	g.GET("/page/:page",companySrv.Page)
	g.GET("/search/:search",companySrv.Page)
	g.GET("/search/:search/:page",companySrv.Page)
	g.GET("/:id",companySrv.Get)
	g.GET("/:company_id/user",userSrv.Page)
	g.GET("/:company_id/user/list",userSrv.List)
	g.POST("",companySrv.Create)
	g.PUT("/:id",companySrv.Update)
	g.PUT("/:id/open",companySrv.Open)
	g.PUT("/:id/close",companySrv.Close)
	g.DELETE("/:id",companySrv.Delete)
}

func RoleGroup(g echo.Group)  {
	roleSrv := new(handler.RoleService)
	g.GET("",roleSrv.Page)
	g.GET("/page/:page",roleSrv.Page)
	g.GET("/search/:search",roleSrv.Page)
	g.GET("/search/:search/:page",roleSrv.Page)
	g.GET("/:id",roleSrv.Get)
	g.POST("",roleSrv.Create)
	g.PUT("/:id",roleSrv.Update)
	g.PUT("/:id/open",roleSrv.Open)
	g.PUT("/:id/close",roleSrv.Close)
	g.DELETE("/:id",roleSrv.Delete)

	userRoleSrv := new(handler.UserRoleService)
	g.GET("/:role_id/user",userRoleSrv.UserPage)
	g.GET("/:role_id/user/:page",userRoleSrv.UserPage)
	g.DELETE("/:role_id/user",userRoleSrv.DeleteAllUserForRole)

	roleRuleSrv := new(handler.RoleRuleService)
	g.GET("/:role_id/rule",roleRuleSrv.RulePage)
	g.GET("/:role_id/rule/:page",userRoleSrv.RolePage)

	g.POST("/rule",roleRuleSrv.Create)
	g.DELETE("/rule/:role_id/:rule_id",roleRuleSrv.Delete)
	g.DELETE("/:role_id/rule",roleRuleSrv.DeleteRules)
}

func RuleGroup(g echo.Group)  {
	ruleSrv := new(handler.RuleService)
	g.GET("",ruleSrv.Page)
	g.GET("/page/:page",ruleSrv.Page)
	g.GET("/search/:search",ruleSrv.Page)
	g.GET("/search/:search/:page",ruleSrv.Page)
	g.GET("/:id",ruleSrv.Get)
	g.POST("",ruleSrv.Create)
	g.PUT("/:id",ruleSrv.Update)
	g.PUT("/:id/open",ruleSrv.Open)
	g.PUT("/:id/close",ruleSrv.Close)
	g.DELETE("/:id",ruleSrv.Delete)
}

func ProjectGroup(g echo.Group)  {
	projectSrv := new(handler.ProjectService)
	g.GET("",projectSrv.Page)
	g.GET("/page/:page",projectSrv.Page)
	g.GET("/search/:search",projectSrv.Page)
	g.GET("/search/:search/:page",projectSrv.Page)
	g.GET("/:id",projectSrv.Get)
	g.POST("",projectSrv.Create)
	g.PUT("/:id",projectSrv.Update)
	g.PUT("/:id/open",projectSrv.Open)
	g.PUT("/:id/close",projectSrv.Close)
	g.DELETE("/:id",projectSrv.Delete)

	projectUserSrv := new(handler.ProjectUserService)
	g.GET("/user/:id",projectUserSrv.Get)
	g.GET("/:project_id/user/list",projectUserSrv.GetUserByProjectList)
	g.GET("/:project_id/user",projectUserSrv.GetUserByProject)
	g.GET("/:project_id/user/page/:page",projectUserSrv.GetUserByProject)
	g.GET("/:project_id/user/search/:search",projectUserSrv.GetUserByProject)
	g.GET("/:project_id/user/search/:search/:page",projectUserSrv.GetUserByProject)
	
	g.POST("/:project_id/user",projectUserSrv.Create)
	g.PUT("/user/:id",projectUserSrv.Update)
	g.PUT("/user/:id/open",projectUserSrv.Open)
	g.PUT("/user/:id/close",projectUserSrv.Close)
	g.DELETE("/user/:id",projectUserSrv.Delete)

	nodeSrv := new(handler.NodeService)
	g.GET("/:project_id/node/list",nodeSrv.List)
	g.GET("/:project_id/node/tree",nodeSrv.Tree)
	g.GET("/:project_id/node",nodeSrv.Page)
	g.GET("/:project_id/node/page/:page",nodeSrv.Page)
	g.GET("/:project_id/node/search/:search",nodeSrv.Page)
	g.GET("/:project_id/node/search/:search/:page",nodeSrv.Page)
	g.GET("/node/:id",nodeSrv.Get)
	g.POST("/:project_id/node",nodeSrv.Create)
	g.PUT("/node/:id",nodeSrv.Update)
	g.PUT("/node/:id/open",nodeSrv.Open)
	g.PUT("/node/:id/close",nodeSrv.Close)
	g.DELETE("/node/:id",nodeSrv.Delete)

	nodeConfigSrv := new(handler.NodeConfigService)
	g.GET("/:id/node/config",nodeConfigSrv.Get)
	g.POST("/:id/node/config",nodeConfigSrv.Create)
	g.PUT("/:id/node/config",nodeConfigSrv.Update)
	g.PUT("/:id/node/config/open",nodeConfigSrv.Open)
	g.PUT("/:id/node/config/close",nodeConfigSrv.Close)
	g.DELETE("/:id/node/config",nodeConfigSrv.Delete)

	projectClientSrv := new(handler.ProjectClientService)
	g.GET("/client/:id",projectClientSrv.Get)
	g.GET("/:project_id/client/list",projectClientSrv.GetUserByProjectList)
	g.GET("/:project_id/client",projectClientSrv.GetUserByProject)
	g.GET("/:project_id/client/page/:page",projectClientSrv.GetUserByProject)
	g.GET("/:project_id/client/search/:search",projectClientSrv.GetUserByProject)
	g.GET("/:project_id/client/search/:search/:page",projectClientSrv.GetUserByProject)

	g.POST("/:project_id/client",projectClientSrv.Create)
	g.PUT("/client/:id",projectClientSrv.Update)
	g.PUT("/client/:id/open",projectClientSrv.Open)
	g.PUT("/client/:id/close",projectClientSrv.Close)
	g.DELETE("/client/:id",projectClientSrv.Delete)

	projectWXACodeSrv := new(handler.ProjectWXACodeService)
	g.GET("/:id/wxacode",projectWXACodeSrv.Get)
	g.POST("/:id/wxacode/check",projectWXACodeSrv.CheckCode)
	g.POST("/:id/wxacode",projectWXACodeSrv.Create)
	g.PUT("/:id/wxacode",projectWXACodeSrv.Update)
	g.PUT("/:id/wxacode/open",projectWXACodeSrv.Open)
	g.PUT("/:id/wxacode/close",projectWXACodeSrv.Close)
	g.DELETE("/:id/wxacode",projectWXACodeSrv.Delete)

	projectReportSrv := new(handler.ReportService)
	g.GET("/:project_id/report",projectReportSrv.Page)
	g.GET("/:project_id/report/:page",projectReportSrv.Page)
	g.GET("/:project_id/report/:search",projectReportSrv.Page)
	g.GET("/:project_id/report/search/:search/:page",projectReportSrv.Page)
	g.GET("/:project_id/report/list",projectReportSrv.List)
	g.GET("/:project_id/report/list/:start",projectReportSrv.List)
	g.GET("/:project_id/report/list/search/:search",projectReportSrv.List)
	g.GET("/:project_id/report/list/search/:search/:start",projectReportSrv.List)
	g.GET("/:project_id/report/html",projectReportSrv.GetReportHtml)
	g.POST("/:project_id/report",projectReportSrv.Create)
	g.PUT("/report/:id",projectReportSrv.Update)
	g.PUT("/report/:id/open",projectReportSrv.Open)
	g.PUT("/report/:id/close",projectReportSrv.Close)
	g.DELETE("/report/:id",projectReportSrv.Delete)
	g.POST("/report/:id/sendMail",projectReportSrv.SendMail)
}

func TagGroup(g echo.Group)  {
	tagSrv := new(handler.TagService)
	g.GET("",tagSrv.Page)
	g.GET("/page/:page",tagSrv.Page)
	g.GET("/search/:search",tagSrv.Page)
	g.GET("/search/:search/:page",tagSrv.Page)
	g.GET("/:id",tagSrv.Get)
	g.GET("/:id/tree",tagSrv.Tree)
	g.POST("",tagSrv.CreateTag)
	g.POST("/folder",tagSrv.CreateFolder)
	g.PUT("/:id",tagSrv.Update)
	g.PUT("/:id/open",tagSrv.Open)
	g.PUT("/:id/close",tagSrv.Close)
	g.DELETE("/:id",tagSrv.Delete)

	tagGroupSrv := new(handler.TagGroupService)
	g.GET("/group",tagGroupSrv.Page)
	g.GET("/group/page/:page",tagGroupSrv.Page)
	g.GET("/group/search/:search",tagGroupSrv.Page)
	g.GET("/group/search/:search/:page",tagGroupSrv.Page)
	g.GET("/group/:id",tagGroupSrv.Get)
	g.GET("/group/:tag_group/tree",tagSrv.Tree)

	g.POST("/group",tagGroupSrv.Create)
	g.PUT("/group/:id",tagGroupSrv.Update)
	g.PUT("/group/:id/open",tagGroupSrv.Open)
	g.PUT("/group/:id/close",tagGroupSrv.Close)
	g.DELETE("/group/:id",tagGroupSrv.Delete)
}

/*	e.GET("/user/:id",userSrv.Get)
	e.GET("/user",userSrv.Page)
	e.GET("/user/page/:page",userSrv.Page)
	e.GET("/user/search/:search",userSrv.Page)
	e.GET("/user/search/:search/:page",userSrv.Page)
	e.GET("/user/info",userSrv.GetInfo)

	e.POST("/user",userSrv.Create)
	e.DELETE("/user/:id",userSrv.Delete)
	e.PUT("/user/:id",userSrv.Update)
	e.PUT("/user/:id/open",userSrv.Open)
	e.PUT("/user/:id/close",userSrv.Close)
	e.PUT("/user/:id/changePwd",userSrv.ChangePwd)
	e.PUT("/user/info",userSrv.UpdateInfo)

	companySrv := new(handler.CompanyService)
	e.GET("/company",companySrv.Page)
	e.GET("/company/list",companySrv.List)
	e.GET("/company/page/:page",companySrv.Page)
	e.GET("/company/search/:search",companySrv.Page)
	e.GET("/company/search/:search/:page",companySrv.Page)
	e.GET("/company/:id",companySrv.Get)
	e.GET("/company/:company_id/user",userSrv.Page)
	e.GET("/company/:company_id/user/list",userSrv.List)

	e.POST("/company",companySrv.Create)
	e.PUT("/company/:id",companySrv.Update)
	e.PUT("/company/:id/open",companySrv.Open)
	e.PUT("/company/:id/close",companySrv.Close)
	e.DELETE("/company/:id",companySrv.Delete)

	roleSrv := new(handler.RoleService)
	e.GET("/role",roleSrv.Page)
	e.GET("/role/page/:page",roleSrv.Page)
	e.GET("/role/search/:search",roleSrv.Page)
	e.GET("/role/search/:search/:page",roleSrv.Page)
	e.GET("/role/:id",roleSrv.Get)

	e.POST("/role",roleSrv.Create)
	e.PUT("/role/:id",roleSrv.Update)
	e.PUT("/role/:id/open",roleSrv.Open)
	e.PUT("/role/:id/close",roleSrv.Close)
	e.DELETE("/role/:id",roleSrv.Delete)

	projectSrv := new(handler.ProjectService)
	e.GET("/project",projectSrv.Page)
	e.GET("/project/page/:page",projectSrv.Page)
	e.GET("/project/search/:search",projectSrv.Page)
	e.GET("/project/search/:search/:page",projectSrv.Page)
	e.GET("/project/:id",projectSrv.Get)

	e.POST("/project",projectSrv.Create)
	e.PUT("/project/:id",projectSrv.Update)
	e.PUT("/project/:id/open",projectSrv.Open)
	e.PUT("/project/:id/close",projectSrv.Close)
	e.DELETE("/project/:id",projectSrv.Delete)

	projectUserSrv := new(handler.ProjectUserService)
	//e.GET("/project/user",projectUserSrv.Page)
	//e.GET("/project/user/page/:page",projectUserSrv.Page)
	//e.GET("/project/user/search/:search",projectUserSrv.Page)
	//e.GET("/project/user/search/:search/:page",projectUserSrv.Page)
	e.GET("/project/user/:id",projectUserSrv.Get)
	e.GET("/project/:project_id/user/list",projectUserSrv.GetUserByProjectList)
	e.GET("/project/:project_id/user",projectUserSrv.GetUserByProject)
	e.GET("/project/:project_id/user/page/:page",projectUserSrv.GetUserByProject)
	e.GET("/project/:project_id/user/search/:search",projectUserSrv.GetUserByProject)
	e.GET("/project/:project_id/user/search/:search/:page",projectUserSrv.GetUserByProject)

	e.GET("/user/:user_id/project",projectUserSrv.GetProjectByUser)
	e.GET("/user/:user_id/project/page/:page",projectUserSrv.GetProjectByUser)
	e.GET("/user/:user_id/project/search/:search",projectUserSrv.GetProjectByUser)
	e.GET("/user/:user_id/project/search/:search/:page",projectUserSrv.GetProjectByUser)

	e.POST("/project/:project_id/user",projectUserSrv.Create)
	e.PUT("/project/user/:id",projectUserSrv.Update)
	e.PUT("/project/user/:id/open",projectUserSrv.Open)
	e.PUT("/project/user/:id/close",projectUserSrv.Close)
	e.DELETE("/project/user/:id",projectUserSrv.Delete)

	ruleSrv := new(handler.RuleService)
	e.GET("/rule",ruleSrv.Page)
	e.GET("/rule/page/:page",ruleSrv.Page)
	e.GET("/rule/search/:search",ruleSrv.Page)
	e.GET("/rule/search/:search/:page",ruleSrv.Page)
	e.GET("/rule/:id",ruleSrv.Get)

	e.POST("/rule",ruleSrv.Create)
	e.PUT("/rule/:id",ruleSrv.Update)
	e.PUT("/rule/:id/open",ruleSrv.Open)
	e.PUT("/rule/:id/close",ruleSrv.Close)
	e.DELETE("/rule/:id",ruleSrv.Delete)

	roleRuleSrv := new(handler.RoleRuleService)
	e.GET("/role/:role_id/rule",roleRuleSrv.RulePage)
	e.GET("/role/:role_id/rule/:page",userRoleSrv.RolePage)

	e.POST("/role/rule",roleRuleSrv.Create)
	e.DELETE("/role/rule/:role_id/:rule_id",roleRuleSrv.Delete)
	e.DELETE("/role/:role_id/rule",roleRuleSrv.DeleteRules)
	e.DELETE("/rule/:rule_id/role",roleRuleSrv.DeleteRoles)

	userRoleSrv := new(handler.UserRoleService)
	e.GET("/user/:user_id/role",userRoleSrv.RolePage)
	e.GET("/user/:user_id/role/:page",userRoleSrv.RolePage)
	e.GET("/role/:role_id/user",userRoleSrv.UserPage)
	e.GET("/role/:role_id/user/:page",userRoleSrv.UserPage)

	e.POST("/user/role",userRoleSrv.Create)
	e.DELETE("/user/role/:user_id/:role_id",userRoleSrv.Delete)
	e.DELETE("/user/:user_id/role",userRoleSrv.DeleteAllRoleForUser)
	e.DELETE("/role/:role_id/user",userRoleSrv.DeleteAllUserForRole)

	nodeSrv := new(handler.NodeService)
	e.GET("/project/:project_id/node/list",nodeSrv.List)
	e.GET("/project/:project_id/node",nodeSrv.Page)
	e.GET("/project/:project_id/node/page/:page",nodeSrv.Page)
	e.GET("/project/:project_id/node/search/:search",nodeSrv.Page)
	e.GET("/project/:project_id/node/search/:search/:page",nodeSrv.Page)
	e.GET("/project/node/:id",nodeSrv.Get)

	e.POST("/project/:project_id/node",nodeSrv.Create)
	e.PUT("/project/node/:id",nodeSrv.Update)
	e.PUT("/project/node/:id/open",nodeSrv.Open)
	e.PUT("/project/node/:id/close",nodeSrv.Close)
	e.DELETE("/project/node/:id",nodeSrv.Delete)

	nodeConfigSrv := new(handler.NodeConfigService)
	e.GET("/project/:id/node/config",nodeConfigSrv.Get)

	e.POST("/project/:id/node/config",nodeConfigSrv.Create)
	e.PUT("/project/:id/node/config",nodeConfigSrv.Update)
	e.PUT("/project/:id/node/config/open",nodeConfigSrv.Open)
	e.PUT("/project/:id/node/config/close",nodeConfigSrv.Close)
	e.DELETE("/project/:id/node/config",nodeConfigSrv.Delete)

	tagSrv := new(handler.TagService)
	e.GET("/tag",tagSrv.Page)
	e.GET("/tag/page/:page",tagSrv.Page)
	e.GET("/tag/search/:search",tagSrv.Page)
	e.GET("/tag/search/:search/:page",tagSrv.Page)
	e.GET("/tag/:id",tagSrv.Get)
	e.GET("/tag/:id/tree",tagSrv.Tree)

	e.POST("/tag",tagSrv.CreateTag)
	e.POST("/tag/folder",tagSrv.CreateFolder)
	e.PUT("/tag/:id",tagSrv.Update)
	e.PUT("/tag/:id/open",tagSrv.Open)
	e.PUT("/tag/:id/close",tagSrv.Close)
	e.DELETE("/tag/:id",tagSrv.Delete)

	tagGroupSrv := new(handler.TagGroupService)
	e.GET("/tag/group",tagGroupSrv.Page)
	e.GET("/tag/group/page/:page",tagGroupSrv.Page)
	e.GET("/tag/group/search/:search",tagGroupSrv.Page)
	e.GET("/tag/group/search/:search/:page",tagGroupSrv.Page)
	e.GET("/tag/group/:id",tagGroupSrv.Get)
	e.GET("/tag/group/:tag_group/tree",tagSrv.Tree)

	e.POST("/tag/group",tagGroupSrv.Create)
	e.PUT("/tag/group/:id",tagGroupSrv.Update)
	e.PUT("/tag/group/:id/open",tagGroupSrv.Open)
	e.PUT("/tag/group/:id/close",tagGroupSrv.Close)
	e.DELETE("/tag/group/:id",tagGroupSrv.Delete)



*/