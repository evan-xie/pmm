package handler

import (
	"context"
	"fmt"
	"github.com/labstack/echo"
	"github.com/micro/go-micro/client"
	"net/http"
	"pmm/api-srv/plugin"
	"pmm/public/config"
	"pmm/public/proto/project/user"
	"pmm/public/proto/project/wxaCode"
)

var projectWxACodeCli = wxaCode.NewProjectWXACodeServiceClient(config.Namespace+"srv.project",client.DefaultClient)

type ProjectWXACodeService struct {
}

//新建
func (s ProjectWXACodeService) Create(c echo.Context) error {
	p := plugin.GetIdByRequest(c)

	rsp,err := projectWxACodeCli.Create(context.TODO(),p)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	plugin.FmtDate(*rsp.ProjectWXACode,rsp.ProjectWXACode)
	return c.JSON(http.StatusCreated,rsp.ProjectWXACode)
}

//更新
func (s ProjectWXACodeService) Update(c echo.Context) error {
	p := s.getParamByRequest(c)

	rsp,err := projectWxACodeCli.Update(context.TODO(),p)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.ProjectWXACode,rsp.ProjectWXACode)
		return c.JSON(http.StatusCreated,rsp.ProjectWXACode)
	}
}

//删除
func (s ProjectWXACodeService) Delete(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := projectWxACodeCli.Delete(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		return c.JSON(http.StatusOK,rsp)
	}
}

//启用
func (s ProjectWXACodeService) Open(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := projectWxACodeCli.Open(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.ProjectWXACode,rsp.ProjectWXACode)
		return c.JSON(http.StatusCreated,rsp.ProjectWXACode)
	}
}

//禁用
func (s ProjectWXACodeService) Close(c echo.Context) error {
	d := plugin.GetIdByRequest(c)


	rsp,err := projectWxACodeCli.Close(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.ProjectWXACode,rsp.ProjectWXACode)
		return c.JSON(http.StatusCreated,rsp.ProjectWXACode)
	}
}

//取得信息
func (s ProjectWXACodeService) Get(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := projectWxACodeCli.Get(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.ProjectWXACode,rsp.ProjectWXACode)
		return c.JSON(http.StatusCreated,rsp.ProjectWXACode)
	}
}

//取得信息
func (s ProjectWXACodeService) CheckCode(c echo.Context) error {
	d := &wxaCode.CheckCodeReq{}
	plugin.GetParams(c,d)

	rsp,err := projectWxACodeCli.CheckCode(context.TODO(),d)
	if err != nil {
		fmt.Println("错误"+err.Error())
		return plugin.FmtErr(err,c)
	}

/*	if rsp.Success != true  {
		return c.JSON(http.StatusOK,rsp)
	}*/
	fmt.Println("返回结果")
	fmt.Println(rsp)
	//return c.JSON(http.StatusOK,rsp)
	//新增关系
	uid := plugin.GetUid(c)
	fmt.Println("取得UID:"+uid)
	r := &user.ListRequest{
		ProjectUser:&user.ProjectUser{
			UserId:uid,
			ProjectId:d.Id,
		},
	}

	res,err := projectUserCli.GetList(context.TODO(),r)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	if len(res.List) > 0 {
		info := res.List[0]
		plugin.FmtDate(*info,info)
		return c.JSON(http.StatusOK,info)
	}
	fmt.Println("查询是否存在完成")
	//添加关系
	data := r.ProjectUser
	data.IsClient = config.OPEN_STATUS
	r1,err := projectUserCli.Create(context.TODO(),data)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	fmt.Println("添加关系完成")
	r1.ProjectUser.IsManager = 0
	r1.ProjectUser.IsDirectors = 0
	plugin.FmtDate(*r1.ProjectUser,r1.ProjectUser)
	return c.JSON(http.StatusOK,r1.ProjectUser)
}

func (s ProjectWXACodeService) getParamByRequest(c echo.Context) *wxaCode.ProjectWXACode {
	u := &wxaCode.ProjectWXACode{}
	plugin.GetParams(c,u)
	return u
}
