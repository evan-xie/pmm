package handler

import (
	"context"
	"github.com/labstack/echo"
	"github.com/micro/go-micro/client"
	"net/http"
	"pmm/api-srv/plugin"
	"pmm/public/config"
	"pmm/public/proto/rule"
)

var ruleCli = rule.NewRuleServiceClient(config.Namespace+"srv.user",client.DefaultClient)

type RuleService struct {
}

//新建
func (s RuleService) Create(c echo.Context) error {
	p := s.getParamByRequest(c)

	rsp,err := ruleCli.Create(context.TODO(),p)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	return c.JSON(http.StatusCreated,rsp.Rule)
}

//更新
func (s RuleService) Update(c echo.Context) error {
	p := s.getParamByRequest(c)

	rsp,err := ruleCli.Update(context.TODO(),p)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Rule,rsp.Rule)
		return c.JSON(http.StatusCreated,rsp.Rule)
	}
}

//删除
func (s RuleService) Delete(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := ruleCli.Delete(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		return c.JSON(http.StatusOK,rsp)
	}
}

//启用
func (s RuleService) Open(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := ruleCli.Open(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Rule,rsp.Rule)
		return c.JSON(http.StatusCreated,rsp.Rule)
	}
}

//禁用
func (s RuleService) Close(c echo.Context) error {
	d := plugin.GetIdByRequest(c)


	rsp,err := ruleCli.Close(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Rule,rsp.Rule)
		return c.JSON(http.StatusCreated,rsp.Rule)
	}
}

//取得信息
func (s RuleService) Get(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := ruleCli.Get(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Rule,rsp.Rule)
		return c.JSON(http.StatusCreated,rsp.Rule)
	}
}


func (s RuleService) List(c echo.Context) error {
	u := s.getParamByRequest(c)
	d := plugin.GetListRequestByRequest(c,[]string{"name","remark","domain","title"})
	req := &rule.ListRequest{
		Default:d,
		Rule:u,
	}

	rsp,err := ruleCli.GetList(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		for _, value := range rsp.List {
			plugin.FmtDate(value,&value)
		}
		return c.JSON(http.StatusCreated,plugin.FmtList(rsp.List))
	}
}

func (s RuleService) Page(c echo.Context) error {
	u := s.getParamByRequest(c)
	d := plugin.GetPageRequestByRequest(c,[]string{"name","remark","domain","title"})
	req := &rule.PageRequest{
		Default:d,
		Rule:u,
	}

	rsp,err := ruleCli.GetPage(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		for key, value := range rsp.List {
			v := *value
			plugin.FmtDate(v,&v)
			rsp.List[key] = &v
		}
		return c.JSON(http.StatusCreated,rsp)
	}
}

func (s RuleService) getParamByRequest(c echo.Context) *rule.Rule {
	u := &rule.Rule{}
	plugin.GetParams(c,u)
	return u
}
