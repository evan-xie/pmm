package handler

import (
	"context"
	"github.com/labstack/echo"
	"github.com/micro/go-micro/client"
	"net/http"
	"pmm/api-srv/plugin"
	"pmm/public/config"
	"pmm/public/proto/project"
)

var projectCli = project.NewProjectServiceClient(config.Namespace+"srv.project",client.DefaultClient)

type ProjectService struct {
}

//新建
func (s ProjectService) Create(c echo.Context) error {
	p := s.getParamByRequest(c)
	p.ServiceCompany = DefaultCompany.Id

	rsp,err := projectCli.Create(context.TODO(),p)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	return c.JSON(http.StatusCreated,rsp.Project)
}

//更新
func (s ProjectService) Update(c echo.Context) error {
	p := s.getParamByRequest(c)

	rsp,err := projectCli.Update(context.TODO(),p)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Project,rsp.Project)
		return c.JSON(http.StatusCreated,rsp.Project)
	}
}

//删除
func (s ProjectService) Delete(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := projectCli.Delete(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		return c.JSON(http.StatusOK,rsp)
	}
}

//启用
func (s ProjectService) Open(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := projectCli.Open(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Project,rsp.Project)
		return c.JSON(http.StatusCreated,rsp.Project)
	}
}

//禁用
func (s ProjectService) Close(c echo.Context) error {
	d := plugin.GetIdByRequest(c)


	rsp,err := projectCli.Close(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Project,rsp.Project)
		return c.JSON(http.StatusCreated,rsp.Project)
	}
}

//取得信息
func (s ProjectService) Get(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := projectCli.Get(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Project,rsp.Project)
		return c.JSON(http.StatusCreated,rsp.Project)
	}
}

func (s ProjectService) List(c echo.Context) error {
	u := s.getParamByRequest(c)
	d := plugin.GetListRequestByRequest(c,[]string{"number","demand_number","title"})
	req := &project.ListRequest{
		Default:d,
		Project:u,
	}

	rsp,err := projectCli.GetList(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		for _, value := range rsp.List {
			plugin.FmtDate(value,&value)
		}
		return c.JSON(http.StatusCreated,plugin.FmtList(rsp.List))
	}
}

func (s ProjectService) Page(c echo.Context) error {
	u := s.getParamByRequest(c)
	d := plugin.GetPageRequestByRequest(c,[]string{"number","demand_number","title"})
	req := &project.PageRequest{
		Default:d,
		Project:u,
	}

	rsp,err := projectCli.GetPage(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		for key, value := range rsp.List {
			v := *value
			plugin.FmtDate(v,&v)
			rsp.List[key] = &v
		}
		return c.JSON(http.StatusCreated,rsp)
	}
}

func (s ProjectService) getParamByRequest(c echo.Context) *project.Project {
	u := &project.Project{}
	plugin.GetParams(c,u)
	return u
}
