package handler

import (
	"context"
	gv "github.com/asaskevich/govalidator"
	"github.com/labstack/echo"
	"github.com/micro/go-micro/client"
	"net/http"
	"pmm/api-srv/plugin"

	"pmm/public/config"
	"pmm/public/proto/verify/code"
	"pmm/public/utils/validate"
)

var codeCli = code.NewVerifyCodeServiceClient(config.Namespace+"srv.user",client.DefaultClient)


func (s UserService) Code(c echo.Context) error {
	key := c.Param("key")
	req := &code.Request{}
	if gv.IsEmail(key) {
		req.Email = key
		r,err := codeCli.EmailCode(context.TODO(),req)
		if err != nil {
			return plugin.FmtErr(err,c)
		}else {
			return c.JSON(201,r)
		}
	}else if validate.IsMobile(key) {
		req.Mobile = key
		r,err := codeCli.PhoneCode(context.TODO(),req)
		if err != nil {
			return plugin.FmtErr(err,c)
		}else {
			return c.JSON(201,r)
		}
	}
	m := map[string]string{"error":"错误的请求，只能取得手机号码或者邮件的验证码"}
	return c.JSON(http.StatusBadRequest,m)
}