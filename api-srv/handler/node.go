package handler

import (
	"context"
	"fmt"
	"github.com/labstack/echo"
	"github.com/micro/go-micro/client"
	"net/http"
	"pmm/api-srv/plugin"
	"pmm/public/config"
	"pmm/public/proto"
	"pmm/public/proto/node"
	"time"
)

var nodeCli = node.NewNodeServiceClient(config.Namespace+"srv.node",client.DefaultClient)

type NodeService struct {
}

//新建
func (s NodeService) Create(c echo.Context) error {
	p := s.getParamByRequest(c)

	rsp,err := nodeCli.Create(context.TODO(),p)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	plugin.FmtDate(*rsp.Node,rsp.Node)
	return c.JSON(http.StatusCreated,rsp.Node)
}

//更新
func (s NodeService) Update(c echo.Context) error {
	p := s.getParamByRequest(c)

	rsp,err := nodeCli.Update(context.TODO(),p)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Node,rsp.Node)
		return c.JSON(http.StatusCreated,rsp.Node)
	}
}

//删除
func (s NodeService) Delete(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := nodeCli.Delete(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		return c.JSON(http.StatusOK,rsp)
	}
}

//启用
func (s NodeService) Open(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := nodeCli.Open(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Node,rsp.Node)
		return c.JSON(http.StatusCreated,rsp.Node)
	}
}

//禁用
func (s NodeService) Close(c echo.Context) error {
	d := plugin.GetIdByRequest(c)


	rsp,err := nodeCli.Close(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Node,rsp.Node)
		return c.JSON(http.StatusCreated,rsp.Node)
	}
}

//取得信息
func (s NodeService) Get(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := nodeCli.Get(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Node,rsp.Node)
		return c.JSON(http.StatusCreated,rsp.Node)
	}
}

func (s NodeService) List(c echo.Context) error {
	u := s.getParamByRequest(c)
	d := plugin.GetListRequestByRequest(c,[]string{"name","remark","domain","title"})
	req := &node.ListRequest{
		Default:d,
		Node:u,
	}

	rsp,err := nodeCli.GetList(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		for key, value := range rsp.List {
			v := *value
			plugin.FmtDate(v,&v)
			rsp.List[key] = &v
		}
		return c.JSON(http.StatusCreated,plugin.FmtList(rsp.List))
	}
}

func (s NodeService) Page(c echo.Context) error {
	u := s.getParamByRequest(c)
	d := plugin.GetPageRequestByRequest(c,[]string{"title"})
	req := &node.PageRequest{
		Default:d,
		Node:u,
	}

	rsp,err := nodeCli.GetPage(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		for key, value := range rsp.List {
			v := *value
			plugin.FmtDate(v,&v)
			rsp.List[key] = &v
		}
		return c.JSON(http.StatusCreated,rsp)
	}
}

func (s NodeService) Tree(c echo.Context) error{
	u := &node.TreeRequest{}
	plugin.GetParams(c,u)

	rsp,err := nodeCli.GetTree(context.TODO(),u)
	if err != nil {
		return plugin.FmtErr(err,c)
	}

	s.fmtTree(rsp.Trees)
	return c.JSON(http.StatusCreated,rsp.Trees)

}

func (s NodeService) getParamByRequest(c echo.Context) *node.Node {
	u := &node.Node{}
	plugin.GetParams(c,u)
	return u
}

func (s NodeService) fmtTree(trees []*node.NodeTree) {
	for _, tree := range trees {
		if tree.Child != nil {
			s.fmtTree(tree.Child)
		}
		plugin.FmtDate(*tree.Node,tree.Node)
	}
}

func (s NodeService) getWeekReportHtml(pid string) (string,error) {
	u := &node.TreeRequest{}
	u.ProjectId = pid
	
	//取得并验证项目状态
	i := &proto.IdRequest{}
	i.Id = u.ProjectId
	
	r,err := projectCli.Get(context.TODO(),i)
	if err != nil {
		return "",err
	}
	p := r.Project

	rsp,err := nodeCli.GetTree(context.TODO(),u)
	if err != nil {
		return "",err
	}

	var html = "<style> .box h1,.box h2{width:100%;text-align: center;} .main{width: 80%;margin:0 auto;border:1px solid black;}td{padding:0 15px;}</style><div class='box'><h1>Weekly Progress Report</h1><h2>项目周报</h2><div class='content'><div class='main'><table width='100%' height='123' border='1' cellpadding='0' cellspacing='0'><tr><td ><h3> 一、目前项目完成情况</br>Accomplished Tasks in Last Week</h3></td> </tr><tr><td width='100%'><ul>"

	html += s.getNodeHtml(rsp.Trees)
	html += "</ul></td></tr>"

	html += "<tr><td><h3>二、沟通记录 Communication Records</h3></td></tr></tr><tr><td><strong >"+p.Remark+"</br></strong></td></tr>"

	html += "<tr><td ><h3>三、下周计划：Planned Tasks for Next Week</h3></td></tr><tr><td><strong ></br></strong></td></tr>"

	html += "</table></div></div></div>"

	return html,nil
}

func (s NodeService) getNodeHtml(treeNode []*node.NodeTree) string {
	var html string
	for _, tree := range treeNode {
		n := tree.Node

		//判断是否完成
		if n.Progress >= 1 {
			ua, err := time.Parse("2006-01-02T15:04:05+08:00", n.UpdatedAt)
			if err != nil {
				ua,_ = time.Parse("2006-01-02 15:04:05",n.UpdatedAt)
			}
			ts := ua.Format("2006年01月02日")
			html += "<li>"+ts+"	<strong>已完成 "+n.Title+"</strong>"
		}else{
			ts := time.Now().Format("2006年01月02日")
			p := fmt.Sprintf("%.2f",n.Progress*100)+"%"
			html += "<li>"+ts+" <strong>"+n.Title+"</strong> 的进度为"+p
		}
		if len(tree.Child) > 0 {
			html += "<ul>"
			html += s.getNodeHtml(tree.Child)
			html += "</ul>"
		}
		html += "</li>"
	}
	return html
}