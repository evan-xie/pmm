package handler

import (
	"context"
	"github.com/labstack/echo"
	"github.com/micro/go-micro/client"
	"net/http"
	"pmm/api-srv/plugin"
	"pmm/public/config"
	tagGroup "pmm/public/proto/tag/group"
)

var tagGroupCli = tagGroup.NewTagGroupServiceClient(config.Namespace+"srv.tag",client.DefaultClient)

type TagGroupService struct {
}

//新建
func (s TagGroupService) Create(c echo.Context) error {
	p := s.getParamByRequest(c)

	rsp,err := tagGroupCli.Create(context.TODO(),p)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	return c.JSON(http.StatusCreated,rsp.TagGroup)
}

//更新
func (s TagGroupService) Update(c echo.Context) error {
	p := s.getParamByRequest(c)

	rsp,err := tagGroupCli.Update(context.TODO(),p)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.TagGroup,rsp.TagGroup)
		return c.JSON(http.StatusCreated,rsp.TagGroup)
	}
}

//删除
func (s TagGroupService) Delete(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := tagGroupCli.Delete(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		return c.JSON(http.StatusOK,rsp)
	}
}

//启用
func (s TagGroupService) Open(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := tagGroupCli.Open(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.TagGroup,rsp.TagGroup)
		return c.JSON(http.StatusCreated,rsp.TagGroup)
	}
}

//禁用
func (s TagGroupService) Close(c echo.Context) error {
	d := plugin.GetIdByRequest(c)


	rsp,err := tagGroupCli.Close(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.TagGroup,rsp.TagGroup)
		return c.JSON(http.StatusCreated,rsp.TagGroup)
	}
}

//取得信息
func (s TagGroupService) Get(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := tagGroupCli.Get(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.TagGroup,rsp.TagGroup)
		return c.JSON(http.StatusCreated,rsp.TagGroup)
	}
}

func (s TagGroupService) List(c echo.Context) error {
	u := s.getParamByRequest(c)
	d := plugin.GetListRequestByRequest(c,[]string{"name"})
	req := &tagGroup.ListRequest{
		Default:d,
		TagGroup:u,
	}

	rsp,err := tagGroupCli.GetList(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		for _, value := range rsp.List {
			plugin.FmtDate(value,&value)
		}
		return c.JSON(http.StatusCreated,plugin.FmtList(rsp.List))
	}
}

func (s TagGroupService) Page(c echo.Context) error {
	u := s.getParamByRequest(c)
	d := plugin.GetPageRequestByRequest(c,[]string{"name"})
	req := &tagGroup.PageRequest{
		Default:d,
		TagGroup:u,
	}

	rsp,err := tagGroupCli.GetPage(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		for key, value := range rsp.List {
			v := *value
			plugin.FmtDate(v,&v)
			rsp.List[key] = &v
		}
		return c.JSON(http.StatusCreated,rsp)
	}
}

func (s TagGroupService) getParamByRequest(c echo.Context) *tagGroup.TagGroup {
	u := &tagGroup.TagGroup{}
	plugin.GetParams(c,u)
	return u
}