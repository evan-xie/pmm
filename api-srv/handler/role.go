package handler

import (
	"context"
	"github.com/labstack/echo"
	"github.com/micro/go-micro/client"
	"net/http"
	"pmm/api-srv/plugin"
	"pmm/public/config"
	"pmm/public/proto/role"
)

var roleCli = role.NewRoleServiceClient(config.Namespace+"srv.user",client.DefaultClient)

type RoleService struct {
}

//新建
func (s RoleService) Create(c echo.Context) error {
	p := s.getParamByRequest(c)

	rsp,err := roleCli.Create(context.TODO(),p)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	return c.JSON(http.StatusCreated,rsp.Role)
}

//更新
func (s RoleService) Update(c echo.Context) error {
	u := s.getParamByRequest(c)


	rsp,err := roleCli.Update(context.TODO(),u)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Role,rsp.Role)
		return c.JSON(http.StatusCreated,rsp.Role)
	}
}

//删除
func (s RoleService) Delete(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := roleCli.Delete(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		return c.JSON(http.StatusOK,rsp)
	}
}

//启用
func (s RoleService) Open(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := roleCli.Open(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Role,rsp.Role)
		return c.JSON(http.StatusCreated,rsp.Role)
	}
}

//禁用
func (s RoleService) Close(c echo.Context) error {
	d := plugin.GetIdByRequest(c)


	rsp,err := roleCli.Close(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Role,rsp.Role)
		return c.JSON(http.StatusCreated,rsp.Role)
	}
}

//取得信息
func (s RoleService) Get(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := roleCli.Get(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Role,rsp.Role)
		return c.JSON(http.StatusCreated,rsp.Role)
	}
}


func (s RoleService) List(c echo.Context) error {
	u := s.getParamByRequest(c)
	d := plugin.GetListRequestByRequest(c,[]string{"name","remark"})
	req := &role.ListRequest{
		Default:d,
		Role:u,
	}

	rsp,err := roleCli.GetList(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		for _, value := range rsp.List {
			plugin.FmtDate(value,&value)
		}
		return c.JSON(http.StatusCreated,plugin.FmtList(rsp.List))
	}
}

func (s RoleService) Page(c echo.Context) error {
	u := s.getParamByRequest(c)
	d := plugin.GetPageRequestByRequest(c,[]string{"name","remark"})
	req := &role.PageRequest{
		Default:d,
		Role:u,
	}

	rsp,err := roleCli.GetPage(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		for key, value := range rsp.List {
			v := *value
			plugin.FmtDate(v,&v)
			rsp.List[key] = &v
		}
		return c.JSON(http.StatusCreated,rsp)
	}
}

func (s RoleService) getParamByRequest(c echo.Context) *role.Role {
	u := &role.Role{}
	u.Name = c.QueryParam("name")
	u.Remark = c.QueryParam("remark")
	u.Id = c.Param("id")
	return u
}

