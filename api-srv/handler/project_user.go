package handler

import (
	"context"
	"github.com/labstack/echo"
	"github.com/micro/go-micro/client"
	"net/http"
	"pmm/api-srv/plugin"
	"pmm/public/config"
	"pmm/public/proto"
	"pmm/public/proto/project"
	pu "pmm/public/proto/project/user"
	"pmm/public/proto/user"
)

var projectUserCli = pu.NewProjectUserServiceClient(config.Namespace+"srv.project",client.DefaultClient)

type ProjectUserService struct {
	ProjectUser pu.ProjectUser `json:"project_user,omitempty"`
	User user.User `json:"user,omitempty"`
}

type ProjectUserPage struct {
	List []ProjectUserService `json:"list,omitempty"`
	Page proto.Page `json:"page,omitempty"`
}

type UserProject struct {
	ProjectUser pu.ProjectUser `json:"project_user,omitempty"`
	Project project.Project `json:"project,omitempty"`
}

type UserProjectPage struct {
	List []UserProject `json:"list,omitempty"`
	Page proto.Page `json:"page,omitempty"`
}

//新建
func (s ProjectUserService) Create(c echo.Context) error {
	p := s.getParamByRequest(c)
	p.IsClient = config.CLOSE_STATUS

	rsp,err := projectUserCli.Create(context.TODO(),p)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	return c.JSON(http.StatusCreated,rsp.ProjectUser)
}

//更新
func (s ProjectUserService) Update(c echo.Context) error {
	p := s.getParamByRequest(c)

	rsp,err := projectUserCli.Update(context.TODO(),p)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.ProjectUser,rsp.ProjectUser)
		return c.JSON(http.StatusCreated,rsp.ProjectUser)
	}
}

//删除
func (s ProjectUserService) Delete(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := projectUserCli.Delete(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		return c.JSON(http.StatusOK,rsp)
	}
}

//启用
func (s ProjectUserService) Open(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := projectUserCli.Open(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.ProjectUser,rsp.ProjectUser)
		return c.JSON(http.StatusCreated,rsp.ProjectUser)
	}
}

//禁用
func (s ProjectUserService) Close(c echo.Context) error {
	d := plugin.GetIdByRequest(c)


	rsp,err := projectUserCli.Close(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.ProjectUser,rsp.ProjectUser)
		return c.JSON(http.StatusCreated,rsp.ProjectUser)
	}
}

//取得信息
func (s ProjectUserService) Get(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := projectUserCli.Get(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}

	plugin.FmtDate(*rsp.ProjectUser,rsp.ProjectUser)
	return c.JSON(http.StatusCreated,rsp.ProjectUser)

}

func (s ProjectUserService) List(c echo.Context) error {
	u := s.getParamByRequest(c)
	u.IsClient = config.CLOSE_STATUS
	d := plugin.GetListRequestByRequest(c,[]string{})
	req := &pu.ListRequest{
		Default:d,
		ProjectUser:u,
	}

	rsp,err := projectUserCli.GetList(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		for _, value := range rsp.List {
			plugin.FmtDate(value,&value)
		}
		return c.JSON(http.StatusCreated,plugin.FmtList(rsp.List))
	}
}

func (s ProjectUserService) Page(c echo.Context) error {
	u := s.getParamByRequest(c)
	u.IsClient = config.CLOSE_STATUS
	d := plugin.GetPageRequestByRequest(c,[]string{})
	req := &pu.PageRequest{
		Default:d,
		ProjectUser:u,
	}

	rsp,err := projectUserCli.GetPage(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	if len(rsp.List) == 0 {
		return c.JSON(http.StatusCreated,rsp)
	}

	for key, value := range rsp.List {
		v := *value
		plugin.FmtDate(v,&v)
		rsp.List[key] = &v
	}
	return c.JSON(http.StatusCreated,rsp)

}

func (s ProjectUserService) GetUserByProjectList(c echo.Context) error {
	p := s.getParamByRequest(c)
	p.IsClient = config.CLOSE_STATUS
	d := plugin.GetListRequestByRequest(c,[]string{})
	req := &pu.ListRequest{
		Default:d,
		ProjectUser:p,
	}

	rsp,err := projectUserCli.GetList(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	if len(rsp.List) == 0 {
		return c.JSON(http.StatusCreated,user.PageResponse{})
	}

	var ids []string
	m := map[string]pu.ProjectUser{}
	for _, value := range rsp.List {
		ids = append(ids, value.UserId)
		v := *value
		plugin.FmtDate(v,&v)
		m[value.UserId] = v
	}

	dd := plugin.GetListRequestByRequest(c,[]string{"nickname","username","mobile","email"})

	//dd := plugin.GetPageRequestByRequest(c,[]string{"nickname","username","mobile","email"})
	dd.Ids = ids
	u := &user.User{}
	plugin.GetParams(c,u)
	rr := &user.ListRequest{
		Default:dd,
		User:u,
	}
	list,err := userCli.GetList(context.TODO(),rr)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	var ps []ProjectUserService
	for _, uu := range list.List {
		v := *uu
		plugin.FmtDate(v,&v)
		pus := ProjectUserService{
			User:v,
			ProjectUser:m[uu.Id],
		}
		ps = append(ps, pus)
	}
	pp := &ProjectUserPage{
		List:ps,
	}
	return c.JSON(http.StatusCreated,pp)
}

func (s ProjectUserService) GetUserByProject(c echo.Context) error {
	p := s.getParamByRequest(c)
	p.IsClient = config.CLOSE_STATUS
	d := plugin.GetListRequestByRequest(c,[]string{})
	req := &pu.ListRequest{
		Default:d,
		ProjectUser:p,
	}

	rsp,err := projectUserCli.GetList(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	if len(rsp.List) == 0 {
		return c.JSON(http.StatusCreated,user.PageResponse{})
	}

	var ids []string
	m := map[string]pu.ProjectUser{}
	for _, value := range rsp.List {
		ids = append(ids, value.UserId)
		v := *value
		plugin.FmtDate(v,&v)
		m[value.UserId] = v
	}

	dd := plugin.GetPageRequestByRequest(c,[]string{"nickname","username","mobile","email"})
	dd.Ids = ids
	u := &user.User{}
	plugin.GetParams(c,u)
	rr := &user.PageRequest{
		Default:dd,
		User:u,
	}
	list,err := userCli.GetPage(context.TODO(),rr)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	var ps []ProjectUserService
	for _, uu := range list.List {
		v := *uu
		plugin.FmtDate(v,&v)
		pus := ProjectUserService{
			User:v,
			ProjectUser:m[uu.Id],
		}
		ps = append(ps, pus)
	}
	pp := &ProjectUserPage{
		List:ps,
		Page:*list.Page,
	}
	return c.JSON(http.StatusCreated,pp)
}

func (s ProjectUserService) GetProjectByUser(c echo.Context) error {
	p := s.getParamByRequest(c)
	d := plugin.GetListRequestByRequest(c,[]string{})
	req := &pu.ListRequest{
		Default:d,
		ProjectUser:p,
	}

	rsp,err := projectUserCli.GetList(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	if len(rsp.List) == 0 {
		return c.JSON(http.StatusCreated,user.PageResponse{})
	}

	var ids []string
	m := map[string]pu.ProjectUser{}
	for _, value := range rsp.List {
		ids = append(ids, value.ProjectId)
		v := *value
		plugin.FmtDate(v,&v)
		m[value.ProjectId] = v
	}

	dd := plugin.GetPageRequestByRequest(c,[]string{"title","type","number","demand_number"})
	dd.Ids = ids
	u := &project.Project{}
	plugin.GetParams(c,u)
	rr := &project.PageRequest{
		Default:dd,
		Project:u,
	}
	list,err := projectCli.GetPage(context.TODO(),rr)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	var ps []UserProject
	for _, uu := range list.List {
		v := *uu
		plugin.FmtDate(v,&v)
		pus := UserProject{
			Project:v,
			ProjectUser:m[uu.Id],
		}
		ps = append(ps, pus)
	}
	pp := &UserProjectPage{
		List:ps,
		Page:*list.Page,
	}
	return c.JSON(http.StatusCreated,pp)
}
func (s ProjectUserService) getParamByRequest(c echo.Context) *pu.ProjectUser {
	u := &pu.ProjectUser{}
	plugin.GetParams(c,u)
	return u
}

func (s ProjectUserService) GetUserListByPid(pid string) ([]*user.User,error){
	u := &pu.ProjectUser{}
	u.ProjectId = pid
	req := &pu.ListRequest{
		ProjectUser:u,
	}

	rsp,err := projectUserCli.GetList(context.TODO(),req)
	if err != nil {
		return nil,err
	}
	if len(rsp.List) == 0 {
		return nil,err
	}

	var ids []string
	m := map[string]pu.ProjectUser{}
	for _, value := range rsp.List {
		ids = append(ids, value.UserId)
		v := *value
		plugin.FmtDate(v,&v)
		m[value.UserId] = v
	}

	rr := &user.ListRequest{
		Default:&proto.ListRequest{
			Ids:ids,
		},
	}
	list,err := userCli.GetList(context.TODO(),rr)
	if err != nil {
		return nil,err
	}
	return list.List, nil
}
