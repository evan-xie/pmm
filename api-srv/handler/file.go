package handler

import (
	"context"
	"fmt"
	"github.com/labstack/echo"
	"github.com/micro/go-micro/client"
	"net/http"
	"php"
	"pmm/api-srv/conf"
	"pmm/api-srv/plugin"
	"pmm/public/config"
	"pmm/public/proto/file"
	"pmm/public/utils/oss"
	"strconv"
)
type FileService struct {
}
var fileCli = file.NewFileServiceClient(config.Namespace+"srv.file",client.DefaultClient)

//新建
func (s FileService) OssCallback(c echo.Context) error {
	data := &file.File{}
	data.Path = plugin.GetParam(c,"filename")
	p := php.Explode(",",data.Path)
	data.Name = php.Pop(p).(string)
	data.Size,_ =  strconv.ParseInt(plugin.GetParam(c,"size"),10,64)
	data.Mimetype = plugin.GetParam(c,"mimeType")
	data.Type = plugin.GetParam(c,"imageInfo.format")
	data.Sha1 = plugin.GetParam(c,"etag")
	data.Url = conf.OSSPath+plugin.GetParam(c,"filename")
	data.Md5 = c.Request().Header.Get("content-md5")
	fmt.Println(data)
	rsp,err := fileCli.Create(context.TODO(),data)
	if err != nil {
		fmt.Println(err)
		return plugin.FmtErr(err,c)
	}
	plugin.FmtDate(*rsp.File,rsp.File)
	return c.JSON(http.StatusCreated,rsp.File)
}

//新建
func (s FileService) Create(c echo.Context) error {
	p := s.getParamByRequest(c)

	rsp,err := fileCli.Create(context.TODO(),p)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	plugin.FmtDate(*rsp.File,rsp.File)
	return c.JSON(http.StatusCreated,rsp.File)
}

//更新
func (s FileService) Update(c echo.Context) error {
	p := s.getParamByRequest(c)

	rsp,err := fileCli.Update(context.TODO(),p)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.File,rsp.File)
		return c.JSON(http.StatusCreated,rsp.File)
	}
}

//删除
func (s FileService) Delete(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := fileCli.Delete(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		return c.JSON(http.StatusOK,rsp)
	}
}

//启用
func (s FileService) Open(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := fileCli.Open(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.File,rsp.File)
		return c.JSON(http.StatusCreated,rsp.File)
	}
}

//禁用
func (s FileService) Close(c echo.Context) error {
	d := plugin.GetIdByRequest(c)


	rsp,err := fileCli.Close(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.File,rsp.File)
		return c.JSON(http.StatusCreated,rsp.File)
	}
}

//取得信息
func (s FileService) Get(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := fileCli.Get(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.File,rsp.File)
		return c.JSON(http.StatusCreated,rsp.File)
	}
}


func (s FileService) List(c echo.Context) error {
	u := s.getParamByRequest(c)
	d := plugin.GetListRequestByRequest(c,[]string{"name","remark","domain","title"})
	req := &file.ListRequest{
		Default:d,
		File:u,
	}

	rsp,err := fileCli.GetList(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		for key, value := range rsp.List {
			v := *value
			plugin.FmtDate(v,&v)
			rsp.List[key] = &v
		}
		return c.JSON(http.StatusCreated,plugin.FmtList(rsp.List))
	}
}

func (s FileService) Page(c echo.Context) error {
	u := s.getParamByRequest(c)
	d := plugin.GetPageRequestByRequest(c,[]string{"title"})
	req := &file.PageRequest{
		Default:d,
		File:u,
	}

	rsp,err := fileCli.GetPage(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		for key, value := range rsp.List {
			v := *value
			plugin.FmtDate(v,&v)
			rsp.List[key] = &v
		}
		return c.JSON(http.StatusCreated,rsp)
	}
}

func (s FileService) getParamByRequest(c echo.Context) *file.File {
	u := &file.File{}
	plugin.GetParams(c,u)
	return u
}

//新建
func (s FileService) GetOssCallback(c echo.Context) error {
/*	t := c.Get("user").(*jwt.Token)

	u:=token.TokenUtils{}
	cu,err := u.Decode(t.Raw,"access")
	fmt.Println(cu.Subject)
	fmt.Println(err)*/
	pt := oss.GetPolicyToken()
	return c.JSON(http.StatusCreated,pt)
}
