package handler

import (
	"context"
	"fmt"
	"github.com/asaskevich/govalidator"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"github.com/micro/go-micro/client"
	"net/http"
	"php"
	"pmm/api-srv/plugin"
	"pmm/public/config"
	"pmm/public/proto"
	"pmm/public/proto/auth"
	"pmm/public/proto/user"
	"pmm/public/proto/wxApp"
	"strconv"
)

var userCli = user.NewUserServiceClient(config.Namespace+"srv.user",client.DefaultClient)
var authCli = auth.NewAuthServiceClient(config.Namespace+"srv.user",client.DefaultClient)
var wxCli = wxApp.NewWxAppServiceClient(config.Namespace+"srv.user",client.DefaultClient)

type UserService struct {
}

//wx注册
func (s UserService) WxRegister(c echo.Context) error {
	u := &wxApp.WxRegisterReq{}
	plugin.GetParams(c,u)
	req := u

	rsp,err := wxCli.WxRegisterByPhone(context.TODO(),req)
	if err != nil {
		fmt.Println(err)
		return plugin.FmtErr(err,c)
	}
	plugin.FmtDate(*rsp.User,rsp.User)
	return c.JSON(http.StatusCreated,rsp)
}
//wx注册
func (s UserService) WxRegisterByUserInfo(c echo.Context) error {
	u := &wxApp.WxRegisterReq{}
	plugin.GetParams(c,u)
	req := u

	rsp,err := wxCli.WxRegisterByPhone(context.TODO(),req)
	if err != nil {
		fmt.Println(err)
		return plugin.FmtErr(err,c)
	}
	plugin.FmtDate(*rsp.User,rsp.User)
	return c.JSON(http.StatusCreated,rsp)
}


//wx登录
func (s UserService) WxLogin(c echo.Context) error {
	u := &wxApp.WxLoginReq{}
	plugin.GetParams(c,u)
	req := u

	rsp,err := wxCli.WxLogin(context.TODO(),req)
	if err != nil {
		fmt.Println(err)
		return plugin.FmtErr(err,c)
	}
	plugin.FmtDate(*rsp.User,rsp.User)
	return c.JSON(http.StatusCreated,rsp)
}
//注册
func (s UserService) Register(c echo.Context) error {
	u := s.getParamByRequest(c)
	verifyCode := plugin.GetParam(c,"code")
	req := &user.RegisterRqeuset{
		User:u,
		Code:verifyCode,
	}


	rsp,err := userCli.Register(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	plugin.FmtDate(*rsp.User,rsp.User)
	return c.JSON(http.StatusCreated,rsp.User)
}

//新建
func (s UserService) Create(c echo.Context) error {
	u := s.getParamByRequest(c)
	u.CompanyId = DefaultCompany.Id

	rsp,err := userCli.Save(context.TODO(),u)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	plugin.FmtDate(*rsp.User,rsp.User)
	return c.JSON(http.StatusCreated,rsp.User)
}

//更新
func (s UserService) Update(c echo.Context) error {
	u := s.getParamByRequest(c)

	rsp,err := userCli.Update(context.TODO(),u)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.User,rsp.User)
		return c.JSON(http.StatusCreated,rsp.User)
	}
}

//删除
func (s UserService) Delete(c echo.Context) error {
	u := s.getUserByRequest(c)


	rsp,err := userCli.Delete(context.TODO(),u)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		return c.JSON(http.StatusOK,rsp)
	}
}

//启用
func (s UserService) Open(c echo.Context) error {
	u := s.getUserByRequest(c)


	rsp,err := userCli.Open(context.TODO(),u)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.User,rsp.User)
		return c.JSON(http.StatusCreated,rsp.User)
	}
}

//禁用
func (s UserService) Close(c echo.Context) error {
	u := s.getUserByRequest(c)


	rsp,err := userCli.Close(context.TODO(),u)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.User,rsp.User)
		return c.JSON(http.StatusCreated,rsp.User)
	}
}

//更改密码
func (s UserService) ChangePwd(c echo.Context) error {
	u := s.getUserByRequest(c)

	rsp,err := userCli.ChangePassword(context.TODO(),u)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.User,rsp.User)
		return c.JSON(http.StatusCreated,rsp.User)
	}
}

/**
修改当前用户密码
 */
func (s UserService) ChangeLoginPwd(c echo.Context) error {
	//token := c.Request().Header.Get("Access-Token")
	t := c.Get("user").(*jwt.Token)
	token := t.Raw
	old := c.QueryParam("old_pwd")
	newPwd := c.QueryParam("new_pwd")
	req := &user.ChangeLoginPasswordReq{
		Token:token,
		OldPwd:old,
		NewPwd:newPwd,
	}
	rsp,err := userCli.ChangeLoginPassword(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.User,rsp.User)
		return c.JSON(http.StatusCreated,rsp.User)
	}
}

func (s UserService) UpdateInfo(c echo.Context) error {
/*	token := c.Request().Header.Get("Access-Token")
	r := &auth.Token{
		TokenType:"access",
		TokenStr:token,
	}
	rsp,err := authCli.GetTokenSub(context.TODO(),r)
	if err != nil {
		return plugin.FmtErr(err,c)
	}*/
	uid := plugin.GetUid(c)
	u := s.getParamByRequest(c)
	u.Id = uid

	info,err := userCli.Update(context.TODO(),u)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	plugin.FmtDate(*info.User,info.User)
	return c.JSON(http.StatusCreated,info.User)
}
//取得用户信息
func (s UserService) Get(c echo.Context) error {
	u := s.getUserByRequest(c)


	rsp,err := userCli.Get(context.TODO(),u)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.User,rsp.User)
		return c.JSON(http.StatusCreated,rsp.User)
	}
}
//登录
func (s UserService) Login(c echo.Context) error {
	u := s.getUserByRequest(c)
	
	rsp,err := userCli.Login(context.TODO(),u)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.User,rsp.User)
		return c.JSON(http.StatusCreated,rsp)
	}
}

func (s UserService) List(c echo.Context) error {
	u := s.getParamByRequest(c)
	d := s.getListRequestByRequest(c)
	req := &user.ListRequest{
		Default:d,
		User:u,
	}

	rsp,err := userCli.GetList(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		for key, value := range rsp.List {
			v := *value
			plugin.FmtDate(v,&v)
			rsp.List[key] = &v
		}
		return c.JSON(http.StatusCreated,plugin.FmtList(rsp.List))
	}
}

func (s UserService) Page(c echo.Context) error {
	u := s.getUserByRequest(c)
	d := s.getPageRequestByRequest(c)
	req := &user.PageRequest{
		Default:d,
		User:u,
	}

	rsp,err := userCli.GetPage(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		for key, value := range rsp.List {
			v := *value
			plugin.FmtDate(v,&v)
			rsp.List[key] = &v
		}
		return c.JSON(http.StatusCreated,rsp)
	}
}

func (s UserService) GetInfo(c echo.Context) error {
	token := plugin.GetToken(c)
	req := &auth.Token{
		TokenStr:token,
		TokenType:"access",
	}

	rsp,err := authCli.GetUserInfoByToken(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.User,rsp.User)
		return c.JSON(http.StatusCreated,rsp.User)
	}
}

func (s UserService) getUserByRequest(c echo.Context) *user.User {
	u := &user.User{}
	u.Mobile = c.QueryParam("mobile")
	u.Email = c.QueryParam("email")
	u.Password = c.QueryParam("password")
	u.Nickname = c.QueryParam("nickname")
	u.Username = c.QueryParam("username")
	u.Id = c.Param("id")
	return u
}

func (s UserService) getParamByRequest(c echo.Context) *user.User {
	u := &user.User{}
	plugin.GetParams(c,u)
	return u
}

func (s UserService) getListRequestByRequest(c echo.Context) *proto.ListRequest {
	req := &proto.ListRequest{}
	ids := c.QueryParam("ids")
	if !govalidator.IsNull(ids) {
		ida := php.Explode(",",ids)
		req.Ids = ida
	}

	start := c.QueryParam("start")
	if !govalidator.IsNull(start) {
		si,_ := strconv.ParseInt(start,10,64)
		req.Start = si
	}

	limit := c.QueryParam("start")
	if !govalidator.IsNull(limit) {
		l,_ := strconv.ParseInt(limit,10,64)
		req.Limit = l
	}
	
	search := c.QueryParam("search")
	if !govalidator.IsNull(search) {
		req.Search["mobile"] = search
		req.Search["email"] = search
		req.Search["username"] = search
		req.Search["nickname"] = search
	}
	
	return req
}

func (s UserService) getPageRequestByRequest(c echo.Context) *proto.PageRequest {
	req := &proto.PageRequest{}
	ids := c.QueryParam("ids")
	if !govalidator.IsNull(ids) {
		ida := php.Explode(",",ids)
		req.Ids = ida
	}

	page := c.Param("page")
	if !govalidator.IsNull(page) {
		p,_ := strconv.ParseInt(page,10,64)
		req.Page = p
	}else {
		req.Page = config.Page
	}

	size := c.Param("size")
	if !govalidator.IsNull(size) {
		l,_ := strconv.ParseInt(size,10,64)
		req.Size = l
	}

	search := c.Param("search")
	if !govalidator.IsNull(search) {
		req.Search = map[string]string{
			"mobile":search,
			"username":search,
			"email":search,
			"nickname":search,
		}
	}

	return req
}
