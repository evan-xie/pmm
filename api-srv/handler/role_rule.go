package handler

import (
	"context"
	"github.com/labstack/echo"
	"github.com/micro/go-micro/client"
	"net/http"
	"pmm/api-srv/plugin"
	"pmm/public/config"
	roleRule "pmm/public/proto/role/rule"
	"pmm/public/proto/rule"
)

var roleRuleCli = roleRule.NewRoleRuleServiceClient(config.Namespace+"srv.user",client.DefaultClient)

type RoleRuleService struct {
}

//新建
func (s RoleRuleService) Create(c echo.Context) error {
	p := s.getParamByRequest(c)

	rsp,err := roleRuleCli.Create(context.TODO(),p)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	return c.JSON(http.StatusCreated,rsp)
}

//删除
func (s RoleRuleService) Delete(c echo.Context) error {
	d := s.getParamByRequest(c)

	rsp,err := roleRuleCli.Delete(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		return c.JSON(http.StatusOK,rsp)
	}
}

//删除角色所有用户关联
func (s RoleRuleService) DeleteRoles(c echo.Context) error {
	d := s.getParamByRequest(c)

	rsp,err := roleRuleCli.DeleteRoles(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		return c.JSON(http.StatusOK,rsp)
	}
}

//删除用户所有角色关联
func (s RoleRuleService) DeleteRules(c echo.Context) error {
	d := s.getParamByRequest(c)

	rsp,err := roleRuleCli.DeleteRules(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		return c.JSON(http.StatusOK,rsp)
	}
}
func (s RoleRuleService) RulePage(c echo.Context) error {
	req := s.getParamByRequest(c)

	rsp,err := roleRuleCli.GetRuleIdsByRole(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	d := plugin.GetPageRequestByRequest(c,[]string{"title","name","domain","remark"})
	d.Ids = rsp.RuleIds
	p := &rule.Rule{}
	plugin.GetParams(c,p)
	request := &rule.PageRequest{
		Default:d,
		Rule:p,
	}

	r,err := ruleCli.GetPage(context.TODO(),request)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	for key, value := range r.List {
		v := *value
		plugin.FmtDate(v,&v)
		r.List[key] = &v
	}
	return c.JSON(http.StatusCreated,r)
}


func (s RoleRuleService) getParamByRequest(c echo.Context) *roleRule.RoleRule {
	u := &roleRule.RoleRule{}
	plugin.GetParams(c,u)
	return u
}

