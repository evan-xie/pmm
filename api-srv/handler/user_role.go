package handler

import (
	"context"
	"github.com/asaskevich/govalidator"
	"github.com/labstack/echo"
	"github.com/micro/go-micro/client"
	"net/http"
	"pmm/api-srv/plugin"
	"pmm/public/config"
	userRole "pmm/public/proto/user/role"
	"strconv"
)

var userRoleCli = userRole.NewUserRoleServiceClient(config.Namespace+"srv.user",client.DefaultClient)

type UserRoleService struct {
}

//新建
func (s UserRoleService) Create(c echo.Context) error {
	p := s.getParamByRequest(c)

	rsp,err := userRoleCli.Create(context.TODO(),p)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	return c.JSON(http.StatusCreated,rsp)
}

//删除
func (s UserRoleService) Delete(c echo.Context) error {
	d := s.getParamByRequest(c)

	rsp,err := userRoleCli.Delete(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		return c.JSON(http.StatusOK,rsp)
	}
}

//删除角色所有用户关联
func (s UserRoleService) DeleteAllUserForRole(c echo.Context) error {
	d := s.getParamByRequest(c)

	rsp,err := userRoleCli.DeleteRoles(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		return c.JSON(http.StatusOK,rsp)
	}
}

//删除用户所有角色关联
func (s UserRoleService) DeleteAllRoleForUser(c echo.Context) error {
	d := s.getParamByRequest(c)

	rsp,err := userRoleCli.DeleteUsers(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		return c.JSON(http.StatusOK,rsp)
	}
}
func (s UserRoleService) UserPage(c echo.Context) error {
	req := s.getPageUserParamByRequest(c,nil)

	rsp,err := userRoleCli.GetUserPageByRole(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		for key, value := range rsp.List {
			v := *value
			plugin.FmtDate(v,&v)
			rsp.List[key] = &v
		}
		return c.JSON(http.StatusCreated,rsp)
	}
}

func (s UserRoleService) RolePage(c echo.Context) error {
	req := s.getPageRoleParamByRequest(c,nil)

	rsp,err := userRoleCli.GetRolePageByUser(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		for key, value := range rsp.List {
			v := *value
			plugin.FmtDate(v,&v)
			rsp.List[key] = &v
		}
		return c.JSON(http.StatusCreated,rsp)
	}
}

func (s UserRoleService) getParamByRequest(c echo.Context) *userRole.UserRole {
	u := &userRole.UserRole{}
	u.UserId = c.Param("user_id")
	if govalidator.IsNull(u.UserId) {
		u.UserId = c.QueryParam("user_id")
	}
	u.RoleId = c.Param("role_id")
	if govalidator.IsNull(u.RoleId) {
		u.RoleId = c.QueryParam("role_id")
	}
	u.Domain = c.Param("domain")
	if govalidator.IsNull(u.Domain) {
		u.Domain = c.QueryParam("domain")
	}
	return u
}

func (s UserRoleService) getPageUserParamByRequest(c echo.Context,fields []string) *userRole.PageUserRequest {
	req := &userRole.PageUserRequest{}

	page := c.Param("page")
	if !govalidator.IsNull(page) {
		p,_ := strconv.ParseInt(page,10,64)
		req.Page = p
	}else {
		req.Page = config.Page
	}

	size := c.Param("size")
	if !govalidator.IsNull(size) {
		l,_ := strconv.ParseInt(size,10,64)
		req.Size = l
	}

	roleId := c.Param("role_id")
	req.RoleId = roleId

	search := c.Param("search")
	if !govalidator.IsNull(search) {
		m := make(map[string]string)
		for _, f := range fields {
			m[f] = search
		}
		req.Search = m
	}

	return req
}

func (s UserRoleService) getPageRoleParamByRequest(c echo.Context,fields []string) *userRole.PageRoleRequest {
	req := &userRole.PageRoleRequest{}

	page := c.Param("page")
	if !govalidator.IsNull(page) {
		p,_ := strconv.ParseInt(page,10,64)
		req.Page = p
	}else {
		req.Page = config.Page
	}

	size := c.Param("size")
	if !govalidator.IsNull(size) {
		l,_ := strconv.ParseInt(size,10,64)
		req.Size = l
	}

	uid := c.Param("user_id")
	req.UserId = uid

	search := c.Param("search")
	if !govalidator.IsNull(search) {
		m := make(map[string]string)
		for _, f := range fields {
			m[f] = search
		}
		req.Search = m
	}

	return req
}