package handler

import (
	"context"
	"github.com/labstack/echo"
	"github.com/micro/go-micro/client"
	"net/http"
	"pmm/api-srv/plugin"
	"pmm/public/config"
	nodeConfig "pmm/public/proto/node/config"
)

var nodeConfigCli = nodeConfig.NewNodeConfigServiceClient(config.Namespace+"srv.node",client.DefaultClient)

type NodeConfigService struct {
}

//新建
func (s NodeConfigService) Create(c echo.Context) error {
	p := s.getParamByRequest(c)

	rsp,err := nodeConfigCli.Create(context.TODO(),p)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	return c.JSON(http.StatusCreated,rsp.NodeConfig)
}

//更新
func (s NodeConfigService) Update(c echo.Context) error {
	p := s.getParamByRequest(c)

	rsp,err := nodeConfigCli.Update(context.TODO(),p)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.NodeConfig,rsp.NodeConfig)
		return c.JSON(http.StatusCreated,rsp.NodeConfig)
	}
}

//删除
func (s NodeConfigService) Delete(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := nodeConfigCli.Delete(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		return c.JSON(http.StatusOK,rsp)
	}
}

//启用
func (s NodeConfigService) Open(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := nodeConfigCli.Open(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.NodeConfig,rsp.NodeConfig)
		return c.JSON(http.StatusCreated,rsp.NodeConfig)
	}
}

//禁用
func (s NodeConfigService) Close(c echo.Context) error {
	d := plugin.GetIdByRequest(c)


	rsp,err := nodeConfigCli.Close(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.NodeConfig,rsp.NodeConfig)
		return c.JSON(http.StatusCreated,rsp.NodeConfig)
	}
}

//取得信息
func (s NodeConfigService) Get(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := nodeConfigCli.Get(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.NodeConfig,rsp.NodeConfig)
		return c.JSON(http.StatusCreated,rsp.NodeConfig)
	}
}


func (s NodeConfigService) getParamByRequest(c echo.Context) *nodeConfig.NodeConfig {
	u := &nodeConfig.NodeConfig{}
	plugin.GetParams(c,u)
	return u
}
