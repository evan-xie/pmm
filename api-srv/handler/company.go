package handler

import (
	"context"
	"fmt"
	"github.com/asaskevich/govalidator"
	"github.com/labstack/echo"
	"github.com/micro/go-micro/client"
	"net/http"
	"php"
	"pmm/api-srv/plugin"
	"pmm/public/config"
	"pmm/public/proto"
	"pmm/public/proto/company"
	"strconv"
)

var companyCli = company.NewCompanyServiceClient(config.Namespace+"srv.company",client.DefaultClient)
var DefaultCompany = company.Company{
	Id:"7e5ab68f-a017-4067-a9b4-a520e67f3876",
	Name:"上海绿然环境信息技术有限公司",
}

type CompanyService struct {
}


func (s CompanyService) Init()  {
	req := &company.ListRequest{}
	req.Company = &company.Company{
		Name:"上海绿然环境信息技术有限公司",
	}

	rsp,err := companyCli.GetList(context.TODO(),req)
	if err != nil {
		fmt.Println("公司初始化失败")
	}
	if len(rsp.List) > 0 {
		DefaultCompany = *rsp.List[0]
	}else {
		c := &company.Company{
			Name:"上海绿然环境信息技术有限公司",
		}
		rsp,err := companyCli.Create(context.TODO(),c)
		if err != nil {
			fmt.Println("公司初始化失败")
		}
		DefaultCompany = *rsp.Company
	}

}

//新建
func (s CompanyService) Create(c echo.Context) error {
	p := s.getParamByRequest(c)

	rsp,err := companyCli.Create(context.TODO(),p)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	return c.JSON(http.StatusCreated,rsp.Company)
}

//更新
func (s CompanyService) Update(c echo.Context) error {
	u := s.getParamByRequest(c)


	rsp,err := companyCli.Update(context.TODO(),u)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Company,rsp.Company)
		return c.JSON(http.StatusCreated,rsp.Company)
	}
}

//删除
func (s CompanyService) Delete(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := companyCli.Delete(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		return c.JSON(http.StatusOK,rsp)
	}
}

//启用
func (s CompanyService) Open(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := companyCli.Open(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Company,rsp.Company)
		return c.JSON(http.StatusCreated,rsp.Company)
	}
}

//禁用
func (s CompanyService) Close(c echo.Context) error {
	d := plugin.GetIdByRequest(c)


	rsp,err := companyCli.Close(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Company,rsp.Company)
		return c.JSON(http.StatusCreated,rsp.Company)
	}
}

//取得信息
func (s CompanyService) Get(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := companyCli.Get(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Company,rsp.Company)
		return c.JSON(http.StatusCreated,rsp.Company)
	}
}


func (s CompanyService) List(c echo.Context) error {
	u := s.getParamByRequest(c)
	d := s.getListRequestByRequest(c)
	req := &company.ListRequest{
		Default:d,
		Company:u,
	}

	rsp,err := companyCli.GetList(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		for key, value := range rsp.List {
			v := *value
			plugin.FmtDate(v,&v)
			rsp.List[key] = &v
		}
		return c.JSON(http.StatusCreated,plugin.FmtList(rsp.List))
	}
}

func (s CompanyService) Page(c echo.Context) error {
	u := s.getParamByRequest(c)
	d := s.getPageRequestByRequest(c)
	req := &company.PageRequest{
		Default:d,
		Company:u,
	}

	rsp,err := companyCli.GetPage(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		for key, value := range rsp.List {
			v := *value
			plugin.FmtDate(v,&v)
			rsp.List[key] = &v
		}
		return c.JSON(http.StatusCreated,rsp)
	}
}

func (s CompanyService) getParamByRequest(c echo.Context) *company.Company {
	u := &company.Company{}
	u.Name = c.QueryParam("name")
	u.Id = c.Param("id")
	return u
}

func (s CompanyService) getListRequestByRequest(c echo.Context) *proto.ListRequest {
	req := &proto.ListRequest{}
	ids := c.QueryParam("ids")
	if !govalidator.IsNull(ids) {
		ida := php.Explode(",",ids)
		req.Ids = ida
	}

	start := c.QueryParam("start")
	if !govalidator.IsNull(start) {
		si,_ := strconv.ParseInt(start,10,64)
		req.Start = si
	}

	limit := c.QueryParam("start")
	if !govalidator.IsNull(limit) {
		l,_ := strconv.ParseInt(limit,10,64)
		req.Limit = l
	}

	search := c.QueryParam("search")
	if !govalidator.IsNull(search) {
		req.Search["name"] = search
	}

	return req
}

func (s CompanyService) getPageRequestByRequest(c echo.Context) *proto.PageRequest {
	req := &proto.PageRequest{}
	ids := c.QueryParam("ids")
	if !govalidator.IsNull(ids) {
		ida := php.Explode(",",ids)
		req.Ids = ida
	}

	page := c.Param("page")
	if !govalidator.IsNull(page) {
		p,_ := strconv.ParseInt(page,10,64)
		req.Page = p
	}else {
		req.Page = config.Page
	}

	size := c.Param("size")
	if !govalidator.IsNull(size) {
		l,_ := strconv.ParseInt(size,10,64)
		req.Size = l
	}

	search := c.Param("search")
	if !govalidator.IsNull(search) {
		req.Search = map[string]string{
			"name":search,
		}
	}

	return req
}