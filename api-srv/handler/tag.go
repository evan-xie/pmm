package handler

import (
	"context"
	"github.com/labstack/echo"
	"github.com/micro/go-micro/client"
	"net/http"
	"pmm/api-srv/plugin"
	"pmm/public/config"
	"pmm/public/proto/tag"
)

var tagCli = tag.NewTagServiceClient(config.Namespace+"srv.tag",client.DefaultClient)

type TagService struct {
}

//新建
func (s TagService) CreateTag(c echo.Context) error {
	p := s.getParamByRequest(c)
	p.Type = "tag"

	rsp,err := tagCli.CreateTag(context.TODO(),p)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	return c.JSON(http.StatusCreated,rsp.Tag)
}

//新建
func (s TagService) CreateFolder(c echo.Context) error {
	p := s.getParamByRequest(c)
	p.Type = "folder"

	rsp,err := tagCli.CreateFolder(context.TODO(),p)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	return c.JSON(http.StatusCreated,rsp.Tag)
}

//更新
func (s TagService) Update(c echo.Context) error {
	p := s.getParamByRequest(c)

	rsp,err := tagCli.Update(context.TODO(),p)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Tag,rsp.Tag)
		return c.JSON(http.StatusCreated,rsp.Tag)
	}
}

//删除
func (s TagService) Delete(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := tagCli.Delete(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		return c.JSON(http.StatusOK,rsp)
	}
}

//启用
func (s TagService) Open(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := tagCli.Open(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Tag,rsp.Tag)
		return c.JSON(http.StatusCreated,rsp.Tag)
	}
}

//禁用
func (s TagService) Close(c echo.Context) error {
	d := plugin.GetIdByRequest(c)


	rsp,err := tagCli.Close(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Tag,rsp.Tag)
		return c.JSON(http.StatusCreated,rsp.Tag)
	}
}

//取得信息
func (s TagService) Get(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := tagCli.Get(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Tag,rsp.Tag)
		return c.JSON(http.StatusCreated,rsp.Tag)
	}
}

func (s TagService) List(c echo.Context) error {
	u := s.getParamByRequest(c)
	d := plugin.GetListRequestByRequest(c,[]string{"name","type"})
	req := &tag.ListRequest{
		Default:d,
		Tag:u,
	}

	rsp,err := tagCli.GetList(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		for _, value := range rsp.List {
			plugin.FmtDate(value,&value)
		}
		return c.JSON(http.StatusCreated,plugin.FmtList(rsp.List))
	}
}

func (s TagService) Tree(c echo.Context) error {
	p := &tag.TreeRequest{}
	plugin.GetParams(c,p)

	rsp,err := tagCli.GetTree(context.TODO(),p)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	s.fmtTree(rsp.Trees)
	tree := s.fmtTrees(rsp.Trees)
	return c.JSON(http.StatusCreated,tree)

}

func (s TagService) Page(c echo.Context) error {
	u := s.getParamByRequest(c)
	d := plugin.GetPageRequestByRequest(c,[]string{"name","type"})
	req := &tag.PageRequest{
		Default:d,
		Tag:u,
	}

	rsp,err := tagCli.GetPage(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		for key, value := range rsp.List {
			v := *value
			plugin.FmtDate(v,&v)
			rsp.List[key] = &v
		}
		return c.JSON(http.StatusCreated,rsp)
	}
}

func (s TagService) getParamByRequest(c echo.Context) *tag.Tag {
	u := &tag.Tag{}
	plugin.GetParams(c,u)
	return u
}

func (s TagService) fmtTree(trees []*tag.TagTree) {
	for _, tree := range trees {
		if tree.Child != nil {
			s.fmtTree(tree.Child)
		}
		plugin.FmtDate(*tree.Tag,tree.Tag)
	}
}

type FmtTree struct {
	tag.Tag
	Children []FmtTree `json:"children,omitempty"`
}


func (s TagService) fmtTrees(trees []*tag.TagTree) []FmtTree {
	var ts []FmtTree
	for _, tree := range trees {
		t := FmtTree{}
		t.Tag = *tree.Tag
		if tree.Child != nil {
			t.Children = s.fmtTrees(tree.Child)
		}
		ts = append(ts, t)
	}
	return ts
}