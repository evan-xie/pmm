package handler

import (
	"fmt"
	"github.com/labstack/echo"
	"github.com/micro/go-micro/client"
	"net/http"
	"pmm/api-srv/plugin"
	"pmm/public/config"
	"pmm/public/proto"
	"pmm/public/proto/node"
	"context"
	"pmm/public/proto/report"
	"pmm/public/proto/user"
	"pmm/public/utils"
	"strconv"
	"time"
)

var reportCli = report.NewReportServiceClient(config.Namespace+"srv.public",client.DefaultClient)

type ReportService struct {
}

//新建
func (s ReportService) Create(c echo.Context) error {
	p := s.getParamByRequest(c)
	if p.Title == "" {
		p.Title = time.Now().Format("2006年01月02日 15:04:05")+"项目报告"
	}
	if p.Value == "" {
		html,err := s.getNodeReport(p.ProjectId)
		if err != nil {
			return plugin.FmtErr(err,c)
		}
		p.Value = html
	}

	rsp,err := reportCli.Create(context.TODO(),p)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	plugin.FmtDate(*rsp.Report,rsp.Report)
	return c.JSON(http.StatusCreated,rsp.Report)
}

//更新
func (s ReportService) Update(c echo.Context) error {
	p := s.getParamByRequest(c)

	rsp,err := reportCli.Update(context.TODO(),p)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Report,rsp.Report)
		return c.JSON(http.StatusCreated,rsp.Report)
	}
}

//删除
func (s ReportService) Delete(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := reportCli.Delete(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		return c.JSON(http.StatusOK,rsp)
	}
}

//启用
func (s ReportService) Open(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := reportCli.Open(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Report,rsp.Report)
		return c.JSON(http.StatusCreated,rsp.Report)
	}
}

//禁用
func (s ReportService) Close(c echo.Context) error {
	d := plugin.GetIdByRequest(c)


	rsp,err := reportCli.Close(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Report,rsp.Report)
		return c.JSON(http.StatusCreated,rsp.Report)
	}
}

//取得信息
func (s ReportService) Get(c echo.Context) error {
	d := plugin.GetIdByRequest(c)

	rsp,err := reportCli.Get(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		plugin.FmtDate(*rsp.Report,rsp.Report)
		return c.JSON(http.StatusCreated,rsp.Report)
	}
}

func (s ReportService) List(c echo.Context) error {
	u := s.getParamByRequest(c)
	d := plugin.GetListRequestByRequest(c,[]string{"title"})
	req := &report.ListRequest{
		Default:d,
		Report:u,
	}

	rsp,err := reportCli.GetList(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		for key, value := range rsp.List {
			v := *value
			plugin.FmtDate(v,&v)
			rsp.List[key] = &v
		}
		list := plugin.FmtList(rsp.List)
		return c.JSON(http.StatusOK,list)
	}
}

func (s ReportService) Page(c echo.Context) error {
	u := s.getParamByRequest(c)
	d := plugin.GetPageRequestByRequest(c,[]string{"title"})
	req := &report.PageRequest{
		Default:d,
		Report:u,
	}

	rsp,err := reportCli.GetPage(context.TODO(),req)
	if err != nil {
		return plugin.FmtErr(err,c)
	}else {
		for key, value := range rsp.List {
			v := *value
			plugin.FmtDate(v,&v)
			rsp.List[key] = &v
		}
		return c.JSON(http.StatusCreated,rsp)
	}
}

func (s ReportService) SendMail(c echo.Context) error {
	d := plugin.GetIdByRequest(c)
	rsp,err := reportCli.Get(context.TODO(),d)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	if rsp.Report == nil {
		return c.JSON(http.StatusBadRequest,map[string]string{"error":"发送失败"})
	}
	r := rsp.Report
	users := s.getProjectUser(r.ProjectId)
	var msg string
	if len(users) > 0 {
		u := &proto.IdRequest{}
		u.Id = r.ProjectId
		prsp,err := projectCli.Get(context.TODO(),u)
		if err != nil {
			return plugin.FmtErr(err,c)
		}
		p := prsp.Project
		if p == nil {
			return c.JSON(http.StatusBadRequest,map[string]string{"error":"项目不存在"})
		}
		var s,f int
		for _, u := range users {
			if u.Email == "" {
				continue
			}
			re := utils.SendMail(u.Email,p.Title+"项目周报",r.Value,"html")
			if re != nil{
				f += 1
			}
			s  += 1
		}
		msg = "发送完毕："
		if s > 0 {
			msg += "成功"+strconv.Itoa(s)+"封"
		}
		if f > 0 {
			msg += "失败"+strconv.Itoa(f)+"封"
		}
		return c.JSON(http.StatusOK,map[string]string{
			"message":msg,
		})
	}else {
		msg = "发送失败：无用户"
		return c.JSON(http.StatusBadRequest,map[string]string{
			"error":msg,
		})
	}
}

func (s ReportService) getParamByRequest(c echo.Context) *report.Report {
	u := &report.Report{}
	plugin.GetParams(c,u)
	return u
}

func (s ReportService) GetReportHtml(c echo.Context) error {
	u := s.getParamByRequest(c)
	html,err := s.getNodeReport(u.ProjectId)
	if err != nil {
		return plugin.FmtErr(err,c)
	}
	m := map[string]string{
		"html":html,
	}
	return c.JSON(http.StatusOK,m)
}

func (s ReportService) getNodeReport(pid string) (string,error) {
	if pid == "" {
		return "", nil
	}
	u := &proto.IdRequest{}
	u.Id = pid
	rsp,err := projectCli.Get(context.TODO(),u)
	if err != nil {
		return "",err
	}
	p := rsp.Project
	if p == nil {
		return "", nil
	}

	ntreq := &node.TreeRequest{}
	ntreq.ProjectId = pid
	nrsp,err := nodeCli.GetTree(context.TODO(),ntreq)
	if err != nil {
		return "",err
	}
	var html = "<style> .box h1,.box h2{width:100%;text-align: center;} .main{width: 80%;margin:0 auto;border:1px solid black;}td{padding:0 15px;}</style><div class='box'><h1>Weekly Progress Report</h1><h2>项目周报</h2><div class='content'><div class='main'><table width='100%' height='123' border='1' cellpadding='0' cellspacing='0'><tr><td ><h3> 一、目前项目完成情况</br>Accomplished Tasks in Last Week</h3></td> </tr><tr><td width='100%'><ul>"
	html += s.getNodeHtml(nrsp.Trees)
	html += "</ul></td></tr>"
	html += "<tr><td><h3>二、沟通记录 Communication Records</h3></td></tr></tr><tr><td><strong >"+p.Remark+"</br></strong></td></tr>"
	html += "<tr><td ><h3>三、下周计划：Planned Tasks for Next Week</h3></td></tr><tr><td><strong ></br></strong></td></tr>"
	html += "</table></div></div></div>"

	return html, nil
}

func (s ReportService) getNodeHtml(treeNode []*node.NodeTree) string {
	var html string
	for _, tree := range treeNode {
		n := tree.Node

		//判断是否完成
		if n.Progress >= 1 {
			ua, err := time.Parse("2006-01-02T15:04:05+08:00", n.UpdatedAt)
			if err != nil {
				ua,_ = time.Parse("2006-01-02 15:04:05",n.UpdatedAt)
			}
			ts := ua.Format("2006年01月02日")
			html += "<li>"+ts+" <strong>已完成 "+n.Title+"</strong>"
		}else{
			ts := time.Now().Format("2006年01月02日")
			p := fmt.Sprintf("%.2f",n.Progress*100)+"%"
			html += "<li>"+ts+" <strong>"+n.Title+"</strong> 的进度为"+p
		}
		if len(tree.Child) > 0 {
			html += "<ul>"
			html += s.getNodeHtml(tree.Child)
			html += "</ul>"
		}
		html += "</li>"
	}
	return html
}

func (s ReportService) getProjectUser(pid string) []*user.User{
	projectUserSrv := new(ProjectUserService)
	list,err := projectUserSrv.GetUserListByPid(pid)
	if err != nil {
		return nil
	}
	return list
}