package token

import (
	"github.com/dgrijalva/jwt-go"
	"time"
	"pmm/user-srv/config"
	"pmm/public/utils"
)


// 在加密后作为 JWT 的第二部分返回给客户端
type CustomClaims struct {
	jwt.StandardClaims
}

type TokenUtils struct {

}

/**
	取得token字符串
	tokenType 包含 access 和 refresh 两种
	access:标识验证
	refresh:标识刷新
 */
func (t *TokenUtils) Encode(uid string,tokenType string) (token string, err error) {
	l := config.AccessTokenLifeCycle
	k := config.AccessTokenPrivateKey
	if tokenType == "refresh" {
		l = config.RefreshTokenLifeCycle
		k = config.RefreshTokenPrivateKey
	}

	//过期时间
	ext := time.Now().Add(l).Unix()
	claims := CustomClaims{
		jwt.StandardClaims{
			Issuer:config.ServiceName,//签发者
			IssuedAt:time.Now().Unix(),//签发时间
			Subject:uid,//签发的对象
			Audience:"*",//接受对象 默认为空/*
			ExpiresAt:ext,//过期时间
			NotBefore:time.Now().Unix(),//生效时间
			Id:utils.Md5(uid),//唯一标识
		},
	}
	jwtToken := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return jwtToken.SignedString([]byte(k))
}

func (t *TokenUtils) Decode(tokenStr string,tokenType string) (*CustomClaims, error) {
	k := config.AccessTokenPrivateKey
	if tokenType == "refresh" {
		k = config.RefreshTokenPrivateKey
	}
	key := []byte(k)

	t1,err := jwt.ParseWithClaims(tokenStr, &CustomClaims{}, func(token *jwt.Token) (interface{}, error) {
		return key,nil
	})

	if err != nil {
		return nil,err
	}

	// 解密转换类型并返回
	if claims, ok := t1.Claims.(*CustomClaims); ok && t1.Valid {
		return claims, nil
	} else {
		return nil, err
	}

}
