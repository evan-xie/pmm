package verify

import (
	"encoding/json"
	gv "github.com/asaskevich/govalidator"
	mes "github.com/micro/go-micro/errors"
	"math"
	"php"
	"pmm/public/utils"
	"pmm/public/utils/validate"
	sconf "pmm/user-srv/config"
	"pmm/user-srv/database"
	"strconv"
	"time"
)

type Verify struct {
	Set int64
	Code int64
	Number int64
}

var redis = database.ConnRedis()

func (v *Verify) PhoneCode(mobile string) (remain int64,err error) {
	if !validate.IsMobile(mobile){
		return 0,mes.BadRequest("user-srv","手机号码格式不正确")
	}

	cn := sconf.VerifyCodePrefix+mobile
	i := sconf.VerifyCodeInterval

	val,err := redis.Get(cn).Result()
/*	if err != nil {
		return 0,mes.InternalServerError("user-srv","Redis服务器异常,请稍后再试")
	}*/
	
	if !gv.IsNull(val) {
		var m Verify
		json.Unmarshal([]byte(val),&m)
		t := time.Now().Unix()- m.Set
		return int64(i) - t,nil
	}

	code := php.Rand(100000,999999)
	l := sconf.VerifyCodeLifeCycle
	if err := v.push(code,int64(l),mobile);err != nil {
		return 0,err
	}
/*	x := math.Ceil(float64(l)/60)
	msg := "短信验证码为:  "+strconv.FormatInt(code,10)+"  ,有效期为"+strconv.Itoa(int(x))+"分钟【绿然环境】"
	if err := utils.SendSMS(mobile,msg);err != nil {
		return 0,mes.InternalServerError("user-srv",err.Error())
	}*/
	data := Verify{
		Set:time.Now().Unix(),
		Code:code,
		Number:int64(sconf.VerifyCodeMaxNumber),
	}
	j,_ := json.Marshal(data)
	ex := time.Duration(l)*time.Second
	err = redis.Set(cn,j,ex).Err()
	if err != nil {
		return 0,mes.InternalServerError("user-srv","Redis服务器异常,请稍后再试")
	}
	return int64(i),nil
}

func (v *Verify) EmailCode(email string) (remain int64,err error) {
	if !gv.IsEmail(email){
		return 0,mes.BadRequest("user-srv","邮箱格式不正确")
	}

	cn := sconf.VerifyCodePrefix+email
	i := sconf.VerifyCodeInterval

	val,err := redis.Get(cn).Result()
	/*	if err != nil {
			return 0,mes.InternalServerError("user-srv","Redis服务器异常,请稍后再试")
		}*/
	if !gv.IsNull(val) {
		var m Verify
		json.Unmarshal([]byte(val),&m)
		t := time.Now().Unix()- m.Set
		return int64(i) - t,nil
	}

	code := php.Rand(100000,999999)
	l := sconf.VerifyCodeLifeCycle
	if err := v.push(code,int64(l),email);err != nil {
		return 0,err
	}
	/*	x := math.Ceil(float64(l)/60)
		msg := "短信验证码为:  "+strconv.FormatInt(code,10)+"  ,有效期为"+strconv.Itoa(int(x))+"分钟【绿然环境】"
		if err := utils.SendSMS(email,msg);err != nil {
			return 0,mes.InternalServerError("user-srv",err.Error())
		}*/
	data := Verify{
		Set:time.Now().Unix(),
		Code:code,
		Number:int64(sconf.VerifyCodeMaxNumber),
	}
	j,_ := json.Marshal(data)
	ex := time.Duration(l)*time.Second
	err = redis.Set(cn,j,ex).Err()
	if err != nil {
		return 0,mes.InternalServerError("user-srv","Redis服务器异常,请稍后再试")
	}
	return int64(i),nil
}

func (v *Verify) CheckCode(key string,code int64) bool {
	if gv.IsNull(key) || code == 0 {
		return false
	}
	
	cn := sconf.VerifyCodePrefix+key
	val,_ := redis.Get(cn).Result()
	if gv.IsNull(val) {
		return false
	}
	
	var m Verify
	json.Unmarshal([]byte(val),&m)
	if m.Code != code {
		if m.Number <= 0 {
			return false
		}
		m.Number --
		j,_ := json.Marshal(m)
		ex := time.Duration(int64(sconf.VerifyCodeLifeCycle) - time.Now().Unix() - m.Set)*time.Second
		redis.Set(cn,j,ex)
		return false
	}
	redis.Del(cn)
	return true
}

func (v *Verify) push(code,lifecycle int64,to string) error {
	x := math.Ceil(float64(lifecycle)/60)
	if gv.IsEmail(to) {
		title := "邮箱验证码[绿然环境]"
		msg := `<table cellpadding=0 cellspacing=0 border=0 dir=LTR style="   background-color: #F0F7FC;   border: 1px solid #A5CAE4;   border-radius: 5px;   direction: LTR;">
					<tbody>
						<tr>
							<td style="background-color: #D7EDFC;padding: 5px 10px;border-bottom: 1px solid #A5CAE4;border-top-left-radius: 4px;border-top-right-radius: 4px;font-family: \'Trebuchet MS\', Helvetica, Arial, sans-serif;font-size: 11px;line-height: 1.231;">
								<a href="#" style="color: #176093; text-decoration:none" target=_blank>尊敬的用户:`+to+`</a>
						<tr><td style="background-color: #FCFCFF;padding: 1em;color: #141414;font-family: \'Trebuchet MS\', Helvetica, Arial, sans-serif;font-size: 13px;line-height: 1.231;">
								<p style="margin-top: 0">'.'您的邮件验证码为:  `+strconv.FormatInt(code,10)+`  ,有效期为 `+strconv.Itoa(int(x))+` 分钟 ，</p><p>感谢使用！<br>上海绿然环境</p><tr><td style="background-color: #F0F7FC;padding: 5px 10px;border-top: 1px solid #D7EDFC;border-bottom-left-radius: 4px;border-bottom-right-radius: 4px;text-align: right;font-family: \'Trebuchet MS\', Helvetica, Arial, sans-serif;font-size: 11px;line-height: 1.231;"></table>`
		err := utils.SendMail(to,title,msg,"html")
		if err != nil {
			return mes.InternalServerError("user-srv",err.Error())
		}
	}
	if validate.IsMobile(to){
		msg := "短信验证码为:  "+strconv.FormatInt(code,10)+"  ,有效期为"+strconv.Itoa(int(x))+"分钟【绿然环境】"
		if err := utils.SendSMS(to,msg);err != nil {
			return mes.InternalServerError("user-srv",err.Error())
		}
	}
	return nil
}