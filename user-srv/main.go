package main

import (
	"pmm/public/proto/verify/code"
	"pmm/public/proto/wxApp"
	"pmm/user-srv/database"
	"log"
	pb "pmm/public/proto/user"
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/server"
	"pmm/user-srv/config"
	"pmm/user-srv/handler"
		pbr "pmm/public/proto/role"
	"github.com/casbin/casbin"
	pbur "pmm/public/proto/user/role"
	"pmm/public/proto/rule"
	pbrr "pmm/public/proto/role/rule"
	"pmm/public/proto/auth"
)

func main() {
	//连接数据库
	db,err := database.CreateConnection()
	defer db.Close()
	db.LogMode(true)

	if err != nil {
		log.Fatalf("数据库连接失败: %v\n",err)
	}

	// 全局禁用表名复数
	db.SingularTable(true) // 如果设置为true,`User`的默认表名为`user`,使用`TableName`设置的表名不受影响

	// 自动检查 表 结构是否变化
	db.AutoMigrate(database.User{})//用户
	db.AutoMigrate(database.AuthUserRole{})//用户角色关联
	db.AutoMigrate(database.Role{})//角色
	db.AutoMigrate(database.Rule{})//权限节点
	db.AutoMigrate(database.WxApp{})//微信登录

	//casbin连接数据库
	a := database.ConnAuthCasbin()
	e := casbin.NewEnforcer("./user-srv/config/rbac_model.conf", a,false)
	// Load the policy from DB.
	e.LoadPolicy()
	//b := e.Enforce("efea76d6-193a-4b6a-b362-fcdde70847f8","测试28","*","app1")
	//log.Println(b)
	srv := micro.NewService(
		micro.Name(config.ServiceName),
		micro.Version("latest"),
	)

	srv.Init()

	//注册推送监听

	//注册服务
	pb.RegisterUserServiceHandler(srv.Server(),handler.NewUserHandler(),server.InternalHandler(true))
	//au.RegisterAuthUserRoleServiceHandler(srv.Server(),handler.NewAuthUserRoleHandler(),server.InternalHandler(true))
	pbr.RegisterRoleServiceHandler(srv.Server(),handler.NewRoleHandler(),server.InternalHandler(true))
	pbur.RegisterUserRoleServiceHandler(srv.Server(),handler.NewUserRoleHandler(e),server.InternalHandler(true))
	rule.RegisterRuleServiceHandler(srv.Server(),handler.NewRuleHandler(handler.NewRoleRuleHandler(e)),server.InternalHandler(true))
	pbrr.RegisterRoleRuleServiceHandler(srv.Server(),handler.NewRoleRuleHandler(e),server.InternalHandler(true))
	auth.RegisterAuthServiceHandler(srv.Server(),handler.NewAuthHandler(e),server.InternalHandler(true))
	code.RegisterVerifyCodeServiceHandler(srv.Server(),handler.NewVerifyCodeHandler(),server.InternalHandler(true))
	wxApp.RegisterWxAppServiceHandler(srv.Server(),handler.NewUserHandler(),server.InternalHandler(true))

	log.Println("启动user-srv服务...")
	if err := srv.Run(); err != nil {
		log.Fatalf("user-srv服务启动失败: %v\n", err)
	}
}
