package config

import (
	"time"
	"pmm/public/config"
)

const RuleTypeFile  = "file" //权限节点
const RuleTypeMennu  = "menu" //权限节点
const IsMenuTrue  = 1
const IsMenuFalse = -1

var (
	ServiceName = config.Namespace + "srv.user"
	//access token 生命周期 15min
	AccessTokenLifeCycle = time.Minute * 60 * 24 * 30
	//access key 秘钥
	AccessTokenPrivateKey = "com.greenment.user.auth.access.token.private.key"

	//refresh Token 生命周期 30day
	RefreshTokenLifeCycle = time.Hour * 24 * 30
	//refresh key 秘钥
	RefreshTokenPrivateKey = "com.greenment.user.auth.refresh.token.private.key"

	//默认每页条数
	PageSize int64 = 20
	//默认页数
	Page int64 = 1
	//默认最大每页条数
	DefaultMaxPageSize int64 = 100

	//默认规则类型
	DefaultRuleType = RuleTypeFile
	RuleTypes = []string{RuleTypeFile,RuleTypeMennu}

	//默认Rule顶层pid值
	DefaultRuleTopPidValue = "-"
	//默认domain值
	DefaultRuleDomainValue = "*"

	//有效期 秒
	VerifyCodeLifeCycle = 1800
	//间隔时间 秒
	VerifyCodeInterval = 60
	//最大验证次数
	VerifyCodeMaxNumber = 5
	//前缀
	VerifyCodePrefix = "validate_code:"

	//微信小程序相关
	AppID = "wx1cca5405a5dde3f8"
	AppSecret = "a37b33daf578aaa729b8aff3e4599c87"
	CodeToSessURL = "https://api.weixin.qq.com/sns/jscode2session?appid=APPID&secret=SECRET&js_code=JSCODE&grant_type=authorization_code"
	AccessTokenURL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=SECRET"
	CreateWXAQRCode = "https://api.weixin.qq.com/cgi-bin/wxaapp/createwxaqrcode?access_token=ACCESS_TOKEN"
)
