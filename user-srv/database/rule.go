package database

import (
	"pmm/public/model"
	pb "pmm/public/proto/rule"
	"pmm/public/config"
	"pmm/public/utils"
	"github.com/jinzhu/gorm"
	"time"
	"github.com/satori/go.uuid"
	"log"
)

// 设置的表名
func (Rule) TableName() string {
	return "rule"
}

type Rule struct {
	pb.Rule
	model.Comm
}

/**
创建
 */
func (repo *Rule) Create(data *pb.Rule) (*Rule,error) {
	d := &Rule{
		Rule:*data,
	}
	if err := Conn.Create(&d).Error; err != nil {
		return nil,err
	}
	return d,nil
}

/**
更新
condition 条件 参数1为是否更新密码 2位是否更新状态
 */
func (repo *Rule) Update(data *pb.Rule,condition... string) (error)  {
	d := &Rule{}
	d.Rule = *data

	query := Conn.Model(&d)
	//过滤参数
	if len(condition) >= 1 {
		query = query.Omit(condition...)
	}

	if err := query.Updates(d).Error;err != nil {
		return err
	}
	return nil
}

/**
 判断是否存在 存在返回查询到的结果
 */
func (repo *Rule) IsExist(pb *pb.Rule,isOpen bool) (*Rule) {
	if pb == nil {
		return nil
	}

	d := &Rule{}
	d.Name = pb.Name
	d.Title = pb.Title
	d.Pid = pb.Pid
	d.Domain = pb.Domain

	query := Conn
	if isOpen {
		query = query.Where("status = ?",config.OPEN_STATUS)
	}
	err := query.Where(pb).First(&d).Error

	if err == nil {
		return d
	}

	return nil
}

/**
根据id取得信息
 */
func (repo *Rule) GetInfoById(id string,isOpen bool) (*Rule) {
	if id == "" {
		return nil
	}

	d := &Rule{}

	query := Conn.Where("id = ?",id)
	if isOpen {
		query = query.Where("status = ?",config.OPEN_STATUS)
	}
	err := query.First(&d).Error

	if err == nil {
		return d
	}

	return nil
}

/**
根据编号删除
 */
func (repo *Rule) Delete(id ...string) (error) {
	str := ""
	if len(id) > 1 {
		str = "id in ("
		for _, value := range id {
			str = str+"'"+value+"',"
		}
		str = string([]rune(str)[:len(str)-1])+")"
	}else {
		str = "id = '" + id[0] +"'"
	}

	err := Conn.Where(str).Delete(&Rule{}).Error

	if err != nil {
		return err
	}

	return nil
}

/**
取得列表
 */
func (repo *Rule) GetList(req *pb.ListRequest,args ...interface{}) ([]*pb.Rule,error) {
	var list []*pb.Rule
	d := req.Rule
	fs := utils.GetDbNameByStruct(Conn,pb.Rule{})
	query := Conn.Model(&Rule{}).Where(d)
	//过滤参数
	if len(fs) > 1 {
		query = query.Select(fs)
	}
	if req.Default != nil {
		if req.Default.Start > 0 {
			query = query.Offset(req.Default.Start)
		}
		if req.Default.Limit > 0 {
			query = query.Limit(req.Default.Limit)
		}
		if req.Default.Search != nil {
			query = utils.AndOrLikes(query,req.Default.Search,true)
		}
		if req.Default.Ids != nil {
			query = query.Where(req.Default.Ids)
		}
	}
	if args != nil && len(args) > 0 {
		for _, value := range args {
			query = query.Where(value)
		}
	}
	err := query.Scan(&list).Error
	if err != nil {
		return nil,err
	}
	return list,nil
}

func (repo *Rule) GetCount(req *pb.PageRequest,args ...interface{}) (*int64,error) {
	data := req.Rule
	query := Conn.Model(&Rule{}).Where(data)
	if req.Default!= nil && req.Default.Search != nil {
		m := utils.MapToWhereStr(req.Default.Search)
		query = query.Where(m)
	}
	if args != nil && len(args) > 0 {
		for _, value := range args {
			query = query.Where(value)
		}
	}
	var count *int64
	err := query.Count(&count).Error
	if err != nil {
		return count,err
	}
	return count,nil
}

func (repo *Rule) BeforeCreate(scope *gorm.Scope) error {
	uid, err := uuid.NewV4()
	if err != nil {
		log.Fatalf("created uuid error: %v\n", err)
	}
	scope.SetColumn("Id", uid.String())
	scope.SetColumn("CreatedAt", time.Now().Format("2006-01-02 15:04:05"))
	scope.SetColumn("UpdatedAt", time.Now().Format("2006-01-02 15:04:05"))
	scope.SetColumn("Status", config.OPEN_STATUS)
	return nil
}