package database

import (
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
	"pmm/public/config"
	"pmm/public/model"
	pb "pmm/public/proto/user"
	"pmm/public/utils"
)

// 设置User的表名为`user`
func (User) TableName() string {
	return "user"
}

type User struct {
	model.Comm
	pb.User
}

/**
创建
 */
func (repo *User) Create(u *pb.User) (*User,error) {
	hashedPwd,err := repo.GetHashPassword(u.Password)
	if err != nil {
		return nil,err
	}
	u.Password = hashedPwd

	user := &User{
		User:*u,
	}
	if err := Conn.Create(&user).Error; err != nil {
		return nil,err
	}
	return user,nil
}

/**
更新
condition 条件 参数1为是否更新密码 2位是否更新状态
 */
func (repo *User) Update(u *pb.User,condition... string) (error)  {
	if u.Password != "" {
		hashedPwd,err := repo.GetHashPassword(u.Password)
		if err != nil {
			return err
		}
		u.Password = hashedPwd
	}
	user := &User{}
	user.User = *u

	query := Conn.Model(&user)
	//过滤参数
	if len(condition) >= 1 {
		query = query.Omit(condition...)
	}

	if err := query.Updates(user).Error;err != nil {
		return err
	}
	return nil
}


/**
 判断是否存在 存在返回查询到的结果
 */
func (repo *User) IsExist(u *pb.User,isOpen bool) (*User) {
	if u == nil {
		return nil
	}

	m := map[string]interface{}{
		"mobile":u.Mobile,
		"email":u.Email,
		"username":u.Username,
		"id":u.Id,
	}

	user := &User{}

	query := Conn
	if isOpen {
		query = query.Where("status = ?",config.OPEN_STATUS)
	}
	err := utils.AndOrs(query,m,true).First(&user).Error

	if err == nil {
		return user
	}

	return nil
}

/**
根据id取得用户信息
 */
func (repo *User) GetInfoById(id string,isOpen bool) (*User) {
	if id == "" {
		return nil
	}

	user := &User{}

	query := Conn.Where("id = ?",id)
	if isOpen {
		query = query.Where("status = ?",config.OPEN_STATUS)
	}
	err := query.First(&user).Error

	if err == nil {
		return user
	}

	return nil
}

/**
根据编号删除用户
 */
func (repo *User) Delete(id string) (error) {
	u := &pb.User{
		Id:id,
	}
	user := &User{
		User:*u,
	}

	err := Conn.Delete(&user).Error

	if err != nil {
		return err
	}

	return nil
}

func (repo *User) OperationDB(s interface{}) (*gorm.DB) {
	return Conn.Model(s)
}

/**
取得列表
 */
func (repo *User) GetList(req *pb.ListRequest,args ...interface{}) ([]*pb.User,error) {
	var users []*pb.User
	user := req.User
	fs := utils.GetDbNameByStruct(Conn,pb.User{},"password")
	query := Conn.Model(&User{}).Where(user)

	//过滤参数
	if len(fs) > 1 {
		query = query.Select(fs)
	}
	if req.Default != nil {
		if req.Default.Start > 0 {
			query = query.Offset(req.Default.Start)
		}
		if req.Default.Limit > 0 {
			query = query.Limit(req.Default.Limit)
		}
		if req.Default.Search != nil {
			//m := utils.MapToWhereStr(req.Default.Search)
			//query = query.Where(m)
			query = utils.AndOrLikes(query,req.Default.Search,true)
		}
		if len(req.Default.Ids) > 0{
			query = query.Where(req.Default.Ids)
		}
	}
	if args != nil && len(args) > 0 {
		for _, value := range args {
			query = query.Where(value)
		}
	}

	err := query.Scan(&users).Error
	if err != nil {
		return nil,err
	}
	return users,nil
}

func (repo *User) GetCount(req *pb.PageRequest,args ...interface{}) (*int64,error) {
	user := req.User
	query := Conn.Model(&User{}).Where(user)
	if req.Default != nil {
		if req.Default.Search != nil {
			query = utils.AndOrLikes(query,req.Default.Search,true)
		}
		if len(req.Default.Ids) > 0 {
			query = query.Where(req.Default.Ids)
		}
	}

	if args != nil && len(args) > 0 {
		for _, value := range args {
			query = query.Where(value)
		}
	}

	var count *int64
	err := query.Count(&count).Error
	if err != nil {
		return count,err
	}
	return count,nil
}
/**
取得hash密码
 */
func (repo *User) GetHashPassword(pwd string) (string,error) {
	// 哈希处理用户输入的密码
	hashedPwd,err := bcrypt.GenerateFromPassword([]byte(pwd),bcrypt.DefaultCost)
	if err != nil {
		return "",err
	}
	return string(hashedPwd),nil
}
