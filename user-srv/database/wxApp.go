package database

import (
	"github.com/jinzhu/gorm"
	"pmm/public/config"
	"pmm/public/model"
	"pmm/public/proto/wxApp"
	"pmm/public/utils"
)

// 设置User的表名为`user`
func (WxApp) TableName() string {
	return "wxApp"
}

type WxApp struct {
	model.Comm
	wxApp.WxLogin
}

/**
创建
 */
func (repo *WxApp) Create(u *wxApp.WxLogin) (*wxApp.WxLogin,error) {
	data := &WxApp{
		WxLogin:*u,
	}
	if err := Conn.Create(&data).Error; err != nil {
		return nil,err
	}
	return &data.WxLogin,nil
}

/**
更新
condition 条件 参数1为是否更新密码 2位是否更新状态
 */
func (repo *WxApp) Update(u *wxApp.WxLogin,condition... string) (error)  {
	data := &WxApp{}
	data.WxLogin = *u

	query := Conn.Model(&data)
	//过滤参数
	if len(condition) >= 1 {
		query = query.Omit(condition...)
	}

	if err := query.Updates(data).Error;err != nil {
		return err
	}
	return nil
}


/**
 判断是否存在 存在返回查询到的结果
 */
func (repo *WxApp) IsExist(u *wxApp.WxLogin,isOpen bool) (*WxApp) {
	if u == nil {
		return nil
	}

	m := map[string]interface{}{
		"uid":u.Uid,
		"openid":u.Openid,
		"unionid":u.Unionid,
	}

	data := WxApp{}

	query := Conn
	if isOpen {
		query = query.Where("status = ?",config.OPEN_STATUS)
	}
	err := utils.AndOrs(query,m,true).First(&data).Error

	if err == nil {
		return &data
	}

	return nil
}

/**
根据id取得用户信息
 */
func (repo *WxApp) GetInfoById(id string,isOpen bool) (*WxApp) {
	if id == "" {
		return nil
	}

	user := &WxApp{}

	query := Conn.Where("uid = ?",id)
	if isOpen {
		query = query.Where("status = ?",config.OPEN_STATUS)
	}
	err := query.First(&user).Error

	if err == nil {
		return user
	}

	return nil
}

/**
根据编号删除用户
 */
func (repo *WxApp) Delete(id string) (error) {
	u := &wxApp.WxLogin{
		Uid:id,
	}
	data := &WxApp{
		WxLogin:*u,
	}

	err := Conn.Delete(&data).Error

	if err != nil {
		return err
	}

	return nil
}

func (repo *WxApp) OperationDB(s interface{}) (*gorm.DB) {
	return Conn.Model(s)
}

