package database

import (
	"pmm/public/model"
	 pb "pmm/public/proto/aur"
	"pmm/public/config"
	"pmm/public/utils"
	"github.com/jinzhu/gorm"
	"time"
	"github.com/satori/go.uuid"
	"log"
)

// 设置的表名
func (AuthUserRole) TableName() string {
	return "auth_user_role"
}

type AuthUserRole struct {
	pb.AuthUserRole
	model.Comm
}

/**
创建
 */
func (repo *AuthUserRole) Create(data *pb.AuthUserRole) (*AuthUserRole,error) {
	d := &AuthUserRole{
		AuthUserRole:*data,
	}
	if err := Conn.Create(&d).Error; err != nil {
		return nil,err
	}
	return d,nil
}

/**
更新
condition 条件 参数1为是否更新密码 2位是否更新状态
 */
func (repo *AuthUserRole) Update(data *pb.AuthUserRole,condition... string) (error)  {
	d := &AuthUserRole{}
	d.AuthUserRole = *data

	query := Conn.Model(&d)
	//过滤参数
	if len(condition) >= 1 {
		query = query.Omit(condition...)
	}

	if err := query.Updates(d).Error;err != nil {
		return err
	}
	return nil
}

/**
 判断是否存在 存在返回查询到的结果
 */
func (repo *AuthUserRole) IsExist(pb *pb.AuthUserRole,isOpen bool) (*AuthUserRole) {
	if pb == nil {
		return nil
	}

	d := &AuthUserRole{}
	d.UserId = pb.UserId
	d.AuthRoleId = pb.AuthRoleId

	query := Conn
	if isOpen {
		query = query.Where("status = ?",config.OPEN_STATUS)
	}
	err := query.Where(pb).First(&d).Error

	if err == nil {
		return d
	}

	return nil
}

/**
根据id取得信息
 */
func (repo *AuthUserRole) GetInfoById(id string,isOpen bool) (*AuthUserRole) {
	if id == "" {
		return nil
	}

	d := &AuthUserRole{}

	query := Conn.Where("id = ?",id)
	if isOpen {
		query = query.Where("status = ?",config.OPEN_STATUS)
	}
	err := query.First(&d).Error

	if err == nil {
		return d
	}

	return nil
}

/**
根据编号删除
 */
func (repo *AuthUserRole) Delete(id ...string) (error) {
	str := ""
	if len(id) > 1 {
		str = "id in ("
		for _, value := range id {
			str = str+"'"+value+"',"
		}
		str = string([]rune(str)[:len(str)-1])+")"
	}else {
		str = "id = '" + id[0] +"'"
	}

	err := Conn.Where(str).Delete(&AuthUserRole{}).Error

	if err != nil {
		return err
	}

	return nil
}

/**
取得列表
 */
func (repo *AuthUserRole) GetList(req *pb.ListRequest) ([]*pb.AuthUserRole,error) {
	var list []*pb.AuthUserRole
	d := req.AuthUserRole
	fs := utils.GetDbNameByStruct(Conn,pb.AuthUserRole{})
	query := Conn.Model(&AuthUserRole{}).Where(d)
	//过滤参数
	if len(fs) > 1 {
		query = query.Select(fs)
	}
	if req.Start > 0 {
		query = query.Offset(req.Start)
	}
	if req.Limit > 0 {
		query = query.Limit(req.Limit)
	}
	if req.Search != nil {
		m := utils.MapToWhereStr(req.Search)
		query = query.Where(m)
	}

	err := query.Scan(&list).Error
	if err != nil {
		return nil,err
	}
	return list,nil
}

func (repo *AuthUserRole) GetCount(req *pb.PageRequest) (*int64,error) {
	data := req.AuthUserRole
	query := Conn.Model(&AuthUserRole{}).Where(data)
	if req.Search != nil {
		m := utils.MapToWhereStr(req.Search)
		query = query.Where(m)
	}
	var count *int64
	err := query.Count(&count).Error
	if err != nil {
		return count,err
	}
	return count,nil
}

func (repo *AuthUserRole) BeforeCreate(scope *gorm.Scope) error {
	uid, err := uuid.NewV4()
	if err != nil {
		log.Fatalf("created uuid error: %v\n", err)
	}
	scope.SetColumn("Id", uid.String())
	scope.SetColumn("CreatedAt", time.Now().Format("2006-01-02 15:04:05"))
	scope.SetColumn("UpdatedAt", time.Now().Format("2006-01-02 15:04:05"))
	scope.SetColumn("Status", config.OPEN_STATUS)
	return nil
}