package handler

import (
	"context"
	pb "pmm/public/proto/verify/code"
	"pmm/user-srv/utils/verify"
)

type VerifyCodeHandler struct {
	//logger *zap.Logger
	v *verify.Verify
}

func NewVerifyCodeHandler() *VerifyCodeHandler{
	return &VerifyCodeHandler{
		//logger:tl.Instance().Named("UserHandler"),
		v:&verify.Verify{},
	}
}

func (h *VerifyCodeHandler) PhoneCode(ctx context.Context,req *pb.Request,rsp *pb.Response)error {
	r,err := h.v.PhoneCode(req.Mobile)
	if err != nil {
		return err
	}
	rsp.Remain = r
	return nil
}

func (h *VerifyCodeHandler) EmailCode(ctx context.Context,req *pb.Request,rsp *pb.Response)error {
	r,err := h.v.EmailCode(req.Mobile)
	if err != nil {
		return err
	}
	rsp.Remain = r
	return nil
}