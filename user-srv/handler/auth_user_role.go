package handler

import (
	"github.com/jinzhu/gorm"
	"log"
	pb "pmm/public/proto/aur"
	"context"
	mes "github.com/micro/go-micro/errors"
	gv "github.com/asaskevich/govalidator"
	"github.com/pkg/errors"
	sconf "pmm/user-srv/config"
	"pmm/public/utils/validate"
	"pmm/public/config"
	"math"
	"pmm/public/proto"
	"pmm/user-srv/database"
)

type AuthUserRoleHandler struct {
	//logger *zap.Logger
	Model database.AuthUserRole
	db *gorm.DB
}


// new一个TagHandler
func NewAuthUserRoleHandler() *AuthUserRoleHandler{
	return &AuthUserRoleHandler{
		//logger:tl.Instance().Named("TagHandler"),
		Model:database.AuthUserRole{},
		db:database.Conn,
	}
}


func (h *AuthUserRoleHandler) Create(ctx context.Context,req *pb.DefaultRequest,rsp *pb.Response)error{
	log.Println("新增...")
	if gv.IsNull(req.UserId) {
		return errors.New(mes.BadRequest("auth-srv","用户编号不能为空").Error())
	}
	if gv.IsNull(req.AuthRoleId) {
		return errors.New(mes.BadRequest("auth-srv","角色编号不能为空").Error())
	}

	data := &pb.AuthUserRole{
		UserId:req.UserId,
		AuthRoleId:req.AuthRoleId,
	}
	check := h.Model.IsExist(data,false)
	if check != nil {
		return errors.New(mes.BadRequest("auth-srv","用户与角色关联关系已存在,请勿重复创建").Error())
	}

	info,err := h.Model.Create(data)
	if err != nil {
		return errors.New(mes.InternalServerError("auth-srv",err.Error()).Error())
	}

	rsp.AuthUserRole = &info.AuthUserRole
	return nil
}


func (h *AuthUserRoleHandler) Delete(ctx context.Context,req *pb.IdRequest,rsp *pb.DelResponse)error{
	log.Println("删除...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("auth-srv","用户角色关联编号不合法").Error())
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.NotFound("auth-srv","用户角色关联不存在").Error())
	}

	err := h.Model.Delete(req.Id)

	if err != nil {
		return errors.New(mes.InternalServerError("auth-srv",err.Error()).Error())
	}
	rsp.Success = true
	return nil
}

func (h *AuthUserRoleHandler) Open(ctx context.Context,req *pb.IdRequest,rsp *pb.Response)error{
	log.Println("启用...")
	info,err := h.changeStatus(req.Id,config.OPEN_STATUS)
	if err != nil {
		return err
	}

	rsp.AuthUserRole = &info.AuthUserRole
	return nil
}

func (h *AuthUserRoleHandler) Close(ctx context.Context,req *pb.IdRequest,rsp *pb.Response)error{
	log.Println("禁用...")
	info,err := h.changeStatus(req.Id,config.CLOSE_STATUS)
	if err != nil {
		return err
	}

	rsp.AuthUserRole = &info.AuthUserRole
	return nil
}

func (h *AuthUserRoleHandler) Get(ctx context.Context,req *pb.IdRequest,rsp *pb.Response)error{
	log.Println("查询...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New("用户角色关联编号不合法")
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.NotFound("auth-srv","用户角色关联不存在").Error())
	}

	rsp.AuthUserRole = &info.AuthUserRole
	return nil
}

func (h *AuthUserRoleHandler) GetPage(ctx context.Context,req *pb.PageRequest,rsp *pb.PageResponse)error{
	log.Println("根据页面查询...")
	//设置默认每页条数,页数
	count,err := h.Model.GetCount(req)
	if err != nil {
		return errors.New(mes.InternalServerError("auth-srv",err.Error()).Error())
	}
	if req.Size <= 0 || req.Size > sconf.DefaultMaxPageSize{
		req.Size = sconf.PageSize
	}
	if req.Page <= 0 {
		req.Page = sconf.Page
	}

	lr := &pb.ListRequest{
		Start:(req.Page-1)*req.Size,
		Limit:req.Size,
		AuthUserRole:req.AuthUserRole,
		Search:req.Search,
	}
	list,err := h.Model.GetList(lr)
	if err != nil {
		return errors.New(mes.InternalServerError("auth-srv",err.Error()).Error())
	}

	page := &proto.Page{
		Total:*count,
		Index:req.Page,
		TotalPage:int64(math.Ceil(float64(*count)/float64(req.Size))),
		PageSize:req.Size,
	}
	rsp.Page = page
	rsp.AuthUserRoles = list
	return nil
}

func (h *AuthUserRoleHandler) GetList(ctx context.Context,req *pb.ListRequest,rsp *pb.ListResponse)error{
	log.Println("查询所有...")
	list,err := h.Model.GetList(req)
	if err != nil {
		return errors.New(mes.InternalServerError("auth-srv",err.Error()).Error())
	}
	rsp.AuthUserRoles = list
	return nil
}

func (h *AuthUserRoleHandler) changeStatus(id string,status int)(*database.AuthUserRole,error){
	if validate.HasAndCheckString(gv.IsUUIDv4, id) == false {
		return nil,errors.New(mes.BadRequest("auth-srv","用户角色关联编号不合法").Error())
	}

	info := h.Model.GetInfoById(id,false)
	if info == nil {
		return nil,errors.New(mes.NotFound("auth-srv","用户角色关联不存在").Error())
	}

	u := &pb.AuthUserRole{
		Id:id,
		Status:int64(status),
	}

	err := h.Model.Update(u)
	if err != nil {
		return nil,errors.New(mes.InternalServerError("auth-srv",err.Error()).Error())
	}
	info = h.Model.GetInfoById(id,false)

	return info,nil
}

func (h *AuthUserRoleHandler) checkStatus(id string)(*database.AuthUserRole,error){
	if validate.HasAndCheckString(gv.IsUUIDv4, id) == false {
		return nil,errors.New(mes.BadRequest("auth-srv","用户角色关联编号不合法").Error())
	}

	info := h.Model.GetInfoById(id,true)
	if info == nil {
		return nil,errors.New(mes.NotFound("auth-srv","用户角色关联不存在").Error())
	}
	return info,nil
}