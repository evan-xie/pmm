package handler

import (
	"context"
	"errors"
	gv "github.com/asaskevich/govalidator"
	"github.com/casbin/casbin"
	mes "github.com/micro/go-micro/errors"
	"log"
	"pmm/public/config"
	"pmm/public/proto"
	pb "pmm/public/proto/auth"
	"pmm/public/proto/user"
	"pmm/public/utils/validate"
	"pmm/user-srv/database"
	"pmm/user-srv/utils/token"
	"strings"
)

type AuthHandler struct {
	//logger *zap.Logger
	e *casbin.Enforcer
	User database.User
}

func NewAuthHandler(e *casbin.Enforcer) *AuthHandler{
	return &AuthHandler{
		//logger:tl.Instance().Named("TagHandler"),
		e:e,
		User:database.User{},
	}
}

func (h *AuthHandler) AuthRoleRule(ctx context.Context,req *pb.AuthRule,rsp *proto.BoolResponse)error{
	log.Println("根据用户组和规则验证...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.RoleId) == false {
		return errors.New(mes.BadRequest("auth-srv","角色编号不合法").Error())
	}
	if gv.IsNull(req.Rule) {
		return errors.New(mes.BadRequest("auth-srv","规则不能为空").Error())
	}

	b := h.e.Enforce(req.RoleId,req.Rule,req.Method,req.Domain)
	rsp.Success = b
	return nil
}

func (h *AuthHandler) ValidateToken(ctx context.Context,req *pb.Token,rsp *proto.BoolResponse)error{
	log.Println("验证Token...")
	_,err := h.GetTokenInfo(req.TokenStr,req.TokenType)
	if err != nil {
		return err
	}
	rsp.Success = true
	return nil
}

func (h *AuthHandler) GetTokenSub(ctx context.Context,req *pb.Token,rsp *pb.TokenSub)error{
	log.Println("取得Token的Sub...")
	if gv.IsNull(req.TokenStr) {
		return errors.New(mes.BadRequest("auth-srv","Token不能为空").Error())
	}
	if gv.IsNull(req.TokenType) {
		return errors.New(mes.BadRequest("auth-srv","Token类型不能为空").Error())
	}

	uuid,err := h.GetTokenInfo(req.TokenStr,req.TokenType)
	if err != nil {
		return err
	}
	rsp.Sub = uuid
	return nil
}

func (h *AuthHandler) GetUserInfoByToken(ctx context.Context,req *pb.Token,rsp *user.Response)error{
	log.Println("根据Token取得用户信息...")
	if gv.IsNull(req.TokenStr) {
		return errors.New(mes.BadRequest("auth-srv","Token不能为空").Error())
	}
	if gv.IsNull(req.TokenType) {
		return errors.New(mes.BadRequest("auth-srv","Token类型不能为空").Error())
	}
	uuid,err := h.GetTokenInfo(req.TokenStr,req.TokenType)
	if err != nil {
		return err
	}
	if gv.IsNull(uuid) {
		return  errors.New(mes.BadRequest("auth-srv","用户不存在").Error())
	}
	u := h.User.GetInfoById(uuid,false)
	if u == nil {
		return  errors.New(mes.BadRequest("auth-srv","用户不存在").Error())
	}
	if u.Status != config.OPEN_STATUS {
		return  errors.New(mes.BadRequest("auth-srv","用户不被禁用").Error())
	}
	u.User.Password = ""
	rsp.User = &u.User
	return nil
}


func (h *AuthHandler) GetTokenInfo(tokenStr string,tokenType string) (string,error){
	if !gv.IsIn(strings.ToLower(tokenType),"access","refresh"){
		return "",errors.New(mes.BadRequest("auth-srv","Token类似错误").Error())
	}
	//解析token
	t := token.TokenUtils{}
	claims,err := t.Decode(tokenStr,tokenType)
	if err != nil {
		return "",errors.New(mes.Unauthorized("auth-srv","Token验证未通过").Error())
	}
	if gv.IsNull(claims.Subject) {
		return "",errors.New(mes.Unauthorized("auth-srv","Token对象为空，错误Token").Error())
	}

	return claims.Subject,nil
}