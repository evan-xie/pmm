package handler

import (
	"github.com/jinzhu/gorm"
	"log"
	pb "pmm/public/proto/rule"
	"context"
	mes "github.com/micro/go-micro/errors"
	gv "github.com/asaskevich/govalidator"
	"github.com/pkg/errors"
	sconf "pmm/user-srv/config"
	"pmm/public/utils/validate"
	"pmm/public/config"
	"math"
	"pmm/public/proto"
	"pmm/user-srv/database"
	"strings"
	)

type RuleHandler struct {
	//logger *zap.Logger
	Model database.Rule
	db *gorm.DB
	rr RoleRuleHandler
}


// new一个TagHandler
func NewRuleHandler(rrh *RoleRuleHandler) *RuleHandler{
	return &RuleHandler{
		//logger:tl.Instance().Named("TagHandler"),
		Model:database.Rule{},
		db:database.Conn,
		rr:*rrh,
	}
}


func (h *RuleHandler) Create(ctx context.Context,req *pb.Rule,rsp *pb.Response)error{
	log.Println("新增...")
	if gv.IsNull(req.Name) {
		return errors.New(mes.BadRequest("Rule-srv","规则名称不能为空").Error())
	}
	if gv.IsNull(req.Title) {
		return errors.New(mes.BadRequest("Rule-srv","规则标题不能为空").Error())
	}
	if gv.IsNull(req.Type) {
		req.Type = sconf.DefaultRuleType
	}else {
		req.Type = strings.ToLower(req.Type)
		if !gv.IsIn(req.Type,sconf.RuleTypes...) {
			return errors.New(mes.BadRequest("Rule-srv","规则类型错误不能为空").Error())
		}
	}
	if req.Ismenu == 0 {
		req.Ismenu = sconf.IsMenuFalse
	}else{
		if req.Ismenu != sconf.IsMenuTrue && req.Ismenu != sconf.IsMenuFalse {
			return errors.New(mes.BadRequest("Rule-srv","是否按钮格式错误").Error())
		}
	}

	if gv.IsNull(req.Pid) {
		req.Pid = sconf.DefaultRuleTopPidValue
	}
	if gv.IsNull(req.Method) {
		req.Method = config.DefaultMethod
	}
	if gv.IsNull(req.Domain) {
		req.Domain = sconf.DefaultRuleDomainValue
	}


	data := &pb.Rule{
		Name:req.Name,
		Title:req.Title,
		Domain:req.Domain,
		Pid:req.Pid,
	}
	info := h.Model.IsExist(data,false)
	if info != nil {
		return errors.New(mes.BadRequest("Rule-srv","规则已存在").Error())
	}

	info,err := h.Model.Create(req)
	if err != nil {
		return errors.New(mes.InternalServerError("Rule-srv",err.Error()).Error())
	}

	rsp.Rule = &info.Rule
	return nil
}

func (h *RuleHandler) Update(ctx context.Context,req *pb.Rule,rsp *pb.Response)error{
	log.Println("更新...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("Rule-srv","规则编号不合法").Error())
	}
	if !gv.IsNull(req.Type) {
		req.Type = strings.ToLower(req.Type)
		if !gv.IsIn(req.Type,sconf.RuleTypes...) {
			return errors.New(mes.BadRequest("Rule-srv","规则类型错误不能为空").Error())
		}
	}
	if req.Ismenu != 0 && req.Ismenu != sconf.IsMenuTrue && req.Ismenu != sconf.IsMenuFalse {
		return errors.New(mes.BadRequest("Rule-srv","是否按钮格式错误").Error())
	}

	old := h.Model.GetInfoById(req.Id,false)
	if old == nil {
		return errors.New(mes.BadRequest("Rule-srv", "规则不存在").Error())
	}

	data := &pb.Rule{
		Name:req.Name,
		Title:req.Title,
		Domain:req.Domain,
		Pid:req.Pid,
	}
	if !gv.IsNull(req.Name) && !gv.IsNull(req.Title) && !gv.IsNull(req.Domain) {
		info := h.Model.IsExist(data,false)
		if info != nil && info.Id != req.Id{
			return errors.New(mes.BadRequest("Rule-srv","要修改成规则已存在").Error())
		}
	}

	err := h.Model.Update(req)
	if err != nil {
		return errors.New(mes.InternalServerError("Rule-srv",err.Error()).Error())
	}
	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.BadRequest("Rule-srv","规则不存在").Error())
	}

	h.rr.changeByRuleUpdate(&old.Rule,&info.Rule,true)
	rsp.Rule = &info.Rule
	return nil
}

func (h *RuleHandler) Delete(ctx context.Context,req *proto.IdRequest,rsp *proto.BoolResponse)error{
	log.Println("删除...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("Rule-srv","规则编号不合法").Error())
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.NotFound("Rule-srv","规则不存在").Error())
	}

	err := h.Model.Delete(req.Id)

	if err != nil {
		return errors.New(mes.InternalServerError("Rule-srv",err.Error()).Error())
	}
	//授权表更改
	h.rr.changeByRuleUpdate(&info.Rule,nil,false)

	rsp.Success = true
	return nil
}

func (h *RuleHandler) Open(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("启用...")
	info,err := h.changeStatus(req.Id,config.OPEN_STATUS)
	if err != nil {
		return err
	}

	rsp.Rule = &info.Rule
	return nil
}

func (h *RuleHandler) Close(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("禁用...")
	info,err := h.changeStatus(req.Id,config.CLOSE_STATUS)
	if err != nil {
		return err
	}

	rsp.Rule = &info.Rule
	return nil
}

func (h *RuleHandler) Get(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("查询...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New("编号不合法")
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.NotFound("Rule-srv","规则不存在").Error())
	}

	rsp.Rule = &info.Rule
	return nil
}

func (h *RuleHandler) GetPage(ctx context.Context,req *pb.PageRequest,rsp *pb.PageResponse)error{
	log.Println("根据页面查询...")
	//设置默认每页条数,页数
	count,err := h.Model.GetCount(req)
	if err != nil {
		return errors.New(mes.InternalServerError("Rule-srv",err.Error()).Error())
	}
	if *count == 0 {
		rsp.Page = &proto.Page{}
		return nil
	}
	if req.Default != nil {
		if req.Default.Size <= 0 || req.Default.Size > sconf.DefaultMaxPageSize{
			req.Default.Size = sconf.PageSize
		}
		if req.Default.Page <= 0 {
			req.Default.Page = sconf.Page
		}
	}else {
		req.Default = &proto.PageRequest{
			Page:sconf.Page,
			Size:sconf.PageSize,
			Ids:nil,
			Search:nil,
		}
	}

	lr := &pb.ListRequest{
		Default:&proto.ListRequest{
			Start:(req.Default.Page-1)*req.Default.Size,
			Limit:req.Default.Size,
			Ids:req.Default.Ids,
			Search:req.Default.Search,
		},
		Rule:req.Rule,
	}
	list,err := h.Model.GetList(lr)
	if err != nil {
		return errors.New(mes.InternalServerError("Rule-srv",err.Error()).Error())
	}

	page := &proto.Page{
		Total:*count,
		Index:req.Default.Page,
		TotalPage:int64(math.Ceil(float64(*count)/float64(req.Default.Size))),
		PageSize:req.Default.Size,
	}
	rsp.Page = page
	rsp.List = list
	return nil
}

func (h *RuleHandler) GetList(ctx context.Context,req *pb.ListRequest,rsp *pb.ListResponse)error{
	log.Println("查询所有...")
	list,err := h.Model.GetList(req)
	if err != nil {
		return errors.New(mes.InternalServerError("Rule-srv",err.Error()).Error())
	}
	rsp.List = list
	return nil
}

func (h *RuleHandler) GetTree(ctx context.Context,req *pb.TreeRequest,rsp *pb.TreeResponse)error{
	log.Println("查询所有并取得树形结果...")

	//取得单个文件夹下的tree结构
	if !gv.IsNull(req.Id) {
		perms,err := h.treeById(req.Id)
		if err != nil {
			return err
		}
		rsp.Trees = perms
		return nil
	}

	//取得所有
	t := &pb.ListRequest{
		Rule:&pb.Rule{
			Pid:sconf.DefaultRuleTopPidValue,
			Status:config.OPEN_STATUS,
			Domain:req.Domain,
		},
	}

	list,err := h.Model.GetList(t)
	if err != nil {
		return errors.New(mes.InternalServerError("tag-srv",err.Error()).Error())
	}
	//制定Tree
	perms := h.tree(list)
	rsp.Trees = perms
	return nil
}

func (h *RuleHandler) changeStatus(id string,status int)(*database.Rule,error){
	if validate.HasAndCheckString(gv.IsUUIDv4, id) == false {
		return nil,errors.New(mes.BadRequest("Rule-srv","规则编号不合法").Error())
	}

	old := h.Model.GetInfoById(id,false)
	if old == nil {
		return nil,errors.New(mes.NotFound("Rule-srv","规则不存在").Error())
	}

	u := &pb.Rule{
		Id:id,
		Status:int64(status),
	}

	err := h.Model.Update(u)
	if err != nil {
		return nil,errors.New(mes.InternalServerError("Rule-srv",err.Error()).Error())
	}
	info := h.Model.GetInfoById(id,false)

	//关联的授权表更改
	b := true
	h.rr.changeByRuleUpdate(&old.Rule,&info.Rule,b)

	return info,nil
}

func (h *RuleHandler) checkStatus(id string)(*database.Rule,error){
	if validate.HasAndCheckString(gv.IsUUIDv4, id) == false {
		return nil,errors.New(mes.BadRequest("Rule-srv","规则编号不合法").Error())
	}

	info := h.Model.GetInfoById(id,true)
	if info == nil {
		return nil,errors.New(mes.NotFound("Rule-srv","规则不存在").Error())
	}
	return info,nil
}

func (h *RuleHandler) tree(list []*pb.Rule) []*pb.RuleTree  {
	var perms []*pb.RuleTree
	if list == nil {
		return perms
	}
	for _, v := range list {
		child := pb.RuleTree{
			Rule:v,
			Child:[]*pb.RuleTree{},
		}
		h.mkTree(v.Id,&child)
		perms = append(perms, &child)
	}
	return perms
}

func (h *RuleHandler) mkTree(pid string,treeNode *pb.RuleTree) error {
	t := &pb.ListRequest{
		Rule:&pb.Rule{
			Pid:pid,
			Status:config.OPEN_STATUS,
		},
	}
	childs,err := h.Model.GetList(t)
	if err != nil {
		return err
	}
	for _, value := range childs {
		child := pb.RuleTree{
			Rule:value,
			Child:[]*pb.RuleTree{},
		}
		treeNode.Child = append(treeNode.Child,&child)
		h.mkTree(value.Id,&child)
	}
	return nil
}

func (h *RuleHandler) treeById(id string) ([]*pb.RuleTree,error) {
	var perms []*pb.RuleTree
	r := h.Model.GetInfoById(id,true)
	if r == nil {
		return perms,nil
	}
	child := pb.RuleTree{
		Rule:&r.Rule,
		Child:[]*pb.RuleTree{},
	}
	h.mkTree(r.Id,&child)
	perms = append(perms, &child)
	return perms,nil
}
