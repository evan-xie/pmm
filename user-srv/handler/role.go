package handler

import (
	"github.com/jinzhu/gorm"
	"log"
	pb "pmm/public/proto/role"
	"context"
	mes "github.com/micro/go-micro/errors"
	gv "github.com/asaskevich/govalidator"
	"github.com/pkg/errors"
	sconf "pmm/user-srv/config"
	"pmm/public/utils/validate"
	"pmm/public/config"
	"math"
	"pmm/public/proto"
	"pmm/user-srv/database"
)

type RoleHandler struct {
	//logger *zap.Logger
	Model database.Role
	db *gorm.DB
}


// new一个TagHandler
func NewRoleHandler() *RoleHandler{
	return &RoleHandler{
		//logger:tl.Instance().Named("TagHandler"),
		Model:database.Role{},
		db:database.Conn,
	}
}


func (h *RoleHandler) Create(ctx context.Context,req *pb.Role,rsp *pb.Response)error{
	log.Println("新增...")
	if gv.IsNull(req.Name) {
		return errors.New(mes.BadRequest("Role-srv","角色名称不能为空").Error())
	}

	data := &pb.Role{
		Name:req.Name,
	}
	info := h.Model.IsExist(data,false)
	if info != nil {
		return errors.New(mes.BadRequest("Role-srv","角色名称已存在").Error())
	}

	info,err := h.Model.Create(data)
	if err != nil {
		return errors.New(mes.InternalServerError("Role-srv",err.Error()).Error())
	}

	rsp.Role = &info.Role
	return nil
}

func (h *RoleHandler) Update(ctx context.Context,req *pb.Role,rsp *pb.Response)error{
	log.Println("更新...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("Role-srv","角色编号不合法").Error())
	}
	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.BadRequest("Role-srv","角色不存在").Error())
	}

	data := &pb.Role{
		Name:req.Name,
	}
	if !gv.IsNull(req.Name) {
		info := h.Model.IsExist(data,false)
		if info != nil && info.Id != req.Id{
			return errors.New(mes.BadRequest("Role-srv","角色名称已被占用").Error())
		}
	}

	err := h.Model.Update(data)
	if err != nil {
		return errors.New(mes.InternalServerError("Role-srv",err.Error()).Error())
	}
	info = h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.BadRequest("Role-srv","角色不存在").Error())
	}
	
	rsp.Role = &info.Role
	return nil
}

func (h *RoleHandler) Delete(ctx context.Context,req *proto.IdRequest,rsp *pb.DelResponse)error{
	log.Println("删除...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("Role-srv","角色编号不合法").Error())
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.NotFound("Role-srv","角色不存在").Error())
	}

	err := h.Model.Delete(req.Id)

	if err != nil {
		return errors.New(mes.InternalServerError("Role-srv",err.Error()).Error())
	}
	rsp.Success = true
	return nil
}

func (h *RoleHandler) Open(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("启用...")
	info,err := h.changeStatus(req.Id,config.OPEN_STATUS)
	if err != nil {
		return err
	}

	rsp.Role = &info.Role
	return nil
}

func (h *RoleHandler) Close(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("禁用...")
	info,err := h.changeStatus(req.Id,config.CLOSE_STATUS)
	if err != nil {
		return err
	}

	rsp.Role = &info.Role
	return nil
}

func (h *RoleHandler) Get(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("查询...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New("编号不合法")
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.NotFound("Role-srv","角色不存在").Error())
	}

	rsp.Role = &info.Role
	return nil
}

func (h *RoleHandler) GetPage(ctx context.Context,req *pb.PageRequest,rsp *pb.PageResponse)error{
	log.Println("根据页面查询...")
	//设置默认每页条数,页数
	count,err := h.Model.GetCount(req)
	if err != nil {
		return errors.New(mes.InternalServerError("Role-srv",err.Error()).Error())
	}
	if *count == 0 {
		rsp.Page = &proto.Page{}
		return nil
	}
	lr := &pb.ListRequest{
		Role:req.Role,
	}
	if req.Default != nil {
		if req.Default.Size <= 0 || req.Default.Size > sconf.DefaultMaxPageSize{
			req.Default.Size = sconf.PageSize
		}
		if req.Default.Page <= 0 {
			req.Default.Page = sconf.Page
		}
		d := &proto.ListRequest{
			Start:(req.Default.Page-1)*req.Default.Size,
			Limit:req.Default.Size,
			Search:req.Default.Search,
		}
		lr.Default = d
	}else{
		req.Default = &proto.PageRequest{
			Size:sconf.PageSize,
			Page:sconf.Page,
		}
	}

	list,err := h.Model.GetList(lr)
	if err != nil {
		return errors.New(mes.InternalServerError("user-srv",err.Error()).Error())
	}

	page := &proto.Page{
		Total:*count,
		Index:req.Default.Page,
		TotalPage:int64(math.Ceil(float64(*count)/float64(req.Default.Size))),
		PageSize:req.Default.Size,
	}
	rsp.Page = page
	rsp.List = list


	return nil
}

func (h *RoleHandler) GetList(ctx context.Context,req *pb.ListRequest,rsp *pb.ListResponse)error{
	log.Println("查询所有...")
	list,err := h.Model.GetList(req)
	if err != nil {
		return errors.New(mes.InternalServerError("Role-srv",err.Error()).Error())
	}
	rsp.List = list
	return nil
}

func (h *RoleHandler) changeStatus(id string,status int)(*database.Role,error){
	if validate.HasAndCheckString(gv.IsUUIDv4, id) == false {
		return nil,errors.New(mes.BadRequest("Role-srv","角色编号不合法").Error())
	}

	info := h.Model.GetInfoById(id,false)
	if info == nil {
		return nil,errors.New(mes.NotFound("Role-srv","角色不存在").Error())
	}

	u := &pb.Role{
		Id:id,
		Status:int64(status),
	}

	err := h.Model.Update(u)
	if err != nil {
		return nil,errors.New(mes.InternalServerError("Role-srv",err.Error()).Error())
	}
	info = h.Model.GetInfoById(id,false)

	return info,nil
}

func (h *RoleHandler) checkStatus(id string)(*database.Role,error){
	if validate.HasAndCheckString(gv.IsUUIDv4, id) == false {
		return nil,errors.New(mes.BadRequest("Role-srv","角色编号不合法").Error())
	}

	info := h.Model.GetInfoById(id,true)
	if info == nil {
		return nil,errors.New(mes.NotFound("Role-srv","角色不存在").Error())
	}
	return info,nil
}