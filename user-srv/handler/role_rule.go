package handler

import (
	"context"
	gv "github.com/asaskevich/govalidator"
	"github.com/casbin/casbin"
	mes "github.com/micro/go-micro/errors"
	"github.com/pkg/errors"
	"log"
	"pmm/public/proto"
	pb "pmm/public/proto/role/rule"
	pbrule "pmm/public/proto/rule"
	"pmm/public/utils/validate"
	sconf "pmm/user-srv/config"
	"pmm/user-srv/database"
	"strconv"
)

type RoleRuleHandler struct {
	//logger *zap.Logger *Enforcer
	e *casbin.Enforcer
	Rule database.Rule
	Role database.Role
}


// new一个TagHandler
func NewRoleRuleHandler(ea *casbin.Enforcer) *RoleRuleHandler{
	return &RoleRuleHandler{
		//logger:tl.Instance().Named("TagHandler"),
		e:ea,
		Rule:database.Rule{},
		Role:database.Role{},
	}
}


func (h *RoleRuleHandler) Create(ctx context.Context,req *pb.RoleRule,rsp *proto.BoolResponse)error{
	log.Println("新增...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.RuleId) == false {
		return errors.New(mes.BadRequest("user-role-srv","规则编号不合法").Error())
	}
	if validate.MustAndCheckString(gv.IsUUIDv4, req.RoleId) == false {
		return errors.New(mes.BadRequest("user-role-srv","角色编号不合法").Error())
	}
	var b bool

	rule := h.Rule.GetInfoById(req.RuleId,false)
	if rule == nil  {
		return errors.New(mes.BadRequest("user-role-srv","规则不存在").Error())
	}
	if gv.IsNull(rule.Domain) {
		rule.Domain = sconf.DefaultRuleDomainValue
	}

	b = h.e.HasPermissionForUser(req.RoleId,rule.Title,rule.Method,rule.Domain,strconv.Itoa(int(rule.Status)),rule.Id)

	if b {
		rsp.Success = true
		return nil
	}
	b = h.e.AddPermissionForUser(req.RoleId,rule.Title,rule.Method,rule.Domain,strconv.Itoa(int(rule.Status)),rule.Id)

	if b == false {
		return errors.New(mes.InternalServerError("role-rule-casbin-srv","casbin异常错误").Error())
	}
	// Save the policy back to DB.
	h.e.SavePolicy()
	rsp.Success = true
	return nil
}

func (h *RoleRuleHandler) Delete(ctx context.Context,req *pb.RoleRule,rsp *proto.BoolResponse)error {
	log.Println("删除...")

	if validate.MustAndCheckString(gv.IsUUIDv4, req.RuleId) == false {
		return errors.New(mes.BadRequest("user-role-srv","规则编号不合法").Error())
	}
	if validate.MustAndCheckString(gv.IsUUIDv4, req.RoleId) == false {
		return errors.New(mes.BadRequest("user-role-srv","角色编号不合法").Error())
	}

	var b bool
	rule := h.Rule.GetInfoById(req.RuleId,false)
	if rule == nil  {
		return errors.New(mes.BadRequest("user-role-srv","规则不存在").Error())
	}

	if gv.IsNull(rule.Domain) {
		rule.Domain = sconf.DefaultRuleDomainValue
	}
	b = h.e.DeletePermissionForUser(req.RoleId,rule.Title,rule.Method,rule.Domain,strconv.FormatInt(rule.Status,10),rule.Id)
	if !b {
		return errors.New(mes.InternalServerError("role-rule-casbin-srv","casbin异常错误").Error())
	}
	h.e.SavePolicy()
	rsp.Success = true
	return nil
}

func (h *RoleRuleHandler) DeleteRoles(ctx context.Context,req *pb.RoleRule,rsp *proto.BoolResponse)error {
	log.Println("删除规则的所有关联关系...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.RuleId) == false {
		return errors.New(mes.BadRequest("user-role-srv","规则编号不合法").Error())
	}

	rule := h.Rule.GetInfoById(req.RuleId,false)
	if rule == nil  {
		return errors.New(mes.BadRequest("user-role-srv","规则不存在").Error())
	}
	
	var b bool
	if gv.IsNull(rule.Domain) {
		b = h.e.DeletePermission(rule.Title,rule.Method)
	}else{
		b = h.e.DeletePermission(rule.Title,rule.Method,rule.Domain)
	}
	
	if !b {
		return errors.New(mes.InternalServerError("role-rule-casbin-srv","casbin异常错误").Error())
	}
	rsp.Success = true
	h.e.SavePolicy()
	return nil
}

func (h *RoleRuleHandler) DeleteRules(ctx context.Context,req *pb.RoleRule,rsp *proto.BoolResponse)error {
	log.Println("删除角色所有的关联规则关系...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.RoleId) == false {
		return errors.New(mes.BadRequest("user-role-srv","角色编号不合法").Error())
	}
	b := h.e.DeletePermissionsForUser(req.RoleId)
	if !b {
		return errors.New(mes.InternalServerError("role-rule-casbin-srv","casbin异常错误").Error())
	}
	rsp.Success = true
	h.e.SavePolicy()
	return nil
}

func (h *RoleRuleHandler) GetRuleIdsByRole(ctx context.Context,req *pb.RoleRule,rsp *pb.RuleIdsResponse)error {
	log.Println("查询角色所有的规则id 集合...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.RoleId) == false {
		return errors.New(mes.BadRequest("user-role-srv","角色编号不合法").Error())
	}

	arr := h.e.GetPermissionsForUser(req.RoleId)

	if arr!= nil && len(arr) > 0 {
		for _,v := range arr {
			if len(v) < 5 {
				continue
			}
			rsp.RuleIds = append(rsp.RuleIds, v[5])
		}
	}

	return nil
}

/*func (h *RoleRuleHandler) GetRuleByRole(ctx context.Context,req *pb.ListRuleRequest,rsp *pb.ListRuleResponse)error {
	log.Println("查询角色所有的规则 集合...")
	if req.Start <= 0 {
		req.Start = 0
	}
	if req.Limit <= 0 || req.Limit > config.DefaultMaxPageSize{
		req.Limit = config.PageSize
	}

	arr := h.e.GetPermissionsForUser(req.RoleId)

	if arr!= nil && len(arr) > 0 {
		if len(arr) <= int(req.Start) {
			rsp.RuleIds = nil
			return nil
		}
		var ll [][]string
		if len(arr) >= int(req.Start+req.Limit) {
			ll = arr[req.Start:req.Limit]
		}else {
			ll = arr[req.Start:]
		}
		for _,v := range ll {
			if len(v) < 4 {
				continue
			}
			rsp.RuleIds = append(rsp.RuleIds, v[4])
		}
	}

	return nil
}*/

func (h *RoleRuleHandler) changeByRuleUpdate(old *pbrule.Rule, new *pbrule.Rule,idAdd bool) {
	if old == nil {
		return
	}
	p := h.e.GetPolicy()
	if p == nil || len(p) == 0 {
		return
	}
	b := true
	for _, value := range p {
		if value == nil {
			continue
		}
		if len(value) < 5 {
			continue
		}
		if value[5] == old.Id {
			v := value[1:]
			if b {
				b = !h.e.DeletePermission(v...)
			}
			if idAdd {
				h.e.AddPermissionForUser(value[0],new.Title,new.Method,new.Domain,strconv.Itoa(int(new.Status)),new.Id)
			}
		}
	}
	h.e.SavePolicy()
}
