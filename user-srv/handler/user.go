package handler

import (
	"context"
	"errors"
	"fmt"
	gv "github.com/asaskevich/govalidator"
	"github.com/jinzhu/gorm"
	mes "github.com/micro/go-micro/errors"
	"golang.org/x/crypto/bcrypt"
	"log"
	"math"
	"net/http"
	"php"
	"pmm/public/config"
	"pmm/public/proto"
	pb "pmm/public/proto/user"
	"pmm/public/proto/wxApp"
	"pmm/public/utils/validate"
	usconf "pmm/user-srv/config"
	"pmm/user-srv/database"
	"pmm/user-srv/utils"
	"pmm/user-srv/utils/token"
	"pmm/user-srv/utils/verify"
	"strconv"
	"strings"
	"encoding/json"
)



type UserHandler struct {
	//logger *zap.Logger
	User database.User
	token token.TokenUtils
	db *gorm.DB
	v *verify.Verify
	wx database.WxApp
}



// new一个UserHandler
func NewUserHandler() *UserHandler{
	return &UserHandler{
		//logger:tl.Instance().Named("UserHandler"),
		User:database.User{},
		db:database.Conn,
		v:&verify.Verify{},
		wx:database.WxApp{},
	}
}

//rpc Save(User) returns(User){}
//rpc Update(User) returns(User){}
//rpc Delete(User) returns(Response){}
//
//rpc Open(User) returns(User){}
//rpc Close(User) returns(User){}
//rpc Login(User) returns(LoginResponse){}
//
//rpc Get(User) returns(User){}
//rpc GetList(ListRequest) returns(ListRequest){}
//rpc GetPage(PageRequest) returns(PageResponse){}

func (h *UserHandler) Register(ctx context.Context,req *pb.RegisterRqeuset,rsp *pb.Response)error {
	log.Println("注册...")
	user,err := h.register(req)
	if err != nil {
		return err
	}

	user.Password = ""
	rsp.User = user
	return nil
}

func (h *UserHandler) Save(ctx context.Context,req *pb.User,rsp *pb.Response)error{
	log.Println("新增用户...")
	if req.Username == "" && req.Mobile == ""  && req.Email == ""{
		return errors.New(mes.BadRequest("user-srv","用户名/手机/邮箱不能为空").Error())
	}
	if validate.HasAndCheckString(gv.IsEmail,req.Email) == false {
		return errors.New(mes.BadRequest("user-srv","邮箱格式不正确").Error())
	}
	if validate.HasAndCheckString(validate.IsMobile,req.Mobile) == false {
		return errors.New(mes.BadRequest("user-srv","手机号码格式不正确").Error())
	}
	if validate.HasAndCheckStrings(gv.RuneLength, req.Username, "6", "20") == false {
		return errors.New(mes.BadRequest("user-srv","用户名长度不正确，必须为6-20个字符").Error())
	}
	if validate.MustAndCheckStrings(gv.RuneLength, req.Password, "8", "20") == false  {
		return errors.New(mes.BadRequest("user-srv","密码格式错误必须为8-20个字符").Error())
	}

	//检验传入参数是否合理...
	User := h.User
	//验证用户是否存在
	u := User.IsExist(req,false)
	//user,err := h.repo.IsExist(req)
	if u != nil {
		return errors.New(mes.BadRequest("user-srv","用户已存在").Error())
	}

	user,err := User.Create(req)
	if err != nil {
		return errors.New(mes.InternalServerError("user-srv",err.Error()).Error())
	}

	//屏蔽密码
	user.Password = ""
	rsp.User = &user.User
	return nil
}

func (h *UserHandler) Update(ctx context.Context,req *pb.User,rsp *pb.Response)error{
	log.Println("修改用户...")

	if validate.HasAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("user-srv","用户编号不合法").Error())
	}
	if validate.HasAndCheckString(gv.IsEmail,req.Email) == false {
		return errors.New(mes.BadRequest("user-srv","邮箱格式不正确").Error())
	}
	if validate.HasAndCheckString(validate.IsMobile,req.Mobile)  == false {
		return errors.New(mes.BadRequest("user-srv","手机号码格式不正确").Error())
	}
	if validate.HasAndCheckStrings(gv.RuneLength, req.Username, "6", "20") == false  {
		return errors.New(mes.BadRequest("user-srv","用户名长度不正确，必须为6-20个字符").Error())
	}
	
	user := h.User.IsExist(&pb.User{Id:req.Id},false)
	if user == nil {
		return errors.New(mes.BadRequest("user-srv","用户不存在").Error())
	}

	//根据用户名取得用户
	if !gv.IsNull(req.Username) {
		checkUser := h.User.IsExist(&pb.User{Username:req.Username,},false)
		if checkUser != nil && user.Id != checkUser.Id {
			return errors.New(mes.BadRequest("user-srv","此用户名已被占用").Error())
		}
	}

	//根据邮箱取得用户
	if !gv.IsNull(req.Email) {
		checkUser := h.User.IsExist(&pb.User{Email:req.Email,},false)
		if checkUser != nil && user.Id != checkUser.Id {
			return errors.New(mes.BadRequest("user-srv","此邮箱已被绑定").Error())
		}
	}

	//根据手机号取得用户
	if !gv.IsNull(req.Email) {
		checkUser := h.User.IsExist(&pb.User{Mobile:req.Mobile,},false)
		if checkUser != nil && user.Id != checkUser.Id {
			return errors.New(mes.BadRequest("user-srv","此手机已被绑定").Error())
		}
	}

	err := h.User.Update(req,"status")
	//err := h.User.Update(req,"password","status")
	if err != nil {
		return errors.New(mes.InternalServerError("user-srv",err.Error()).Error())
	}

	user = h.User.GetInfoById(req.Id,false)
	if user == nil {
		return errors.New(mes.BadRequest("user-srv","用户不存在").Error())
	}
	//屏蔽密码
	user.Password = ""
	rsp.User = &user.User
	return nil
}

func (h *UserHandler) Delete(ctx context.Context,req *pb.User,rsp *pb.DelResponse)error{
	log.Println("删除用户...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("user-srv","用户编号不合法").Error())
	}

	user := h.User.GetInfoById(req.Id,false)
	if user == nil {
		return errors.New(mes.NotFound("user-srv","用户不存在").Error())
	}

	err := h.User.Delete(req.Id)
	if err != nil {
		return errors.New(mes.InternalServerError("user-srv",err.Error()).Error())
	}
	rsp.Success = true
	return nil
}

func (h *UserHandler) Open(ctx context.Context,req *pb.User,rsp *pb.Response)error{
	log.Println("启用用户...")

	user,err := h.changeStatus(req.Id,config.OPEN_STATUS)
	if err != nil {
		return err
	}

	//屏蔽密码
	user.Password = ""
	rsp.User = &user.User
	return nil
}

func (h *UserHandler) Close(ctx context.Context,req *pb.User,rsp *pb.Response)error{
	log.Println("禁用用户...")

	user,err := h.changeStatus(req.Id,config.CLOSE_STATUS)
	if err != nil {
		return err
	}

	//屏蔽密码
	user.Password = ""
	rsp.User = &user.User
	return nil
}

func (h *UserHandler) Login(ctx context.Context,req *pb.User,rsp *pb.LoginResponse)error{
	log.Println("登录中...")

	user := h.User.IsExist(req,false)
	if user == nil {
		return errors.New(mes.NotFound("user-srv","用户不存在").Error())
	}
	if user.Status == config.CLOSE_STATUS {
		return errors.New(mes.Forbidden("user-srv","用户被禁用").Error())
	}

	// 进行密码验证
	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(req.Password)); err != nil {
		return errors.New(mes.BadRequest("user-srv","用户名/密码错误").Error())
	}

	t,err := h.getTokens(user.Id)
	if err != nil {
		return err
	}

	rsp.Token = t

	//隐藏密码
	user.Password = ""
	rsp.User = &user.User
	return nil
}

func (h *UserHandler) Get(ctx context.Context,req *pb.User,rsp *pb.Response)error{
	log.Println("查询用户...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New("用户编号不合法")
	}

	user := h.User.GetInfoById(req.Id,false)
	if user == nil {
		return errors.New(mes.NotFound("user-srv","用户不存在").Error())
	}

	//屏蔽密码
	user.Password = ""
	rsp.User = &user.User
	return nil
}

func (h *UserHandler) GetPage(ctx context.Context,req *pb.PageRequest,rsp *pb.PageResponse)error{
	log.Println("根据页面查询用户...")
	//设置默认每页条数,页数
	count,err := h.User.GetCount(req)
	if err != nil {
		return errors.New(mes.InternalServerError("user-srv",err.Error()).Error())
	}
	if *count == 0 {
		rsp.Page = &proto.Page{}
		return nil
	}
	lr := &pb.ListRequest{
		User:req.User,
	}
	if req.Default != nil {
		if req.Default.Size <= 0 || req.Default.Size > usconf.DefaultMaxPageSize{
			req.Default.Size = usconf.PageSize
		}
		if req.Default.Page <= 0 {
			req.Default.Page = usconf.Page
		}
		d := &proto.ListRequest{
			Start:(req.Default.Page-1)*req.Default.Size,
			Limit:req.Default.Size,
			Search:req.Default.Search,
			Ids:req.Default.Ids,
		}
		lr.Default = d
	}else{
		req.Default = &proto.PageRequest{
			Size:usconf.PageSize,
			Page:usconf.Page,
		}
	}

	list,err := h.User.GetList(lr)
	if err != nil {
		return errors.New(mes.InternalServerError("user-srv",err.Error()).Error())
	}

	page := &proto.Page{
		Total:*count,
		Index:req.Default.Page,
		TotalPage:int64(math.Ceil(float64(*count)/float64(req.Default.Size))),
		PageSize:req.Default.Size,
	}
	rsp.Page = page
	rsp.List = list
	return nil
}

func (h *UserHandler) GetList(ctx context.Context,req *pb.ListRequest,rsp *pb.ListResponse)error{
	log.Println("查询所有用户...")
	list,err := h.User.GetList(req)
	if err != nil {
		return errors.New(mes.InternalServerError("user-srv",err.Error()).Error())
	}
	rsp.List = list
	return nil
}

func (h *UserHandler) changeStatus(id string,status int)(*database.User,error){
	if validate.HasAndCheckString(gv.IsUUIDv4, id) == false {
		return nil,errors.New(mes.BadRequest("user-srv","用户编号不合法").Error())
	}

	user := h.User.IsExist(&pb.User{Id:id},false)
	if user == nil {
		return nil,errors.New(mes.NotFound("user-srv","用户不存在").Error())
	}

	u := &pb.User{
		Id:id,
		Status:int64(status),
	}

	err := h.User.Update(u)
	if err != nil {
		return nil,errors.New(mes.InternalServerError("user-srv",err.Error()).Error())
	}
	user = h.User.GetInfoById(id,false)

	return user,nil
}

func (h *UserHandler) CheckStatus(ctx context.Context,req *pb.User,rsp *pb.UserStatus)(error){
	log.Println("验证用户状态...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.NotFound("user-srv","用户编号不合法").Error())
	}

	user := h.User.GetInfoById(req.Id,false)
	if user == nil {
		return errors.New(mes.NotFound("user-srv","用户不存在").Error())
	}

	//屏蔽密码
	user.Password = ""
	rsp.User = &user.User
	rsp.Open = user.Status == config.OPEN_STATUS
	return nil
}

func (h *UserHandler) ChangePassword(ctx context.Context,req *pb.User,rsp *pb.Response)(error){
	log.Println("修改用户密码...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("user-srv","用户编号不合法").Error())
	}
	if validate.MustAndCheckStrings(gv.RuneLength, req.Password, "8", "20") == false  {
		return errors.New(mes.BadRequest("user-srv","密码格式错误必须为8-20个字符").Error())
	}

	user := h.User.GetInfoById(req.Id,false)
	if user == nil {
		return errors.New(mes.NotFound("user-srv","用户不存在").Error())
	}

	err := h.User.Update(req,"status")
	if err != nil {
		return errors.New(mes.InternalServerError("user-srv",err.Error()).Error())
	}

	//屏蔽密码
	user.Password = ""
	rsp.User = &user.User
	return nil
}

func (h *UserHandler) ChangeLoginPassword(ctx context.Context,req *pb.ChangeLoginPasswordReq,rsp *pb.Response) error {
	log.Println("修改当前用户密码...")
	if gv.IsNull(req.Token) {
		return errors.New(mes.BadRequest("auth-srv","Token不能为空").Error())
	}
	if gv.IsNull(req.OldPwd) {
		return errors.New(mes.BadRequest("auth-srv","旧密码不能为空").Error())
	}
	if gv.IsNull(req.NewPwd) {
		return errors.New(mes.BadRequest("auth-srv","新密码不能为空").Error())
	}
	auth := AuthHandler{}
	uid,err := auth.GetTokenInfo(req.Token,"access")
	if err != nil {
		return err
	}
	if gv.IsNull(uid) {
		return  errors.New(mes.BadRequest("auth-srv","用户不存在").Error())
	}
	u := h.User.GetInfoById(uid,false)
	if u == nil {
		return  errors.New(mes.BadRequest("auth-srv","用户不存在").Error())
	}
	if u.Status != config.OPEN_STATUS {
		return  errors.New(mes.BadRequest("auth-srv","用户不被禁用").Error())
	}
	if err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(req.OldPwd)); err != nil {
		return  errors.New(mes.BadRequest("auth-srv","旧密码与原密码不匹配").Error())
	}
	data := &pb.User{
		Id:uid,
		Password:req.NewPwd,
	}
	if err := h.User.Update(data,"status");err != nil {
		return errors.New(mes.InternalServerError("user-srv",err.Error()).Error())
	}
	u.Password = ""
	rsp.User = &u.User
	return nil
}

func (h *UserHandler) register(req *pb.RegisterRqeuset) (*pb.User,error){
	if req.User == nil {
		return nil,errors.New(mes.BadRequest("user-srv","注册用户信息不能为空").Error())
	}
	if gv.IsNull(req.User.Mobile) && gv.IsNull(req.User.Email){
		return nil,errors.New(mes.BadRequest("user-srv","邮箱或手机号码不能为空").Error())
	}
	if validate.HasAndCheckString(gv.IsEmail,req.User.Email) == false {
		return nil,errors.New(mes.BadRequest("user-srv","邮箱格式不正确").Error())
	}
	if validate.HasAndCheckString(validate.IsMobile,req.User.Mobile) == false {
		return nil,errors.New(mes.BadRequest("user-srv","手机号码格式不正确").Error())
	}

	if validate.MustAndCheckStrings(gv.RuneLength, req.User.Password, "8", "20") == false  {
		return nil,errors.New(mes.BadRequest("user-srv","密码格式错误必须为8-20个字符").Error())
	}
	if gv.IsNull(req.Code) {
		return nil,errors.New(mes.BadRequest("user-srv","验证码不能为空").Error())
	}

	key := req.User.Mobile
	if gv.IsNull(key) {
		key = req.User.Email
	}
	code,_ := strconv.ParseInt(req.Code,10,64)
	b := h.v.CheckCode(key,code)
	if b == false {
		return nil,errors.New(mes.BadRequest("user-srv", "验证码不合法").Error())
	}

	u := req.User
	info := h.User.IsExist(u,false)
	if info != nil {
		return nil,errors.New(mes.BadRequest("user-srv","用户已存在").Error())
	}

	uu,err := h.User.Create(u)
	if err != nil {
		return nil,errors.New(mes.InternalServerError("user-srv",err.Error()).Error())
	}

	return &uu.User,nil
}

func (h *UserHandler) WxLogin(ctx context.Context,req *wxApp.WxLoginReq,rsp *pb.LoginResponse)(error){
	log.Println("微信小程序登录...")
	if gv.IsNull(req.Code) {
		return errors.New(mes.NotFound("user-srv","微信登录凭缺失").Error())
	}

	appID         := usconf.AppID
	secret        := usconf.AppSecret
	CodeToSessURL := usconf.CodeToSessURL
	CodeToSessURL  = strings.Replace(CodeToSessURL, "APPID",  appID,  -1)
	CodeToSessURL  = strings.Replace(CodeToSessURL, "SECRET", secret, -1)
	CodeToSessURL  = strings.Replace(CodeToSessURL, "JSCODE",   req.Code,   -1)

	resp, err := http.Get(CodeToSessURL)
	if err != nil {
		fmt.Println(err.Error())
		return errors.New(mes.NotFound("user-srv","取得微信用户信息失败").Error())
	}

	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return errors.New(mes.NotFound("user-srv","取得微信用户信息失败").Error())
	}

	var data map[string]interface{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return errors.New(mes.NotFound("user-srv","微信用户信息解析失败").Error())
	}

	errCode,_ := data["errcode"].(string)
	if !gv.IsNull(errCode) {
		return errors.New(mes.NotFound("user-srv","获取微信用户信息错误，请稍后重试").Error())
	}

	if _, ok := data["session_key"]; !ok {
		fmt.Println("session_key 不存在")
		return errors.New(mes.NotFound("user-srv","微信用户会话密钥不存在").Error())
	}

	var openID string
	openID     = data["openid"].(string)

	wx := wxApp.WxLogin{
		Openid:openID,
	}
	w := h.wx.IsExist(&wx,false)
	if w == nil || gv.IsNull(w.Uid) {
		return errors.New(mes.NotFound("user-srv","用户不存在").Error())
	}

	user := h.User.GetInfoById(w.Uid,false)
	if user == nil {
		return errors.New(mes.NotFound("user-srv","用户不存在").Error())
	}
	if user.Status != config.OPEN_STATUS {
		return errors.New(mes.NotFound("user-srv","用户被禁用").Error())
	}

	t,err := h.getTokens(user.Id)
	if err != nil {
		return err
	}
	user.Password = ""

	rsp.Token = t
	rsp.User = &user.User
	return nil
}

func (h *UserHandler) WxRegister(ctx context.Context,req *wxApp.WxRegisterReq,rsp *pb.LoginResponse)(error){
	log.Println("微信小程序登录...")
	if gv.IsNull(req.Iv) {
		return errors.New(mes.NotFound("user-srv","微信小程序参数缺失").Error())
	}
	if gv.IsNull(req.Signature) {
		return errors.New(mes.NotFound("user-srv","微信小程序参数缺失").Error())
	}
	if gv.IsNull(req.RawData) {
		return errors.New(mes.NotFound("user-srv","微信小程序参数缺失").Error())
	}
	if gv.IsNull(req.EncryptedData) {
		return errors.New(mes.NotFound("user-srv","微信小程序参数缺失").Error())
	}
	if gv.IsNull(req.Code) {
		return errors.New(mes.NotFound("user-srv","微信登录凭缺失").Error())
	}

	appID         := usconf.AppID
	secret        := usconf.AppSecret
	CodeToSessURL := usconf.CodeToSessURL
	CodeToSessURL  = strings.Replace(CodeToSessURL, "APPID",  appID,  -1)
	CodeToSessURL  = strings.Replace(CodeToSessURL, "SECRET", secret, -1)
	CodeToSessURL  = strings.Replace(CodeToSessURL, "JSCODE",   req.Code,   -1)

	resp, err := http.Get(CodeToSessURL)
	if err != nil {
		fmt.Println(err.Error())
		return errors.New(mes.NotFound("user-srv","取得微信用户信息失败").Error())
	}

	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return errors.New(mes.NotFound("user-srv","取得微信用户信息失败").Error())
	}
	fmt.Println(req.RawData)
	var data map[string]interface{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return errors.New(mes.NotFound("user-srv","微信用户信息解析失败").Error())
	}

	errCode,_ := data["errcode"].(string)
	if !gv.IsNull(errCode) {
		return errors.New(mes.NotFound("user-srv","获取微信用户信息错误，请稍后重试").Error())
	}

	if _, ok := data["session_key"]; !ok {
		fmt.Println("session_key 不存在")
		return errors.New(mes.NotFound("user-srv","微信用户会话密钥不存在").Error())
	}

	sessionKey := data["session_key"].(string)
	userInfoStr, err := utils.DecodeWeAppUserInfo(req.EncryptedData,sessionKey,req.Iv)
	if err != nil {
		return errors.New(mes.NotFound("user-srv","微信用户信息解析失败").Error())
	}
	fmt.Println(userInfoStr)
	var wxUser map[string]interface{}

	if err = json.Unmarshal([]byte(userInfoStr),&wxUser);err != nil {
		return errors.New(mes.NotFound("user-srv","微信用户信息解析失败").Error())
	}


	var openID string
	openID     = data["openid"].(string)
	wx := wxApp.WxLogin{
		Openid:openID,
	}

	w := h.wx.IsExist(&wx,false)
	if w == nil {
		user := pb.User{}
		user.Nickname = wxUser["nickName"].(string)
		user.HeadPic = wxUser["avatarUrl"].(string)
		userInfo,err := h.User.Create(&user)
		if err != nil {
			return errors.New(mes.InternalServerError("user-srv",err.Error()).Error())
		}

		wx.Uid = userInfo.Id
		if _,err := h.wx.Create(&wx);err != nil {
			return errors.New(mes.InternalServerError("user-srv",err.Error()).Error())
		}
		w.Uid = wx.Uid
	}

	user := h.User.GetInfoById(w.Uid,false)
	if user == nil {
		return errors.New(mes.NotFound("user-srv","用户不存在").Error())
	}
	if user.Status != config.OPEN_STATUS {
		return errors.New(mes.NotFound("user-srv","用户被禁用").Error())
	}

	t,err := h.getTokens(user.Id)
	if err != nil {
		return err
	}
	user.Password = ""

	rsp.Token = t
	rsp.User = &user.User
	return nil
}

func (h *UserHandler) WxRegisterByPhone(ctx context.Context,req *wxApp.WxRegisterReq,rsp *pb.LoginResponse)(error){
	log.Println("微信小程序登录...")
	if gv.IsNull(req.Iv) {
		return errors.New(mes.NotFound("user-srv","微信小程序参数缺失").Error())
	}
	if gv.IsNull(req.EncryptedData) {
		return errors.New(mes.NotFound("user-srv","微信小程序参数缺失").Error())
	}
	if gv.IsNull(req.Code) {
		return errors.New(mes.NotFound("user-srv","微信登录凭缺失").Error())
	}

	appID         := usconf.AppID
	secret        := usconf.AppSecret
	CodeToSessURL := usconf.CodeToSessURL
	CodeToSessURL  = strings.Replace(CodeToSessURL, "APPID",  appID,  -1)
	CodeToSessURL  = strings.Replace(CodeToSessURL, "SECRET", secret, -1)
	CodeToSessURL  = strings.Replace(CodeToSessURL, "JSCODE",   req.Code,   -1)

	resp, err := http.Get(CodeToSessURL)
	if err != nil {
		fmt.Println(err.Error())
		return errors.New(mes.NotFound("user-srv","取得微信用户信息失败").Error())
	}

	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return errors.New(mes.NotFound("user-srv","取得微信用户信息失败").Error())
	}

	var data map[string]interface{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return errors.New(mes.NotFound("user-srv","微信用户信息解析失败").Error())
	}

	errCode,_ := data["errcode"].(string)
	if !gv.IsNull(errCode) {
		return errors.New(mes.NotFound("user-srv","获取微信用户信息错误，请稍后重试").Error())
	}
	if _, ok := data["session_key"]; !ok {
		fmt.Println("session_key 不存在")
		return errors.New(mes.NotFound("user-srv","微信用户会话密钥不存在").Error())
	}

	sessionKey := data["session_key"].(string)
	userInfoStr, err := utils.DecodeWeAppUserInfo(req.EncryptedData,sessionKey,req.Iv)
	if err != nil {
		return errors.New(mes.NotFound("user-srv","微信用户信息解析失败").Error())
	}
	fmt.Println(userInfoStr)

	var wxUser map[string]interface{}

	if err = json.Unmarshal([]byte(userInfoStr),&wxUser);err != nil {
		return errors.New(mes.NotFound("user-srv","微信用户信息解析失败").Error())
	}

	var openID string
	openID     = data["openid"].(string)
	wx := wxApp.WxLogin{
		Openid:openID,
	}
	w := h.wx.IsExist(&wx,false)
	fmt.Println(w)
	var uid string
	if w == nil {
		//判断是否有已经绑定的手机号码
		user := pb.User{}
		user.Mobile = wxUser["phoneNumber"].(string)
		u := h.User.IsExist(&user,false)
		if u == nil {
			user.Nickname = wxUser["purePhoneNumber"].(string)
			user.Username = wxUser["phoneNumber"].(string)
			user.Password = strconv.FormatInt(php.Rand(10000000,999999999),10)
			//user.Nickname = wxUser["nickName"].(string)
			//user.HeadPic = wxUser["avatarUrl"].(string)
			userInfo,err := h.User.Create(&user)
			if err != nil {
				return errors.New(mes.InternalServerError("user-srv",err.Error()).Error())
			}
			uid = userInfo.Id
		}else{
			uid = u.Id
		}
		wx.Uid = uid
		wx.Status = config.OPEN_STATUS
		if _,err := h.wx.Create(&wx);err != nil {
			return errors.New(mes.InternalServerError("user-srv",err.Error()).Error())
		}
	}else{
		uid = w.Uid
	}

	user := h.User.GetInfoById(uid,false)
	if user == nil {
		return errors.New(mes.NotFound("user-srv","用户不存在").Error())
	}
	if user.Status != config.OPEN_STATUS {
		return errors.New(mes.NotFound("user-srv","用户被禁用").Error())
	}

	t,err := h.getTokens(user.Id)
	if err != nil {
		return err
	}
	user.Password = ""

	rsp.Token = t
	rsp.User = &user.User
	return nil
}

func (h *UserHandler) getTokens(uid string) (*pb.Token,error){
	at,err := h.token.Encode(uid,"access")
	if err != nil {
		return nil,errors.New(mes.InternalServerError("user-srv","服务发生异常：token生成失败"+err.Error()).Error())
	}
	rt,err := h.token.Encode(uid,"access")
	if err != nil {
		return nil,errors.New(mes.InternalServerError("user-srv","服务发生异常：token生成失败"+err.Error()).Error())
	}

	t := &pb.Token{
		AccessToken:at,
		RefreshToken:rt,
	}
	return t,nil
}