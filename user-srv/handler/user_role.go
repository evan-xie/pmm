package handler

import (
	"context"
	gv "github.com/asaskevich/govalidator"
	"github.com/casbin/casbin"
	mes "github.com/micro/go-micro/errors"
	"github.com/pkg/errors"
	"log"
	"math"
	"pmm/public/config"
	"pmm/public/proto"
	"pmm/public/proto/role"
	"pmm/public/proto/user"
	pb "pmm/public/proto/user/role"
	"pmm/public/utils/validate"
	"pmm/user-srv/database"
)

type UserRoleHandler struct {
	//logger *zap.Logger *Enforcer
	e *casbin.Enforcer
	User database.User
	Role database.Role
}


// new一个TagHandler
func NewUserRoleHandler(ea *casbin.Enforcer) *UserRoleHandler{
	return &UserRoleHandler{
		//logger:tl.Instance().Named("TagHandler"),
		e:ea,
		User:database.User{},
		Role:database.Role{},
	}
}


func (h *UserRoleHandler) Create(ctx context.Context,req *pb.UserRole,rsp *proto.BoolResponse)error{
	log.Println("新增...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.UserId) == false {
		return errors.New(mes.BadRequest("user-role-srv","用户编号不合法").Error())
	}
	if validate.MustAndCheckString(gv.IsUUIDv4, req.RoleId) == false {
		return errors.New(mes.BadRequest("user-role-srv","角色编号不合法").Error())
	}
	var b bool

	b = h.e.HasRoleForUser(req.UserId,req.RoleId)

	if b {
		rsp.Success = true
		return nil
	}

	if gv.IsNull(req.Domain) {
		b = h.e.AddRoleForUser(req.UserId,req.RoleId)
	}else{
		b = h.e.AddRoleForUserInDomain(req.UserId,req.RoleId,req.Domain)
	}

	if b == false {
		return errors.New(mes.InternalServerError("user-role-casbin-srv","casbin异常错误").Error())
	}
	// Save the policy back to DB.
	h.e.SavePolicy()
	rsp.Success = true
	return nil
}

func (h *UserRoleHandler) Delete(ctx context.Context,req *pb.UserRole,rsp *proto.BoolResponse)error {
	log.Println("删除...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.UserId) == false {
		return errors.New(mes.BadRequest("user-role-srv","用户编号不合法").Error())
	}
	if validate.MustAndCheckString(gv.IsUUIDv4, req.RoleId) == false {
		return errors.New(mes.BadRequest("user-role-srv","角色编号不合法").Error())
	}

	var b bool
	if gv.IsNull(req.Domain) {
		b = h.e.DeleteRoleForUser(req.UserId,req.RoleId)
	}else{
		b = h.e.DeleteRoleForUserInDomain(req.UserId,req.RoleId,req.Domain)
	}

	if !b {
		return errors.New(mes.InternalServerError("user-role-casbin-srv","casbin异常错误").Error())
	}
	h.e.SavePolicy()
	rsp.Success = true
	return nil
}

func (h *UserRoleHandler) DeleteUsers(ctx context.Context,req *pb.UserRole,rsp *proto.BoolResponse)error {
	log.Println("删除所有用户的关联...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.UserId) == false {
		return errors.New(mes.BadRequest("user-role-srv","用户编号不合法").Error())
	}
	b := h.e.DeleteUser(req.UserId)

	if !b {
		return errors.New(mes.InternalServerError("user-role-casbin-srv","casbin异常错误").Error())
	}
	rsp.Success = true
	return nil
}

func (h *UserRoleHandler) DeleteRoles(ctx context.Context,req *pb.UserRole,rsp *proto.BoolResponse)error {
	log.Println("删除所有角色的关联...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.RoleId) == false {
		return errors.New(mes.BadRequest("user-role-srv","角色编号不合法").Error())
	}
	h.e.DeleteRole(req.RoleId)

	rsp.Success = true
	return nil
}

func (h *UserRoleHandler) GetUsersByRole(ctx context.Context,req *pb.ListUserRequest,rsp *user.ListResponse)error {
	log.Println("查询角色所拥有的用户 列表...")
	if req.Start <= 0 {
		req.Start = 0
	}
	if req.Limit <= 0 || req.Limit > config.DefaultMaxPageSize{
		req.Limit = config.PageSize
	}

	arr := h.e.GetUsersForRole(req.RoleId)

	var users []*user.User
	if arr!= nil && len(arr) > 0 {
		r := &user.ListRequest{
			Default:&proto.ListRequest{
				Start:req.Start,
				Limit:req.Limit,
			},
		}
		var err error
		users,err = h.User.GetList(r,arr)
		if err != nil {
			return errors.New(mes.InternalServerError("user-role-srv",err.Error()).Error())
		}

	}

	rsp.List = users
	return nil
}

func (h *UserRoleHandler) GetUserPageByRole(ctx context.Context,req *pb.PageUserRequest,rsp *user.PageResponse)error {
	log.Println("查询角色所拥有的用户 分页...")
	if req.Page <= 0 {
		req.Page = config.Page
	}
	if req.Size <= 0 || req.Size > config.DefaultMaxPageSize{
		req.Size = config.PageSize
	}

	arr := h.e.GetUsersForRole(req.RoleId)

	var users []*user.User
	var count int64
	if arr!= nil && len(arr) > 0 {
		var err error
		c,err := h.User.GetCount(&user.PageRequest{},arr)
		count = *c
		if err != nil {
			return errors.New(mes.InternalServerError("user-role-srv",err.Error()).Error())
		}

		r := &user.ListRequest{
			Default:&proto.ListRequest{
				Start:(req.Page-1)*req.Size,
				Limit:req.Size,
			},
		}
		users,err = h.User.GetList(r,arr)
		if err != nil {
			return errors.New(mes.InternalServerError("user-role-srv",err.Error()).Error())
		}
	}

	rsp.Page = &proto.Page{
		Index:req.Page,
		Total:count,
		TotalPage:int64(math.Ceil(float64(count)/float64(req.Size))),
		PageSize:req.Size,
	}
	rsp.List = users
	return nil
}

func (h *UserRoleHandler) GetRolesByUser(ctx context.Context,req *pb.ListRoleRequest,rsp *role.ListResponse)error {
	log.Println("查询用户所属的角色 列表...")
	if req.Start <= 0 {
		req.Start = 0
	}
	if req.Limit <= 0 || req.Limit > config.DefaultMaxPageSize{
		req.Limit = config.PageSize
	}

	var arr []string
	if !gv.IsNull(req.Domain) {
		arr = h.e.GetRolesForUserInDomain(req.UserId,req.Domain)
	}else{
		arr = h.e.GetRolesForUser(req.UserId)
	}

	var roles []*role.Role
	if arr!= nil && len(arr) > 0 {
		r := &role.ListRequest{
			Default:&proto.ListRequest{
				Start:req.Start,
				Limit:req.Limit,
			},
		}
		var err error
		roles,err = h.Role.GetList(r,arr)
		if err != nil {
			return errors.New(mes.InternalServerError("user-role-srv",err.Error()).Error())
		}
	}

	rsp.List = roles
	return nil
}

func (h *UserRoleHandler) GetRolePageByUser(ctx context.Context,req *pb.PageRoleRequest,rsp *role.PageResponse)error {
	log.Println("查询用户所属的角色 分页...")
	if req.Page <= 0 {
		req.Page = config.Page
	}
	if req.Size <= 0 || req.Size > config.DefaultMaxPageSize{
		req.Size = config.PageSize
	}

	var arr []string
	if !gv.IsNull(req.Domain) {
		arr = h.e.GetRolesForUserInDomain(req.UserId,req.Domain)
	}else{
		arr = h.e.GetRolesForUser(req.UserId)
	}

	var roles []*role.Role
	var count *int64
	if arr!= nil && len(arr) > 0 {
		var err error
		count,err = h.Role.GetCount(&role.PageRequest{},arr)
		if err != nil {
			return errors.New(mes.InternalServerError("user-role-srv",err.Error()).Error())
		}

		r := &role.ListRequest{
			Default:&proto.ListRequest{
				Start:(req.Page-1)*req.Size,
				Limit:req.Size,
			},
		}
		roles,err = h.Role.GetList(r,arr)
		if err != nil {
			return errors.New(mes.InternalServerError("user-role-srv",err.Error()).Error())
		}
	}

	rsp.Page = &proto.Page{
		Index:req.Page,
		Total:*count,
		TotalPage:int64(math.Ceil(float64(*count)/float64(req.Size))),
		PageSize:req.Size,
	}
	rsp.List = roles
	return nil
}
