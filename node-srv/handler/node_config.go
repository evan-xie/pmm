package handler

import (
	"github.com/jinzhu/gorm"
	"log"
	pb "pmm/public/proto/node/config"
	"context"
	mes "github.com/micro/go-micro/errors"
	gv "github.com/asaskevich/govalidator"
	"github.com/pkg/errors"
	sconf "pmm/node-srv/config"
	"pmm/public/utils/validate"
	"pmm/public/config"
		"pmm/public/proto"
	"pmm/node-srv/database"
)

type NodeConfigHandler struct {
	//logger *zap.Logger
	Model database.NodeConfig
	db *gorm.DB
}


// new一个TagHandler
func NewNodeConfigHandler() *NodeConfigHandler{
	return &NodeConfigHandler{
		//logger:tl.Instance().Named("TagHandler"),
		Model:database.NodeConfig{},
		db:database.Conn,
	}
}


func (h *NodeConfigHandler) Create(ctx context.Context,req *pb.NodeConfig,rsp *pb.Response)error{
	log.Println("新增...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("node-srv","项目编号不合法").Error())
	}
	if !gv.IsNull(req.NodeUnit){
		if !gv.IsIn(req.NodeUnit, sconf.NodeUnit...) {
			return errors.New(mes.BadRequest("node-srv","节点单位类型错误").Error())
		}
	}else {
		req.NodeUnit = sconf.DefaultNodeUnit
	}

	
	info := h.Model.IsExist(req,false)
	if info != nil {
		return errors.New(mes.BadRequest("node-srv","此项目节点配置已存在").Error())
	}
	
	info,err := h.Model.Create(req)
	if err != nil {
		return errors.New(mes.InternalServerError("node-srv",err.Error()).Error())
	}

	rsp.NodeConfig = &info.NodeConfig
	return nil
}

func (h *NodeConfigHandler) Update(ctx context.Context,req *pb.NodeConfig,rsp *pb.Response)error{
	log.Println("更新...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("node-srv","项目编号不合法").Error())
	}
	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.BadRequest("node-srv","此项目节点配置已存在").Error())
	}
	if !gv.IsNull(req.NodeUnit){
		if !gv.IsIn(req.NodeUnit, sconf.NodeUnit...) {
			return errors.New(mes.BadRequest("node-srv","节点单位类型错误").Error())
		}
	}else {
		req.NodeUnit = sconf.DefaultNodeUnit
	}

	err := h.Model.Update(req)
	if err != nil {
		return errors.New(mes.InternalServerError("node-srv",err.Error()).Error())
	}
	info = h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.BadRequest("node-srv","项目节点配置不存在").Error())
	}
	
	rsp.NodeConfig = &info.NodeConfig
	return nil
}

func (h *NodeConfigHandler) Delete(ctx context.Context,req *proto.IdRequest,rsp *proto.BoolResponse)error{
	log.Println("删除...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("node-srv","项目编号不合法").Error())
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.NotFound("node-srv","项目节点配置不存在").Error())
	}

	err := h.Model.Delete(req.Id)

	if err != nil {
		return errors.New(mes.InternalServerError("node-srv",err.Error()).Error())
	}
	rsp.Success = true
	return nil
}

func (h *NodeConfigHandler) Open(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("启用...")
	info,err := h.changeStatus(req.Id,config.OPEN_STATUS)
	if err != nil {
		return err
	}

	rsp.NodeConfig = &info.NodeConfig
	return nil
}

func (h *NodeConfigHandler) Close(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("禁用...")
	info,err := h.changeStatus(req.Id,config.CLOSE_STATUS)
	if err != nil {
		return err
	}

	rsp.NodeConfig = &info.NodeConfig
	return nil
}

func (h *NodeConfigHandler) Get(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("查询...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New("编号不合法")
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.NotFound("node-srv","项目节点配置不存在").Error())
	}

	rsp.NodeConfig = &info.NodeConfig
	return nil
}

func (h *NodeConfigHandler) changeStatus(id string,status int)(*database.NodeConfig,error){
	if validate.HasAndCheckString(gv.IsUUIDv4, id) == false {
		return nil,errors.New(mes.BadRequest("node-srv","项目编号不合法").Error())
	}

	info := h.Model.GetInfoById(id,false)
	if info == nil {
		return nil,errors.New(mes.NotFound("node-srv","项目节点配置不存在").Error())
	}

	u := &pb.NodeConfig{
		Id:id,
		Status:int64(status),
	}

	err := h.Model.Update(u)
	if err != nil {
		return nil,errors.New(mes.InternalServerError("node-srv",err.Error()).Error())
	}
	info = h.Model.GetInfoById(id,false)

	return info,nil
}

func (h *NodeConfigHandler) checkStatus(id string)(*database.NodeConfig,error){
	if validate.HasAndCheckString(gv.IsUUIDv4, id) == false {
		return nil,errors.New(mes.BadRequest("node-srv","项目编号不合法").Error())
	}

	info := h.Model.GetInfoById(id,true)
	if info == nil {
		return nil,errors.New(mes.NotFound("node-srv","项目不存在").Error())
	}
	return info,nil
}