package handler

import (
	"context"
	gv "github.com/asaskevich/govalidator"
	"github.com/jinzhu/gorm"
	mes "github.com/micro/go-micro/errors"
	"github.com/pkg/errors"
	"log"
	"math"
	sconf "pmm/node-srv/config"
	"pmm/node-srv/database"
	"pmm/public/config"
	"pmm/public/proto"
	pb "pmm/public/proto/node"
	"pmm/public/utils"
	"pmm/public/utils/validate"
	"time"
)

type NodeHandler struct {
	//logger *zap.Logger
	Model database.Node
	db *gorm.DB
}


// new一个TagHandler
func NewNodeHandler() *NodeHandler{
	return &NodeHandler{
		//logger:tl.Instance().Named("TagHandler"),
		Model:database.Node{},
		db:database.Conn,
	}
}


func (h *NodeHandler) Create(ctx context.Context,req *pb.Node,rsp *pb.Response)error{
	log.Println("新增...")
	if gv.IsNull(req.Title) {
		return errors.New(mes.BadRequest("node-srv","节点标题不能为空").Error())
	}
	if validate.MustAndCheckString(gv.IsUUIDv4, req.ProjectId) == false {
		return errors.New(mes.BadRequest("node-srv","项目编号不合法").Error())
	}
	if gv.IsNull(req.StartDate) || !gv.IsTime(req.StartDate,"2006-01-02 15:04:05"){
		return errors.New(mes.BadRequest("project-srv","开始时间格式错误").Error())
	}
	if !gv.IsNull(req.Pid)  && req.Pid != sconf.DefaultTopPidValue{
		p := h.Model.GetInfoById(req.Pid,false)
		if p == nil {
			return errors.New(mes.BadRequest("node-srv","父级节点不存在").Error())
		}
		if p.ProjectId != req.ProjectId {
			return errors.New(mes.BadRequest("node-srv","必须和父级在同一项目下").Error())
		}
	}else {
		req.Pid = sconf.DefaultTopPidValue
	}
	if req.Duration == 0 {
		req.Duration = 1
	}
	if req.Progress < 0 || req.Progress > 1 {
		return errors.New(mes.BadRequest("project-srv","百分比取值范围超出").Error())
	}
	
	info := h.Model.IsExist(req,false)
	if info != nil {
		return errors.New(mes.BadRequest("node-srv","此项目节点配置已存在").Error())
	}

	
	info,err := h.Model.Create(req)
	if err != nil {
		return errors.New(mes.InternalServerError("node-srv",err.Error()).Error())
	}
	h.updateParents(info.Id)
	rsp.Node = &info.Node
	return nil
}

func (h *NodeHandler) Update(ctx context.Context,req *pb.Node,rsp *pb.Response)error{
	log.Println("更新...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("node-srv","节点编号不合法").Error())
	}
	if !gv.IsNull(req.ProjectId) {
		req.ProjectId = ""
	}
	if !gv.IsNull(req.StartDate) && !gv.IsTime(req.StartDate,"2006-01-02 15:04:05"){
		return errors.New(mes.BadRequest("node-srv","开始时间格式错误").Error())
	}
	if !gv.IsNull(req.Pid) && req.Pid != sconf.DefaultTopPidValue {
		p := h.Model.GetInfoById(req.Pid,false)
		if p == nil {
			return errors.New(mes.BadRequest("node-srv","父级节点不存在").Error())
		}
		if p.ProjectId != req.ProjectId {
			return errors.New(mes.BadRequest("node-srv","必须和父级在同一项目下").Error())
		}
	}
	if req.Duration < 0 {
		return errors.New(mes.BadRequest("node-srv","单位值必须大于0").Error())
	}
	if req.Progress < 0 || req.Progress > 1 {
		return errors.New(mes.BadRequest("project-srv","百分比取值范围超出").Error())
	}
	old := h.Model.GetInfoById(req.Id,false)
	if old == nil {
		return errors.New(mes.BadRequest("node-srv","项目节点不存在").Error())
	}
	//if gv.IsNull(req.Pid) {
	//	req.Pid = old.Pid
	//}
	err := h.Model.Update(req)
	if err != nil {
		return errors.New(mes.InternalServerError("node-srv",err.Error()).Error())
	}
	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.BadRequest("node-srv","节点不存在").Error())
	}
	h.updateParents(req.Id)
	rsp.Node = &info.Node
	return nil
}

func (h *NodeHandler) Delete(ctx context.Context,req *proto.IdRequest,rsp *proto.BoolResponse)error{
	log.Println("删除...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New(mes.BadRequest("node-srv","节点编号不合法").Error())
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.NotFound("node-srv","项目节点不存在").Error())
	}

	err := h.Model.Delete(req.Id)

	if err != nil {
		return errors.New(mes.InternalServerError("node-srv",err.Error()).Error())
	}
	h.updateParents(info.Pid)
	rsp.Success = true
	return nil
}

func (h *NodeHandler) Open(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("启用...")
	info,err := h.changeStatus(req.Id,config.OPEN_STATUS)
	if err != nil {
		return err
	}

	rsp.Node = &info.Node
	return nil
}

func (h *NodeHandler) Close(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("禁用...")
	info,err := h.changeStatus(req.Id,config.CLOSE_STATUS)
	if err != nil {
		return err
	}

	rsp.Node = &info.Node
	return nil
}

func (h *NodeHandler) Get(ctx context.Context,req *proto.IdRequest,rsp *pb.Response)error{
	log.Println("查询...")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.Id) == false {
		return errors.New("编号不合法")
	}

	info := h.Model.GetInfoById(req.Id,false)
	if info == nil {
		return errors.New(mes.NotFound("node-srv","项目节点不存在").Error())
	}

	rsp.Node = &info.Node
	return nil
}

func (h *NodeHandler) GetPage(ctx context.Context,req *pb.PageRequest,rsp *pb.PageResponse)error{
	log.Println("根据页面查询...")
	//设置默认每页条数,页数
	count,err := h.Model.GetCount(req)
	if err != nil {
		return errors.New(mes.InternalServerError("node-srv",err.Error()).Error())
	}

	if *count == 0 {
		rsp.Page = &proto.Page{}
		return nil
	}
	lr := &pb.ListRequest{
		Default:&proto.ListRequest{},
		Node:req.Node,
	}

	if req.Default != nil {
		if req.Default.Size <= 0 || req.Default.Size > sconf.DefaultMaxPageSize{
			req.Default.Size = sconf.PageSize
		}
		if req.Default.Page <= 0 {
			req.Default.Page = sconf.Page
		}

		lr.Default = &proto.ListRequest{
			Start:(req.Default.Page-1)*req.Default.Size,
			Limit:req.Default.Size,
			Search:req.Default.Search,
			Ids:req.Default.Ids,
		}
	}else {
		req.Default = &proto.PageRequest{
			Size:sconf.PageSize,
			Page:sconf.Page,
		}
	}

	list,err := h.Model.GetList(lr)
	if err != nil {
		return errors.New(mes.InternalServerError("node-srv",err.Error()).Error())
	}

	page := &proto.Page{
		Total:*count,
		Index:req.Default.Page,
		TotalPage:int64(math.Ceil(float64(*count)/float64(req.Default.Size))),
		PageSize:req.Default.Size,
	}
	rsp.Page = page
	rsp.List = list
	return nil
}

func (h *NodeHandler) GetList(ctx context.Context,req *pb.ListRequest,rsp *pb.ListResponse)error{
	log.Println("查询所有...")
	list,err := h.Model.GetList(req)
	if err != nil {
		return errors.New(mes.InternalServerError("node-srv",err.Error()).Error())
	}
	rsp.List = list
	return nil
}

func (h *NodeHandler) GetTree(ctx context.Context,req *pb.TreeRequest,rsp *pb.TreeResponse)error{
	log.Println("树型图")
	if validate.MustAndCheckString(gv.IsUUIDv4, req.ProjectId) == false {
		return errors.New(mes.BadRequest("node-srv","项目编号不合法").Error())
	}
	r := &pb.ListRequest{
		Node:&pb.Node{
			ProjectId:req.ProjectId,
			Status:config.OPEN_STATUS,
		},
	}
	list,err := h.Model.GetList(r)
	if err != nil {
		return errors.New(mes.InternalServerError("node-srv",err.Error()).Error())
	}
	tree := h.getTree(list,sconf.DefaultTopPidValue)
	rsp.Trees = tree
	return nil
}

func (h *NodeHandler) changeStatus(id string,status int)(*database.Node,error){
	if validate.HasAndCheckString(gv.IsUUIDv4, id) == false {
		return nil,errors.New(mes.BadRequest("node-srv","项目节点编号不合法").Error())
	}

	info := h.Model.GetInfoById(id,false)
	if info == nil {
		return nil,errors.New(mes.NotFound("node-srv","项目节点不存在").Error())
	}

	u := &pb.Node{
		Id:id,
		Status:int64(status),
	}

	err := h.Model.Update(u)
	if err != nil {
		return nil,errors.New(mes.InternalServerError("node-srv",err.Error()).Error())
	}
	info = h.Model.GetInfoById(id,false)
	h.updateParents(id)
	return info,nil
}

func (h *NodeHandler) checkStatus(id string)(*database.Node,error){
	if validate.HasAndCheckString(gv.IsUUIDv4, id) == false {
		return nil,errors.New(mes.BadRequest("node-srv","项目编号不合法").Error())
	}

	info := h.Model.GetInfoById(id,true)
	if info == nil {
		return nil,errors.New(mes.NotFound("node-srv","项目不存在").Error())
	}
	return info,nil
}

/*func (h *NodeHandler) updateParents(old *database.Node,new *database.Node) error {
	log.Println("跟新父级...")
	if old == nil {
		return errors.New(mes.BadRequest("node-srv","项目节点不存在").Error())
	}
	if old.Pid == sconf.DefaultTopPidValue {
		return nil
	}
	p := h.Model.GetInfoById(old.Pid,false)
	if p == nil {
		return errors.New(mes.BadRequest("node-srv","父节点不存在").Error())
	}
	duration := p.Duration+old.Duration-new.Duration
	pro := old.Progress*float64(old.Duration)/float64(duration)
	value, _ := strconv.ParseFloat(fmt.Sprintf("%.2f", pro), 64)
	data := &pb.Node{
		Id:p.Id,
		Duration:duration,
		Progress:value,
	}
	err := h.Model.Update(data)
	if err != nil {
		return errors.New(mes.InternalServerError("node-srv",err.Error()).Error())
	}
	info := h.Model.GetInfoById(data.Id,false)
	if err := h.updateParents(p,info);err != nil {
		return err
	}

	return nil
}*/
func (h *NodeHandler) updateParents(id string) {
	if gv.IsNull(id) {
		return
	}
	tid := h.getTopNode(id)
	if tid == sconf.DefaultTopPidValue {
		return
	}
	t := h.Model.GetInfoById(tid,false)
	if t == nil {
		return
	}
	h.updateParent1(t)
}

func (h *NodeHandler) updateParent(node *database.Node)  {
	req := &pb.ListRequest{
		Node:&pb.Node{},
	}
	req.Node.Pid = node.Id
	req.Node.Status = config.OPEN_STATUS
	
	list,_ := h.Model.GetList(req)
	if list == nil || len(list) == 0{
		return
	}
	var td int64
	var tp float64

	for _, n := range list {
		data := database.Node{Node:*n,}
		h.updateParent(&data)
		td = td + data.Duration
		tp = tp + data.Progress*float64(data.Duration)
	}
	node.Duration = td
	node.Progress = utils.Decimal(tp/float64(td))
	d := map[string]interface{}{"duration":td,"progress":node.Progress}
	h.db.Model(node).Where("id = ?",node.Id).Updates(d)
}

func (h *NodeHandler) updateParent1(node *database.Node)  {
	req := &pb.ListRequest{
		Node:&pb.Node{},
	}
	req.Node.Pid = node.Id
	req.Node.Status = config.OPEN_STATUS

	list,_ := h.Model.GetList(req)
	if list == nil || len(list) == 0{
		return
	}
	var td int64
	var tp float64
	var startDate time.Time
	var endDate time.Time

	for _, n := range list {
		data := database.Node{Node:*n,}
		h.updateParent1(&data)

		theTime, err := time.ParseInLocation("2006-01-02 15:04:05", data.StartDate, time.Local)
		if err == nil {
			//取得最早的开始时间
			if startDate.IsZero() || startDate.After(theTime){
				startDate = theTime
			}
			//取得最晚的时间段
			et := theTime
			et = et.AddDate(0,0,int(data.Duration))
			if endDate.IsZero() || endDate.Before(et) {
				endDate = et
			}
		}

		td = td + data.Duration
		tp = tp + data.Progress*float64(data.Duration)
	}

	node.Duration = int64(math.Ceil(endDate.Sub(startDate).Hours()/24))
	node.StartDate = startDate.Format("2006-01-02 15:04:05")
	node.Progress = utils.Decimal(tp/float64(td))
	d := map[string]interface{}{"duration":node.Duration,"progress":node.Progress,"start_date":node.StartDate}
	h.db.Model(node).Where("id = ?",node.Id).Updates(d)
}


func (h *NodeHandler) getTopNode(id string) string {
	if id == sconf.DefaultTopPidValue {
		return id
	}
	info := h.Model.GetInfoById(id,false)
	if info == nil {//已经被删除
		info = h.Model.GetDelInfoById(id,true)
	}
	if info.Pid == sconf.DefaultTopPidValue {
		return info.Id
	}
	return h.getTopNode(info.Pid)
}

func (h *NodeHandler) getTree(list []*pb.Node,pid string) []*pb.NodeTree {
	if len(list) == 0 {
		return nil
	}
	var nodeTreeArr []*pb.NodeTree
	for _, node := range list {
		if node.Pid != pid {
			continue
		}
		var nodeTree pb.NodeTree
		nodeTree.Node = node
		nodeTree.Child = h.getTree(list,node.Id)
		nodeTreeArr = append(nodeTreeArr, &nodeTree)
	}
	return nodeTreeArr
}