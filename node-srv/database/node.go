package database

import (
	"pmm/public/model"
	pb "pmm/public/proto/node"
	"pmm/public/config"
	"github.com/jinzhu/gorm"
	"time"
	"pmm/public/utils"
	"github.com/satori/go.uuid"
	"log"
)

// 设置的表名
func (Node) TableName() string {
	return "node"
}

type Node struct {
	pb.Node
	model.Comm
}

/**
创建
 */
func (repo *Node) Create(data *pb.Node) (*Node, error) {
	d := &Node{
		Node: *data,
	}
	if err := Conn.Create(&d).Error; err != nil {
		return nil, err
	}
	return d, nil
}

/**
更新
condition 条件 参数1为是否更新密码 2位是否更新状态
 */
func (repo *Node) Update(data interface{}, condition ... string) (error) {
	d := &Node{}

	query := Conn.Model(&d)
	//过滤参数
	if len(condition) >= 1 {
		query = query.Omit(condition...)
	}

	query = query.Updates(data)
	m := utils.StructToMap(data,true)
	if m["progress"].(float64) == 0{
		query = query.Update(map[string]interface{}{"progress":0})
	}
	if err := query.Error; err != nil {
		return err
	}
	return nil
}

/**
 判断是否存在 存在返回查询到的结果
 */
func (repo *Node) IsExist(pb *pb.Node, isOpen bool) (*Node) {
	if pb == nil {
		return nil
	}

	d := &Node{}
	d.Title = pb.Title
	d.ProjectId = pb.ProjectId

	query := Conn
	if isOpen {
		query = query.Where("status = ?", config.OPEN_STATUS)
	}
	err := query.Where(pb).First(&d).Error

	if err == nil {
		return d
	}

	return nil
}

/**
根据id取得信息
 */
func (repo *Node) GetInfoById(id string, isOpen bool) (*Node) {
	if id == "" {
		return nil
	}

	d := &Node{}

	query := Conn.Where("id = ?", id)
	if isOpen {
		query = query.Where("status = ?", config.OPEN_STATUS)
	}
	err := query.First(&d).Error

	if err == nil {
		return d
	}

	return nil
}

/**
根据id取得信息
 */
func (repo *Node) GetDelInfoById(id string, isDel bool) (*Node) {
	if id == "" {
		return nil
	}

	d := &Node{}

	query := Conn.Where("id = ?", id)
	if isDel {
		query = query.Unscoped()
	}
	err := query.First(&d).Error

	if err == nil {
		return d
	}

	return nil
}

/**
根据编号删除
 */
func (repo *Node) Delete(id ...string) (error) {
	str := ""
	if len(id) > 1 {
		str = "id in ("
		for _, value := range id {
			str = str + "'" + value + "',"
		}
		str = string([]rune(str)[:len(str)-1]) + ")"
	} else {
		str = "id = '" + id[0] + "'"
	}

	err := Conn.Where(str).Delete(&Node{}).Error

	if err != nil {
		return err
	}

	return nil
}

/**
取得列表
 */
func (repo *Node) GetList(req *pb.ListRequest, args ...interface{}) ([]*pb.Node, error) {
	var list []*pb.Node
	d := req.Node
	fs := utils.GetDbNameByStruct(Conn, pb.Node{})
	query := Conn.Model(&Node{}).Where(d)
	//过滤参数
	if len(fs) > 1 {
		query = query.Select(fs)
	}
	if req.Default != nil {
		if req.Default.Start > 0 {
			query = query.Offset(req.Default.Start)
		}
		if req.Default.Limit > 0 {
			query = query.Limit(req.Default.Limit)
		}
		if req.Default.Search != nil {
			//m := utils.MapToWhereStr(req.Default.Search)
			//query = query.Where(m)
			query = utils.AndOrLikes(query,req.Default.Search,true)
		}
		if len(req.Default.Ids) > 0{
			query = query.Where(req.Default.Ids)
		}
	}

	if args != nil && len(args) > 0 {
		for _, value := range args {
			query = query.Where(value)
		}
	}
	query = query.Order("created_at asc")
	err := query.Scan(&list).Error
	if err != nil {
		return nil, err
	}
	return list, nil
}

func (repo *Node) GetCount(req *pb.PageRequest, args ...interface{}) (*int64, error) {
	data := req.Node
	query := Conn.Model(&Node{}).Where(data)
	if req.Default != nil {
		if req.Default.Search != nil {
			query = utils.AndOrLikes(query,req.Default.Search,true)
		}
		if len(req.Default.Ids) > 0 {
			query = query.Where(req.Default.Ids)
		}
	}
	if args != nil && len(args) > 0 {
		for _, value := range args {
			query = query.Where(value)
		}
	}
	var count *int64
	err := query.Count(&count).Error
	if err != nil {
		return count, err
	}
	return count, nil
}

func (repo *Node) BeforeCreate(scope *gorm.Scope) error {
	uid, err := uuid.NewV4()
	if err != nil {
		log.Fatalf("created uuid error: %v\n", err)
	}
	scope.SetColumn("Id", uid.String())
	scope.SetColumn("CreatedAt", time.Now().Format("2006-01-02 15:04:05"))
	scope.SetColumn("UpdatedAt", time.Now().Format("2006-01-02 15:04:05"))
	scope.SetColumn("Status", config.OPEN_STATUS)
	return nil
}
