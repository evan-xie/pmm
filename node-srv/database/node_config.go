package database

import (
	"pmm/public/model"
	 pb "pmm/public/proto/node/config"
	"pmm/public/config"
		"github.com/jinzhu/gorm"
	"time"
		)

// 设置的表名
func (NodeConfig) TableName() string {
	return "node_config"
}

type NodeConfig struct {
	pb.NodeConfig
	model.Comm
}

/**
创建
 */
func (repo *NodeConfig) Create(data *pb.NodeConfig) (*NodeConfig,error) {
	d := &NodeConfig{
		NodeConfig:*data,
	}
	if err := Conn.Create(&d).Error; err != nil {
		return nil,err
	}
	return d,nil
}

/**
更新
condition 条件 参数1为是否更新密码 2位是否更新状态
 */
func (repo *NodeConfig) Update(data *pb.NodeConfig,condition... string) (error)  {
	d := &NodeConfig{}
	d.NodeConfig = *data

	query := Conn.Model(&d)
	//过滤参数
	if len(condition) >= 1 {
		query = query.Omit(condition...)
	}

	if err := query.Updates(d).Error;err != nil {
		return err
	}
	return nil
}

/**
 判断是否存在 存在返回查询到的结果
 */
func (repo *NodeConfig) IsExist(pb *pb.NodeConfig,isOpen bool) (*NodeConfig) {
	if pb == nil {
		return nil
	}

	d := &NodeConfig{}
	d.Id = pb.Id

	query := Conn
	if isOpen {
		query = query.Where("status = ?",config.OPEN_STATUS)
	}
	err := query.Where(pb).First(&d).Error

	if err == nil {
		return d
	}

	return nil
}

/**
根据id取得信息
 */
func (repo *NodeConfig) GetInfoById(id string,isOpen bool) (*NodeConfig) {
	if id == "" {
		return nil
	}

	d := &NodeConfig{}

	query := Conn.Where("id = ?",id)
	if isOpen {
		query = query.Where("status = ?",config.OPEN_STATUS)
	}
	err := query.First(&d).Error

	if err == nil {
		return d
	}

	return nil
}

/**
根据编号删除
 */
func (repo *NodeConfig) Delete(id ...string) (error) {
	str := ""
	if len(id) > 1 {
		str = "id in ("
		for _, value := range id {
			str = str+"'"+value+"',"
		}
		str = string([]rune(str)[:len(str)-1])+")"
	}else {
		str = "id = '" + id[0] +"'"
	}

	err := Conn.Where(str).Delete(&NodeConfig{}).Error

	if err != nil {
		return err
	}

	return nil
}

func (repo *NodeConfig) BeforeCreate(scope *gorm.Scope) error {
	scope.SetColumn("CreatedAt", time.Now().Format("2006-01-02 15:04:05"))
	scope.SetColumn("UpdatedAt", time.Now().Format("2006-01-02 15:04:05"))
	scope.SetColumn("Status", config.OPEN_STATUS)
	return nil
}