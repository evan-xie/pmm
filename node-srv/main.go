package main

import (
	"log"
	pbnc "pmm/public/proto/node/config"
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/server"
	"pmm/node-srv/config"
	"pmm/node-srv/handler"
	"pmm/node-srv/database"
	"pmm/public/proto/node"
)

func main() {
	//连接数据库
	db,err := database.CreateConnection()
	defer db.Close()
	db.LogMode(true)

	if err != nil {
		log.Fatalf("数据库连接失败: %v\n",err)
	}

	// 全局禁用表名复数
	db.SingularTable(true) // 如果设置为true,`User`的默认表名为`user`,使用`TableName`设置的表名不受影响

	// 自动检查 表 结构是否变化
	db.AutoMigrate(database.Node{})//项目
	db.AutoMigrate(database.NodeConfig{})//项目

	srv := micro.NewService(
		micro.Name(config.ServiceName),
		micro.Version("latest"),
		//micro.WrapHandler(fanc),//注册拦截器 //注册验证
	)

	srv.Init()

	//注册推送监听
	//publisher := micro.NewPublisher("node",srv.Client())
	//注册日志

	//注册服务
	pbnc.RegisterNodeConfigServiceHandler(srv.Server(),handler.NewNodeConfigHandler(),server.InternalHandler(true))
	node.RegisterNodeServiceHandler(srv.Server(),handler.NewNodeHandler(),server.InternalHandler(true))

	log.Println("启动node-srv服务...")
	if err := srv.Run(); err != nil {
		log.Fatalf("node-srv服务启动失败: %v\n", err)
	}
}
